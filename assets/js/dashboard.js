 // Load the Visualization API and the line package.
google.charts.load('current', {packages: ['corechart', 'bar']});

function showMe(page){
    if(page === 'site_analytics'){
        $("#site_analytics").show();
        $("#hr_graphs").hide();
        $("#site_analytics_tab").addClass('active');
        $("#hr_graphs_tab").removeClass('active');
    }else{
        show_waitMe(jQuery('body'));
        $("#hr_graphs").show();
        $("#site_analytics").hide();
        $("#hr_graphs_tab").addClass('active');
        $("#site_analytics_tab").removeClass('active');
        refreshDeptList();
        demographics()
    }
}

function clearFilter(except){
    $("#filterDiv").show();
    $("#filterDiv div select").each(function(){
        if(except != $(this).attr('id')){
            $(this).val('')
        }
    })
    if (except != "department" && except != "branch") {
        refreshDeptList();
    }
}

function refreshDeptList(branch) {
    $.post(SITEROOT + '/dashboard/ajax_getDepartmentsPerBranch', {branch: branch}, function (options) {
        options = $.parseJSON(options);
        var disabled = "";
        if (options.length == 0) {
            disabled = "disabled";
        }
        var html = "<select id='department' name='department' " + disabled + ">";
        html += "<option value=''>All</option>";
        for (var i = 0; i < options.length; i++) {
            html += "<option value='" + options[i] + "'>" + options[i] + "</option>";
        }
        html += "</select>";
        $("#department_container").html(html);
    });
}

function demographics(data) {
    $("#age_group_filter").hide()
    $('#bar_chart').html('');
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_users_age',
        data: data,
        success: function (data1) {
            hide_waitMe();

            //Parse data into Json
            var jsonData = $.parseJSON(data1);
            var total_registered = jsonData.total_registered
            var total_respondents = jsonData.total_respondents
            jsonData = jsonData.data

            $("#total_registered").text(total_registered);
            $("#total_respondents").text(total_respondents);
            
            var data = google.visualization.arrayToDataTable([
                ['Age Group', 'Male', 'Female'],
                ['0-9', (jsonData.Male ? jsonData.Male.zero : 0), (jsonData.Female ? jsonData.Female.zero : 0)],
                ['10-19', (jsonData.Male ? jsonData.Male.ten : 0), (jsonData.Female ? jsonData.Female.ten : 0)],
                ['20-29', (jsonData.Male ? jsonData.Male.twenty : 0), (jsonData.Female ? jsonData.Female.twenty : 0)],
                ['30-39', (jsonData.Male ? jsonData.Male.thirty : 0), (jsonData.Female ? jsonData.Female.thirty : 0)],
                ['40-49', (jsonData.Male ? jsonData.Male.forty : 0), (jsonData.Female ? jsonData.Female.forty : 0)],
                ['50-59', (jsonData.Male ? jsonData.Male.fifty : 0), (jsonData.Female ? jsonData.Female.fifty : 0)],
                ['60-69', (jsonData.Male ? jsonData.Male.sixty : 0), (jsonData.Female ? jsonData.Female.sixty : 0)],
                ['70-79', (jsonData.Male ? jsonData.Male.seventy : 0), (jsonData.Female ? jsonData.Female.seventy : 0)]
            ]);
            var options = {
                chartArea: {'width': '100%', 'height': '350'},
                height: 350,
                width: '100%',
                colors: ['#80CDE9', '#EEC5E2'],
                vAxis: {
                    gridlines: {count: 10, color: 'transparent'}
                }
            };
            var chart = new google.charts.Bar(document.getElementById('bar_chart'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    });
}

function health_status(data){
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_users_health_score',
        data: data,
        success: function (data1) {
            hide_waitMe();
            var jsonData = $.parseJSON(data1);
            var data_h = [];
            var Header = ['', 'Health Risk Scores', {role: 'style'}];
            data_h.push(Header);

            for (var i = 0; i < jsonData.healthscore.length; i++) {
                var temp = [];
                temp.push(i+1, parseFloat(jsonData['healthscore'][i]), jsonData['color'][i]);
                data_h.push(temp);
            }
            if(jsonData.healthscore.length == 0){
                data_h.push([null, 0, null]);
            }
            var chartdata3 = google.visualization.arrayToDataTable(data_h);
            var options3 = {
                title: 'DISTRIBUTION OF HEALTH RISK SCORES',
                vAxis: {title: 'Health Risk Scores', viewWindow: {min: 0, max: 10}, format: "#", gridlines: {count: 10, color: 'transparent'}},
                hAxis: {gridlines: {color: 'transparent'}, textPosition: 'none'},
                legend: 'none',
                width: 600,
                height: 500,
                chartArea: {'width': '80%', 'height': '80%'}
            };
            var chart3 = new google.visualization.ScatterChart(document.getElementById('health_scattered_chart'));
            chart3.draw(chartdata3, options3);
            //  center title
            $("text:contains(" + options3.title + ")").attr({'x':'120', 'y':'20'})

            var chartdata1 = google.visualization.arrayToDataTable([
                ['Task', 'Health Risk Score'],
                ['Optimal', jsonData.counts.optimal],
                ['Suboptimal', jsonData.counts.suboptimal],
                ['Neutral', jsonData.counts.neutral],
                ['Compromised', jsonData.counts.compromised],
                ['Alarming', jsonData.counts.alarming],
                ['Incomplete Data', jsonData.counts.incomplete]
            ]);

            var options1 = {
                title: 'DISTRIBUTION OF HEALTH STATUS',
                chartArea: {width: '80%', height: '90%'},
                width: 500,
                height: 500,
                colors: ['#72A153', '#71B2CA', '#E1AF29', '#D77F1A', '#FF0000', '#9C9E9E'],
                sliceVisibilityThreshold: 0
            };
            var chart1 = new google.visualization.PieChart(document.getElementById('health_pie_chart'));
            chart1.draw(chartdata1, options1);
            //  center title
            $("text:contains(" + options1.title + ")").attr({'x':'80', 'y':'20'})
        }
    });

    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_users_doh',
        data: data,
        success: function (data1) {
            var jsonData = $.parseJSON(data1);
            var chartdata2 = google.visualization.arrayToDataTable([
                    ['Element', 'Mean Score', {role: 'style'}, {role: 'annotation'}],
                    ['Sleep', parseFloat(jsonData.sleep.mean), jsonData.sleep.color, 'Median Score: ' + parseFloat(jsonData.sleep.median) + ' (Range: '+ jsonData.sleep.min_median + '-' + jsonData.sleep.max_median +')'],
                    ['Weight', parseFloat(jsonData.weightScore.mean), jsonData.weightScore.color, 'Median Score: ' + parseFloat(jsonData.weightScore.median) + ' (Range: '+ jsonData.weightScore.min_median + '-' + jsonData.weightScore.max_median +')'],
                    ['Nutrition', parseFloat(jsonData.nutrition.mean), jsonData.nutrition.color, 'Median Score: ' + parseFloat(jsonData.nutrition.median) + ' (Range: '+ jsonData.nutrition.min_median + '-' + jsonData.nutrition.max_median +')'],
                    ['Movement', parseFloat(jsonData.exercise.mean), jsonData.exercise.color, 'Median Score: ' + parseFloat(jsonData.exercise.median) + ' (Range: '+ jsonData.exercise.min_median + '-' + jsonData.exercise.max_median +')'],
                    ['Stress', parseFloat(jsonData.stress.mean), jsonData.stress.color, 'Median Score: ' + parseFloat(jsonData.stress.median) + ' (Range: '+ jsonData.stress.min_median + '-' + jsonData.stress.max_median +')'],
                    ['Detox', parseFloat(jsonData.detox.mean), jsonData.detox.color, 'Median Score: ' + parseFloat(jsonData.detox.median) + ' (Range: '+ jsonData.detox.min_median + '-' + jsonData.detox.max_median +')']
                ]);
            var options2 = {
                title: 'STATUS OF DETERMINANTS OF HEALTH',
                chartArea: {width: '70%', height: '80%'},
                width: '100%',
                height: 550,
                legend: 'none'
            };
            var chart2 = new google.visualization.BarChart(document.getElementById('health_bar_chart'));
            chart2.draw(chartdata2, options2);
            //  center title
            $("text:contains(" + options2.title + ")").attr({'x':'30%', 'y':'20'})
        }
    });

}

function chronic_health_issues(data){
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_users_health',
        data: data,
        success: function (data1) {
            hide_waitMe();

            var jsonData = $.parseJSON(data1);

            var data = google.visualization.arrayToDataTable([
                ['Element', 'Users', {role: 'style'}, {role: 'annotation'}],
                ['Cancer', jsonData.cancer, '#83b85f', jsonData.cancer],
                ['Chronic illness', jsonData.chronic, '#d383b6', jsonData.chronic],
                ['Diabetes', jsonData.diabetes, '#fbce5e', jsonData.diabetes],
                ['Heart', jsonData.heart, '#81cce7', jsonData.heart],
                ['Hypertension', jsonData.hypertension, 'color: #34bcaf', jsonData.hypertension],
                ['Stroke', jsonData.stroke, 'color: #f493a1', jsonData.stroke],
                ['Weight', jsonData.weight, 'color: #f7a872', jsonData.weight],
                ['None', jsonData.none, 'color: #e5e4e2', jsonData.none]
            ]);

            var options = {
                title: 'Chronic Health Issues',
                chartArea: {width: '70%', height: '65%'},
                hAxis: {
                    minValue: 0,
                    format: '#'
                },
                width: 950,
                height: 550,
                vAxis: {
                    title: 'Issues'
                },
                legend: 'none'
            };
            var chart = new google.visualization.BarChart(document.getElementById('health_chart'));
            chart.draw(data, options);
        }
    });
}

function msq(data){
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_msq_scattered',
        data: data,
        success: function (data1) {
            hide_waitMe();
            var jsonData = $.parseJSON(data1);
            var data_h = [];
            var Header = ['', 'MSQ Scores', {role: 'style'}];
            data_h.push(Header);

            $("#optimal_cnt").text(jsonData['counts']['optimal']);
            $("#optimal_pct").text((jsonData['counts']['optimal'] > 0 ? ((jsonData['counts']['optimal']/jsonData['counts']['total'])*100).toFixed(2) : 0) + '%');
            $("#mild_cnt").text(jsonData['counts']['mild']);
            $("#mild_pct").text((jsonData['counts']['mild'] > 0 ? ((jsonData['counts']['mild']/jsonData['counts']['total'])*100).toFixed(2) : 0) + '%');
            $("#moderate_cnt").text(jsonData['counts']['moderate']);
            $("#moderate_pct").text((jsonData['counts']['moderate'] > 0 ? ((jsonData['counts']['moderate']/jsonData['counts']['total'])*100).toFixed(2) : 0) + '%');
            $("#severe_cnt").text(jsonData['counts']['severe']);
            $("#severe_pct").text((jsonData['counts']['severe'] > 0 ? ((jsonData['counts']['severe']/jsonData['counts']['total'])*100).toFixed(2) : 0) + '%');

            for (var i = 0; i < jsonData.healthscore.length; i++) {
                var temp = [];
                temp.push(i, parseInt(jsonData['healthscore'][i]), jsonData['color'][i]);

                data_h.push(temp);
            }
            if(jsonData.healthscore.length == 0){
                data_h.push([null, 0, null]);
            }
            var data1 = google.visualization.arrayToDataTable(data_h);
            var options1 = {
                title: 'MEDICAL SYMPTOMS QUESTIONNAIRE SCORES',
                vAxis: {title: 'MSQ Scores'},
                hAxis: {gridlines: {color: 'transparent'}, textPosition: 'none'},
                legend: 'none',
                width: 950,
                height: 700,
                chartArea: {'width': '80%', 'height': '90%', 'bottom': '1%'}
            };
            var chart1 = new google.visualization.ScatterChart(document.getElementById('msq_scattered_chart'));
            chart1.draw(data1, options1);
            //  center title
            $("text:contains(" + options1.title + ")").attr({'x':'200', 'y':'20'})
        }
    });
    
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_msq_score',
        data: data,
        success: function (data1) {
            hide_waitMe();
            var jsonData = $.parseJSON(data1);
            var data = google.visualization.arrayToDataTable([
                ['Element', '', {role: 'style'}, {role: 'annotation'}],
                ['Weight', jsonData.weight_score, '#81cce7', jsonData.weight_score],
                ['Skin', jsonData.skin_score, '#83b85f', jsonData.skin_score],
                ['Other', jsonData.other_score, '#d383b6', jsonData.other_score],
                ['Nose', jsonData.nose_score, '#fbce5e', jsonData.nose_score],
                ['Mouth and Throat', jsonData.mouth_and_throat_score, 'color: #81cce7', jsonData.mouth_and_throat_score],
                ['Mind', jsonData.mind_score, 'color: #34bcaf', jsonData.mind_score],
                ['Lungs', jsonData.lungs_score, 'color: #f493a1', jsonData.lungs_score],
                ['Joint and Muscles', jsonData.joints_and_muscles_score, 'color: #f7a872', jsonData.joints_and_muscles_score],
                ['Heart', jsonData.heart_score, 'color: #e5e4e2', jsonData.heart_score],
                ['Head', jsonData.head_score, 'color: #83b85f', jsonData.head_score],
                ['Eyes', jsonData.eyes_score, 'color: #fbce5e', jsonData.eyes_score],
                ['Energy', jsonData.energy_score, 'color: #81cce7', jsonData.energy_score],
                ['Emotions', jsonData.emotions_score, 'color: #34bcaf', jsonData.emotions_score],
                ['Ears', jsonData.ears_score, 'color: #f7a872', jsonData.ears_score],
                ['Digestive', jsonData.digestive_score, 'color: #e5e4e2', jsonData.digestive_score]
            ]);
            var options = {
                title: 'COUNTS OF USERS WITH MSQ SCORES >10 PER SYSTEM',
                chartArea: {width: '70%', height: '85%'},
                hAxis: { format: '#' },
                width: 1000,
                height: 800,
                vAxis: {
                    title: 'SYSTEM'
                },
                legend: 'none'
            };
            var chart = new google.visualization.BarChart(document.getElementById('msq_score_chart'));
            chart.draw(data, options);
            //  center title
//            $("text:contains(" + options.title + ")").attr({'x':'150', 'y':'20'})
        }
    });
}

function readiness(filter){
    $.ajax({
        type: 'POST',
        url: SITEROOT + '/dashboard/get_users_readiness',
        data: {filter: filter},
        success: function (data1) {
            hide_waitMe();
            var jsonData = $.parseJSON(data1);

            var data = [];
            var Header = ['Element', 'Not Ready', 'Ready'];
            data.push(Header);

            for (var i = 0; i < jsonData.length; i++) {
                data.push([
                    jsonData[i]['name'], parseInt(jsonData[i]['not_ready']), parseInt(jsonData[i]['ready'])
                ]);
            }
            if(jsonData.length == 0){
                data.push([
                    null, 0, 0
                ]);
            }
            // set inner height to 30 pixels per row
            var chartAreaHeight = data.length * 30;
            // add padding to outer height to accomodate title, axis labels, etc
            var chartHeight = chartAreaHeight + 80;

            var chartdata = google.visualization.arrayToDataTable(data);
            var options = {
                title: 'READINESS TO CHANGE',
                chartArea: {width: '40%', height: '80%'},
                isStacked: 'percent',
                height: chartHeight,
                colors: ['#EEC5E2', '#80CDE9'],
                vAxis: {
                    textStyle : {
                        fontSize: 12 // or the number you want
                    }
                }
            };
            var chart = new google.visualization.BarChart(document.getElementById('readiness_chart'));
            chart.draw(chartdata, options);
            //  center title
            $("text:contains(" + options.title + ")").attr({'x':'40%', 'y':'10'})
        }
    });
}

jQuery(document).ready(function ($v) {
  
   $v(".dm").on("click", function (e) {
        clearFilter()
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');
        $v('#dm').removeClass('hidden');
        $("#age_group_filter").show()
        
        chronic_health_issues();
   });

    $v(".demographics").on("click", function (e) {
        clearFilter()
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');
        $v('#demographics').removeClass('hidden');
           
        demographics()
   });

    $v(".rtfm").on("click", function (e) {
        $("#filterDiv").hide();
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');
        $v('#rtfm').removeClass('hidden');

        $(".rtfm_branch").css('color', 'blue');
        
        readiness('branch');
   });

    $v(".msq").on("click", function (e) {
        clearFilter()
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');
        $v('#msq').removeClass('hidden');
        $("#age_group_filter").show()

        msq();
   });

    $v(".hsahs").on("click", function (e) {
        clearFilter()
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');	  
        $v('#hsahs').removeClass('hidden');
        $("#age_group_filter").show()
        
        health_status()
   });
   
    $v(".upstream").on("click", function (e) {
        clearFilter();
        show_waitMe(jQuery('body'));
        $v('.dashboard-tab-color').removeClass('active');
        $v(this).addClass('active');
        $(".graphs").children().addClass('hidden');	  
        $v('#upstream').removeClass('hidden');
        $("#age_group_filter").show()

        var data = {}
        renderUpstreamTables(data);
    });
    
    $v("#branch, #department, #job_grade, #high_risk, #age_group").on("change", function (e) {
        show_waitMe(jQuery('body'));

        var data = $("#filterDiv select").serializeArray().filter(function(v){
            return v.value!==''
        });
        if($(this).attr("id") == "branch"){
            refreshDeptList(jQuery(this).val())
        }
        if($("a.demographics").hasClass("active")){
            demographics(data);
        }else if($("a.dm").hasClass("active")){
            chronic_health_issues(data);
        }else if($("a.msq").hasClass("active")){
            msq(data);
        }else if($("a.hsahs").hasClass("active")){
            health_status(data);
        }else if($("a.upstream").hasClass("active")){
            renderUpstreamTables(data);
        }

    });

    $v(".rtfm_branch, .rtfm_dept, .rtfm_jobgrade, .rtfm_status, .rtfm_age").on("click", function (e) {
        $('div#rtfm a').css('color', '#8e24aa');
        $(this).css('color', 'blue');
        show_waitMe(jQuery('body'));
        var filter;
        if($(this).hasClass("rtfm_branch")){
            filter = 'branch';
        }else if($(this).hasClass("rtfm_dept")){
            filter = 'department';
        }else if($(this).hasClass("rtfm_jobgrade")){
            filter = 'job_grade';
        }else if($(this).hasClass("rtfm_status")){
            filter = 'high_risk';
        }else{
            filter = 'age';
        }

        readiness(filter)
    });
    
    function hideInactive(exceptMe){
        $(".graphs").children().addClass('hidden');
        $(".graphs "+exceptMe).removeClass('hidden');
    }
    
    function renderUpstreamTables(filter){
        $('#upstream_tables').html("");
        
        $.post(SITEROOT + '/dashboard/upstream', filter, function (response) {
            hideInactive("#upstream");
            $('#upstream_tables').append(response);
            filter.table = null;
            filter.table = 'foodchoices'
            $.post(SITEROOT + '/dashboard/upstream_tables', filter, function (response1) {
                $('#upstream_tables').append(response1);
                filter.table = null;
                filter.table = 'stresssource'
                $.post(SITEROOT + '/dashboard/upstream_tables', filter, function (response2) {
                    $('#upstream_tables').append(response2);
                    filter.table = null;
                    filter.table = 'systemrank'
                    $.post(SITEROOT + '/dashboard/upstream_tables', filter, function (response3) {
                        $('#upstream_tables').append(response3);
                        filter.table = null;
                        filter.table = 'symptoms'
                        $.post(SITEROOT + '/dashboard/upstream_tables', filter, function (response4) {
                            $('#upstream_tables').append(response4);
                            hide_waitMe();
                        });
                    });
                });
            });
        });
    }
    
	$v(".msq-scores").on("click", function (e) {
	   $v('#msq_scattered_chart').addClass('hidden');
	   $v('.msq-scores, .small-note').addClass('hidden');
	   $v('#msq_score_chart').removeClass('hidden');
	   $v('.msq-scattered').removeClass('hidden');			  
	});

	$v(".msq-scattered").on("click", function (e) {
	   $v('#msq_scattered_chart').removeClass('hidden');
	   $v('.msq-scores, .small-note').removeClass('hidden');
	   $v('#msq_score_chart').addClass('hidden');
	   $v('.msq-scattered').addClass('hidden');			  
	});
	
});            

