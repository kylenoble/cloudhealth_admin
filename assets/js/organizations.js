jQuery(document).ready(function ($v) {

// Add new organization
    $v(document).on("click", ".add-orgnization .add-org-save", function (e) {
        e.preventDefault();
        $("#vip-organization-add").submit();
    });

    // Edit organization
    $v(document).on("submit", "#vip-organization-add", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: SITEROOT + "organizations/add_org_save",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function ()
            {
                show_waitMe(jQuery('body'));
            },
            success: function (response)
            {
                hide_waitMe();
                var response = jQuery.parseJSON(response);
                if (response.result.success) {
                    var data = {};
                    data.callback = 'organizations/index';
                    data.form_data = {};
                    page_ajaxify(data);
                } else {
                    $(".alert-danger").removeClass('hidden');
                    $(".alert-danger").html(response.result.errors);
                }
            },
            error: function (e)
            {
                console.log(e)
                hide_waitMe();
                $(".alert-danger").removeClass('hidden');
                $(".alert-danger").html(e);
            }
        });
    })

    $v(document).on("blur", "form#vip-organization-add input#org_name", function (e) {
        var org_name = $v.trim($v(this).val()).replace(" ", "_");
        if(org_name !== ''){
            $v("#email").val(org_name + '_admin@cloudhealthasia.com');
        }else{
            $v("#email").val('');
        }
    })

});