jQuery(document).ready(function ($v) {

    function doTask(data, next, url, table, i, total, type) {
        if ($.trim(data) !== '') {
            let row = [];
            let cells = data;

            var time = Math.floor(Math.random() * 3000);

            setTimeout(function () {
                if(type == 'enrollment'){
                    row.push(cells.row);
                    row.push(cells.email);
                    row.push(cells.name);
                    row.push(cells.dob);
                    row.push(cells.gender);
                    row.push(cells.branch);
                    row.push(cells.department);
                    row.push(cells.employee_id);
                    row.push(cells.job_grade);
                    row.push(cells.high_risk);
                }else{
                    row.push(cells.row);
                    row.push(cells.name);
                    row.push(cells.email);
                }

                processEmailSending(cells, row, i+1, total, url);

                table.row.add(row).draw();
                table.order([0, 'desc']).draw();

                next();
            }, time);
        }

    }

    function createTask(data, url, table, i, total, type) {
        return function (next) {
            doTask(data, next, url, table, i, total, type);
        }
    }

    $v(document).on('click', '.upload-employees', function () {
        //check if file field is not empty
        if($("#employee_file").val() !== ""){
            show_waitMe(jQuery('body'));
            uploadFile();
        }
    });
       
    var canonicalizeNewlines = function(str) {
        return str.replace(/(\r\n?|\r|\n)/g, '\n');
    };
    
    function uploadFile(){
        var file_data = $v('#employee_file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('is_update', $v('#update-users').is(":checked"));
        console.log(file_data);
        $.ajax({
            url: SITEROOT + '/healthprofile/upload_employeesFile/',
            dataType: 'text', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                hide_waitMe();
                const r = jQuery.parseJSON(response);
                if (r.result.errors != null) {
                    $('.upload-message').html(r.result.errors);
                } else {
                    if($v('#update-users').is(":checked")){
                        $('.upload-message').html("Employee emails were successfully updated.");
                    }else{
                        $("#employee-enrollment-modal").modal("open")
                        $(".modal-close").attr("disabled", true);
                        $('.upload-message').html("Employee List uploaded successfully.");
                        processEmployeeUpload(r.result.data);
                    }
                }
            }
        });
    }
    
    function processEmployeeUpload(data){
        $(".progress_upload").addClass("hidden");
        const url = SITEROOT + '/healthprofile/employee_enrollment';
        var table = $("#email-upload").DataTable();
        table.clear().draw();

        for (let i = 0; i < data.length; i++) {
            $v(document).queue('tasks', createTask(data[i], url, table, i, data.length, 'enrollment'));
        }
        
        $v(document).dequeue('tasks');
    }
    
    function lineCount( text ) {
        var nLines = 0;
        var hdr_row = 1;
        for( var i = hdr_row, n = text.length;  i < n;  ++i ) {
            if( text[i] != '' ) {
                nLines++;
            }
        }
        return nLines;
    }
    
    function processUpload(){
        if (typeof (FileReader) != "undefined") {
            $("#email-sending-modal").modal("open")
            
            $(".progress_upload").addClass("hidden");
            $(".modal-close").attr("disabled", true);
            
            let reader = new FileReader();
            let table = $("#email-consent").DataTable();
            const url = SITEROOT + '/healthprofile/upload_employees_consent';

            table.clear().draw();

            reader.onload = function (e) {
                let rows = canonicalizeNewlines(e.target.result).split("\n");
                let rowCnt = lineCount(rows)
console.log(rowCnt);
                for (let i = 1; i < rows.length; i++) {
                    let data = new Object();
                    let cell= rows[i].split(",")
                    if(cell[0] !== '' && cell[1] !== '' && cell[1] !== ''){
                        data.row = i;
                        data.name = cell[0];
                        data.email = cell[1];
                        $v(document).queue('tasks', createTask(data, url, table, i-1, rowCnt, 'consent'));
                    }
                }
                $v(document).dequeue('tasks');
            }
            reader.readAsText($("#employee_consent_file")[0].files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }

    }
    
    function processEmailSending(data, cell, i, total, url){
        var bar = $('.bar');
        var percent = $('.percent');
        percent.html('');
        var loader = '<div id="p'+i+'" class="progress">'
                + '<div class="indeterminate"></div>'
                + '</div>';
        
        $(".progress_upload").removeClass("hidden");
        var percentVal = ((i/total)*100) + '%';
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function () {
                bar.width(percentVal)
                percent.html(i + '/' + total);
                cell.push(loader);
            },
            uploadProgress: function (event, position, total, percentComplete) {
                bar.width(percentVal)
                percent.html(i + '/' + total);
            },
            complete: function (xhr) {
                bar.width(percentVal)
                percent.html(i + '/' + total);
                
                var response = xhr.responseJSON;
                $("div#p"+i).removeClass("progress").addClass(response.color+" delivery-status")
                $("div#p"+i+" div").text(response.status)
                console.log(total)
                if(i === total){
                    $(".modal-close").removeAttr("disabled");
                }
            }
         });
    }
    
    $v(document).on('click', '.upload-employees-consent-file', function () {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
        if (regex.test($("#employee_consent_file").val().toLowerCase())) {
            processUpload();
        } else {
            alert("Please upload a valid CSV file.");
        }
    });
    
    // Add more event actions
    $v(document).on("click", ".add_more", function (e) {
        var fieldHTML = '<div class="row m-b-15">';
        fieldHTML += '<input type="hidden" value="0" name="branches_id[]" />';
        fieldHTML += '<span class="col-sm-3"><input class="form-control" name="branch_name[]" placeholder="Branch Name">';
        fieldHTML += '</span>';
        fieldHTML += '<span class="col-sm-3"><input class="form-control" name="branch_address[]" placeholder="Branch Address">';
        fieldHTML += '</span>';
        fieldHTML += '<span class="col-sm-3"><input type="text" class="form-control" value="" name="branch_contact[]" placeholder="Contact Number"></span>';
        fieldHTML += '<span class="remove-btn col-sm-1"><a href="javascript:void(0);" class="remove_button" title="Remove field">';
        fieldHTML += '<span class="ti ti-trash"></span></a></span>';
        fieldHTML += '</div>';
        $v('#add-action').append(fieldHTML);
    });

    $v(document).on("click", ".add_more_dept", function (e) {
        $.get(SITEROOT + '/organizations/getBranches/' + $("#id").val(), function(response){
            var data = jQuery.parseJSON(response)
            var options = "<option value=''> - </option>";
            for (var i = 0; i < data.length; i++) {
                options += "<option value='" + data[i]['id'] + "'>" + data[i]['br_name'] + "</option>";
            }
            var fieldHTML = '<div class="row m-b-15">';
            fieldHTML += '<input type="hidden" value="0" name="departments_id[]" />';
            fieldHTML += '<span class="col-sm-4">';
            fieldHTML += '<select class="form-control" name="branch_id[]" placeholder="Select Branch">'+options+'</select>';
            fieldHTML += '</span>';
            fieldHTML += '<span class="col-sm-4">';
            fieldHTML += '<input class="form-control" name="dept_name[]" placeholder="Department Name">';
            fieldHTML += '</span>';
            fieldHTML += '<span class="remove-btn col-sm-2"><a href="javascript:void(0);" class="remove_button" title="Remove field">';
            fieldHTML += '<span class="ti ti-trash"></span></a></span>';
            fieldHTML += '</div>';
            $v('#add-action-department').append(fieldHTML);
        })
    });

    //Remove action
    $v(document).on("click", ".remove_button", function (e) {
        e.preventDefault();
        $v(this).parent().closest('div').remove();
    });

    $v(document).on("click", ".edit-org-save", function(e){
        e.preventDefault();
        $("#vip-organization-update").submit()
    })

    // Edit organization
    $v(document).on("submit", "#vip-organization-update", function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        $.ajax({
            url: SITEROOT + "organizations/org_update",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function ()
            {
                show_waitMe(jQuery('body'));
            },
            success: function (response)
            {
                hide_waitMe();
                var response = jQuery.parseJSON(response);
                if (response.result.success) {
                    var data = {};
                    var company_id = $("#id").val();
                    if ($("#is_hr").val() == true) {
                        data.callback = 'healthprofile/index';
                    } else {
                        data.callback = 'organizations/view/' + company_id;
                    }
                    page_ajaxify(data);
                } else {
                    $(".alert-danger").removeClass('hidden');
                    $(".alert-danger").html(response.result.errors);
                }
            },
            error: function (e)
            {
                console.log(e)
                hide_waitMe();
                $(".alert-danger").removeClass('hidden');
                $(".alert-danger").html(e);
            }
        });
    })

})

function showSummaryAndAnalysis(obj) {
//    show_waitMe(jQuery('body'));
    $("#company_registration").addClass("hidden");
    $.get(SITEROOT + '/healthprofile/summary_and_analysis/', function (response) {
        $("#healthprofile_content ul li").removeClass('active');
        $(obj).closest('li').addClass('active');
//        hide_waitMe();
        $("#summary_and_analysis").html(response);
        $("#summary_and_analysis").removeClass("hidden");
    });
}