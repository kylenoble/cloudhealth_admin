 // Load the Visualization API and the line package.
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(showData);


jQuery(document).ready(function ($v) {

    $(".no-data").hide();

    $("#schedule_from, #schedule_to").pickadate({
        selectMonths: true, // Creates a dropdown to control month
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        format: 'mmm/dd/yyyy',
        formatSubmit: 'mmm/dd/yyyy',
        closeOnSelect: false, // Close upon selecting a date,
        onClose: function() {
            $('.datepicker').blur();
            $('body').focus();
        }
    });
    var date = new Date()
    var date_from = $("#date_from").pickadate({
        selectMonths: true,
        selectYears: true,
        format: 'mm/dd/yyyy'
    });
    var picker1 = $(date_from).pickadate("picker")
    picker1.set('select', new Date(date.getFullYear()-1, date.getMonth(), date.getDate()))
    
    var date_to = $("#date_to").pickadate({
        selectMonths: true,
        selectYears: true,
        format: 'mm/dd/yyyy',
        min: $("#date_from").val()
    });
    var picker2 = $(date_to).pickadate("picker")
    picker2.set('select', new Date(date.getFullYear(), date.getMonth(), date.getDate()))

    $(".view-analytics").on('click', function(){
        show_waitMe(jQuery('body'));
        showPieChart1()
    })
    
    $(".view-patient-outcomes").on('click', function(){
        show_waitMe(jQuery('body'));
        //draw patient outcomes pie chart
        drawPieChart2()
    })
    
    $("input:radio[name='viewby']").on('click', function(){
        $(".chart").addClass("hidden")
        if($(this).attr("id") == "md"){
            $("#md_analytics_info").removeClass("hidden")
            drawChart1()
        }else if($(this).attr("id") == "company_rdo"){
            $("#co_analytics_info").removeClass("hidden")
            drawChart2()
        }else{
            $("#analytics_info").removeClass("hidden")
            drawBarChart()
        }
    })
    
    $(".reload-line-outcomes").on("click", function(){
        showLineChart();
    })

});            

function showData() {
    showPieChart1();
    //draw patient outcomes pie chart
    drawPieChart2();
}

function showLineChart(){
    $.post(SITEROOT + '/dashboard/getPatientOutcomesLineChart',
        $('#linechart_filter :input').serialize(),
        function (response) {          
            //draw line chart
            drawLineChart(response.line_data, response.line_daterange);
        }, 'json');
}

function showPieChart1(){
    $.post(SITEROOT + '/dashboard/getMDAdminAnalyticsData',
        $('#analytics-filter').serializeArray(),
        function (response) {
            drawPieChart(response.result);
        },
        'json'
    );
}

function drawPieChart2(){
    $.post(SITEROOT + '/dashboard/getPatientOutcomesAnalyticsData',
        $('#patient-outcomes-filter').serializeArray(),
        function (response) {
            var data = response.data
            console.log(data)
            var dataTable = new google.visualization.DataTable();
            
            dataTable.addColumn('string', 'Status');
            dataTable.addColumn('number', 'Count');
            // A column for custom tooltip content
            dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
            dataTable.addRows([
                ['Improving', data.improving ? data.improving.length : 0, data.improving ? showEmployees(data.improving) : null],
                ['Not Improving', data.notImproving ? data.notImproving.length : 0, data.notImproving ? showEmployees(data.notImproving) : null],
                ['For Immediate follow up', data.forImmediateFollowUp ? data.forImmediateFollowUp.length : 0, data.forImmediateFollowUp ? showEmployees(data.forImmediateFollowUp) : null],
                ['Follow up Next Month', data.followUpNextMonth ? data.followUpNextMonth.length : 0, data.followUpNextMonth ? showEmployees(data.followUpNextMonth) : null],
                ['Stalled or needs Recovery', data.stalledOrNeedsRecovery ? data.stalledOrNeedsRecovery.length : 0, data.stalledOrNeedsRecovery ? showEmployees(data.stalledOrNeedsRecovery) : null]
            ]);

            //draw pie chart
            var options1 = {
                chartArea: {width: '100%', height: '100%', top: '0%', right: '10%'},
                width: 300,
                height: 300,
                colors: ['#72a153', '#71b2ca', '#dfaf29', '#d77f1a', '#9c9e9e'],
                legend: 'none',
                backgroundColor: { fill:'transparent' },
                tooltip: { isHtml: true },
                focusTarget: 'category'
            };

            if(Object.keys(data).length == 0){
                $("#linechart").hide();
                $("#piechart_outcomes, #linechart_outcomes").html("")
                $("#piechart_outcomes").addClass("no-data").css({width: '100%'}).text("No available data")
                hide_waitMe();
            }else{
                $("#linechart").show();
                $("#piechart_outcomes").removeClass("no-data").removeAttr("style").text("")
                
                var chart1 = new google.visualization.PieChart(document.getElementById('piechart_outcomes'));
                chart1.draw(dataTable, options1);
                
                //draw line chart
                showLineChart();
            }
        },
        'json'
    );
}

function showEmployees(data) {
    var table = '<div style="padding:2px">';
    table += '<table>';
    table += '<thead><tr>' +
                '<th>Employee ID</th><th>Name</th><th>Age / Sex</th><th>Previous Outcome</th>' +
                '</tr></thead>' +
                '<tbody>';
    data.forEach((value, index) => {
        var status = value.previous_outcome_status;
        if(value.previous_outcome_status_label){
            status = value.previous_outcome_status_label;
        }
        table += '<tr>' +
                '<td class="center">' +value.employee_id+ '</td>' +
                '<td>' +value.name+ '</td>' +
                '<td class="center">' +value.age_sex+ '</td>' +
                '<td class="center">' + status + '</td>' +
                '</tr>';
    });
    table += '</tbody></table>';
    table += '</div>';
  
    return table;
}

function drawLineChart(data, date_range){
    var dataTable = new google.visualization.DataTable();

    dataTable.addColumn('string', 'Month/Year');
    dataTable.addColumn('number', 'Improving');
    dataTable.addColumn('number', 'Not Improving');
    dataTable.addColumn('number', 'For Immediate follow up');
    dataTable.addColumn('number', 'Follow up Next Month');
    dataTable.addColumn('number', 'Stalled or needs Recovery');
console.log(data)
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if((data[key]).hasOwnProperty('total')){
                dataTable.addRow([key, 
                parseInt(data[key]['improving']), 
                parseInt(data[key]['notImproving']), 
                parseInt(data[key]['forImmediateFollowUp']), 
                parseInt(data[key]['followUpNextMonth']), 
                parseInt(data[key]['stalledOrNeedsRecovery'])])
            }else{
                dataTable.addRow([key, 0, 0, 0, 0, 0])
            }
        }
    }

    //draw pie chart
    var options1 = {
        title: date_range.from + ' - ' + date_range.to,
        curveType: 'function',
        chartArea: {width: '100%', height: '100%', top: '25%', left: '10%', bottom: '25%'},
        width: 750,
        height: 320,
        colors: ['#72a153', '#71b2ca', '#dfaf29', '#d77f1a', '#9c9e9e'],
        backgroundColor: {fill: 'transparent'},
        vAxis: {viewWindow: {min: 0}, format: '0'},
        legend: {position: 'top'},
        pointSize: 15,
        series: {
            0: { pointShape: { type: 'star', sides: 5, dent: 0.5 } },
            1: { pointShape: { type: 'triangle', sides: 5 } },
            2: { pointShape: { type: 'cirle' } },
            3: { pointShape: { type: 'square' } },
            4: { pointShape: { type: 'star', sides: 5, dent: 0.7 } }
        }
    };

    $("#linechart_outcomes").removeClass("no-data").text("")

    var chart1 = new google.visualization.LineChart(document.getElementById('linechart_outcomes'));
    chart1.draw(dataTable, options1);

    hide_waitMe();
}

function _getPercentage(x, total){
    let ans = (parseInt(x) / parseInt(total)) * 100;
    if(ans > 0 && ans < 100){
        return ans.toFixed(1);
    }
    return ans;
}

function drawPieChart(data){
    var total_approved, total_rescheduled, total = 0;
    total_approved = parseInt(data.approved_completed) + parseInt(data.approved_missed) + parseInt(data.approved_pending);
    total_rescheduled = parseInt(data.rescheduled_completed) + parseInt(data.rescheduled_missed) + parseInt(data.rescheduled_pending);
    total = total_approved + parseInt(data.cancelled) + total_rescheduled + parseInt(data.pending_for_approval)

    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn('string', 'Status');
    dataTable.addColumn('number', 'Count');
    // A column for custom tooltip content
    dataTable.addColumn({type: 'string', role: 'tooltip'});
    dataTable.addRows([
        ['Approved', total_approved, 'APPROVED: '+total_approved+' ('+_getPercentage(parseInt(total_approved), total)+'%) \r\n\r\nCompleted: '+data.approved_completed +' ('+_getPercentage(parseInt(data.approved_completed), total_approved)+'%)\r\nMissed: '+data.approved_missed +' ('+_getPercentage(parseInt(data.approved_missed), total_approved)+'%)\r\nPending: '+data.approved_pending +' ('+_getPercentage(parseInt(data.approved_pending), total_approved)+'%)'],
        ['Cancelled', parseInt(data.cancelled), 'CANCELLED: '+data.cancelled+' ('+_getPercentage(parseInt(data.cancelled), total)+'%)'],
        ['Rescheduled', total_rescheduled, 'RESCHEDULED: '+total_rescheduled+' ('+_getPercentage(parseInt(total_rescheduled), total)+'%) \r\n\r\nCompleted: '+data.rescheduled_completed +' ('+_getPercentage(parseInt(data.rescheduled_completed), total_rescheduled)+'%)\r\nMissed: '+data.rescheduled_missed +' ('+_getPercentage(parseInt(data.rescheduled_missed), total_rescheduled)+'%)\r\nPending: '+data.rescheduled_pending +' ('+_getPercentage(parseInt(data.rescheduled_pending), total_rescheduled)+'%)'],
        ['Pending', parseInt(data.pending_for_approval), 'PENDING FOR APPROVAL: '+data.pending_for_approval+' ('+_getPercentage(parseInt(data.pending_for_approval), total)+'%)']
    ]); 
    
    var options1 = {
        chartArea: {width: '100%', height: '100%', top: '0%', right: '10%'},
        width: 300,
        height: 300,
        colors: ['#71B2CA', '#9C9E9E', '#E1AF29', '#D77F1A'],
        legend: 'none',
        backgroundColor: { fill:'transparent' }
    };
    
    if(data.approved_completed == 0 && data.approved_missed == 0 && data.approved_pending == 0 && data.cancelled == 0 && data.pending_for_approval == 0 && data.rescheduled_completed == 0 && data.rescheduled_missed == 0 && data.rescheduled_pending == 0){
        $("#barchart").hide();
        $("#piechart").html("").addClass("no-data").css({width: '100%'}).text("No available data")
    }else{
        $("#piechart").removeClass("no-data").removeAttr("style").text("")

        var chart1 = new google.visualization.PieChart(document.getElementById('piechart'));
        chart1.draw(dataTable, options1);
        
        //display appointments barchart
        $("#barchart").show();
        $("input:radio[name='viewby']:checked").trigger('click')
    }
    hide_waitMe();
}

function drawBarChart(){
    $.post(SITEROOT + '/dashboard/getAppointmentsAnalytics',
        $("#analytics-filter").serializeArray(),
        function (data) {
            hide_waitMe();
            showDefaultChart(data)
        }, 
    'json');
}

function drawChart1(){
    $.post(SITEROOT + '/dashboard/getAppointmentsAnalytics/MD',
        $("#analytics-filter").serializeArray(),
        function (data) {
            hide_waitMe();
            showChart_MD(data)
        }, 
    'json');
}

function drawChart2(){
    $.post(SITEROOT + '/dashboard/getAppointmentsAnalytics/company',
        $("#analytics-filter").serializeArray(),
        function (data) {
            hide_waitMe();
            showChart_Company(data)
        }, 
    'json');
}

function showDefaultChart(jsonData){
    var data = [];
    var total_approved, total_rescheduled, total = 0;
    var Header = ['Element', 'Approved', {type: 'string', role: 'tooltip'}, 'Cancelled', 'Rescheduled', {type: 'string', role: 'tooltip'}, 'Pending'];
    data.push(Header);

    if(jsonData.length > 0){
        for (let i = 0; i < jsonData.length; i++) {
            
            total_approved = parseInt(jsonData[i]['approved_completed']) + parseInt(jsonData[i]['approved_missed']) + parseInt(jsonData[i]['approved_pending']);
            total_rescheduled = parseInt(jsonData[i]['rescheduled_completed']) + parseInt(jsonData[i]['rescheduled_missed']) + parseInt(jsonData[i]['rescheduled_pending']);
            total = total_approved + parseInt(jsonData[i]['cancelled']) + total_rescheduled + parseInt(jsonData[i]['pending_for_approval'])

            let approved_tooltip = 'Completed: ' + jsonData[i]['approved_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_completed']), total_approved) + '%)\r\nMissed: ' + jsonData[i]['approved_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_missed']), total_approved) + '%)\r\nPending: ' + jsonData[i]['approved_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_pending']), total_approved) + '%)\r\n\r\nApproved Appointment(s): '+total_approved;
            
            let rescheduled_tooltip = 'Completed: ' + jsonData[i]['rescheduled_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_completed']), total_rescheduled) + '%)\r\nMissed: ' + jsonData[i]['rescheduled_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_missed']), total_rescheduled) + '%)\r\nPending: ' + jsonData[i]['rescheduled_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_pending']), total_rescheduled) + '%)\r\n\r\nRescheduled Appointment(s): '+total_rescheduled
    
            data.push([
                jsonData[i]['column_select'], 
                total_approved, approved_tooltip,
                parseInt(jsonData[i]['cancelled']),
                total_rescheduled, rescheduled_tooltip, 
                parseInt(jsonData[i]['pending_for_approval'])
            ]);
        }
    }else{
        data.push([0, 0, 0, 0, 0]);
    }
    var chartdata = google.visualization.arrayToDataTable(data);
    
    var options = {
        chartArea: {width: '100%', height: '80%', top: '10%', left: '10%'},
        width: 700,
        height: 300,
        isStacked: 'percent',
        colors: ['#71B2CA', '#9C9E9E', '#E1AF29', '#D77F1A'],
        bar: { groupWidth: '75%' },
        vAxis: {
            minValue: 0,
            ticks: [0, .2, .4, .6, .8, 1]
        },
        backgroundColor: { fill:'transparent' }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('analytics_info'));
    chart.draw(chartdata, options);

    $("text:contains(" + options.title + ")").css({'font-size': '20px'})
}

function showChart_MD(jsonData){
    var data = [];
    var total_approved, total_rescheduled, total = 0;
    var Header = ['Element', 'Approved', {type: 'string', role: 'tooltip'}, 'Cancelled', 'Rescheduled', {type: 'string', role: 'tooltip'}, 'Pending'];
    data.push(Header);

    if(jsonData.length > 0){
        for (var i = 0; i < jsonData.length; i++) {
            total_approved = parseInt(jsonData[i]['approved_completed']) + parseInt(jsonData[i]['approved_missed']) + parseInt(jsonData[i]['approved_pending']);
            total_rescheduled = parseInt(jsonData[i]['rescheduled_completed']) + parseInt(jsonData[i]['rescheduled_missed']) + parseInt(jsonData[i]['rescheduled_pending']);
            total = total_approved + parseInt(jsonData[i]['cancelled']) + total_rescheduled + parseInt(jsonData[i]['pending_for_approval'])

            let approved_tooltip = 'Completed: ' + jsonData[i]['approved_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_completed']), total_approved) + '%)\r\nMissed: ' + jsonData[i]['approved_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_missed']), total_approved) + '%)\r\nPending: ' + jsonData[i]['approved_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_pending']), total_approved) + '%)\r\n\r\nApproved Appointment(s): '+total_approved;
            
            let rescheduled_tooltip = 'Completed: ' + jsonData[i]['rescheduled_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_completed']), total_rescheduled) + '%)\r\nMissed: ' + jsonData[i]['rescheduled_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_missed']), total_rescheduled) + '%)\r\nPending: ' + jsonData[i]['rescheduled_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_pending']), total_rescheduled) + '%)\r\n\r\nRescheduled Appointment(s): '+total_rescheduled;
    
            data.push([
                jsonData[i]['md_name'], 
                total_approved, approved_tooltip,
                parseInt(jsonData[i]['cancelled']),
                total_rescheduled, rescheduled_tooltip, 
                parseInt(jsonData[i]['pending_for_approval'])
            ]);
        }
    }else{
        data.push([0, 0, 0, 0, 0]);
    }
    var chartdata = google.visualization.arrayToDataTable(data);
    var options = {
        chartArea: {width: '100%', height: '80%', top: '10%', left: '10%'},
        width: 700,
        height: 300,
        legend: 'none',
        isStacked: 'percent',
        colors: ['#71B2CA', '#9C9E9E', '#E1AF29', '#D77F1A'],
        bar: { groupWidth: '75%' },
        vAxis: {
            minValue: 0,
            ticks: [0, .2, .4, .6, .8, 1]
        },
        backgroundColor: { fill:'transparent' }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('md_analytics_info'));
    chart.draw(chartdata, options);

    $("text:contains(" + options.title + ")").css({'font-size': '20px'})
}

function showChart_Company(jsonData){
    var data = [];
    var total_approved, total_rescheduled, total = 0;
    var Header = ['Element', 'Approved', {type: 'string', role: 'tooltip'}, 'Cancelled', 'Rescheduled', {type: 'string', role: 'tooltip'}, 'Pending'];
    data.push(Header);

    if(jsonData.length > 0){
        for (var i = 0; i < jsonData.length; i++) {
            total_approved = parseInt(jsonData[i]['approved_completed']) + parseInt(jsonData[i]['approved_missed']) + parseInt(jsonData[i]['approved_pending']);
            total_rescheduled = parseInt(jsonData[i]['rescheduled_completed']) + parseInt(jsonData[i]['rescheduled_missed']) + parseInt(jsonData[i]['rescheduled_pending']);
            total = total_approved + parseInt(jsonData[i]['cancelled']) + total_rescheduled + parseInt(jsonData[i]['pending_for_approval'])

            let approved_tooltip = 'Completed: ' + jsonData[i]['approved_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_completed']), total_approved) + '%)\r\nMissed: ' + jsonData[i]['approved_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_missed']), total_approved) + '%)\r\nPending: ' + jsonData[i]['approved_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['approved_pending']), total_approved) + '%)\r\n\r\nApproved Appointment(s): '+total_approved;
            
            let rescheduled_tooltip = 'Completed: ' + jsonData[i]['rescheduled_completed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_completed']), total_rescheduled) + '%)\r\nMissed: ' + jsonData[i]['rescheduled_missed'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_missed']), total_rescheduled) + '%)\r\nPending: ' + jsonData[i]['rescheduled_pending'] + ' (' + _getPercentage(parseInt(jsonData[i]['rescheduled_pending']), total_rescheduled) + '%)\r\n\r\nRescheduled Appointment(s): '+total_rescheduled;
    
            data.push([
                jsonData[i]['company'], 
                total_approved, approved_tooltip,
                parseInt(jsonData[i]['cancelled']),
                total_rescheduled, rescheduled_tooltip, 
                parseInt(jsonData[i]['pending_for_approval'])
            ]);
        }
    }else{
        data.push([0, 0, 0, 0, 0]);
    }
    var chartdata = google.visualization.arrayToDataTable(data);
    var options = {
        chartArea: {width: '100%', height: '80%', top: '10%', left: '10%'},
        width: 700,
        height: 300,
        legend: 'none',
        isStacked: 'percent',
        colors: ['#71B2CA', '#9C9E9E', '#E1AF29', '#D77F1A'],
        bar: { groupWidth: '75%' },
        vAxis: {
            minValue: 0,
            ticks: [0, .2, .4, .6, .8, 1]
        },
        backgroundColor: { fill:'transparent' }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('co_analytics_info'));
    chart.draw(chartdata, options);

    $("text:contains(" + options.title + ")").css({'font-size': '20px'})
}