$(document).ready(function() {

    /****************************
     *    ACCESS MANAGEMENT     *
     ****************************/
    $('#users').DataTable({
        scrollY:        400,
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns: {
            leftColumns: 2
        },
        "oLanguage": {
            "sSearch": "Search by Name or Employee ID" //search
        },
        "order": [[ 8, "desc" ]],
        "columnDefs": [
            { "searchable": false, "targets": [2,3,4,5,6,7,8,9,10,11,12,13] },
            { "orderable": false, "targets": [-1] }
        ],
        dom: 'Bfrtip',
        buttons: [{ 
            extend: 'pdf', 
            text: 'Export to PDF',
            title: 'List of Registered Employees: ('+ COMPANY_NAME+')',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13]
                }
            }
        ]
    });
    
    /****************************
     *      ACCESS REVOKE       *
     ****************************/
    $('#users tbody').on('click', 'a.user-access', function(){
        var userid = $(this).attr('id');
        
        if(confirm("This will revoke the access of the user and delete his files in the system. Do you wish to continue?")){
            $.post(SITEROOT + '/account/revoke', {userid: userid}, function(response){
                if(response){
                    show_waitMe(jQuery('body'));
                    $.get(SITEROOT + '/account/users', function (response) {
                        hide_waitMe();
                        $("#content_view").html(response);
                    });
                }else{
                    alert('An error was encountered. Please contact your system administrator.');
                }
            });
        }
        return false;
    });
    
    /****************************
     *    UNLOCK SURVEY FORM    *
     ****************************/
    $(document).on('click', '.unlock-survey-form', function(){
        var selected = $("input:checkbox[id^=checkMe_]:checked");
        if(selected.length > 0){
            if(confirm("Are you sure you want to unlock the Health Survey forms to the selected users?")){
                var userids = [];
                $(selected).each(function(){
                    var tmp_id = ($(this).attr("id")).split("_")
                    userids.push(tmp_id[1]);
                });

                if(userids.length > 0){
                    $.post(SITEROOT + '/account/survey_unlock', {userids: userids}, function(response){
                        if(response){
                            show_waitMe(jQuery('body'));
                            $.get(SITEROOT + '/account/users', function (response) {
                                hide_waitMe();
                                $("#content_view").html(response);
                            });
                        }else{
                            alert('An error was encountered. Please contact your system administrator.');
                        }
                    });
                }
            }
        }else{
            alert("Please select user(s) with LOCKED Health Survey Forms")
        }
    });
    
    $(document).on('click', "input:checkbox#checkAllLocked", function(){
        var select_all = $(this).prop("checked");
        $("input:checkbox[id^=checkMe_]").each(function(index){
            if(select_all){
                $(this).prop("checked", true);
            }else{
                $(this).prop( "indeterminate" , false );
                $(this).prop( "checked" , false );
            }
        });
    });

/**
 * SUBSCRIPTIONS AND RENEWALS
 */
    
    $('.modal').modal({
        dismissible: false
    });
    
    
    
    $(document).on('click', "#employee_table input:checkbox#checkAll", function(){
        var select_all = $(this).prop("checked");
        $("#employee_table input:checkbox:not(#checkAll):not(:disabled)").each(function(index){
            if(select_all){
                $(this).prop("checked", true);
            }else{
                $(this).prop( "indeterminate" , false );
                $(this).prop( "checked" , false );
            }
        });
    });
    
    $(document).on('click', "#package-enroll-form .enroll-confirm-btn", function(){
        //check if there is atleast one employee selected for enrollment
        var count_selected = $("#employee_table input:checkbox:checked:not(#checkAll)").length;
        $("#package-enroll-form").find(".alert-warning").remove();
        if(count_selected > 0){
            var is_basic = $("table#package-subscriptions input:checkbox:checked").data("package-type") == 'Basic';
            if(is_basic){
                //check if there is a selected health talk
                if($("#basic_package_name").val() === ''){
                    var warning = '<div class="alert alert-warning" style="margin-bottom: 0;padding: 10px;">Please select a health talk.</div>';
                    $("#package-enroll-form .modal-header").after(warning);
                    $("#basic_package_name").css('border', '1px solid red');
                    return false;
                }
                $("#basic_package_name").remove('style')
                //add event details
                $("#enroll-confirm #healthtalk-confirm").text($('#basic_package_name').find("option:selected").text())
                $("#enroll-confirm #healthtalk-confirm").removeClass("hidden")
            }else{
                $("#enroll-confirm #healthtalk-confirm").addClass("hidden")
            }
            $("#enroll-confirm #package-name-confirm").text($('#package-name').text())
            $("#enroll-confirm #confirm-list").html($('#employee_table').clone())
            $("#confirm-list-cnt").text(count_selected);
            $("#enroll-confirm #confirm-list #employee_table tbody").find("input:checkbox:not(:checked)").closest('tr').remove();
            $("#enroll-confirm #confirm-list #employee_table tbody td:first-child").remove();
            $("#enroll-confirm #confirm-list #employee_table thead th:first-child").remove();
            $('#enroll-confirm').modal('open');
        }else{
            var warning = '<div class="alert alert-warning" style="margin-bottom: 0;padding: 10px;">Please select which employees you wish to enroll to this program.</div>';
            $("#package-enroll-form .modal-header").after(warning);
        }
    });
    
});