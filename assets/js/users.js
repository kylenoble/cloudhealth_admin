jQuery(document).ready(function ($v) {

$v(document).on("click", "#organization_actions", function () {
		var data = {};
		data.callback ='organizations/add';
		data.form_data = {};			
		page_ajaxify(data);
		return false;
})

    $v(document).on("click", ".remove-record", function (e) {
        $(".alert-danger").addClass('hidden');
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        data.callback = jQuery.trim($v(this).attr('href'));
        data.form_data = {};

		var conf = confirm('Are you sure you want to delete this user?');

		if (conf)
		{
			var r = vip_ajax(data);

			if(r.success== '1') {
				alert('User deleted successfully.');
				var data = {};
				data.callback = 'users/index';            
				page_ajaxify(data);
			}
			else {
				alert('Error in deleting user.Please try later.');
			}
		}
		else {
			return false;
		}
    })
    
});