
	// Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	// (c) 2013 @ElmahdiMahmoud
	// license: https://www.opensource.org/licenses/mit-license.php


    var addOnsBtnState;
        
    $(document).on('click', ".dropdown dt a[id^=addons-select_]", function () {
        var tmp_id = ($(this).attr('id')).split("_");
    
        addOnsBtnState = 1;
    
        $(".dropdown dd#dd_" + tmp_id[1] + " ul").slideToggle('fast');
    });
    
    $(".dropdown dd ul li a").on('click', function () {
        alert('CLOSE');
        $(".dropdown dd ul").hide();
    });
    
    function getSelectedValue(id) {
        return $("#" + id).find("dt a span.value").html();
    }
    
    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
    });
    
    
    
    // check if the data table is ready and loaded
    $('.dataTables_wrapper').ready(function() {
        setTimeout(function() {
            // call checkAddons function
            checkAddons();
        }, 300);
    });
    
    
    function checkAddons() {
        // check if .hida text is equal to 'Show Add-ons' then if yes add the class 'addons-selected'
        $('.dropdown dt a span.hida').each(function() {
            if ($(this).text() === 'Show Add-Ons') {
                $(this).addClass('addons-selected');
            }
        });
    }
    
  
    $(document).on('click', '.multiselect-close_btn', function() {
        $(this).parents('ul').slideToggle('fast');
    });
    
    
    $(document).on('click', ".dropdown dt a[id^=addons-select_]", function () {
        
        var addOnBtn = $(this).offset().top + $(this).height();
        console.log(addOnBtn);
    
        var tableHeight = $('#program-intervention').offset().top + ($('#program-intervention').height() / 2);
    
        if (addOnBtn > tableHeight) {
    
            $(this).parents('dt').siblings('dd').find('.mutliSelect ul').css({ 'transform': 'translate(-50%, -122%)'});
        }
    });
    
    
    
    var title = '';
    
    $(document).on('click', '.mutliSelect input[type="checkbox"]', function () {
    
        var cboxAddonId = $(this).attr('id');
    
        title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
        title = $(this).val() + '';
        var xAddOn = '<i class="close-addon material-icons" id="' + cboxAddonId + '" title="Remove ' + title + '">clear</i>';
    
        if ($(this).is(':checked')) {
    
            var span = '<span class="selected-addons" title="' + title + '"><label>' + title + '</label>' + xAddOn + '</span>';
    
        } else {
            
            $(this).parents('dd').siblings('dt').find('span[title="' + title + '"]').remove();
    
            if ($(this).parents('dd').siblings('dt').find('.multiSel').text().length < 1) {
                $(this).parents('dd').siblings('dt').find(".hida").show();
            } else {
                $(this).parents('dd').siblings('dt').find(".hida").hide();
            }
    
        }
    });
    
    
    $(document).on('click', '.close-addon', function() {
        var xAddonId = $(this).attr('id');
        var hida = $(this).parents('a').find('.hida');
        var multiSel = $(this).parent('.multiSel').text().length;
    
        $(document).find('.mutliSelect input#'+ xAddonId +'').prop('checked', false);
        $(this).parent('.selected-addons').remove();
    
        if (multiSel < 1 ) {      
            hida.show();
        }
    
        $('#addons-head').click();
        
    });