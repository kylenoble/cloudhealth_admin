/**
 * global variable for ajax request
 */
var requestPage;

/**
 * global variable to load script or js file once
 */
LoadedScripts = [];
LoadedStyle = [];

$(function() {

   
  
  // Bind an event to window.onhashchange that, when the hash changes, gets the
  // hash and adds the class "selected" to any matching nav link.
  $(window).on('hashchange', function(e) {
    callback = (location.hash.replace(/^#/, ''))

    if(callback && !requestPage){
      // create object with required data
      var data = {};
      data.callback = callback;
      data.form_data = {};

      // call the ajax function to show the page content
      page_ajaxify(data);
      
      //check if with notifications
      $.getJSON(SITEROOT + '/notifications/getNotification', function(response){
          $(".acc-menu li a").each(function(){
            $(this).html("");
         })
         console.log(response)
          if(response.data === false){
              location.reload();
          }else {
            if(response.data.with_notification){
                var pages = response.data.with_notification.page;
                console.log(pages)
                var flag = '<img src="assets/images/flag_notification.ico" style="width: 35px;" />';
                pages.forEach((page, index) => {
                  $(".acc-menu li a#" + page).html(flag);
                });
            }
          }
      })

      // activate the menu
      $(".left-menu .acc-menu li").removeClass('active');
      $(".left-menu .acc-menu li[data-callback='" + callback + "']").addClass('active');
    }

  }).trigger('hashchange');

});

jQuery(document).ready(function($v) {

  /*$(document).click(function(e) {
    var container = $(".select2-container");

    // check if the clicked area is dropDown or not
    if(container.has(e.target).length === 0){
      $(".select2-dropdown").hide();
    }

  });*/

  /**
   * this will trigger when left menu clicked
   */
  $v(document).on('click', ".hash_ajaxify", function(e) {
    // stop the event or navigation
    e.preventDefault();
    e.stopPropagation();

    // create object with required data
    var data = {};
    data.callback = $v.trim($v(this).attr('data-callback'));
    data.form_data = {};

    window.location.hash = '';
    window.location.hash = data.callback;

    // call the ajax function to show the page content
    // page_ajaxify(data);

    // make the menu active which is just clicked from left Menus
    menu_active($v(this));

  })

  /**
   * this will trigger when left menu clicked
   */
  $v(document).on('click', ".page_ajaxify", function(e) {
    // stop the event or navigation
    e.preventDefault();
    e.stopPropagation();

    // create object with required data
    var data = {};
    data.callback = $v.trim($v(this).attr('data-callback'));
    data.form_data = {};

    // call the ajax function to show the page content
    page_ajaxify(data);

    // make the menu active which is just clicked from left Menus
    menu_active($v(this));

  })
})

/**
 * function to render middle content of the page
 * 
 * @param obj
 */
function page_ajaxify(obj) {
  // show process is going on via loader
  show_waitMe(jQuery('body'));

  // if already ajax request is started then stop this ajax and start new
  if(requestPage){
    requestPage.abort();
  }

  // call ajax request to get the page content
  requestPage = jQuery.ajax({
    url : SITEROOT+obj.callback,
    method : 'POST',
    data : obj.form_data,
    success : function(result) {
      jQuery("#content").html(result);
      hide_waitMe();
      requestPage = null;
    },
    error : function(error) {
      // hide_waitMe();
      requestPage = null

    }
  });

  return true;
}

/**
 * function to call just ajax and get the result
 * 
 * @param obj
 */
function vip_ajax(obj) {
  // show process is going on via loader
  show_waitMe(jQuery('body'));

  var r = false;
  // call ajax request to get the page content
  jQuery.ajax({
    url : SITEROOT+obj.callback,
    method : 'POST',
    data : obj.form_data,
    async : false,
    success : function(result) {
      r = jQuery.parseJSON(result);
      hide_waitMe();
    },
    error : function(error) {
      hide_waitMe();

    }
  });

  return r;
}
/**
 * function to make menu active which is just clicked
 * 
 * @param this_active
 */
function menu_active(this_active) {
  $(".left-menu .acc-menu li").removeClass('active');
  this_active.addClass('active');

  var menu = this_active.attr('data-callback')
  $(".left-menu .acc-menu li[data-callback='" + menu + "']").addClass('active');

  var attr = this_active.attr('parent-callback');
  if(typeof attr !== typeof undefined && attr !== false){
    var main_menu = this_active.attr('parent-callback')
    $(".left-menu .acc-menu li[data-callback='" + main_menu + "']").addClass('active');
  }
}

/**
 * display loader for body
 * 
 * @param this_active
 */
function show_waitMe(field) {
  /* Provide the container name which should be hide */
  field.waitMe({
    effect : 'win8',
    text : 'Please wait...',
    bg : 'rgba(255,255,255,0.7)',
    color : '#000',
    sizeW : '40px',
    sizeH : '40px'
  });
}

/**
 * hide loader for body
 * 
 * @param this_active
 */
function hide_waitMe() {
  jQuery(".waitMe").hide();
}

function crop_image() {
  $(function() {
    // Create variables (in this scope) to hold the API and image size
    var jcrop_api, boundx, boundy,
    // Grab some information about the preview pane
    $preview = $('#preview-pane'), $pcnt = $('#preview-pane .preview-container'), $pimg = $('#preview-pane .preview-container img'),
    // xsize = $pcnt.width(),
    // ysize = $pcnt.height();
    xsize = 150, ysize = 150;

    $('#target').Jcrop({
      onChange : updatePreview,
      onSelect : updatePreview,
      bgOpacity : 0.5,
      aspectRatio : xsize / ysize
    }, function() {
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];

      jcrop_api = this; // Store the API in the jcrop_api variable

      // Move the preview into the jcrop container for css positioning
      $preview.appendTo(jcrop_api.ui.holder);

    });

    function updatePreview(c) {
      if(parseInt(c.w) > 0){
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
        $(".crop-error-message").html('');
        $pimg.css({
          width : Math.round(rx * boundx) + 'px',
          height : Math.round(ry * boundy) + 'px',
          marginLeft : '-' + Math.round(rx * c.x) + 'px',
          marginTop : '-' + Math.round(ry * c.y) + 'px'
        });
      }
    }

  });
}

function crop_logo() {
  $(function() {
    // Create variables (in this scope) to hold the API and image size
    var jcrop_api, boundx, boundy,
    // Grab some information about the preview pane
    $preview = $('#preview-pane-logo'), $pcnt = $('#preview-pane-logo .preview-container-logo'), $pimg = $('#preview-pane-logo .preview-container-logo img'), xsize = $pcnt
        .width(), ysize = $pcnt.height();
    // xsize = 270, ysize = 80;

    $('#target').Jcrop({
      onChange : updatePreview,
      onSelect : updatePreview,
      bgOpacity : 0.5,
      aspectRatio : xsize / ysize
    }, function() {
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];

      jcrop_api = this; // Store the API in the jcrop_api variable

      // Move the preview into the jcrop container for css positioning
      $preview.appendTo(jcrop_api.ui.holder);

    });

    function updatePreview(c) {
      if(parseInt(c.w) > 0){
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
        $(".crop-error-message").html('');
        $pimg.css({
          width : Math.round(rx * boundx) + 'px',
          height : Math.round(ry * boundy) + 'px',
          marginLeft : '-' + Math.round(rx * c.x) + 'px',
          marginTop : '-' + Math.round(ry * c.y) + 'px'
        });
      }
    }

  });

}

function getDate(date){
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return yyyy+ mm + dd;
}