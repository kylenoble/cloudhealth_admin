jQuery(document).ready(function($v) {

  if($v(".dashboard-header").length > 0){
    location.reload();
  }
  /*****************************************************************************
   * login form submit and validation
   * ***************************************************************************
   */
  var validator = $v("#vip-user-login-form").validate({
    errorElement : 'span',
    rules : {
      username : {
        required : true,
        email : true
      },
      pass : {
        required : true
      }
    },
    messages : {
      username : {
        required : "Please enter email.",
        email : "Please enter valid email address."

      },
      pass : {
        required : "Please enter password.",
      }

    },
    submitHandler : function(form) {
      $('#edit-submit').attr('disabled', 'disabled');
      form.submit();
    }
  });

  /*****************************************************************************
   * forgot password form submit and validation
   * ***************************************************************************
   */
  var validator = $v("#vip-user-fpass-form").validate({
    errorElement : 'span',
    rules : {
      email : {
        required : true,
        email : true
      }
    },
    messages : {
      email : {
        required : "Please enter email.",
        email : "Please enter valid email address."

      }
    },
  });

  /*****************************************************************************
   * forgot password form submit and validation
   * ***************************************************************************
   */
  var validator = $v("#vip-user-reset-form").validate({
    errorElement : 'span',
    rules : {
      password : {
        required : true,
      },
      cpassword : {
        required : true,
        equalTo : "#password"
      }
    },
    messages : {
      password : {
        required : "Please enter password.",
      },
      cpassword : {
        required : "Please enter confirm password.",
        equalTo : "Passwords do not match."
      }

    },
  });

})
