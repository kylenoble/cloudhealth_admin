<style>
    .ui-state-highlight {
        height:25px !important;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }
</style>
<script>
    $(function () {
        $(".select-2").select2({
            placeholder: "Select ",
        });
    });
</script>


<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="title">Update Admin User Details</div>
            <div class="container-fluid">
                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row user-update">
                        <div class="col-sm-12">
                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">
                                <?php echo _error_message('danger'); ?>

                                <?php echo form_open(base_url() . 'users/update', array('id' => 'vip-user-update')); ?>

                                <input type='hidden' name='id' id='id' value="<?php echo $result['id'] ?>">
                                <input type='hidden' name='company_id' id='company_id' value="<?php echo $result['company_id'] ?>">
                                <div class="form-group" style="padding: 20px 0;">
                                    <?php echo form_label('Company Name', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo $company['company_name']; ?>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' => trim($result['name']))); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <?php echo form_label('Email<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => trim($result['email']))); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Role', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php
                                        echo form_dropdown('role', $roles, $result['role'], 'class="form-control select-2 select2-hidden-accessible"');
                                        ?>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <a href='javascript:void(0)' class="btn-primary btn edit-user-save">Save</a>
                                                <a data-callback="/organizations/view/<?php echo $result['company_id'] ?>" parent-callback="/organizations/view/<?php echo $result['company_id'] ?>" class="btn-default btn page_ajaxify">Cancel</a>
                                            </div>
                                        </div>  <!-- form group -->
                                    </div><!-- row -->
                                </div><!-- panel footer -->
                            </div><!-- panel -->
                        </div><!-- col 12 -->
                    </div><!-- add user -->
                </div><!-- ui s -->
            </div><!-- container -->
        </div>
        <!-- page-content -->
    </div>
    <!-- static-content -->
</div>
<!-- static-content-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
    $(".edit-user-save").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        data.callback = 'users/user_update';
        data.form_data = null;
        data.form_data = jQuery("#vip-user-update").serializeArray();
     
        var response = vip_ajax(data);
        if (response.result.success != 1) {
            $(".alert-danger").removeClass('hidden');
            $(".alert-danger").html(response.result.errors);
        }else{
            var data = {}
            data.callback = 'organizations/view/' + $("#company_id").val();
            page_ajaxify(data);
        }
    })
})
</script>