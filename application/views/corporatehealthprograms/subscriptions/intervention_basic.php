<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/intervention-style.css" type="text/css" rel="stylesheet">
<?php if(!$table_only) :?>
<style>
    /* adjusts second modal's opacity'*/
    .modal-overlay ~ .modal-overlay{
        opacity: 0 !important;
    }
    div.dataTables_wrapper th, div.dataTables_wrapper td { 
        white-space: normal !important; 
    }
</style>
<?php endif;?>
<?php _load_js($js);?>
<div class="static-content-inner">
    <div class="static-content">
        <div class="page-content">
            <?php if(!$table_only) :?>
            <h4 class="dashboard-title" style="display: inline-block;"><?php echo $company_name; ?></h4>
            <div class="row right" style="margin-top: 10px; margin-right: 10px; margin-bottom: 5px;">
                <div class="col-sm-8">
                    <a data-callback="<?php echo ($from_hr ? '/Healthprofile/index/employee_health_activities' : '/CorporateHealthPrograms/subscriptions/'.$company_id)?>" class="btn-default btn page_ajaxify back">Back</a>
                </div>
            </div>
            <?php endif;?>
            <div class="container-fluid">
                <?php if(!$table_only) :?>
                <div class="title small-tabs active no-hover"><?php echo $program_name; ?></div>
                <div class="row" style="width: 50%; margin-top: 10px; margin-left: 10px; margin-bottom: 5px; text-align: left;">
                    <p style="text-align: left;">Health Talk: <br />
                        <ol>
                        <?php 
                        $disabled = 'disabled';
                        if(is_array($inclusions_healthtalk)) : 
                            foreach($inclusions_healthtalk as $id=>$health_talk):
                                echo '<li>', $health_talk['name'], '</li>';
                                if($with_event){
                                    echo '<a class="btn-default btn btn-small" data-x="'.$id.'" id="show-event-details_'.$id.'">View Event Details</a>';
                                    $disabled = '';
                                }else{
                                    echo '<label style="font-style: italic">No event created yet</label><br />';
                                    echo '<a class="btn-default btn btn-small goto-community">Go to <span class="small-note">My Community</span> to create an event</a>';
                                }
                            endforeach;?>
                        <?php else:?>
                        <?php echo '<li>', $inclusions_healthtalk, '</li>';?>
                        <?php endif;?>
                        </ol>
                    </p>
                </div>
                <div class="clearfix"></div>
                <h6>INTERVENTION</h6>
                <div class="alert alert-dismissable hidden" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);"></div>
                <?php endif;?>
                <table class="striped centered" id="basic-intervention">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Employee Name</th>
                            <?php foreach ($inclusions as $key => $row): ?>
                            <th><?php echo $row; ?></th>
                            <?php endforeach; ?>
                            <?php if ($with_add_ons) : ?>
                                <th id="addons-head">Add-Ons</th>
                            <?php endif; ?>
                            <?php if (!$from_hr): ?>
                                <th style="width: 150px">&nbsp;</th>
                            <?php endif; ?>
                        </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($enrolled_employees as $rec):
                        $intervention = null;
                        if ($rec['details'] != null) {
                            $intervention = json_decode($rec['details'], true);
                        }
                        ?>
                        <tr id="tr_<?php echo $rec['id']; ?>">
                            <td><?php echo $rec['employee_id'], ($rec['is_anonymous'] == 't' ? ' *' : '');?></td>
                            <td><?php echo $rec['name']; ?></td>
                            <?php 
                            foreach ($inclusions as $row):
                                $addon_notes = '';
                                $status = '';
                                $name = '';

                                if ($intervention) { 
                                    $key = str_replace(" ", "_", $row);
                                    $key_pos = array_search($key, array_column($intervention, 'name'));
                                    $name = $intervention[$key_pos]['name'];
                                    $status = $intervention[$key_pos]['status'];
                                }
                                $disabled = '';?>
                            <td>
                                <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                echo form_dropdown($row . '[status]', $statusList, $status, "class='form-control status-dropdown' {$disabled}"); ?>
                            </td>
                            <?php endforeach; ?>
                            <?php if ($with_add_ons): 
                                $addon_notes = '';
                                $status = '';
                                $name = '';
                                $addon_list = [];
                                $with_add_ons_data = false;

                                if ($intervention) {
                                    $key_pos = array_search('add_ons', array_column($intervention, 'name'));
                                    $status = $intervention[$key_pos]['status'];
                                    $addon_notes = (isset($intervention[$key_pos]['notes']) ? $intervention[$key_pos]['notes'] : '');
                                    $addon_list = (isset($intervention[$key_pos]['list']) ? $intervention[$key_pos]['list'] : []);
                                    $with_add_ons_data = (!empty($addon_list) || $addon_notes != '');
                                    $disabled = '';
                                }
                                ?>
                            <td>
                                <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                echo form_dropdown('add_ons[status]', $statusList, $status, "class='form-control status-dropdown' {$disabled}"); ?>
                                <?php if(!$from_hr || ($from_hr && $with_add_ons_data)) :?>
                                <dl class="dropdown <?php echo ($disabled != '' ? 'hidden' : '')?>"> 
                                    <dt>
                                        <a href="Javascript:void(0)" id="addons-select_<?php echo $rec['id']; ?>">
                                            <?php if ($intervention && ($addon_list != null || $addon_notes != '')) :?>
                                            <span class="hida btn-small btn-default addons-selected">Show Add-Ons</span>
                                            <?php else:?>
                                            <span class="hida btn-small btn-default">Select Add-Ons</span>
                                            <?php endif; ?>
                                            <p class="multiSel" id="multiSel_<?php echo $rec['id']; ?>"></p>  
                                        </a>
                                    </dt>
                                    <dd id="dd_<?php echo $rec['id']; ?>">
                                        <div class="mutliSelect">
                                            <ul>
                                                <li><button class="multiselect-close_btn"><i class="material-icons">clear</i></button></li>
<?php foreach ($with_add_ons as $addon) : ?>
                                                    <li>
                                                        <input type="checkbox" name="add_ons[t][]" id="<?php echo $addon, '_', $rec['id']; ?>" value="<?php echo $addon; ?>" <?php echo ($intervention && isset($intervention[$key_pos]['list']) && in_array($addon, $intervention[$key_pos]['list']) ? 'checked' : '') ?> /> <label for="<?php echo $addon, '_', $rec['id']; ?>"><?php echo $addon; ?></label>
                                                    </li>
<?php endforeach; ?>
                                                <li>
                                                    <label>Notes:</label><br />
                                                    <textarea name="add_ons[notes]"><?php echo $addon_notes ?></textarea>
                                                </li>
                                            </ul>
                                        </div>
                                    </dd>
                                </dl>
                                <?php endif;?>
                            </td>
                                <?php endif; ?>
                            <?php if (!$from_hr && !$table_only): ?>
                            <td style="width: 150px">
                                <span class="hidden"><?php echo $rec['upi_id']?></span>
                                <a class="btn-default btn btn-small <?php echo $disabled;?>" style="padding: 0 10px;" id="update-status_<?php echo $rec['id']; ?>" >Update Status</a>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php if(!$table_only) :?>
<div id="eventform-modal" class="modal modal-fixed-footer"></div>
<div id="eventform-confirm" class="modal modal-fixed-footer">
    <p class="modal-header" style="padding-left: 30px;">The following employees will be notified about the event with below details: </p>
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn create-confirm-btn">Confirm</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    </div>
</div>
<?php endif;?>
<script type="text/javascript">
$(document).ready(function () {
    <?php if($from_hr || $table_only) :?>
    $("#basic-intervention").DataTable();
    $("table tbody tr td").find("input, select, textarea").attr("disabled", true)
    <?php else:?>
    var index = $("#basic-intervention").find('th:last').index();
    $("#basic-intervention").DataTable({
        "order": [[index, "asc"]]
    });
        
    $("a[id^=create-event_]").click(function(){
        var tmp_id = ($(this).attr('id')).split("_");
        var data = {}
        data.ceid = tmp_id[1]
        data.htalk = $(this).data("htalk")
        $.post(SITEROOT + '/CorporateHealthPrograms/event_new/', data, function(html){
            $("#eventform-modal .modal-content").html(html)
            $("#eventform-modal").modal("open")
        })
    })
    
    $('a[id^=update-status_]').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var tmp_id = ($(this).attr('id')).split('_');
        var inputs = $("table#basic-intervention tbody").find("tr#tr_"+tmp_id[1]).find("input, select, textarea").filter(function(){
            return $(this).val();
        });

        if(inputs.length > 0){
            console.log(inputs)
            var data = {};
            data.callback = 'CorporateHealthPrograms/setBasicStatus';
            data.form_data = {};
            data.form_data.data = jQuery(inputs).serialize();
            data.form_data.program_code = '<?php echo $program_code; ?>';
            data.form_data.xx = tmp_id[1];

            var response = vip_ajax(data);
            console.log(response)
            if (response.result.success == 1) {
                var data1 = {};
                data1.callback = '/CorporateHealthPrograms/subscriptions_intervention/<?php echo $company_enrollment_id?>';
                data1.form_data = null;
                page_ajaxify(data1);
            } else {
                $(".alert").addClass("alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        }
    });
    <?php endif; ?>
    $('div#basic-intervention_wrapper .dataTables_scrollBody').css('min-height', '500px');
    <?php if(!$table_only) :?>
    $('#eventform-confirm, #eventform-modal').modal({
        dismissible: false
    });

    $(".goto-community").on('click', function(){
        window.location.hash = 'Community';
        $(".acc-menu li[data-callback='Community']").trigger('click');
        var data = {};
        data.form_data = null;
        <?php if(admin_role() == Users_model::ADMIN) :?>
//        data.form_data = {page: 'bulletin'};
        <?php endif;?>
        data.callback = '/community/event_new/1/<?php echo $company_id?>';
        page_ajaxify(data);
        $("#add-healthtalk-btn").trigger('click')
    })
    
    $("a[id^=show-event-details_]").click(function(){
        var event_id = $(this).attr('data-x')
        $.get(SITEROOT + '/community/getEventInfo/' + event_id, function(html){
            $("#eventform-modal").html(html)
            $("#eventform-modal").modal("open")
            
            <?php if($from_hr):?>
            $(".modal-overlay ~ .modal-overlay").remove()
            $("body").removeAttr('style');
            <?php endif;?>
        })
    })
    
    $(document).on("click", ".back", function(e){
        e.preventDefault()
        var data = {};
        data.callback = $(this).attr("data-callback");
        page_ajaxify(data);
    })
    <?php endif;?>

})
</script>
