<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/intervention-style.css" type="text/css" rel="stylesheet">

<div class="static-content-inner">
    <div class="static-content">
        <div class="page-content">
            <h4 class="dashboard-title" style="display: inline-block;"><?php echo $company_name; ?></h4>
            <div class="row right" style="margin-top: 10px; margin-right: 10px; margin-bottom: 5px;">
                <div class="col-sm-8">
                    <a data-callback="<?php echo ($from_hr ? '/Healthprofile/index/employee_health_activities' : '/CorporateHealthPrograms/subscriptions/'.$company_id)?>" class="btn-default btn page_ajaxify">Back</a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="title small-tabs active no-hover"><?php echo $program_name;?></div>
                <h6>INTERVENTION</h6>
                <div class="alert alert-dismissable hidden" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);"></div>
                <table class="striped centered" id="program-intervention">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Employee Name</th>
                            <?php foreach($inclusions as $key=>$row): 
                                if($key != 'add_ons') : ?>
                            <th><?php echo $row;?></th>
                            <?php endif; ?>
                            <?php endforeach;?>
                            <?php if($with_add_ons) :?>
                            <th id="addons-head" style="min-width: 300px;">Add-Ons</th>
                            <?php endif; ?>
                            <?php if(!$from_hr):?>
                            <th>&nbsp;</th>
                            <?php endif;?>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach($enrolled_employees as $rec): 
                        $intervention = null;
                        if($rec['details'] != null){
                            $intervention = json_decode($rec['details'], true);
                        }
                        
                        ?>
                        <tr id="tr_<?php echo $rec['id'];?>">
                            <td><?php echo $rec['employee_id'], ($rec['is_anonymous'] == 't' ? ' *' : '');?></td>
                            <td><?php echo $rec['name']; ?></td>
                            <?php foreach($inclusions as $key => $row): 
                                $addon_notes = '';
                                $date = '';
                                $epoch_date = 0;
                                $time = '';
                                $status = '';
                                $with_add_ons_data = false;
                                
                                if($intervention){
                                    $pos = array_search($key, array_column($intervention, 'name'));
                                    if($intervention[$pos]['name'] == $key){
                                        $status = $intervention[$pos]['status'];
                                        $date = date('d M, Y', strtotime($intervention[$pos]['appointment_date']));
                                        $epoch_date = strtotime($intervention[$pos]['appointment_date']);
                                        $time = $intervention[$pos]['appointment_time'];
                                        $addon_notes = (isset($intervention[$pos]['notes']) ? $intervention[$pos]['notes'] : '');
                                        $with_add_ons_data = (isset($intervention[$pos]['list']) || isset($intervention[$pos]['notes']));
                                    }
                                }
                            if($key != 'add_ons') :  ?>
                            <td data-order="<?php echo $epoch_date; ?>">
                                <?php if($from_hr && $date == '') : ?>
                                <label class="pending">Pending Schedule</label>
                                <?php else: ?>
                                <input type="text" class="datepicker" id="date_<?php echo $key, '_', $rec['id']; ?>" name="<?php echo $key;?>[date]" placeholder="Set Date" value="<?php echo $date; ?>" />
                                <br /><input id="<?php echo $key, '_', $rec['id'];?>" type="text" class="time ui-timepicker-input" autocomplete="off" name="<?php echo $key;?>[time]" placeholder="Time" value="<?php echo $time; ?>" /><br />
                                <?php endif; ?>
                                <?php if($intervention != null && $date != null) :?>
                                    <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                    echo form_dropdown($key.'[status]', $statusList, $status, 'class="form-control status-dropdown"'); ?>
                                <?php endif; ?>
                            </td>
                            <?php else: ?>
                                <?php if($with_add_ons): ?>
                            <td style="min-width: 300px;">
                                <?php if($from_hr && $date == '') :?>
                                <label class="pending">Pending Schedule</label>
                                <?php else: ?>
                                <input type="text" class="datepicker" id="date_addons_<?php echo $rec['id']; ?>" name="add_ons[date]" placeholder="Set Date" data-date="<?php echo $rec['id'];?>" value="<?php echo $date; ?>" />
                                <input id="time_addons_<?php echo $rec['id'];?>" type="text" name="add_ons[time]" class="time ui-timepicker-input" placeholder="Time" data-time="<?php echo $rec['id'];?>" autocomplete="off" value="<?php echo $time; ?>" /><br />
                                <?php endif; ?>
                                <?php if($intervention != null && $date != null) :?>
                                    <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                    echo form_dropdown($key.'[status]', $statusList, $status, 'class="form-control"'); ?>
                                <?php endif; ?>
                                <?php if(!$from_hr || ($from_hr && $with_add_ons_data)) :?>
                                <dl class="dropdown"> 
                                    <dt>
                                        <a href="Javascript:void(0)" id="addons-select_<?php echo $rec['id']; ?>">
                                          <span class="hida btn btn-default"><?php echo ($intervention && (isset($intervention[$pos]['list']) || isset($intervention[$pos]['notes'])) ? 'Show Add-Ons' : 'Select Add-Ons');?></span>    
                                          <p class="multiSel" id="multiSel_<?php echo $rec['id']; ?>"></p>  
                                        </a>
                                    </dt>
                                    <dd id="dd_<?php echo $rec['id']; ?>">
                                        <div class="mutliSelect">
                                            <ul>
                                                <li><button class="multiselect-close_btn"><i class="material-icons">clear</i></button></li>
                                                <?php foreach($with_add_ons as $index=>$addon) :?>
                                                <li>
                                                    <input type="checkbox" name="add_ons[t][]" id="<?php echo $index, '_', $rec['id'];?>" value="<?php echo $addon;?>" <?php echo ($intervention && isset($intervention[$pos]['list']) && in_array($addon, $intervention[$pos]['list']) ? 'checked' : '')?> /> <label for="<?php echo $index, '_', $rec['id'];?>"><?php echo $addon;?></label>
                                                </li>
                                                <?php endforeach;?>
                                                <li>
                                                    <label>Notes:</label><br />
                                                    <textarea name="add_ons[notes]"><?php echo $addon_notes?></textarea>
                                                </li>
                                            </ul>
                                        </div>
                                    </dd>
                                </dl>
                                <?php endif; ?>
                            </td>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php endforeach;?>
                            <?php if(!$from_hr):?>
                            <td>
                                <a class="btn-default btn" style="padding: 0 10px;" id="update-intervention_<?php echo $rec['id'];?>" >Set Schedule</a>
                            </td>
                            <?php endif;?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dropdown_multiple_checkbox.js"></script>
<script>
$(document).ready(function(){
    $("#program-intervention").DataTable().destroy();
    $('.dataTables_scrollBody').css('min-height', '500px');
    
    $('a[id^=update-intervention_]').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var tmp_id = ($(this).attr('id')).split('_');
        var inputs = $("table#program-intervention tbody").find("tr#tr_"+tmp_id[1]).find("input, select, textarea").filter(function(){
            return $(this).val();
        });

        if(inputs.length > 0){
            var data = {};
            data.callback = 'CorporateHealthPrograms/setProgramSchedule';
            data.form_data = {};
            data.form_data.data = jQuery(inputs).serialize();
            data.form_data.program_code = '<?php echo $program_code; ?>';
            data.form_data.xx = tmp_id[1];

            var response = vip_ajax(data);
            if (response.result.success == 1) {
                var data1 = {};
                data1.callback = '/CorporateHealthPrograms/subscriptions_intervention/<?php echo $company_enrollment_id?>';
                data1.form_data = null;
                page_ajaxify(data1);
            } else {
                $(".alert").addClass("alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        }
    });
    
    <?php if(!$from_hr) :?>
    $(document).on('click', '.datepicker', function(){
        var obj = $(this)
        $(this).pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            format: 'dd mmm, yyyy',
            closeOnSelect: false, // Close upon selecting a date,
            onClose: function() {
                $('.datepicker').blur();
                $(obj).parent().children().eq(3).focus();
            }
        });
    });
    
    $('.time').timepicker({ 
        scrollDefault: 'now',
        timeFormat: 'g:i A',
        maxTime: '10:00pm',
        minTime: '6:00am'
    });
    
    $("#program-intervention").DataTable({
        scrollY: 400,
        scrollX: true,
        scrollCollapse: true,
        "order": [[0, "asc"]],
        fixedColumns: {
            leftColumns: 0,
            rightColumns: 1
        }
    });
    <?php else:?>
    $("#program-intervention").DataTable({
        scrollY: 400,
        scrollX: true,
        scrollCollapse: true,
        "order": [[0, "asc"]]
    });
    $("table tbody tr td").find("input, select, textarea").attr("disabled", true)
    <?php endif;?>

});
</script>