<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/intervention-style.css" type="text/css" rel="stylesheet">
<table class="striped centered" id="basic-intervention">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Employee Name</th>
            <?php foreach ($inclusions as $key => $row):
                if ($key != 'add_ons') :
                    ?>
                    <th><?php echo $row; ?></th>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($with_add_ons) : ?>
                <th id="addons-head">Add-Ons</th>
            <?php endif; ?>
            <?php if (!$from_hr): ?>
                <th>&nbsp;</th>
<?php endif; ?>
        </tr>
    </thead>

    <tbody>
        <?php $disabled = 'disabled';
        foreach ($enrolled_employees as $rec):
            $intervention = null;
            if ($rec['details'] != null) {
                $intervention = json_decode($rec['details'], true);
            }
            ?>
            <tr id="tr_<?php echo $rec['upi_id']; ?>">
                <td><?php echo $rec['employee_id'], ($rec['is_anonymous'] == 't' ? ' *' : ''); ?></td>
                <td><?php echo $rec['name']; ?></td>
                <?php
                foreach ($inclusions as $key=>$row):
                    $addon_notes = '';
                    $status = '';
                    $disabled = 'disabled';
                    $addon_list = array();
                    $with_add_ons_data = false;

                    if ($intervention && $event_id != 0) {
                        $pos = array_search($event_id, array_column($intervention, 'event_id'));
                        if(is_int($pos) && isset($intervention[$pos]['inclusions'])){
                            $row_inclusions = $intervention[$pos]['inclusions'];
                            $key_pos = array_search($key, array_column($row_inclusions, 'name'));

                            if(is_int($key_pos) && $row_inclusions[$key_pos]['name'] == $key){
                                $status = $row_inclusions[$key_pos]['status'];
                                $addon_notes = (isset($row_inclusions[$key_pos]['notes']) ? $row_inclusions[$key_pos]['notes'] : '');
                                $addon_list = (isset($row_inclusions[$key_pos]['list']) && is_array($row_inclusions[$key_pos]['list']) ? $row_inclusions[$key_pos]['list'] : array());
                                $with_add_ons_data = (!empty($addon_list) || $addon_notes != '');
                            }
                        }
                        $disabled = '';
                    }
                    if ($key != 'add_ons') :
                        ?>
                        <td>
                        <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                        echo form_dropdown($key . '[status]', $statusList, $status, "class='form-control status-dropdown' {$disabled}");
                        ?>
                        </td>
                        <?php else: ?>
                        <?php if ($with_add_ons && $key == 'add_ons'): ?>
                            <td>
                            <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                            echo form_dropdown($key . '[status]', $statusList, $status, "class='form-control status-dropdown' {$disabled}"); ?>
                                <?php if(!$from_hr || ($from_hr && $with_add_ons_data)) :?>
                                <dl class="dropdown <?php echo ($disabled != '' ? 'hidden' : '')?>"> 
                                    <dt>
                                        <a href="Javascript:void(0)" id="addons-select_<?php echo $rec['id']; ?>">
                                            <?php if ($intervention && ($addon_list != null || $addon_notes != '')) :?>
                                            <span class="hida btn btn-default addons-selected">Show Add-Ons</span>
                                            <?php else:?>
                                            <span class="hida btn btn-default">Select Add-Ons</span>
                                            <?php endif; ?>
                                            <p class="multiSel" id="multiSel_<?php echo $rec['id']; ?>"></p>  
                                        </a>
                                    </dt>
                                    <dd id="dd_<?php echo $rec['id']; ?>">
                                        <div class="mutliSelect">
                                            <ul>
                                                <li><button class="multiselect-close_btn"><i class="material-icons">clear</i></button></li>
<?php foreach ($with_add_ons as $index => $addon) : ?>
                                                    <li>
                                                        <input type="checkbox" name="add_ons[t][]" id="<?php echo $index, '_', $rec['id']; ?>" value="<?php echo $addon; ?>" <?php echo ($intervention && in_array($addon, $addon_list) ? 'checked' : '') ?> /> <label for="<?php echo $index, '_', $rec['id']; ?>"><?php echo $addon; ?></label>
                                                    </li>
<?php endforeach; ?>
                                                <li>
                                                    <label>Notes:</label><br />
                                                    <textarea name="add_ons[notes]"><?php echo $addon_notes ?></textarea>
                                                </li>
                                            </ul>
                                        </div>
                                    </dd>
                                </dl>
                                <?php endif;?>
                            </td>
            <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php if (!$from_hr): ?>
                    <td>
                        <span class="hidden"><?php echo $rec['upi_id']?></span>
                        <a class="btn-default btn <?php echo $disabled; ?>" style="padding: 0 10px;" id="update-status_<?php echo $rec['upi_id']; ?>" >Update Status</a>
                    </td>
    <?php endif; ?>
            </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php _load_js($js); ?>
<script>
$(document).ready(function(){
    <?php if($from_hr) :?>
    $("#basic-intervention").DataTable();
    $("table tbody tr td").find("input, select, textarea").attr("disabled", true)
    <?php else:?>
    var index = $("#basic-intervention").find('th:last').index();
    $("#basic-intervention").DataTable({
        "order": [[index, "asc"]]
    });
    
    $('a[id^=update-status_]').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var tmp_id = ($(this).attr('id')).split('_');
        var inputs = $("table#basic-intervention tbody").find("tr#tr_"+tmp_id[1]).find("input, select, textarea").filter(function(){
            return $(this).val();
        });

        if(inputs.length > 0){
            console.log(inputs)
            var data = {};
            data.callback = 'CorporateHealthPrograms/updateBasicStatus_multiple';
            data.form_data = {};
            data.form_data.data = jQuery(inputs).serialize();
            data.form_data.program_code = '<?php echo $program_code; ?>';
            data.form_data.xx = tmp_id[1];
            data.form_data.event_id = $("#event_id").val()

            var response = vip_ajax(data);
            console.log(response)
            if (response.result.success == 1) {
                var data1 = {};
                data1.callback = '/CorporateHealthPrograms/subscriptions_intervention/<?php echo $company_enrollment_id?>';
                data1.form_data = {}
                data1.form_data.event_type = '<?php echo $event_type?>';
                page_ajaxify(data1);
            } else {
                $(".alert").addClass("alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        }
    });
    <?php endif; ?>
})
</script>