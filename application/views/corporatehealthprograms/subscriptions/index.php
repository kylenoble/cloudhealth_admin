<style>
    a{
        cursor: pointer;
    }
</style>
<div class="static-content-inner">
    <div class="static-content">
        <div class="page-content">
            <h4 class="dashboard-title" style="display: inline-block;"><?php echo $company_name; ?></h4>
            <div class="row right" style="margin-top: 10px; margin-right: 10px; margin-bottom: 5px;">
                <div class="col-sm-8">                
                    <a data-callback="/CorporateHealthPrograms/index" class="btn-default btn page_ajaxify back" parent-callback="CorporateHealthPrograms/index">Back</a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="title small-tabs active no-hover">SUBSCRIPTIONS</div>
                <?php if ($this->session->flashdata('subscription_add_msg') != '') {
                    ?>
                    <div class="alert alert-info alert-dismissable" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                        <?php echo $this->session->flashdata('subscription_add_msg'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php if ($this->session->flashdata('email_sending_error') != '') {
                    ?>
                    <div class="alert alert-warning alert-dismissable" style="margin: 0;visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                        <?php echo $this->session->flashdata('email_sending_error'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="input-group-btn right">
                    <a class="btn btn-info add_subscription active page_ajaxify" id="add-program-btn" data-callback="corporateHealthPrograms/subscriptions_new/<?php echo $company_id;?>" parent-callback="corporateHealthPrograms/subscriptions_new/<?php echo $company_id;?>" href="javascript:void(0)">Add Subscription</a>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <table id='clientlist_table' class="stripe row-border order-column" width="100%" cellspacing="0">
                            <thead>
                                <tr>   
                                    <th>Program Name</th>
                                    <th># of Employees</th>
                                    <th>Subscription Start</th>
                                    <th>Subscription End</th>
                                    <th>Status</th>
                                    <th>CHI MD reports</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indexing_is_ongoing = false;
                                foreach ($subscriptions as $program) {
                                    if(in_array($program['program_code'], array('CHIT', 'CHIR')) && !in_array($program['status'], array('Completed', 'Cancelled'))){
                                        $indexing_is_ongoing = true;
                                    }
                                    $disabled = '';
                                    if(date('Y-m-d', strtotime($program['subscription_end'])) < date('Y-m-d', time()) && ($program['status'] != 'Completed' && $program['status'] != 'Cancelled')){
                                        $disabled = 'color: red';
                                    }
                                    ?>
                                    <tr>                                               
                                        <td>
                                            <a alt="Update Details" title="Update Details" class="update-details" data-callback="/CorporateHealthPrograms/subscriptions_edit/<?php echo $program['company_enrollment_id']?>"><?php echo $program['subscription_name']; ?></a>
                                        </td>
                                        <td style='text-align: center'><?php echo $program['employees_count']; ?></td>
                                        <td style='text-align: center'><?php echo date('M j, Y', strtotime($program['subscription_start'])); ?></td>
                                        <td style='text-align: center; <?php echo $disabled ?>'><?php echo $program['subscription_end'] ? date('M j, Y', strtotime($program['subscription_end'])) : '-'; ?></td> 
                                        <td style='text-align: center'>
                                            <?php if($program['status'] == 'Ongoing' && $program['is_basic'] == true): ?>
                                            <a alt="See Details" title="See Details" class="page_ajaxify update-status" data-callback="/corporateHealthPrograms/subscriptions_intervention/<?php echo $program['company_enrollment_id'] ?>" parent-callback="corporateHealthPrograms/index">Update Status</a>
                                            <?php else: 
                                                echo $program['status']; 
                                            endif;?>
                                        </td>
                                        <td>
                                            <?php 
                                            $reports = [];
                                            $report_files = $this->reports->getReportByCompanyId($company_id, Reports_model::$REPORT_SENT, $program['company_enrollment_id']);
                                            foreach($report_files as $report):
                                                $tmp_file = explode("_", $report['file_name']);
                                                $reports[] = '<a href="'.CHA_URL.'/static/reports/CHI/'.$report['file_name'].'" target="_blank">'.$tmp_file[1].'</a>';
                                            endforeach;
                                            echo implode(", ", $reports);
                                            ?>
                                        </td>
                                    </tr>

                                <?php }
                                ?>
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    $(document).ready(function () {
        $('#clientlist_table').DataTable({
            scrollY: 400,
            scrollX: true,
            scrollCollapse: true,
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'pdf',
                    text: 'Export to PDF',
                    title: 'List of Enrolled Companies (as of ' + getDate(new Date()) + ")",
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                }
            ]
        });
        
        if('<?php echo $indexing_is_ongoing?>' == '1'){
            $("#add-program-btn").attr("disabled", true);
        }
        
        $("#clientlist_table").on("click", ".update-details", function(e){
            e.preventDefault()
            var data = {};
            data.callback = $(this).attr("data-callback");
            page_ajaxify(data);
        })
        
        $(document).on("click", ".add_subscription, .back", function(e){
            e.preventDefault()
            var data = {};
            data.callback = $(this).attr("data-callback");
            page_ajaxify(data);
        })
        
        $("#clientlist_table").on("click", ".update-status", function(e){
            e.preventDefault()
            var data = {};
            data.callback = $(this).attr("data-callback");
            page_ajaxify(data);
        })
    })
</script>