<style>
    /* adjusts second modal's opacity'*/
    .modal-overlay ~ .modal-overlay{
        opacity: 0 !important;
    }
</style>
<div class="static-content-inner">
    <div class="static-content">
        <div class="page-content">
            <h4 class="dashboard-title" style="display: inline-block;"><?php echo $company_name; ?></h4>
            <div class="row right" style="margin-top: 10px; margin-right: 10px; margin-bottom: 5px;">
                <div class="col-sm-8">
                    <a data-callback="<?php echo ($from_hr ? '/Healthprofile/index/employee_health_activities' : '/CorporateHealthPrograms/subscriptions/'.$company_id)?>" class="btn-default btn page_ajaxify">Back</a>
                </div>
            </div>
           <div class="container-fluid">
                <div class="title small-tabs active no-hover"><?php echo $program_name; ?></div>
                <div class="row right" style="margin-right: 10px; text-align: right;">
                    <select style="width: auto;" <?php echo (empty($enrolled_employees) ? 'disabled' : '')?> name="health_talk" id="health_talk">
                        <option value="">Select a Health Talk</option>
                    <?php foreach($inclusions_healthtalk as $type=>$health_talk):?>
                        <option value="<?php echo $type;?>"><?php echo $health_talk;?></option>
                    <?php endforeach;?>
                    </select>
                    <a class="btn-default btn hidden" <?php echo (empty($enrolled_employees) ? 'disabled' : '')?> id="create-event" data-ceid="<?php echo $company_enrollment_id;?>" >Create Event</a>
                    <a class="btn-default btn hidden" <?php echo (empty($enrolled_employees) ? 'disabled' : '')?> id="show-event-details" data-ceid="<?php echo $company_enrollment_id;?>">Show Event Details</a>
                    <a class="btn-default btn hidden" id="send-event-details" data-ceid="<?php echo $company_enrollment_id;?>">Send Event Details</a>
                </div>
                <div class="clearfix"></div>
                <h6>INTERVENTION</h6>
                <div class="alert alert-dismissable hidden" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);"></div>
                <div id="content-table">
                    <link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
                    <link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
                    <link href="<?php echo base_url(); ?>assets/css/intervention-style.css" type="text/css" rel="stylesheet">
                    <?php _load_js($js); ?>
                    <table class="striped centered" id="basic-intervention">
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>Employee Name</th>
                                <?php foreach ($inclusions as $key => $row):
                                    if ($key != 'add_ons') : ?>
                                        <th><?php echo $row; ?></th>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if ($with_add_ons) : ?>
                                    <th id="addons-head">Add-Ons</th>
                                <?php endif; ?>
                                <?php if (!$from_hr): ?>
                                    <th>&nbsp;</th>
                                <?php endif; ?>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $disabled = 'disabled';
                            foreach ($enrolled_employees as $rec): ?>
                                <tr>
                                    <td><?php echo $rec['employee_id'], ($rec['is_anonymous'] == 't' ? ' *' : '');?></td>
                                    <td><?php echo $rec['name']; ?></td>
                                    <?php
                                    foreach ($inclusions as $row):
                                        if ($key != 'add_ons') :   ?>
                                            <td>
                                                <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                                echo form_dropdown($key . '[status]', $statusList, null, "class='form-control status-dropdown' {$disabled}"); ?>
                                            </td>
                                            <?php else: ?>
                                                <?php if ($with_add_ons && $key == 'add_ons'): ?>
                                                <td>
                                                    <?php $statusList = array('Pending' => 'Pending', 'Fulfilled' => 'Fulfilled', 'Cancelled' => 'Cancelled');
                                                    echo form_dropdown($key . '[status]', $statusList, null, "class='form-control status-dropdown' {$disabled}"); ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php if (!$from_hr): ?>
                                        <td>
                                            <span class="hidden"><?php echo $rec['upi_id']?></span>
                                            <a class="btn-default btn <?php echo $disabled;?>" style="padding: 0 10px;" id="update-status_<?php echo $rec['id']; ?>" >Update Status</a>
                                        </td>
                        <?php endif; ?>
                                </tr>
    <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <input type="hidden" value="" id="event_id" name="event_id" />
<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div id="eventform-modal" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn event-confirm-btn" data-target="eventform-confirm">Set Event</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    </div>
</div>

<div id="eventform-confirm" class="modal modal-fixed-footer">
    <p class="modal-header" style="padding-left: 30px;">The following employees will be notified about the event with below details: </p>
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn create-confirm-btn">Confirm</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    </div>
</div>

<input type="hidden" value="<?php echo $basic_multiple_type;?>" name="basic_multiple_type" id="basic_multiple_type" />
<script type="text/javascript">
$(document).ready(function () {
    
    $('#health_talk').change(function(){
        if($(this).val() != ""){
            var data = {};
            data.program_code = '<?php echo $program_code?>';
            data.company_id = '<?php echo $company_id?>';
            var type = $(this).val();
            $.getJSON(SITEROOT + '/community/getProgramEvents', data, function(response){
                $("#show-event-details").addClass("hidden");
                $("#create-event").addClass("hidden");
                var event_id = 0;
                if(response.event.length == 0){
                    $("#create-event").removeClass("hidden");
                }else{
                    for (let val of Object.values(response.event)) {  
                        if(val.multiple_event_info === type){
                            event_id = val.id;
                        }
                    }
                    if(event_id == 0){
                        $("#create-event").removeClass("hidden");
                    }else{
                        $("#show-event-details").removeClass("hidden");
                    }
                }
                $("#event_id").val(event_id);
                refreshTable();
            })
        }
    })
    
    $("#send-event-details").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        
        var data = {};
        data.ceid = $(this).data("ceid");
        data.htalk = $(this).data("htalk");
        data.event_id = $("#event_id").val();
        $.post(SITEROOT + '/CorporateHealthPrograms/event_edit/', data, function(html){
            var new_enrollees = '<input type="hidden" id="send-to-new" name="send-to-new" value="1" />';
            $("#eventform-modal .modal-content").html(html)
            $("#eventform-modal").modal("open")
            $("#eventform-modal .modal-content").find("form#event_form").append(new_enrollees)
            
            <?php if($from_hr):?>
            $(".modal-overlay ~ .modal-overlay").remove();
            $("body").removeAttr('style');
            <?php endif;?>
        })
        
    })
    
    if($("#basic_multiple_type").val() != null){
        $('#health_talk').val($("#basic_multiple_type").val())
        $("#health_talk").trigger("change")
    }
    
    $("a#create-event").click(function(){
        var data = {};
        data.ceid = $(this).data("ceid");
        data.htalk_type = $("#health_talk").val();
        data.htalk = $("#health_talk option:selected").text();
        $.post(SITEROOT + '/CorporateHealthPrograms/event_new/', data, function(html){
            $("#eventform-modal .modal-content").html(html);
            $("#eventform-modal").modal("open");
        });
    });
    
    function refreshTable(){
        show_waitMe(jQuery('body'));
        var data = {};
        data.event_id = $("#event_id").val();
        data.ceid = '<?php echo $company_enrollment_id;?>';
        data.program_code = '<?php echo $program_code?>';
        data.event_type = $("#health_talk").val();
        $.get(SITEROOT + '/CorporateHealthPrograms/getEmployeesIntervention', data, function(response){
            $("#content-table").html(response);
            
            if($("#event_id").val() != 0){
                //check if there are new employees enrolled after the creation and sending of event details
                var cnt_disabled_buttons = $("#content-table table#basic-intervention tbody").find("a.btn.disabled").length;
                if(cnt_disabled_buttons > 0){
                    $("#send-event-details").removeClass("hidden")
                }
            }
            hide_waitMe();
        });
    }
    
    $(".event-confirm-btn").click(function(){
        //check if all required fields have values
        var empty = $("#event_form").find("input.required").filter(function(){
            return !$(this).val();
        });
        if(empty.length > 0){
            $("#event_form").find("input.required").each(function(){
                if($(this).val() == ''){
                    $(this).css('border', '1px solid red');
                }else{
                    $(this).removeAttr('style');
                }
            });
        }else{
            show_waitMe(jQuery('body'));
            $("#event_form").find("input.required").removeAttr('style');
            $.get(SITEROOT + '/account/showSubscriptionEnrollees/' + $("#x").val()+'/'+$("#send-to-new").val(), function(html){
                $("#eventform-confirm .modal-content").html($("#eventform").clone()).find("input, textarea, select").attr("disabled", "true");
                $("#eventform-confirm .modal-content").append(html);
                $("#eventform-confirm").modal("open");
                hide_waitMe();
            })
        }
    })

    $(".create-confirm-btn").click(function(e){
        e.preventDefault();
        e.stopPropagation();

        //save event
        var data = {};
        if($("#event_id").val() == 0){
            data.callback = '/community/event_add/1';
        }else{
            data.callback = '/community/event_update/<?php echo $company_enrollment_id; ?>';
        }
        data.form_data = null;
        data.form_data = jQuery("#event_form").serializeArray();
        var response = vip_ajax(data);
        if (response.result.success > 0) {
            var data = {};
            data.x = $("#x").val();
            data.for_new_enrolled = $("#send-to-new").val()
            if($("#event_id").val() == 0){
                data.event_id = response.result.event_id;
                data.is_update = false;
                $("#event_id").val(response.result.event_id);
            }else{
                data.event_id = $("#event_id").val();
                data.is_update = true;
            }
            $.post(SITEROOT + '/CorporateHealthPrograms/setBasicEvent/', data, function(response1){
                var resp = jQuery.parseJSON(response1)
                if(resp.result.success == 1){
                    $("#show-event-details").removeClass("hidden")
                    $("#create-event").addClass("hidden")
                    refreshTable()
                    $('.modal').modal('close');
                    $(".modal-overlay").remove();
                    $("body").removeAttr('style');
                }else{
                    $(".alert-danger").removeClass('hidden');
                    $(".alert-danger").html(resp.result.errors);
                }
            })
        } else {
            $(".alert-danger").removeClass('hidden');
            $(".alert-danger").html(response.result.errors);
        }
    });
    
    var index = $("#basic-intervention").find('th:last').index();
    $("#basic-intervention").DataTable({
        "order": [[index, "asc"]]
    });
    $('.dataTables_scrollBody').css('min-height', '500px');
    
    
    $('#eventform-confirm, #eventform-modal').modal({
        dismissible: false
    });

    $("a#show-event-details").click(function(){
        var data = {};
        data.ceid = $(this).data("ceid");
        data.htalk = $(this).data("htalk");
        data.event_id = $("#event_id").val();
        $.post(SITEROOT + '/CorporateHealthPrograms/event_edit/', data, function(html){
            $("#eventform-modal .modal-content").html(html);
            $("#eventform-modal").modal("open");
            
            <?php if($from_hr):?>
            $(".modal-overlay ~ .modal-overlay").remove();
            $("body").removeAttr('style');
            <?php endif;?>
        });
    });
    
});
</script>
