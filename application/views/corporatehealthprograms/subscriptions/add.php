<style>
    form{
        padding: 20px;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }
    .control-label {
        margin-bottom: 20px;
    }
    
    input.form-control {
        height: 2rem !important;
    }
    #number_of_users{
        width: 100px;
        margin-left: 20px;
        text-align: center;
    }
</style>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <h4 class="dashboard-title" ><?php echo $company_name; ?></h4>
            <div class="title"><?php echo $title;?></div>
            <div class="container-fluid">
                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row">
                        <div class="col-sm-12">
                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">
                                <?php echo _error_message('danger'); ?>
                                <?php echo form_open(base_url() . 'CorporateHealthPrograms/'.$callback, array('id' => 'subscriptions_form')); ?>
                                <?php if($callback == 'subscriptions_add') :?>
                                <input type='hidden' name='company_id' id='company_id' value="<?php echo $company_id ?>">
                                <?php else:?>
                                <input type='hidden' name='enrollment_id' id='enrollment_id' value="<?php echo $program_info['enrollment_id'] ?>">
                                <?php endif; ?>

                                <div class="form-group">
                                    <?php echo form_label('Program Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php if($callback == 'subscriptions_add') :?>
                                        <?php echo form_dropdown('program_code', $programs, '', 'class="form-control select-2 select2-hidden-accessible"'); ?>
                                        <?php else: ?>
                                        <?php echo $program_info['subscription_name'];?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Start of Subscription<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'subscription_start', 'name' => 'subscription_start', 'class' => 'form-control datepicker', 'value' => ($callback == 'subscriptions_update' ? date('d F, Y', strtotime($program_info['subscription_start'])) : null))); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('End of Subscription<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'subscription_end', 'name' => 'subscription_end', 'class' => 'form-control datepicker', 'value' => ($callback == 'subscriptions_update' && $program_info['subscription_end'] != null ? date('d F, Y', strtotime($program_info['subscription_end'])) : null)));?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Maximum number of users', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <input type="checkbox" <?php echo ($callback != 'subscriptions_update' ? 'checked' : ($program_info['allowed_number_of_users'] == 0 ? 'checked' : ''));?> name="allowed_number_of_users[0]" id="allowed_number_of_users" /><label for="allowed_number_of_users"> Unlimited</label>
                                        <input type="number" step="1" oninput="this.value=this.value.replace(/[^1-9]*,'');" min="1" placeholder="Input value" class="form-control <?php echo ($callback != 'subscriptions_update' ? 'hidden' : ($program_info['allowed_number_of_users'] == 0 ? 'hidden' : ''));?>" value="<?php echo ($callback == 'subscriptions_update' ? $program_info['allowed_number_of_users'] : '')?>" name="allowed_number_of_users[1]" id="number_of_users" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <?php echo form_label('Status', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-<?php echo ($callback == 'subscriptions_update' && $with_unlock_option) ? 2 : 8?>">
                                        <?php $statusList = array(
                                            'Ongoing' => 'Ongoing', 
                                            'Completed' => 'Completed',
                                            'Expired' => 'Expired', 
                                            'Cancelled' => 'Cancelled',
                                            'For Renewal' => 'For Renewal'
                                            );
                                            echo form_dropdown('status', $statusList, ($callback == 'subscriptions_update' ? $program_info['status'] : 'Ongoing'), 'class="form-control select-2 select2-hidden-accessible"'); 
                                        ?>
                                    </div>
                                    <?php if($callback == 'subscriptions_update' && $with_unlock_option):?>
                                    <div id="unlock-wrapper" class="col-sm-7 <?php echo ($program_info['status'] != 'Ongoing' ? 'hidden': '')?>">
                                        <input type="checkbox" name="unlock_all" id="unlock_all" /><label for="unlock_all"> Unlock all survey forms</label>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <?php echo form_label('Remarks', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_textarea(array('id' => 'remarks', 'name' => 'remarks', 'class' => 'form-control', 'value' => ($callback == 'subscriptions_update' ? $program_info['remarks'] : null))); ?>
                                    </div>
                                </div>
<?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <a href='javascript:void(0)' class="btn-primary btn program-save">Save</a>
                                                <a data-callback="/CorporateHealthPrograms/subscriptions/<?php echo $company_id ?>" class="btn-default btn cancel" parent-callback="CorporateHealthPrograms/subscriptions/<?php echo $company_id ?>">Cancel</a>
                                            </div>
                                        </div>  <!-- form group -->
                                    </div><!-- row -->
                                </div><!-- panel footer -->
                            </div><!-- panel -->
                        </div><!-- col 12 -->
                    </div><!-- add user -->
                </div><!-- ui s -->
            </div><!-- container -->
        </div>
        <!-- page-content -->
    </div>
    <!-- static-content -->
</div>
<!-- static-content-wrapper -->
<script>
    var callback = '<?php echo $callback?>';
    $(document).ready(function () {
        
        $('ul.dropdown-content li').first().click(function(){    
            $('ul.dropdown-content li:not(:first-child)').each(function(index) {
                $(this).find("input[type=checkbox]").prop("checked", $('ul.dropdown-content li').first().find("input[type=checkbox]").prop("checked"));                       
            });
        });
        
        $(".program-save").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var data = {};
            data.callback = 'CorporateHealthPrograms/<?php echo $callback;?>';
            data.form_data = null;
            data.form_data = jQuery("#subscriptions_form").serializeArray();

            var response = vip_ajax(data);
            if (response.result.success == 1) {
                data.callback = 'CorporateHealthPrograms/subscriptions/<?php echo $company_id ?>';
                page_ajaxify(data);
            } else {
                $(".alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        })

        $("#allowed_number_of_users").click(function(){
            if(!$(this).is(":checked")){
                $("#number_of_users").removeClass("hidden")
            }else{
                $("#number_of_users").addClass("hidden").val("")
            }
        })

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: false // Close upon selecting a date,
        });
        
        $(".cancel").on("click", function(e){
            e.preventDefault();
            var data = {};
            data.callback = $(this).attr("data-callback");
            page_ajaxify(data);
        })
        
        if(callback == 'subscriptions_update'){
            $("select[name=status]").on("change", function(){
                $("#unlock-wrapper").addClass("hidden");
                if($(this).val() == "Ongoing"){
                    $("#unlock-wrapper").removeClass("hidden");
                }
            })
        }
    });
</script>