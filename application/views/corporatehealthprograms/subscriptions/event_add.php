<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" type="text/css" rel="stylesheet">
<style>
    form{
        padding: 20px;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }

    .datepicker, .time {
        width: 30% !important;
        margin-right: 10px;
        text-align: center;
    }

    .time, .ui-timepicker-wrapper {
        width: 9em !important;
    }

    .ui-timepicker-list li{
        text-align: center;
    }

    textarea {
        height: 100px;
    }

    #event_name, #venue{
        padding-left: 10px;
    }
</style>
<h5><?php echo $title; ?></h5>
<?php echo form_open(base_url() . 'corporatehealthprograms/' . $callback, array('id' => 'event_form')); ?>
<input type="hidden" name="f" id="f" value="<?php echo ($callback == 'event_update' ? $event_info['id'] : 0) ?>" />
<input type="hidden" name="x" id="x" value="<?php echo $x ?>" />
<input type="hidden" name="multiple_basic" id="multiple_basic" value="<?php echo $htalk_type ?>" />
<div id="eventform">
    <div class="form-group">
        <?php echo form_label('Event Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'event_name', 'name' => 'event_name', 'class' => 'form-control required', 'value' => ($callback == 'event_update' ? $event_info['name'] : $default_title))); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Location <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'venue', 'name' => 'venue', 'class' => 'form-control required', 'value' => ($callback == 'event_update' ? $event_info['venue'] : null))); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Starts <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'start_date', 'name' => 'start_date', 'class' => 'form-control required datepicker', 'value' => ($callback == 'event_update' ? date('M d, Y', strtotime($event_info['start_date'])) : null), 'placeholder' => 'Select Date')); ?>
            <?php echo form_input(array('id' => 'start_time', 'name' => 'start_time', 'class' => 'form-control required time ui-timepicker-input', 'value' => ($callback == 'event_update' ? date('h:i A', strtotime($event_info['start_time'])) : null), 'placeholder' => 'Select Time')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Ends <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'end_date', 'name' => 'end_date', 'class' => 'form-control required datepicker', 'value' => ($callback == 'event_update' ? date('M d, Y', strtotime($event_info['end_date'])) : null), 'placeholder' => 'Select Date')); ?>
            <?php echo form_input(array('id' => 'end_time', 'name' => 'end_time', 'class' => 'form-control required time ui-timepicker-input', 'value' => ($callback == 'event_update' ? date('h:i A', strtotime($event_info['end_time'])) : null), 'placeholder' => 'Select Time')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Description', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_textarea(array('id' => 'description', 'name' => 'description', 'class' => 'form-control', 'value' => ($callback == 'event_update' ? $event_info['description'] : null))); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Status', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php
            $statusList = array(
                'Active' => 'Active',
                'Completed' => 'Completed',
                'Cancelled' => 'Cancelled'
            );
            echo form_dropdown('status', $statusList, ($callback == 'event_update' ? $event_info['status'] : 'Active'), 'class="form-control select-2 select2-hidden-accessible"');
            ?>
        </div>
    </div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            format: 'mmm dd, yyyy',
            closeOnSelect: false // Close upon selecting a date,
        });

        $('.time').timepicker({
            'scrollDefault': 'now',
            'timeFormat': 'g:i A',
            maxTime: '10:00pm',
            minTime: '6:00am'
        });

    });
</script>