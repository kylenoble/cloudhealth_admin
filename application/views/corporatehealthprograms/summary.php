<div class="static-content-inner manage-users ">
    <?php // _load_js($js); ?>
    <div class="static-content">
        <div class="page-content">
            <div class="title small-tabs active no-hover">SUMMARY OF CORPORATE HEALTH PROGRAMS</div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <table id='corporate_users' class="stripe row-border order-column" width="100%" cellspacing="0">
                            <thead>
                            <tr>                         
                                <th>Company Name</th>
                                <th>Active Subscriptions</th>
                                <th>Account Manager</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($summary as $row) {
                                $subscription_cnt = (int)$row['subscription_count'];
                                $status = '-';
                                if($subscription_cnt > 0){
                                    if($row['Ongoing'] > 0){
                                        $status = 'Ongoing';
                                    }elseif($row['Completed'] > 0){
                                        $status = 'Completed';
                                    }
                                }
                                ?>
                                <tr>
                                    <td><?php echo addslashes($row['company_name']); ?></td>
                                    <td class="center">
                                        <a title="View Subscriptions" href="/corporateHealthPrograms/subscriptions/<?php echo $row['company_id']?>" alt="View Subscriptions" class="view-subscriptions" data-callback="/corporateHealthPrograms/subscriptions/<?php echo $row['company_id']?>" parent-callback="corporateHealthPrograms/index">
                                            <?php echo $subscription_cnt; ?>
                                        </a>
                                    </td>
                                    <td><?php echo addslashes($row['contact']); ?></td>
                                    <td class="center"><?php echo $status; ?></td>
                                </tr>
                            <?php }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    $(document).ready(function(){
        $('#corporate_users').DataTable({
            scrollY:        400,
            scrollX:        true,
            scrollCollapse: true,
            "order": [[ 0, "asc" ]],
            dom: 'Bfrtip',
            buttons: [{ 
                extend: 'pdf', 
                text: 'Export to PDF',
                title: 'Summary of Corporate Health Programs (as of '+ getDate(new Date()) + ")",
                exportOptions: {
                    columns: [0, 1, 2, 3]
                    }
                }
            ]
        });
        
        $("#corporate_users").on("click", ".view-subscriptions", function(e){
            e.preventDefault();
            var data = {};
            data.callback = $(this).attr("href");
            page_ajaxify(data);
        })
        
        $('table#corporate_users tbody input:checkbox').click(function(){
            let company_name = $(this).attr("data-name");
            var data = {};
            var enable = "enable";
            data.callback = '/CorporateHealthPrograms/company_telemedicine';
            data.form_data = {};
            data.form_data.cid = $(this).attr("data-cid");
            
            if($(this).is(":checked")){
                data.form_data.status = 1;
            }else{
                data.form_data.status = 0;
                enable = "disable";
            }
            console.log(data)
            if(confirm("Are you sure you want to "+enable+" telemedicine for "+company_name+"?")){
                vip_ajax(data);
                var data1 = {};
                data1.callback = '/CorporateHealthPrograms';
                page_ajaxify(data1);
            }else{
                $(this).prop('checked', !($(this).is(':checked')));
            }
        });
    })
</script>