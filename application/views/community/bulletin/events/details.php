<div class="modal-content">
    <ul class="collection">
        <li class="collection-item">
            <label>Event Name: </label>
            <p><?php echo $data['name'];?></p>
        </li>
        <li class="collection-item">
            <label>Event Venue: </label>
            <p><?php echo $data['venue'];?></p>
        </li>
        <li class="collection-item">
            <label>Start: </label>
            <p>
                <?php echo date('M j, Y', strtotime($data['start_date'])), ' ', date('h:i A', strtotime($data['start_time'])); ?>
            </p>
        </li>
        <li class="collection-item">
            <label>End: </label>
            <p>
                <?php echo date('M j, Y', strtotime($data['end_date'])), ' ', date('h:i A', strtotime($data['end_time'])); ?>
            </p>
        </li>
        <li class="collection-item">
            <label>Description: </label>
            <p>
                <?php echo $data['description']; ?>
            </p>
        </li>
        <li class="collection-item">
            <label>Audience/Company: </label>
            <p>
                <?php echo ((int)$data['audience'] > 0 ? $this->Users_model->getCompanyNameByID((int)$data['audience']) : $data['audience'])?>
            </p>
        </li>
        <?php
        $admin = $this->Users_model->getSuperAdminID();
        if($data['health_talk'] == null && $data['created_by'] != $admin['id']){
            $joined_cnt = 0;
            $declined_cnt = 0;
            if($data['read_by'] != null){
                $this->load->model('Events_model');
                $read_by = json_decode($data['read_by'], true);
                foreach($read_by['data'] as $rec){
                    if($rec['status'] == Events_model::joined){
                        $joined_cnt++;
                    }elseif($rec['status'] == Events_model::declined){
                        $declined_cnt++;
                    }
                }
            } ?>
        <li class="collection-item ">
            <label>Statistics: </label>
            <p>Joined: <?php echo $joined_cnt;?></p>
            <p>Declined: <?php echo $declined_cnt;?></p>
        </li>
        <?php }
        ?>
    </ul>
</div>
<div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
</div>