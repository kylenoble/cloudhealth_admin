<?php $role = admin_role();?>
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" type="text/css" rel="stylesheet">
<style>
    form{
        padding: 20px;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }
    .control-label{
        line-height: 3rem;
    }
    .form-control{
        margin: 0 !important;
    }
    .datepicker, .time {
        width: 30% !important;
        margin-right: 10px;
        text-align: center;
    }
    
    .time, .ui-timepicker-wrapper {
        width: 9em !important;
    }
    
    .ui-timepicker-list li{
        text-align: center;
    }
    
    textarea {
        height: 100px;
    }
    
    label[for^="audience_"]{
        margin-right: 30px;
    }
    
    fieldset{
        border-radius: 15px;
        width: 90%;
    }
    
    legend{
        padding: 0 10px 0 5px;
        text-transform: uppercase;
        font-weight: bold;
        margin-top: 10px;
    }
</style>
<?php
$data_callback = '/community/index/bulletin';
if($role == Users_model::HR){
    $data_callback = '/community';
}
?>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="title"><?php echo $title; ?></div>
            <div class="container-fluid">
                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row">
                        <div class="col-sm-12">
                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">
                                <?php echo _error_message('danger'); ?>
                                <?php echo form_open(base_url() . 'community/' . $callback, array('id' => 'event_form')); ?>
                                <input type="hidden" name="f" id="f" value="<?php echo ($callback == 'event_update' ? $event_info['id'] : 0)?>" />
                                <?php if($event_for_basic_programs):?>
                                <?php if($callback != 'event_update'): //create event?>
                                <?php if($role == Users_model::ADMIN):?>
                                <div class="form-group">
                                    <?php echo form_label('Select Company<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('company', $companies, $companies_selected, 'class="form-control select-2 select2-hidden-accessible"'); ?>
                                    </div>
                                </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <?php echo form_label('Select Basic Health Program<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('x', array(), NULL, 'disabled class="form-control select-2 select2-hidden-accessible" id="x"'); ?>
                                    </div>
                                </div>
                                <?php else: //update event?>
                                <?php if($role == Users_model::ADMIN):?>
                                <div class="form-group">
                                    <?php echo form_label('Company', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo $company_name; ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php endif;?>
                                <div class="form-group">
                                    <?php echo form_label('Basic Health Program', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo $health_program; ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php endif;?>
                                <?php endif;?>
                                <div class="clearfix"></div>
                                <fieldset class="event-details-wrapper">
                                    <legend>Event Details</legend>
                                
                                    <div class="form-group">
                                        <?php echo form_label('Event Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'event_name', 'name' => 'event_name', 'class' => 'form-control', 'value' => ($callback == 'event_update' ? $event_info['name'] : null))); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Location <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'venue', 'name' => 'venue', 'class' => 'form-control', 'value' => ($callback == 'event_update' ? $event_info['venue'] : null))); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Starts <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'start_date', 'name' => 'start_date', 'class' => 'form-control datepicker', 'value' => ($callback == 'event_update' ? date('M d, Y', strtotime($event_info['start_date'])) : null), 'placeholder' => 'Select Date')); ?>
                                            <?php echo form_input(array('id' => 'start_time', 'name' => 'start_time', 'class' => 'form-control time ui-timepicker-input', 'value' => ($callback == 'event_update' ? date('h:i A', strtotime($event_info['start_time'])) : null), 'placeholder' => 'Select Time', 'style' => $role == Users_model::MD ? 'height: 30px;' : '')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Ends <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8" style="margin-bottom: 10px;">
                                            <?php echo form_input(array('id' => 'end_date', 'name' => 'end_date', 'class' => 'form-control datepicker', 'value' => ($callback == 'event_update' ? date('M d, Y', strtotime($event_info['end_date'])) : null), 'placeholder' => 'Select Date')); ?>
                                            <?php echo form_input(array('id' => 'end_time', 'name' => 'end_time', 'class' => 'form-control time ui-timepicker-input', 'value' => ($callback == 'event_update' ? date('h:i A', strtotime($event_info['end_time'])) : null), 'placeholder' => 'Select Time', 'style' => $role == Users_model::MD ? 'height: 30px;' : '')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                            <?php echo form_label('Description', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
    <?php echo form_textarea(array('id' => 'description', 'name' => 'description', 'class' => 'form-control', 'value' => ($callback == 'event_update' ? $event_info['description'] : null))); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Status', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php
                                            $statusList = array(
                                                'Active' => 'Active',
                                                'Completed' => 'Completed',
                                                'Cancelled' => 'Cancelled'
                                            );
                                            echo form_dropdown('status', $statusList, ($callback == 'event_update' ? $event_info['status'] : 'Active'), 'class="form-control select-2 select2-hidden-accessible"');
                                            ?>
                                        </div>
                                    </div>
                                    <?php if(!$event_for_basic_programs && (($role == Users_model::ADMIN && $callback == 'event_add') || ($role == Users_model::ADMIN && $callback == 'event_update' && $event_info['health_talk'] == null))):?>
                                    <div class="form-group">
                                        <?php echo form_label('Select Audience <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label', 'style' => 'margin-top: 10px')); ?>
                                        <div class="col-sm-8" style="margin-top: 10px">
                                            <?php $audience = '';
                                            $audienceList = array();
                                            if($callback == 'event_update') {
                                                $audienceList = explode(", ", $event_info['audience']);
                                            }?>
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Patients', $audienceList) ? 'checked' : '')?> value="Patients" name="audience[]" id="audience_px" /><label for="audience_px"> Patients</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Doctors', $audienceList) ? 'checked' : '') ?> value="Medical" name="audience[]" id="audience_md" /><label for="audience_md"> Medical Team</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('HR', $audienceList) ? 'checked' : '') ?> value="HR" name="audience[]" id="audience_hr" /><label for="audience_hr"> HR Users</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Sales', $audienceList) ? 'checked' : '') ?> value="Sales" name="audience[]" id="audience_sales" /><label for="audience_sales"> Sales Team</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Ops', $audienceList) ? 'checked' : '') ?> value="Ops" name="audience[]" id="audience_ops" /><label for="audience_ops"> Ops Team</label>
                                        </div>
                                    </fieldset>
                                </div>
                                <?php endif;?>
<?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <a href='javascript:void(0)' class="btn-primary btn event-save">Save</a>
                                                <a data-callback="<?php echo $data_callback?>" class="btn-default btn page_ajaxify" >Cancel</a>
                                            </div>
                                        </div>  <!-- form group -->
                                    </div><!-- row -->
                                </div><!-- panel footer -->
                            </div><!-- panel -->
                        </div><!-- col 12 -->
                    </div><!-- add user -->
                </div><!-- ui s -->
            </div><!-- container -->
        </div>
        <!-- page-content -->
    </div>
    <!-- static-content -->
</div>
<!-- static-content-wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script>
    $(document).ready(function () {
        var event2_container;
        if('<?php echo $event_for_basic_programs?>' !== '' && '<?php echo $callback?>' !== 'event_update'){
            $.get(SITEROOT + 'Community/ajax_getBasicProgramsPerCompany/'+$("select[name=company]").val(), function(html){
                $("select[name=x]").html(html).removeAttr("disabled");
            })

            $("select[name='x']").change(function(){
                var tmp_val = $(this).val().split("|");
                var tmp_desc = tmp_val[1].split("~")
                if(tmp_desc.length > 1){
                    $(".btn.event-save").text("Save and Create another event");
                    $(".btn.event-save").addClass("multiple-event")
                    event2_container = tmp_desc[1];
                }else{
                    $(".btn.event-save").text("SAVE")
                    $(".btn.event-save").removeClass("multiple-event")
                }
                $("#event_name").val(tmp_desc[0])
            })

            $("select[name='company']").change(function(){
                $.get(SITEROOT + 'Community/ajax_getBasicProgramsPerCompany/'+$(this).val(), function(html){
                    $("select[name=x]").html(html).removeAttr("disabled");
                })
            })
        }

        $(".event-save").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var data = {};
            data.callback = 'community/<?php echo $callback; ?>/<?php echo ($event_for_basic_programs ? true : false)?>';
            data.form_data = null;
            data.form_data = jQuery("#event_form").serializeArray();

            var response = vip_ajax(data);
            console.log(response.result.errors)
            if (response.result.success == 1) {
                if($(this).hasClass("multiple-event")){
                    $("fieldset.event-details-wrapper").find("input, textarea").val("");
                    $("fieldset.event-details-wrapper input#event_name").val(event2_container);
                    $(this).text("SAVE");
                    $(".btn.event-save").removeClass("multiple-event")
                    $("html, body").animate({ scrollTop: 0 });
                }else{
                    var data = {};
                    data.form_data = null;
                    <?php if(admin_role() == Users_model::ADMIN) :?>
                    data.form_data = {page: 'bulletin'};
                    <?php endif;?>
                    data.callback = '<?php echo $data_callback;?>';
                    page_ajaxify(data);
                }
            } else {
                $(".alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        })

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            format: 'mmm dd, yyyy',
            closeOnSelect: false // Close upon selecting a date,
        });
        
        $('.time').timepicker({ 
            'scrollDefault': 'now',
            'timeFormat': 'g:i A',
            maxTime: '10:00pm',
            minTime: '6:00am'
        });
    });
</script>