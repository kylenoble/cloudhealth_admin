<style>
    div#events-div .dataTables_wrapper, div#basic-intervention_wrapper{
        margin: 0 !important;
    }
    .open-details:hover{
        cursor: pointer;
        color: #00bcd4;
    }
    a{
        cursor: pointer;
    }
    div.dataTables_wrapper th, div.dataTables_wrapper td { 
        white-space: normal !important; 
    }
    div#events-enrollees.modal{
        width: 95%;
    }
</style>
<?php $my_userid = $this->session->userdata['logged_in']['userid'];
$role = admin_role();
$user_is_HR = $role == Users_model::HR;
$user_is_Admin = $role == Users_model::ADMIN;
?>

<?php if(isset($my_events)) :?>
<div class="title">My Events</div>
<?php if ($this->session->flashdata('event_add_msg') != '') {
    ?>
    <div class="alert alert-info alert-dismissable" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
        <?php echo $this->session->flashdata('event_add_msg'); ?>
    </div>
    <?php
}
?>
<?php if ($this->session->flashdata('event_add_error') != '') {
    ?>
    <div class="alert alert-warning alert-dismissable" style="margin: 0;visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
        <?php echo $this->session->flashdata('event_add_error'); ?>
    </div>
    <?php
}
?>

<div class="input-group-btn right">
    <a class="btn btn-info add_subscription active page_ajaxify" id="add-program-btn" data-callback="community/event_new" parent-callback="community/event_new/" href="javascript:void(0)">Create an Event</a>
</div>

<div class="row" id="events-div">
    <div class="col-sm-12">

        <table id='events_table' data-name="<?php echo $user_is_Admin ? "Admin" : "HR"?> Events" class="stripe row-border order-column" width="100%" cellspacing="0">
            <thead>
                <tr>   
                    <th>Event Name</th>
                    <th>Location</th>
                    <th>Starts</th>
                    <th>Ends</th>
                    <th>Status</th>
                    <th>Enrollees</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $edit = '';
                $delete = '';
                foreach ($my_events as $event) { 
                    $start_date = $event['start_date'];
                    $start_time = $event['start_time'];
                    $end_date = $event['end_date'];
                    $end_time = $event['end_time'];
                    $edit = '<a href="/community/event_edit/'.$event['id'].'" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/'.$event['id'].'" parent-callback="/community/event_edit/'.$event['id'].'" >EDIT</a>';
                    $delete = '&nbsp;<a class="btn-default btn btn-small" id="delete-my-event_'.$event['id'].'" >DELETE</a>'; ?>
                    <tr>                                               
                        <td data-eid="event-<?php echo $event['id'];?>" style='text-align: center' class="open-details" title="View Details"><?php echo $event['event_name']?></td>
                        <td style='text-align: center'><?php echo $event['venue']; ?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$start_date $start_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$start_date $start_time")); ?>
                        </td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$end_date $end_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$end_date $end_time")); ?>
                        </td>
                        <td class='center'><?php echo $event['status']?></td>
                        <td class='center'>
                            <a class="btn-default show-attendance" id="show-attendance_<?php echo $event['id'];?>" >View List</a>
                        </td>
                        <td style='text-align: center'>
                            <?php if($user_is_Admin && $event['status'] == 'Active') :
                                if($my_userid != $event['created_by']) {
                                    echo $delete;
                                }else{
                                    echo $edit;
                                    echo $delete;
                                } endif ; ?>
                            <?php  if(!$user_is_Admin && $my_userid == $event['created_by']) : echo $edit, $delete; endif; ?>
                        </td>
                    </tr>
<?php } ?>
            </tbody>

        </table>

    </div>

</div>
<?php endif;?>
<?php if(isset($health_events)) :?>
<div class="title">Health Talks</div>
<div class="alert alert-success alert-dismissable hidden health_talk_alert" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);"></div>
<div class="input-group-btn right">
    <a class="btn btn-info add_subscription active page_ajaxify" id="add-healthtalk-btn" data-callback="community/event_new/1" parent-callback="community/event_new/1" href="javascript:void(0)">Create Health Talk</a>
</div>
<div class="row">
    <div class="col-sm-12">

        <table id='health_events_table' data-name="Basic Program: Health Talks" class="wrap stripe row-border order-column" width="100%" cellspacing="0" >
            <thead>
                <tr>
                    <th>Subscription</th>
                    <th>Event Name</th>
                    <th>Location</th>
                    <th>Event Date</th>
                    <th>Status</th>
                    <th style='width: 150px !important'>Enrollees</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($health_events as $event) { 
                    $start_date = $event['start_date'];
                    $start_time = $event['start_time'];
                    $end_date = $event['end_date'];
                    $end_time = $event['end_time'];
                    $send_event_details = $this->subscription->countEventNotSent($event['company_enrollment_id']) > 0;
                    ?>
                    <tr>
                        <td style='text-align: center'><?php echo $event['subscription_name']?></td>
                        <td data-eid="event-<?php echo $event['id'];?>" style='text-align: center; width: 300px' class="open-details" title="View Details"><?php echo $event['event_name']?></td>
                        <td style='text-align: center'><?php echo $event['venue']; ?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$start_date $start_time");?>">
                            <?php echo date('M j, Y', strtotime("$start_date")), '<br />', date('h:i', strtotime("$start_time")) ,'-', date('h:iA', strtotime("$end_time")); ?>
                        </td>
                        <td class='center'><?php echo $event['status']?></td>
                        <td class='center' style='width: 150px !important'>
                            <a class="btn-default btn-small show-enrolled" style='padding: 0' id="show-enrolled_<?php echo $event['company_enrollment_id'];?>" >View List</a>
                            <?php if($send_event_details === true):?>
                            <br />
                            <a class="btn-default btn-small send-details" style='padding: 0' id="send-details_<?php echo $event['id'];?>" >Notify enrollees</a>
                            <?php endif;?>
                        </td>
                        <td style='text-align: center'>
                            <?php if($user_is_Admin) :
                                if($role != $event['created_by']) { ?>
                            <a class="btn-default btn btn-small" id="delete-my-event_<?php echo $event['id'];?>" >DELETE</a>
                                <?php }else{ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>/1" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>/1" parent-callback="/community/event_edit/<?php echo $event['id']?>/1" >EDIT</a>
                                <?php } endif; //$user_is_Admin?>
                            <?php  if($user_is_HR) {
                                if($role == $event['created_by'] || (admin_role() != $event['created_by'] && $event['health_talk'] != '')){ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>/1" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>/1" parent-callback="/community/event_edit/<?php echo $event['id']?>/1" >EDIT</a>
                                <?php }
                            }?>
                        </td>
                    </tr>
<?php }
?>
            </tbody>

        </table>

    </div>

</div>
<?php endif;?>

<?php if(isset($hr_events)) :?>
<div class="title">HR Events</div>
<div class="row">
    <div class="col-sm-12">

        <table id='hr_events_table' data-name="HR Events" class="row-border hover" width="100%" cellspacing="0">
            <thead>
                <tr>   
                    <th>Event Name</th>
                    <th>Location</th>
                    <th>Starts</th>
                    <th>Ends</th>
                    <th>Status</th>
                    <th>Enrollees</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($hr_events as $event) { 
                    $start_date = $event['start_date'];
                    $start_time = $event['start_time'];
                    $end_date = $event['end_date'];
                    $end_time = $event['end_time'];
                    ?>
                <tr>                                               
                        <td data-eid="event-<?php echo $event['id'];?>" style='text-align: center' class="open-details" title="View Details"><?php echo $event['event_name']?></td>
                        <td style='text-align: center'><?php echo $event['venue']; ?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$start_date $start_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$start_date $start_time")); ?>
                        </td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$end_date $end_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$end_date $end_time")); ?>
                        </td>
                        <td class='center'><?php echo $event['status']?></td>
                        <td class='center'>
                            <a class="btn-default show-attendance" id="show-attendance_<?php echo $event['id'];?>" >View List</a>
                        </td>
                        <td class='center'>
                            <?php if($user_is_Admin && $event['status'] == 'Active') :
                                if(admin_role() != $event['created_by']) {?>
                            <a class="btn-default btn btn-small" id="delete-my-event_<?php echo $event['id'];?>" >DELETE</a>
                                <?php }else{ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>" parent-callback="/community/event_edit/<?php echo $event['id']?>" >EDIT</a>
                                <?php } endif; ?>
                            <?php  if($user_is_HR) :
                                if(admin_role() == $event['created_by'] || (admin_role() != $event['created_by'] && $event['health_talk'] != '')){ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>" parent-callback="/community/event_edit/<?php echo $event['id']?>" >EDIT</a>
                                <?php }
                            endif; ?>
                        </td>
                    </tr>
<?php }
?>
            </tbody>

        </table>

    </div>

</div>
<?php endif;?>

<?php if(isset($medical_events)) :?>
<div class="title">Medical Team Events</div>
<div class="row">
    <div class="col-sm-12">

        <table id='medical_events_table' data-name="Medical Events" class="row-border hover" width="100%" cellspacing="0">
            <thead>
                <tr>   
                    <th>Event Name</th>
                    <th>Location</th>
                    <th>Starts</th>
                    <th>Ends</th>
                    <th>Status</th>
                    <th>Enrollees</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($medical_events as $event) { 
                    $start_date = $event['start_date'];
                    $start_time = $event['start_time'];
                    $end_date = $event['end_date'];
                    $end_time = $event['end_time'];
                    ?>
                <tr>                                               
                        <td data-eid="event-<?php echo $event['id'];?>" style='text-align: center' class="open-details" title="View Details"><?php echo $event['event_name']?></td>
                        <td style='text-align: center'><?php echo $event['venue']; ?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$start_date $start_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$start_date $start_time")); ?>
                        </td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$end_date $end_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$end_date $end_time")); ?>
                        </td>
                        <td class='center'><?php echo $event['status']?></td>
                        <td class='center'>
                            <a class="btn-default show-attendance" id="show-attendance_<?php echo $event['id'];?>" >View List</a>
                        </td>
                        <td style='text-align: center'>
                            <?php if($user_is_Admin && $event['status'] == 'Active') :
                                if(admin_role() != $event['created_by']) {?>
                            <a class="btn-default btn btn-small" id="delete-my-event_<?php echo $event['id'];?>" >DELETE</a>
                                <?php }else{ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>" parent-callback="/community/event_edit/<?php echo $event['id']?>" >EDIT</a>
                                <?php } endif; ?>
                            <?php  if($user_is_HR) :
                                if(admin_role() == $event['created_by'] || (admin_role() != $event['created_by'] && $event['health_talk'] != '')){ ?>
                            <a href="/community/event_edit/<?php echo $event['id']?>" class="btn-default btn btn-small page_ajaxify" data-callback="/community/event_edit/<?php echo $event['id']?>" parent-callback="/community/event_edit/<?php echo $event['id']?>" >EDIT</a>
                                <?php }
                            endif; ?>
                        </td>
                    </tr>
<?php }
?>
            </tbody>

        </table>

    </div>

</div>
<?php endif;?>
<div id="events-modal" class="modal modal-fixed-footer"></div>
<div id="events-enrollees" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#events_table, #health_events_table, #hr_events_table, #medical_events_table').DataTable({
            scrollCollapse: true,
            scrollY: 400,
            scrollX: true,
            dom: 'Bfrtip',
            "order": [[2, "desc"]],
            buttons: [{
                extend: 'pdf',
                text: 'Export to PDF',
                title: 'List of '+ $(this).data('name') +' (as of ' + getDate(new Date()) + ")",
                orientation: 'landscape',
                exportOptions: {
                    columns: ':not(:last-child)'
                }
            }]
        });
        
        $('.modal').modal({
            dismissible: false
        });
        
        $("#hr_events_table tbody tr td.open-details, #health_events_table tbody tr td.open-details, #events_table tbody tr td.open-details").on("click", function(){
            var tmp_eid = ($(this).data("eid")).split("-")
            var eid = tmp_eid[1]
            $.get(SITEROOT + '/community/getEventInfo/' + eid, function(html){
                $("#events-modal").html(html);
                $("#events-modal").modal("open")
            })
        })

        $("a[id^=delete-my-event_]").on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            if(confirm("Are you sure you want to delete the event?")){
                var tmp_id = ($(this).attr("id")).split("_");
                $.post(SITEROOT + '/community/event_delete', {id: tmp_id[1]}, function(response){
                    if(response == 1){
                        var data = {};
                        data.form_data = null;
                        <?php if($user_is_Admin) :?>
                        data.form_data = {page: 'bulletin'};
                        <?php endif;?>
                        data.callback = 'community';
                        page_ajaxify(data);
                    }
                })
            }
        })
        
        $(".show-enrolled").on('click', function(){
            show_waitMe(jQuery('body'));
            var id_tmp = ($(this).attr("id")).split("_");
            var id = id_tmp[1]
            $.get(SITEROOT + '/CorporateHealthPrograms/subscriptions_intervention/' + id + '/<?php echo !$from_hr ? 0 : 1?>/1', function(html){
                $("#events-enrollees .modal-content").html(html)
                $("#events-enrollees").modal("open")
                hide_waitMe();
            })
        })
        
        $(".show-attendance").on('click', function(){
            show_waitMe(jQuery('body'));
            var id_tmp = ($(this).attr("id")).split("_");
            var id = id_tmp[1]
            $.get(SITEROOT + '/community/showEventEnrolees/' + id, function(html){
                $("#events-enrollees .modal-content").html(html);
                $("#events-enrollees").modal("open");
                hide_waitMe();
            })
        })
        
        $(".send-details").on('click', function(){
            show_waitMe(jQuery('body'));
            var all = $(this).attr("data-all");
            var id_tmp = ($(this).attr("id")).split("_");
            var id = id_tmp[1]
            $.post(SITEROOT + '/community/ajax_sendEventDetails', {all: all, x: id}, function(response){
                $(".health_talk_alert").html(response.msg).addClass(response.class).removeClass("hidden");
                hide_waitMe();
            }, 'json');
        })

    })
</script>