<table class="striped centered" id="enrolled_employee_table">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Department</th>
            <th>Status</th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($enrolled_employees as $employee) :
            $employee_id = $employee['employee_id'];
            $employee_name = trim($employee['name']);
            if ($employee['joined_status'] === 'Joined') {
                $employee_id .= ($employee['status'] == Users_model::$ANONYMOUS ? ' *' : '');
                $employee_name .= ($employee['status'] == Users_model::$ANONYMOUS ? ' *' : '');
            } else {
                $employee_id = ($employee['status'] == Users_model::$ANONYMOUS ? 'Anonymous' : $employee_id);
                $employee_name = ($employee['status'] == Users_model::$ANONYMOUS ? 'Anonymous' : $employee_name);
            }
            ?>
            <tr>
                <td><?php echo $employee_id; ?></td>
                <td><?php echo $employee_name; ?></td>
                <td><?php echo $employee['branch']; ?></td>
                <td><?php echo $employee['department']; ?></td>
                <td><?php echo $employee['joined_status']; ?></td>
            </tr>
<?php endforeach; ?>
    </tbody>

</table>
<script>
    $(document).ready(function () {
        $("#enrolled_employee_table").DataTable();
    });
</script>