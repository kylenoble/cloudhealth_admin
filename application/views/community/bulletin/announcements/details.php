<div class="modal-content">
    <ul class="collection">
        <li class="collection-item">
            <label>Title: </label>
            <p><?php echo $data['title'];?></p>
        </li>
        <li class="collection-item">
            <label>Recipient(s): </label>
            <p>
                <?php echo ((int)$data['audience'] > 0 ? $this->Users_model->getCompanyNameByID((int)$data['audience']) : $data['audience'])?>
            </p>
        </li>
        <li class="collection-item">
            <label>Details: </label>
            <p>
                <pre><?php echo $data['details']; ?></pre>
            </p>
        </li>
        <li class="collection-item">
            <label>Posted: </label>
            <p>
                <?php $admin = $this->Users_model->get_userdetails(array('id' => $data['created_by'] )); echo '<label class="rank">by ', $admin['name'], 'on ', date('M j, Y h:i A', strtotime($data['date_created'])) , '</label>'?>
            </p>
        </li>
    </ul>
</div>
<div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
</div>