<style>
    .dataTables_wrapper{
        margin: 0 !important;
    }
    .open-details:hover{
        cursor: pointer;
        color: #00bcd4;
    }
</style>
<?php
$my_userid = $this->session->userdata['logged_in']['userid'];
$role = admin_role();
$user_is_HR = $role == Users_model::HR;
$user_is_Admin = $role == Users_model::ADMIN;
?>

<?php if (isset($my_announcements)) : ?>
    <div class="title">MY ANNOUNCEMENTS</div>
    <?php if ($this->session->flashdata('announcement_add_msg') != '') {
        ?>
        <div class="alert alert-info alert-dismissable" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
        <?php echo $this->session->flashdata('announcement_add_msg'); ?>
        </div>
        <?php
    }
    ?>
    <?php if ($this->session->flashdata('announcement_add_error') != '') {
        ?>
        <div class="alert alert-warning alert-dismissable" style="margin: 0;visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
        <?php echo $this->session->flashdata('announcement_add_error'); ?>
        </div>
        <?php
    }
    ?>

    <div class="input-group-btn right">
        <a class="btn btn-info add_subscription active page_ajaxify" id="add-program-btn" data-callback="community/announcement_new" parent-callback="community/announcement_new/" href="javascript:void(0)">Create Announcement</a>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <table id='announcement_table' class="stripe row-border order-column" width="100%" cellspacing="0">
                <thead>
                    <tr>   
                        <th>Title</th>
                        <th>Audience</th>
                        <th>Created By</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $edit = '';
                    $delete = '';
                    foreach ($my_announcements as $announcement) {
                        $edit = '<a href="/community/announcement_edit/' . $announcement['id'] . '" class="btn-default btn page_ajaxify" data-callback="/community/announcement_edit/' . $announcement['id'] . '" parent-callback="/community/announcement_edit/' . $announcement['id'] . '" >EDIT</a>';
                        $delete = '&nbsp;<a class="btn-default btn" id="delete-my-announcement_' . $announcement['id'] . '" >DELETE</a>';
                        ?>
                        <tr>
                            <td data-eid="x-<?php echo $announcement['id'];?>" style='text-align: center' class="open-details" title="View Details"><?php echo $announcement['title']?></td>
                            <td>
        <?php echo ((int) $announcement['audience'] > 0 ? $this->Users_model->getCompanyNameByID((int) $announcement['audience']) : $announcement['audience']) ?>
                            </td>
                            <td><?php echo $announcement['name'] ?></td>
                            <td><?php echo $announcement['status'] ?></td>
                            <td style='text-align: center'>
                                <?php
                                if ($user_is_Admin) :
                                    if ($my_userid != $announcement['created_by']) {
                                        echo $delete;
                                    } else {
                                        echo $edit;
                                        echo $delete;
                                    } endif;
                                ?>
        <?php if (!$user_is_Admin && $my_userid == $announcement['created_by']) : echo $edit, $delete;
        endif; ?>
                            </td>
                        </tr>
    <?php } ?>
                </tbody>

            </table>

        </div>

    </div>
<?php endif; ?>

<?php if (isset($hr_announcements)) : ?>
    <div class="title">HR ANNOUNCEMENTS</div>
    <div class="row">
        <div class="col-sm-12">

            <table id='hr_announcements_table' class="stripe row-border order-column" width="100%" cellspacing="0">
                <thead>
                    <tr>   
                        <th>Title</th>
                        <th>Audience</th>
                        <th>Created By</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($hr_announcements as $announcement) { ?>
                        <tr>                                               
                            <td data-eid="x-<?php echo $announcement['id'];?>" style='text-align: center' class="open-details" title="View Details"><?php echo $announcement['title']?></td>
                            <td><?php echo ((int) $announcement['audience'] > 0 ? $this->Users_model->getCompanyNameByID((int) $announcement['audience']) : $announcement['audience']) ?></td>
                            <td><?php echo $announcement['name'] ?></td>
                            <td><?php echo $announcement['status'] ?></td>
                            <td style='text-align: center'>
                                <?php if ($user_is_Admin) :
                                    if (admin_role() != $announcement['created_by']) {
                                        ?>
                                        <a class="btn-default btn" id="delete-my-announcement_<?php echo $announcement['id']?>" >DELETE</a>
                                    <?php } else { ?>
                                        <a href="/community/announcement_edit/<?php echo $announcement['id'] ?>" class="btn-default btn page_ajaxify" data-callback="/community/announcement_edit/<?php echo $announcement['id'] ?>" parent-callback="/community/announcement_edit/<?php echo $announcement['id'] ?>" >EDIT</a>
                            <?php } endif; ?>
                        <?php if ($user_is_HR) :
                            if (admin_role() == $announcement['created_by'] || (admin_role() != $announcement['created_by'])) {
                                ?>
                                        <a href="/community/announcement_edit/<?php echo $announcement['id'] ?>" class="btn-default btn page_ajaxify" data-callback="/community/announcement_edit/<?php echo $announcement['id'] ?>" parent-callback="/community/announcement_edit/<?php echo $announcement['id'] ?>" >EDIT</a>
            <?php }
        endif;
        ?>
                            </td>
                        </tr>
    <?php }
    ?>
                </tbody>

            </table>

        </div>

    </div>
<?php endif; ?>
<div id="announcement-modal" class="modal modal-fixed-footer"></div>
<script>
    $(document).ready(function () {
        $('#hr_announcements_table, #announcement_table').DataTable({
            scrollCollapse: true,
            scrollY: 400,
            scrollX: true
        });

        $('.modal').modal({
            dismissible: false
        });
        
        $("#hr_announcements_table tbody tr td.open-details, #announcement_table tbody tr td.open-details").on("click", function(){
            var tmp_eid = ($(this).data("eid")).split("-")
            var eid = tmp_eid[1]
            $.get(SITEROOT + '/community/getAnnouncementInfo/' + eid, function(html){
                $("#announcement-modal").html(html);
                $("#announcement-modal").modal("open")
            })
        })

        $("a[id^=delete-my-announcement_]").on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (confirm("Are you sure you want to delete the announcement?")) {
                var tmp_id = ($(this).attr("id")).split("_");
                $.post(SITEROOT + '/community/announcement_delete', {id: tmp_id[1]}, function (response) {
                    console.log(response)
                    if (response == 1) {
                        var data = {};
                        data.form_data = null;
                        <?php if($user_is_Admin) :?>
                        data.callback = 'community/index/bulletin';
                        <?php else:?>
                        data.callback = 'community/index/announcements';
                        <?php endif;?>
                        page_ajaxify(data);
                    }
                })
            }
        })

    })
</script>