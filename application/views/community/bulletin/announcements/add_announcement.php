<?php $role = admin_role(); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" type="text/css" rel="stylesheet">
<style>
    form{
        padding: 20px;
    }

    textarea {
        height: 100px;
    }

    label[for^="audience_"]{
        margin-right: 30px;
    }
</style>
<?php
$data_callback = '';
if ($role != Users_model::HR) {
    $data_callback = '/community/index/bulletin';
} else {
    $data_callback = '/community/index/announcements';
}

?>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="title"><?php echo $title; ?></div>
            <div class="container-fluid">
                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row">
                        <div class="col-sm-12">
                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">
                                <?php echo _error_message('danger'); ?>
                                <?php echo form_open(base_url() . 'services/' . $callback, array('id' => 'announcement_form')); ?>
                                <input type="hidden" name="f" id="f" value="<?php echo ($callback == 'announcement_update' ? $announcement_info['id'] : 0) ?>" />
                                <div class="form-group">
                                    <?php echo form_label('Title<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'title', 'name' => 'title', 'class' => 'form-control', 'value' => ($callback == 'announcement_update' ? $announcement_info['title'] : null))); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Details<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_textarea(array('id' => 'details', 'name' => 'details', 'class' => 'form-control', 'value' => ($callback == 'announcement_update' ? $announcement_info['details'] : null))); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Status', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php
                                        $statusList = array(
                                            'Active' => 'Active',
                                            'Expired' => 'Expired'
                                        );
                                        echo form_dropdown('status', $statusList, ($callback == 'announcement_update' ? $announcement_info['status'] : 'Active'), 'class="form-control select-2 select2-hidden-accessible"');
                                        ?>
                                    </div>
                                </div>
                                <?php if (($role == Users_model::ADMIN && $callback == 'announcement_add') || ($role == Users_model::ADMIN && $callback == 'announcement_update')): ?>
                                    <div class="form-group">
                                        <?php echo form_label('Select Audience <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label', 'style' => 'margin-top: 10px')); ?>
                                        <div class="col-sm-8" style="margin-top: 10px">
                                            <?php
                                            $audience = '';
                                            $audienceList = array();
                                            if ($callback == 'announcement_update') {
                                                $audienceList = explode(", ", $announcement_info['audience']);
                                            }
                                            ?>
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Patients', $audienceList) ? 'checked' : '') ?> value="Patients" name="audience[]" id="audience_px" /><label for="audience_px"> Patients</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Doctors', $audienceList) ? 'checked' : '') ?> value="Medical" name="audience[]" id="audience_md" /><label for="audience_md"> Medical Team</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('HR', $audienceList) ? 'checked' : '') ?> value="HR" name="audience[]" id="audience_hr" /><label for="audience_hr"> HR Users</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Sales', $audienceList) ? 'checked' : '') ?> value="Sales" name="audience[]" id="audience_sales" /><label for="audience_sales"> Sales Team</label><br />
                                            <input type="checkbox" <?php echo (!empty($audienceList) && in_array('Ops', $audienceList) ? 'checked' : '') ?> value="Ops" name="audience[]" id="audience_ops" /><label for="audience_ops"> Ops Team</label>
                                        </div>
                                    </div>
<?php endif; ?>
<?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <a href='javascript:void(0)' class="btn-primary btn announcement-save">Save</a>
                                                <a data-callback="<?php echo $data_callback ?>" class="btn-default btn page_ajaxify" >Cancel</a>
                                            </div>
                                        </div>  <!-- form group -->
                                    </div><!-- row -->
                                </div><!-- panel footer -->
                            </div><!-- panel -->
                        </div><!-- col 12 -->
                    </div><!-- add user -->
                </div><!-- ui s -->
            </div><!-- container -->
        </div>
        <!-- page-content -->
    </div>
    <!-- static-content -->
</div>
<!-- static-content-wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script>
    $(document).ready(function () {

        $(".announcement-save").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var data = {};
            data.callback = 'community/<?php echo $callback; ?>';
            data.form_data = null;
            data.form_data = jQuery("#announcement_form").serializeArray();

            var response = vip_ajax(data);
            console.log(response.result.errors)
            if (response.result.success == 1) {
                var data = {};
                data.form_data = null;
                <?php if(admin_role() == Users_model::ADMIN) :?>
                data.form_data = {page: 'announcements'};
                <?php endif;?>
                data.callback = '<?php echo $data_callback; ?>';
                page_ajaxify(data);
            } else {
                $(".alert-danger").removeClass('hidden');
                $(".alert-danger").html(response.result.errors);
            }
        })
    });
</script>