<div class="small-tabs-container" style="margin-bottom: 30px;">
    <div class="small-tabs active"><a onclick="showMyPage(this)" id="events">EVENTS</a></div>
    <div class="small-tabs"><a onclick="showMyPage(this)" id="announcements">ANNOUNCEMENTS</a></div>
</div>
<div id="subcontent-view"></div>
<script>
    $(document).ready(function(){
        showMyPage("<?php echo ($default_page ? '#'.$default_page : '#events'); ?>");
    });
    
    function showMyPage(obj){
        var url = null
        var page = $(obj).attr("id");
        
        if(page == "announcements"){
            url = SITEROOT + '/community/announcements';
        }else{
            url = SITEROOT + '/community/events/<?php echo $health_events_for_hr?>';
        }
//        show_waitMe(jQuery('body'));
        $.get(url, function (response) {
            
            $("div.small-tabs-container div.small-tabs").removeClass('active');
            $(obj).closest('div.small-tabs').addClass('active');
            hide_waitMe();
            $("#subcontent-view").html(response);
        });
    }
</script>