<style>
    #consults-modal{
        width: 95%;
        max-height: 90% !important;
        height: 100% !important;
        top: 5% !important;
    }
</style>
<table style="margin-left: 30px;" width="100%" cellspacing="0">
    <tbody>
        <tr>
            <td style="text-indent: 20px; width: 40%">Average consults per day: <span class="small-note"><?php echo $total_consults_per_day;?></span></td>
            <td style="vertical-align: middle; text-align: left;" rowspan="3">
                <?php if($with_data) :?>
                <a title="See Graphs" class="btn-default btn" id="a_viewGraph">See Graphs</a>
                <?php endif;?>
            </td>
        </tr>
        <tr>    
            <td style="text-indent: 20px; ">Average consults per month: <span class="small-note"><?php echo $total_consults_per_month;?></span></td>
        </tr>
        <tr>    
            <td style="text-indent: 20px; ">Average number of unique users per month: <span class="small-note"><?php echo $unique_users_per_month;?></span></td>
        </tr>
    </tbody>
</table>
<div id="consults-modal" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.modal').modal({
        dismissible: false
    });
    
    $("#a_viewGraph").click(function(){
        show_waitMe(jQuery('body'));
        $.get(SITEROOT + '/community/consults_graphs', function(html){
            $("#consults-modal .modal-content").html(html);
            $("#consults-modal").modal("open");
            hide_waitMe();
        })
    })

})
</script>