<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
} else {
    header("location: login");
}
?>
<style>
    ul.myTabs li {
        width: 50%;
    }
    
    /* adjusts second modal's opacity'*/
    .modal-overlay ~ .modal-overlay{
        opacity: 0 !important;
    }
</style>
<div class="static-content-inner">

    <div class="static-content" style="margin-bottom: 100px;">
        <ul class="myTabs">
            <li class="active"><a onclick="showPage(this)" id="analytics">ANALYTICS</a></li>
            <li><a onclick="showPage(this)" id="bulletin">COMMUNITY BULLETIN</a></li>
        </ul>
        <div class="page-content" id="content_view"></div>
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    var default_page = "<?php echo ($default_page ? '#' . $default_page : '#analytics'); ?>";
    $(document).ready(function () {
        showPage(default_page);
        
        $(document).on("click", "div#events-modal table tbody tr td a", function(){
            showPage("#bulletin");
            $(".modal-overlay").remove();
            $("body").removeAttr('style');
        })
    });

    function showPage(obj) {
        var page = $(obj).attr("id");
        var url = SITEROOT + '/community/' + page;
        show_waitMe(jQuery('body'));
        $.get(url, function (response) {
            $("ul.myTabs li").removeClass('active');
            $(obj).closest('li').addClass('active');
            hide_waitMe();
            $("#content_view").html(response);
        });
    }

</script>