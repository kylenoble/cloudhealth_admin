<div class="row">
    <div class="col-sm-12">

        <table id='events_table' class="stripe row-border order-column" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Event Name</th>
                    <th>Starts</th>
                    <th>Ends</th>
                    <th>Created By</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $edit = '';
                $delete = '';
                foreach ($events as $event) { 
                    $start_date = $event['start_date'];
                    $start_time = $event['start_time'];
                    $end_date = $event['end_date'];
                    $end_time = $event['end_time'];
                    $type = 'My Events';
                    if($event['health_talk'] != null){
                        $type= 'Basic Program: Health Talks';
                    }else{
                        if($event['created_by'] != $this->session->userdata['logged_in']['userid']){
                            $type = 'HR Events';
                        }
                    }
                    ?>
                    <tr>
                        <td><?php echo $type?></td>
                        <td data-eid="event-<?php echo $event['id'];?>" style='text-align: center; white-space:normal' class="open-details" title="View Details"><?php echo $event['event_name']?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$start_date $start_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$start_date $start_time")); ?>
                        </td>
                        <td style='text-align: center' data-order="<?php echo strtotime("$end_date $end_time");?>">
                            <?php echo date('M j, Y h:i A', strtotime("$end_date $end_time")); ?>
                        </td>
                        <td>
                            <?php $admin = $this->Users_model->get_userdetails(array('id' => $event['created_by'])); 
                            echo $admin['name']; ?>
                        </td>
                        <td style='text-align: center'>
                            <a href="Javascript:void(0)">View/Edit details</a>
                        </td>
                    </tr>
<?php } ?>
            </tbody>

        </table>

    </div>

</div>
<script>
    $(document).ready(function () {
        var groupColumn = 0;
        var table = $('#events_table').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            }
        })

        // Order by the grouping
        $('#events_table tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
        
    })
</script>