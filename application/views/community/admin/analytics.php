<style>
    .modal-content{
        padding: 10px !important;
    }
    
    #events-modal{
        width: 950px;
    }
</style>
<div class="title small-tabs active no-hover">EVENTS</div>
<table style="margin-left: 30px;" width="100%" cellspacing="0">
    <tbody>
        <tr>
            <td style="text-indent: 20px; width: 40%">Number of Events: <span class="small-note"><?php echo $no_of_events?></span></td>
            <td style="vertical-align: middle; text-align: left;" rowspan="2">
                <a title="See List of Events" class="btn-default btn" id="a_eventlist">See List of Events</a>
            </td>
        </tr>
        <tr>    
            <td style="text-indent: 20px; ">Average number of attendees per event: <span class="small-note"><?php echo $avg_attendees?></span></td>
        </tr>
    </tbody>
</table>

<div class="title small-tabs active no-hover">MARKETPLACE</div>
<table style="margin-left: 30px;" width="100%" cellspacing="0">
    <tbody>
        <tr>
            <td style="text-indent: 20px; width: 40%">Number of Daily transactions: 0</td>
            <td style="vertical-align: middle; text-align: left;" rowspan="2">
                <a title="See List of Events" class="btn-default btn" id="a_marketplaceTxns">See summary of transactions</a>
            </td>
        </tr>
        <tr>    
            <td style="text-indent: 20px; ">Total Monthly transactions: 0</td>
        </tr>
    </tbody>
</table>

<div class="title small-tabs active no-hover">CONSULTS</div>
<div id="consults-div"></div>

<div id="events-modal" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>

<script>
$(document).ready(function(){
    $('.modal').modal({
        dismissible: false
    });
    
    $("#a_eventlist").click(function(){
        show_waitMe(jQuery('body'));
        $.get(SITEROOT + '/community/eventsList', function(html){
            $("#events-modal .modal-content").html(html)
            $("#events-modal").modal("open")
            hide_waitMe();
        })
    })
    
    $("#a_marketplaceTxns").click(function(){
        alert("This feature is not yet available")
    })
    
    $("#consults-div").load(SITEROOT + '/community/getConsultsSummary');
})
</script>