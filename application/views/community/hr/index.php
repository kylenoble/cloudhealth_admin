<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
} else {
    header("location: login");
}
?>
<style>
    ul.myTabs {
        margin-left: 10px;
    }
</style>
<div class="static-content-inner">

    <div class="static-content" id="services" style="margin-bottom: 100px;">
        <ul class="myTabs">
            <li class="active"><h5>COMMUNITY BULLETIN</h5></li>
        </ul>
        <div class="page-content" id="content_view"></div>
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    $(document).ready(function () {
        var url = SITEROOT + '/community/bulletin';
        var page = '<?php echo $default_page ? $default_page : 'events'; ?>'
        show_waitMe(jQuery('body'));
        $.post(url, {page: page}, function (response) {
            hide_waitMe();
            $("#content_view").html(response);
        });
    });
</script>