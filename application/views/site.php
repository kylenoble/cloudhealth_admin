<style>
    select.select2-hidden-accessible.initialized{
        display: none;
    }
    
    div.select-wrapper.form-control.select-2.select2-hidden-accessible.initialized{
        display: inline-block;
        margin-left: 15px;
        width: auto;
    }
    
    div.c h6{
        float: left;
        margin-left: 20px;
        text-transform: uppercase;
        font-weight: bold;
    }
    .modal{
        width: 85% !important;
    }

    .show-graph{
        background: url(assets/images/search_icon.png) center center no-repeat;
        background-size: auto;
        display: inline-block;
        background-size: 25px;
        width: 40px;
    }
</style>
<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
} else {
    header("location: login");
}
?>
<!-- START CONTENT -->
<section id="content">
    <!--start container-->
    <div class="container container-fluid" style="margin-top: 20px; padding: 0; margin-left: 0;">
        <div class="title small-tabs active no-hover">ANALYTICS</div>
        
        <div class="row">
            <div class="col s12">
                <div class="col s5">
                    <table class="striped center" style="margin: 10px !important; background: #FFF;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Active</th>
                                <th>Deactivated</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Companies</td>
                                <td class="center"><?php echo $companies['active_cnt'];?></td>
                                <td class="center"><?php echo $companies['inactive_cnt'];?></td>
                            </tr>
                            <tr>
                                <td>Doctors</td>
                                <td class="center"><?php echo $doctors['active_cnt'];?></td>
                                <td class="center"><?php echo $doctors['inactive_cnt'];?></td>
                            </tr>
                        </tbody>
                    </table>

                    <h6 class="sub-title hidden">MARKETPLACE</h6>
                    <p class="hidden">
                        <label>- Total transacions to date (frequency and amount)</label>
                        <strong></strong>
                        <br />
                        <label>- Daily average transactions (frequency and amount)</label>
                        <strong></strong>
                        <br />
                        <label>- Transactions per month (weekly trends), filter by month</label>
                        <strong></strong>
                        <br />
                        <label>- Top products bought (top 10)</label>
                        <strong></strong>
                    </p>
                </div>
                <div class="col s7">
                    <div id="chart_div"></div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 20px">
            <table id="active-companies" class="striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Consent</th>
                        <th>Enrolled</th>
                        <th>System Login</th>
                        <th>Dashboard Login</th>
                        <th>Health Survey<br />Compliance</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="title small-tabs active no-hover">USER SUPPORT TICKETS</div>
        <div class="row" style="margin-left: 20px">
            <div id="tickets"></div>
        </div>
    </div>
    <div id="consent-report-modal" class="modal modal-fixed-footer">
        <div class="modal-content"></div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
        </div>
    </div>
</section>
<!-- END CONTENT -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    $(document).ready(function(){
        var tmp_url = (window.location.href).split("#");
        var is_not_dashboard = (tmp_url.length > 1 && tmp_url[1] !== "" || false);
        if(!(is_not_dashboard)){
            showData()
            showGraph()
            //show tickets
            $("#tickets").load(SITEROOT + '/dashboard/getTickets')
            
            $('.modal').modal({
                dismissible: false
            });
        }
        
        $(document).on('click', "#tickets-modal .modal-close", function(){
            if($(this).hasClass("refresh")){
                //refresh tickets
                $("#tickets").load(SITEROOT + '/dashboard/getTickets')
            }
        })
        
        $("#active-companies").on('click', '.show-consent', function(){
            show_waitMe($('body'));
            var company_id = ($(this).attr("data-company_id"));
            showReport(company_id);
        })
    });
    
    function refreshGraph(obj){
        show_waitMe($('#chart_div'));
        var company_name = ($(obj).attr("data-company"));
        showGraph(company_name);
    }
    
    function showReport(company_id){
        $.get(SITEROOT + '/healthprofile/getConsentList/'+company_id, function(response){
            $('#consent-report-modal .modal-content').html(response);
            $('#consent-report-modal').modal('open');
            hide_waitMe();
        })
    }
    
    function showGraph(company){
        $.get(SITEROOT + '/dashboard/getLoginAnalyticsGraph/'+company, function(response){
            var data = [];
            var graphData = $.parseJSON(response);
            data.push(['Created Date', 'Login Count']);
            $.each(graphData, function(key, value){
                data.push([key, value]);
            });
            var chart_data = google.visualization.arrayToDataTable(data);
            var options = {
                vAxis: {minValue: 0, ticks: calcIntTicks(chart_data, 1)},
                legend: 'none',
                width: '100%',
//                height: 250,
                title: 'SYSTEM LOG-INS (last 10 days)',
                chartArea: {'width': '80%', 'height': '70%'},
                pointsVisible: true
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(chart_data, options);

            $("text:contains(" + options.title + ")").attr({'x':'200', 'y':'20'})
            
            hide_waitMe();
        })
    }
    
    function showData(){
        $("#active-companies").DataTable({
            "order": [[1, "asc"]],
            "bFilter": true,
            "bLengthChange": false,
            dom: '<"c">lfrtip',
            "language": {
                "loadingRecords": "&nbsp;",
                "processing": "Loading..."
            },
            processing: true,
            ajax: {
                url: SITEROOT + '/dashboard/getAdminAnalyticsData',
                dataType: 'json',
                type: 'GET'
            },
            "columnDefs": [
                {"className": "dt-center", "targets": [1, 2, 3, 4, 5]},
                {
                    "targets": 0,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return row.company_name;
                    }
                },
                {
                    "targets": 1,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return '<a class="show-consent" title="View report" data-company_id="'+row.company_id+'">  <strong>View report</strong></a>';
                    }
                },
                {
                    "targets": 2,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return row.total_enrolled;
                    }
                },
                {
                    "targets": 3,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return row.total_unique_system_login + '<a class="show-graph" onclick="refreshGraph(this)" title="View last 10 days data on graph" data-company="'+row.company_name+'">&nbsp;&nbsp;</a>';
                    }
                },
                {
                    "targets": 4,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return row.total_dashboard_logins;
                    }
                },
                {
                    "targets": 5,
                    "visible": true,
                    data: function(row, type, val, meta){
                        return row.compliance_rate+'%';
                    }
                }
            ]
        })
        $(".c").html("<h6>Active Companies</h6>");
    }
    
    function calcIntTicks(dataTable,step) {
        var min = Math.floor(dataTable.getColumnRange(1).min);
        var max = Math.ceil(dataTable.getColumnRange(1).max);
        var vals = [];
        for (var cur = min; cur <= max; cur+=step) {
            vals.push(cur);
        }
        return vals;
    }
</script>