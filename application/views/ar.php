<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Receipt</title>

        <!-- Normalize or reset CSS with your favorite library -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

        <!-- Load paper.css for happy printing -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

        <!-- Set page size here: A5, A4 or A3 -->
        <!-- Set also "landscape" if you need -->
        <style>@page { size: legal landscape }</style>

        <!-- Custom styles for this document -->
        <link href='https://fonts.googleapis.com/css?family=Bree+Serif|Patua+One' rel='stylesheet' type='text/css'>
        <style>
            body   { font-family: 'Bree Serif' }
            h1     { font-family: 'Patua One', cursive; font-size: 40pt; line-height: 18mm}
            h2     { font-family: 'Patua One', cursive; font-size: 24pt; line-height: 7mm }
            /*h4     { font-size: 32pt; line-height: 14mm }*/
            h2 + p { font-size: 18pt; line-height: 7mm }
            h3 + p { font-size: 14pt; line-height: 7mm }
            li     { font-size: 11pt; line-height: 5mm }
            h1      { margin: 0 }
            h1 + ul { margin: 2mm 0 5mm }
            h2, h3  { margin: 0 3mm 3mm 0; float: left }
            h2 + p,
            h3 + p  { margin: 0 0 3mm 50mm }
            /*    h4      { margin: 2mm 0 0 50mm; border-bottom: 2px solid black }
                h4 + ul { margin: 5mm 0 0 50mm }*/
            article {  padding: 2mm 5mm; }
            h4 { 
                font-family: 'Patua One'; 
                font-size: 18pt; 
                line-height: 7mm;
                margin: 0;
                text-align: center
            }

            p{
                margin-bottom: 2px;
                font-size: 17px;
                margin-top: 15px;
            }

            .number {
                font-family: 'Patua One'; font-size: 20pt; color: red
            }

            .date {
                line-height: 140px; 
                font-family: 'Bree Serif';
                height: 42px;
            }

            .name {
                float: left; vertical-align: middle;
                line-height: 8mm;
                font-size: 22pt;
                text-align: left;
                margin-left: 6px;
                margin-top: 28px;
                text-shadow: 2px 2px #ddd;
                /*letter-spacing: 2px;*/
            }

            .sheet.padding-5mm { 
                padding: 5mm;
                padding-top: 30px;
                width: 400mm !important;
                height: 300mm !important;
                margin-top: 0 !important;
            }

            div.ar-container:not(:first-child){
                margin-left: 29px;
                height: 50%;
            }

            .ar{
                outline: double;
                outline-offset: 10px;
                /*margin-bottom: 60px;*/
            }
            
            .ar-container{
                width: 32%;
                height: 100%;
                float: left;
            }
            
            .for{
                font-weight: bold;
                font-size: 18px;
                text-decoration: underline;
                letter-spacing: 2px;
            }
            
            img {
                width: 115px;
                float: left;
            }
            
            input[type=checkbox]{
                -moz-transform: scale(2);
            }
        </style>
    </head>

    <!-- Set "A5", "A4" or "A3" for class name -->
    <!-- Set also "landscape" if you need -->
    <body class="legal landscape">

        <!-- Each sheet element should have the class "sheet" -->
        <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
        <section class="sheet padding-5mm">
            <div class="ar-container">
                <div class="ar" style="margin-bottom: 70px;">
                    <div style="float: left">
                        <img src="https://scontent.fmnl14-1.fna.fbcdn.net/v/t1.15752-9/39969581_1751960878187127_8201053100375539712_n.jpg?_nc_cat=0&oh=c3caad9c0f3a1a2a93e46a03edb9d354&oe=5BFFB8EB" alt="LOGO HERE" />
                        <h4 class="name">The Legian 2C <br /><span>Volunteers</span></h4>
                    </div>
                    <div style="float: right">
                        <div style="float: right; font-size: 20px;">No. <span class="number">00<?php echo $start == null ? '151' : $start;?></span></div>
                        <div style="clear: both"></div>
                        <div class="date"> ____/____/20____</div>
                    </div>
                    <div style="clear: both"></div>
                    <hr>
                    <h4>Acknowledgement Receipt</h4>
                    <article>
                        <div style="text-align: right" >
                        <input type="checkbox">&nbsp; &nbsp;Homeowner &nbsp; &nbsp;&nbsp; &nbsp;
                        <input type="checkbox">&nbsp; &nbsp;Renter
                        </div>
                        <p>Received from _____________________________________</p>
                        <p>of Block ____ Lot ____ Street ____ of The Legian 2C Subd.</p>
                        <p>the amount of _____________________________ (&#x20B1______)</p>
                        <p>as payment for <span class="for">&nbsp;&nbsp;SECURITY SERVICES&nbsp;&nbsp;</span>.</p>
                        
                        <div style="width: 50%; float: right; ">
                            <p>Received By:</p>
                            <p style="margin-bottom: 0;">__________________________</p>
                            <span style="font-size: 10px; display: block; text-align: center">Street Leader Volunteer</span>
                            <div style="clear: both"></div>
                        </div>
                        <div style="clear: both"></div>
                    </article>
                    <div style="clear: both"></div>
                </div>

                <div class="ar" style="outline:double; outline-offset:10px;  ">
                    <div style="float: left">
                        <img src="https://scontent.fmnl14-1.fna.fbcdn.net/v/t1.15752-9/39969581_1751960878187127_8201053100375539712_n.jpg?_nc_cat=0&oh=c3caad9c0f3a1a2a93e46a03edb9d354&oe=5BFFB8EB" alt="LOGO HERE" />
                        <h4 class="name">The Legian 2C<br />Volunteers</h4>
                    </div>
                    <div style="float: right">
                        <div style="float: right; font-size: 20px;">No. <span class="number">00<?php echo $start == null ? '151' : $start;?></span></div>
                        <div style="clear: both"></div>
                        <div class="date"> ____/____/20____</div>
                    </div>
                    <div style="clear: both"></div>
                    <hr>
                    <h4>Acknowledgement Receipt</h4>
                    <article>
                        <div style="text-align: right" >
                        <input type="checkbox">&nbsp; &nbsp;Homeowner &nbsp; &nbsp;&nbsp; &nbsp;
                        <input type="checkbox">&nbsp; &nbsp;Renter
                        </div>
                        <p>Received from _____________________________________</p>
                        <p>of Block ____ Lot ____ Street ____ of The Legian 2C Subd.</p>
                        <p>the amount of _____________________________ (&#x20B1______)</p>
                        <p>as payment for <span class="for">&nbsp;&nbsp;SECURITY SERVICES&nbsp;&nbsp;</span>.</p>

                        <div style="width: 50%; float: right; ">
                            <p>Received By:</p>
                            <p style="margin-bottom: 0;">__________________________</p>
                            <span style="font-size: 10px; display: block; text-align: center">Street Leader Volunteer</span>
                            <div style="clear: both"></div>
                        </div>
                        <div style="clear: both"></div>
                    </article>
                    <div style="clear: both"></div>
                </div>
<div style="clear: both"></div>
            </div>
        </section>
        <div style="clear: both"></div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    </body>

</html>
<script>
    $(document).ready(function () {
        var ar = $(".ar-container")
        var i = <?php echo $start == null ? '152' : $start+1;?>;
        var len = i+1
        for(i; i <=len; i++){
            $(ar).clone().appendTo("section").find(".number").text(pad(i, 5))
        }
        var sec = $("section")
        for(i; i <=<?php echo $start+600 ?>; i){
            $(sec).clone().appendTo("body").find(".ar-container").each(function(){
                $(this).find(".number").text(pad(i, 5))
                i++;
            })
        }
    })
    
    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
</script>