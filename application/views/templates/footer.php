<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jqueryui-1.10.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.print.min.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" ></script>-->
<!-- Load jQueryUI -->
<style>
    .css-1pk4x4d{
        flex: 1 0 100%;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        -moz-box-pack: center;
        -moz-box-align: center;
        display: flex;
        height: 55px;
        margin-top: 20px;
        border-top: 10px solid rgb(54, 69, 99);
        border-bottom: 5px solid rgb(54, 69, 99);
        background: rgb(82, 97, 127) none repeat scroll 0% 0%;
        color: white;
        position: fixed;
        width: 100%;
        bottom: 0;
        right: 0;
        z-index: 999;
    }

    .css-1apnw9v{
        flex-wrap: wrap;
        flex: 1 0 100%;
        justify-content: center;
        -moz-box-pack: center;
        display: flex;
        font-size: x-small;
        margin: 0px;
    }

    .css-1duc7ap{
        cursor: pointer;
        padding: 0px 3px;
        color: rgb(255, 255, 255);
        margin: 0px 10px;
        font-size: x-small;
    }

    .css-1duc7ap:hover{
        border-radius: 10px;
        padding: 0 3px 0 3px;
        border: 1px solid #3498db;
        color: #81cce7;
    }

    .modal{
        width: 65%
    }

    .modal h3{
        font-size: 16px;
        font-weight: bold;
    }

    .modal ul li {
        list-style-type: circle;
        margin-left: 20px;
    }

    .modal .lists{
        margin-bottom: 20px;
    }
    
    .modal .modal-content{
        padding: 40px;
    }

</style>

<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START FOOTER -->
<div class="css-1pk4x4d <?php echo (isset($is_hidden) && $is_hidden ? 'hidden' : '') ?>">
    <div class="css-1apnw9v">
        <span class="css-1duc7ap modal-trigger" href="#privacy_modal">Privacy Policy</span>
        <span class="css-1duc7ap modal-trigger" href="#terms_modal">Terms of Use</span>
        <span class="css-1duc7ap">Contact</span>
    </div>
    <div class="css-1apnw9v">© 2019 admin.cloudhealthasia.com</div>
</div>
<!-- END FOOTER -->

<div id="privacy_modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <?php $this->load->view('privacy_policy');?>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn">Close</a>
    </div>
</div>
<div id="terms_modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <?php $this->load->view('terms_of_use');?>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn">Close</a>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.modal').modal();
        
        $(document).off().on('click', "a.page_ajaxify", function(e) {
            // stop the event or navigation
            e.preventDefault();
            e.stopPropagation();

            // create object with required data
            var data = {};
            data.callback = $.trim($(this).attr('data-callback'));
            data.form_data = {};

            // call the ajax function to show the page content
            page_ajaxify(data);
          })
    })
</script>
</body>
</html>