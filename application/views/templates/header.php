<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cloudhealth Asia Admin</title>

        <link href="<?php echo base_url(); ?>assets/css/themes/collapsible-menu/materialize.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/themes/collapsible-menu/style.css" type="text/css" rel="stylesheet">

        <link type="text/css" href="<?php echo base_url(); ?>assets/css/waitMe.css" rel="stylesheet">
        <!-- Custom CSS-->
        <link href="<?php echo base_url(); ?>assets/css/themes/collapsible-menu/custom.css" type="text/css" rel="stylesheet">
        <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
        <link href="<?php echo base_url(); ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">

        <link type="text/css" href="<?php echo base_url(); ?>assets/fonts/themify-icons/themify-icons.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">
        <link rel="stylesheet" media="screen and (max-width: 600px)" href="<?php echo base_url(); ?>assets/css/smallscreen.css">
        <link rel="stylesheet" media="screen and (min-width: 900px)" href="<?php echo base_url(); ?>assets/css/widescreen.css">
        <script>
            var SITEROOT = "<?php echo base_url(); ?>";
            var COMPANY_NAME = "<?php echo isset($company_name) ? $company_name : ''; ?>";
        </script>

        <link href="<?php echo base_url(); ?>assets/vendors/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/themes/datatables/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
    </head>
    <body class="animated-content dashboard-header">
        <!-- Start Page Loading -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <!-- End Page Loading -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START HEADER -->
        <header id="header" class="page-topbar">
            <!-- start header nav-->
            <div class="navbar-fixed">
                <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">
                    <div class="nav-wrapper">
                        <ul class="right hide-on-med-and-down">
                            <li>
                                <a class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                                    <span class="avatar-status avatar-online">
                                        <?php
                                        $username = ($this->session->userdata['logged_in']['name']);
                                        echo $username;
                                        ?>
                                        <i class="material-icons">keyboard_arrow_down</i>
                                    </span>
                                </a>
                            </li>

                        </ul>

                        <!-- profile-dropdown -->
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li>
                                <a href="<?php echo base_url() ?>/change_password" class="grey-text text-darken-1">
                                    <i class="material-icons">settings</i> Change Password</a>
                            </li>
                            <?php if($this->session->userdata['logged_in']['role'] == Users_model::MD):?>
                            <li>
                                <a class="grey-text text-darken-1 switch-dashboard">
                                    <i class="material-icons">swap_horiz</i> Switch to MD Dashboard</a>
                            </li>
                            <?php endif;?>
                            <li>
                                <a href="<?PHP echo base_url('login/logout'); ?>" class="grey-text text-darken-1">
                                    <i class="material-icons">keyboard_tab</i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <!-- END HEADER -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START MAIN -->
        <div id="main">
            <!-- START WRAPPER -->
            <div class="wrapper">

                <!-- ================================================
              Scripts
              ================================================ -->
                <script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/jquery-3.2.1.min.js"></script>
                <!--materialize js-->
                <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/materialize.min.js"></script>
                <!--scrollbar-->
                <script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
                <!--plugins.js - Some Specific JS codes for Plugin Settings-->
                <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugins.js"></script>

                <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/waitMe.min.js" ></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" ></script>
                <?php 
                $email = $this->session->userdata['logged_in']['email'];
                $user = $this->Users_model->getUserDataByEmail($email);
                ?>
                <script>
                    $(document).ready(function(){
                        var x = '<?php echo base64_encode(base64_encode($user['admin_access_token']). '&&1c'.time());?>'
                        $(".switch-dashboard").on("click", function(e){
                            show_waitMe(jQuery('body'));
                            e.preventDefault();
                            window.location.href = '<?php echo CHA_URL?>/login?' + encodeURIComponent(x);
                        })
                    })
                </script>
                </body>
                </html>