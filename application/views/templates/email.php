<?php
$content = '';
switch($page){
    case 'forgot_password':
        $content = 
            '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                <tbody>
                    <tr>
                        <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;font-weight:bold;margin-bottom: 30px;">Dear '.$name.',</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Warm greetings from CloudHealthAsia!</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">This confirms approval of your request to reset the password of your CloudHealthAsia account.</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top: 10px;font-weight:600;margin-bottom:10px;">Your New Password: '.$password.'</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">If you did not make this request, please report this immediately at <font style="color:#00bcd4">client.services@cloudhealthasia.com</font>.</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                <br>Thank you for trusting us.<br><br>
                                Sincerely,<br><br>
                                CloudHealthAsia Team
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>';
        break;
    
    case 'consent':
        $content = $this->load->view('templates/consent_email', $data, true);
        break;
    
    case 'welcome_admin';
        if(isset($with_nda)){
            $oic1 = $oic <> null ? ' ('. $oic . ')' : '' ;
            $content = 
                '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                    <tbody>
                        <tr>
                            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Warm greetings from CloudHealthAsia!</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Thank you for subscribing to our corporate health indexing service.</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Please find attached the non-disclosure agreement (NDA) for your review. Kindly send the signed NDA via email to your Account Officer-in-Charge'.$oic1.' in order to activate your subscription.</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">For questions and clarifications, you may contact support (<font style="color:#00bcd4">client.services@cloudhealthasia.com</font>) or call us at +632-828-LIFE (5433)</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                    <br>Thank you for trusting us.<br><br>
                                    Sincerely,<br><br>
                                    CloudHealthAsia Team
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>';
        }else{
            $content = 
                '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                    <tbody>
                        <tr>
                            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Warm greetings from CloudHealthAsia!</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Thank you for subscribing to our corporate health indexing service.</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">You may now log-in at <font style="color:#00bcd4">'.BASE_URL.'</font> using the following credentials:</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top: 10px;font-weight:600;margin-bottom:10px;">
                                    Username: '.$email.'<br />
                                    Password: '.$password.'
                                </p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">You will be asked to change your password upon logging-in. Afterwhich, you may start updating your company profile and manage your company dashboard. </p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">For questions and clarifications, you may contact support (<font style="color:#00bcd4">client.services@cloudhealthasia.com</font>) or call us at +632-828-LIFE (5433)</p>
                                <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                    <br>Thank you for trusting us.<br><br>
                                    Sincerely,<br><br>
                                    CloudHealthAsia Team
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>';
        }
        break;
    
    case 'welcome_user':
        $content = 
            '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                <tbody>
                    <tr>
                        <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Warm greetings from CloudHealthAsia!</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Your road to optimum health is just a few clicks away. Begin your journey by logging on to our website at <a href="'.CHA_URL.'/login">'.CHA_URL.'</a> with the following credentials:</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top: 10px;font-weight:600;margin-bottom:10px;">
                                Username: '.$email.'<br />
                                Password: '.$password.'
                            </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">You will be asked to change your password upon logging in. </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                <br>Thank you for trusting us.<br><br>
                                Sincerely,<br><br>
                                CloudHealthAsia Team
                            </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:13px;font-style:italic;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                Note: You are receiving this email because your company enrolled you to this health service. Please do not reply to this auto-generated email. For inquiries and help on our services, please go to our website ('.CHA_URL.') and check out the FAQs/Help & Support page. Thank you.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>';
        break;
    
    case 'welcome_medical_practitioner':
        $content = 
            '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                <tbody>
                    <tr>
                        <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Welcome to CloudHealthAsia!</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Thank you for partnering with us in helping clients achieve their optimum health.</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Access your account by logging-in to our website at '.CHA_URL.' with the following credentials:</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top: 10px;font-weight:600;margin-bottom:10px;">
                                Username: '.$email.'<br />
                                Password: '.$password.'
                            </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">You will be asked to change your password upon logging-in.</p>
                            <br>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Again, welcome to our team! Let\'s keep working towards a healthier community, one client at a time. </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                <br><br>
                                Sincerely,<br><br>
                                CloudHealthAsia Team
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>';
        break;
    
    case 'notification':
        $content = 
            '<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                <tbody>
                    <tr>
                        <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left">
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Greetings from CloudHealthAsia!</p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:20px;font-weight:normal">Please see below notification for your account: </p>
                            <div style="font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;padding:20px; margin:20px 0; border: 1px solid #cccccc; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px, rgba(0, 0, 0, 0.3) 0px 5px 5px -3px">'.$notification_message.'</div>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">Please log-in to your account for further details. </p>
                            <p style="text-align:left;color:#333333;font-family:Montserrat, \'Open Sans\', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;margin-top:10px;font-weight:normal">
                                <br>Thank you.<br><br>
                                Sincerely,<br><br>
                                CloudHealthAsia Team
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>';
        break;
}

?>
<div style="width:100%!important;max-width:740px!important;text-align:center;margin:0 auto">
    <table width="740" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#ffffff;margin:0 auto;text-align:center;border:none;width:100%!important;max-width:740px!important">
        <tbody>
            <tr>
                <td width="90%" valign="top">
                    <div style="border:1px solid #ccc;">
                        <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin-left:auto;margin-right:auto;margin-top:25px;margin-bottom:15px" dir="ltr">
                            <tbody><tr>
                                    <td width="45%" bgcolor="#ffffff" style="text-align: left;">
                                        <a href="<?php echo CHA_URL;?>" style="text-decoration: none">
                                            <img src="<?php echo base_url()?>/assets/images/logo/cha_logo_with_text.png" alt="CloudHealthAsia" title="CloudHealthAsia" height="50" style="display:block;" border="0">
                                        </a>
                                    </td>
                                    <td width="55%" bgcolor="#ffffff"></td>
                                </tr>
                            </tbody>
                        </table>
                        <?php echo $content; ?>
                        <table border="0" cellspacing="0" cellpadding="25" width="100%" dir="ltr">
                            <tbody><tr>
                                    <td width="100%" bgcolor="#cccccc" style="text-align:center; padding: 10px;">
                                        <p style="color:#999999;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:10px;line-height:14px;margin-top:0;padding:0;font-weight:normal">
                                            This message (including any attachments) contains confidential information intended for a specific individual and purpose, and is protected by law. If you are not the intended recipient, you should delete this message. Any disclosure, copying, or distribution of this message, or the taking of any action based on it, is strictly prohibited.
                                        </p>
                                        <p style="color:#999999;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:12px;line-height:14px;margin:0;padding:0;font-weight:normal">
                                            © 2019 CloudHealthAsia. All rights reserved.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>