<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Cloudhealth Asia Admin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">

        <!--cloudhealth-->
        <link href="<?php echo base_url(); ?>assets/css/themes/collapsible-menu/materialize.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/themes/collapsible-menu/style.css" type="text/css" rel="stylesheet">
        <!-- Custome CSS-->
        <link href="<?php echo base_url(); ?>assets/css/layouts/page-center.css" type="text/css" rel="stylesheet">
        <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
        <link href="<?php echo base_url(); ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">

        <script>
            var SITEROOT = "<?php echo base_url(); ?>";
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js" ></script>
    </head>

    <body class="animated-content admin-login cyan">
        <header id="topnav" class="navbar navbar-fixed-top admin-header">
            <div class="logo-area">
                <div class="admin-logo "><a href="<?php echo base_url(); ?>"><img class="text-center img img-responsive" src="<?PHP echo $this->config->item('logo_url'); ?>"></a></div>
            </div><!-- logo-area -->
        </header>

