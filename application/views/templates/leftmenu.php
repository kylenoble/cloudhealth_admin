<?php
$page = $this->router->fetch_class();
$role = admin_role();
?>
<script>
    var url = (window.location.toString()).split("#");
    var page = 'dashboard';
    if (url.length > 1) {
        page = url[1];
        
        page = page.split("/");
        if(page.length > 1){
            page = page[0];
        }
    }

    
    $(document).ready(function () {
        $("ul#slide-out li ul li").each(function () {
            //  remove active
            $(this).removeClass("active")
            if ($(this).children("a").attr("href") == '#' + page) {
                $(this).addClass("active")
            }
        });

        $("ul#slide-out li ul li").on("click", function () {
            $("ul#slide-out li ul li").removeClass("active")
            $(this).addClass("active")
        })
        if('<?php echo $page?>' == 'dashboard' && page == 'dashboard'){
            $("#dashboard").closest("li").addClass("active")
        }
    });

</script>

<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <img width="180px" style="margin-left: 20px" src="<?php echo base_url(); ?>assets/images/logo/cha_logo_with_text.png" alt="CloudHealthAsia">
        </h1>
    </div>
    <ul id="slide-out" class="side-nav fixed leftside-navigation" style="background: rgb(211, 211, 211);">
        <li class="no-padding">
            <ul class="collapsible acc-menu" data-collapsible="accordion" style="margin-top: 50px;">
                <li class="bold <?php echo ($page == 'dashboard' || $page == '' ? 'active' : '') ?>">
                    <a class="waves-effect waves-cyan" id="dashboard" href="<?php echo base_url()?>dashboard">
                    </a>
                </li> 
                <?php
                if ($role == '2') {
                    ?>
                    <li class="bold" data-callback="healthprofile">
                        <a class="waves-effect waves-cyan" id="healthprofile" href="#healthprofile" >
                        </a>
                    </li>
                    <li class="bold" data-callback="community">
                        <a class="waves-effect waves-cyan" href="#community" id="community" >
                        </a>
                    </li>
                    <li class="bold">
                        <a class="waves-effect waves-cyan" id="account" href="#account">
                            <?php if(isset($with_notification) && in_array('account', $with_notification['page'])) :?>
                            <img src="assets/images/flag_notification.ico" style="width: 35px;" />
                            <?php endif;?>
                        </a>
                    </li>
                <?php
                }
                if ($role == '1') {
                    ?>
                    <li class="bold" data-callback="organizations">
                        <a class="waves-effect waves-cyan" href="#organizations" id="organizations">
                        </a>
                    </li>

                    <li class="bold" data-callback="CorporateHealthPrograms">
                        <a class="waves-effect waves-cyan" href="#CorporateHealthPrograms" id="corporate_health_programs">
                            <?php if(isset($with_notification) && in_array('corporate_health_programs', $with_notification['page'])) :?>
                            <img src="assets/images/flag_notification.ico" style="width: 35px;" />
                            <?php endif;?>
                        </a>
                    </li>

                    <li class="bold" data-callback="Community">
                        <a class="waves-effect waves-cyan" href="#Community" id="community">
                        </a>
                    </li>

                    <li class="bold" data-callback="services">
                        <a class="waves-effect waves-cyan" href="#Services" id="services">
                        </a>
                    </li>

                    <li class="bold" data-callback="BillingAndFinance">
                        <a class="waves-effect waves-cyan" href="#BillingAndFinance" id="billing">
                        </a>
                    </li>
            <?php } ?>
                    <li class="bold <?php echo ($page == 'notifications' || $page == '' ? 'active' : '') ?>">
                        <a class="waves-effect waves-cyan" id="notifications" href="#notifications">
                            <?php if(isset($with_notification) && in_array('notifications', $with_notification['page'])) :?>
                            <img src="assets/images/flag_notification.ico" style="width: 35px;" />
                            <?php endif;?>
                        </a>
                    </li>
            </ul>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
        <i class="material-icons">menu</i>
    </a>
</aside>
<!-- END LEFT SIDEBAR NAV-->