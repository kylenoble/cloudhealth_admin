<?php
$encoded_email = urlencode(base64_encode(base64_encode($email). '&&1c'.time()));
?>
<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%" style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
    <tbody>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;">
                Good day <strong><?php echo trim($name);?></strong>,
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:justify;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:10px;font-weight:normal">
                You have received this email because your company, <strong><?php echo $company_name;?></strong>, has subscribed to the Corporate Health Indexing service of CloudHealth Asia (CHA). CHA is an online corporate wellness platform that enables companies to help create a personalized, participative, predictive and preventive employee wellness programs. More importantly, this platform will help you monitor and take charge of your health!
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:justify;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:10px;font-weight:normal">
                For you to be able to enroll in CHA, we would need your consent for your Human Resource (HR) Department to provide us your following details: 
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;padding:0;padding-top: 10px;margin-left:20px;margin-bottom:10px;">
                1.) Full Name<br />
                2.) Date of birth (exact day, month and year)<br />
                3.) Gender<br />
                4.) Official designation<br />
                5.) Department
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left;color:#333333;font-family:Arial,Helvetica,sans-serif;margin:0px;padding:0;padding-top:10px;font-weight:normal;font-size:15px;">
                Please click on YES or NO below. Deadline to respond to this email is <strong><?php echo date('M d, l', strtotime('+1 weekdays'))?></strong>.
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="#ffffff" class="button_wrapper" style="text-align:center;font-family:Arial,Helvetica,sans-serif;font-size:15px;padding-top:25px; width: 100%">
                <div style="display: block; width: 100%; ">
                    <a style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:16px;margin-left:auto; margin-right:auto;padding:10px 20px;background-color: #00bcd4;text-decoration: none;border-radius: 4px;box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2); width: 30%; display: inline-block;" href="<?php echo CHA_URL?>/consent_confirmation?x=1&q=<?php echo $encoded_email;?>">YES</a>
                </div>
                <div style="display: block; width: 100%;font-size:13px; font-style: italic; text-align: center; margin-top: 5px;">I am interested to be enrolled in CloudHealth Asia (CHA) and I am allowing our Human Resource Department to share my full name, gender, date of birth, official designation and department with CHA to complete the enrollment.</div>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="#ffffff" class="button_wrapper" style="text-align:center;font-family:Arial,Helvetica,sans-serif;font-size:15px;padding-top:20px; width: 100%" >
                <div style="display: block; width: 100%; padding-top: 10px; ">
                    <a style="color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:16px;margin-left:auto; margin-right:auto;padding:10px 25px;background-color: #00bcd4;text-decoration: none;border-radius: 4px;box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2); width: 30%; display: inline-block;" href="<?php echo CHA_URL?>/consent_confirmation?x=0&q=<?php echo $encoded_email;?>">NO</a>
                </div>
                <div style="display: block; width: 100%;font-size:13px; font-style: italic; text-align: center; margin-top: 5px;">
                    I am not interested to be enrolled in CloudHealth Asia (CHA) and I am not allowing our Human Resource Department to share my full name, gender, date of birth, official designation and department with CHA to complete the enrollment. 
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:justify;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:10px;font-weight:normal">
                After enrollment, you will be given your own login information and access to your personal CHA dashboard. Upon logging in, you will need to set aside 10 to 20 minutes of your time to answer a health survey. A summary and overview of your health status will then appear on your personal dashboard. With your HR Department, we will guide you through the whole process and next steps. Should you have any questions or concerns, visit the FAQ section at <?php echo CHA_URL?> or contact your HR manager.
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:justify;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:10px;font-weight:normal" >
                For your reference, please see the attached files: “CloudHealthAsia Privacy Policy” and “CloudHealthAsia Terms of Use.”
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left;color:#333333;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:30px; padding-bottom: 10px;font-weight:normal">
                Thank you for trusting us.<br><br>
                    Sincerely,<br>
                    CloudHealthAsia Team
            </td>
        </tr>
    </tbody>
</table>