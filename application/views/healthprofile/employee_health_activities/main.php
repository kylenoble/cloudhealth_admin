<div class="s12 m12">
    <div class="title small-tabs active no-hover">Active Subscriptions</div>
    <div id="main-content"></div>    
    <div>
        <div class="title small-tabs active no-hover">Consults</div>
        <div id="consults-div"></div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#main-content").load(SITEROOT + '/healthprofile/active_subscriptions');   
    $("#consults-div").load(SITEROOT + '/community/getConsultsSummary/1');
});
</script>