<style>
    td{
        padding: 0;
    }
    .modal { 
        width: 90% !important ; 
        height: 85% !important ; 
    }  /* increase the height and width as you desire */

</style>
<table style="margin-left: 30px;" width="100%" cellspacing="0">
    <tbody>
        <?php foreach ($active_subscriptions as $subscription) : ?>
        <tr>
            <td colspan="3">
                <b style="display: inline-block; width: 33%;"><?php echo $subscription['subscription_name']; ?></b>
            </td>
        </tr>
        <tr>
            <td style="text-indent: 20px; width: 20%;">Number of Enrollees: <?php echo $subscription['enrolled']; ?></td>
            <td style="width: 20%;">Approved Enrollment: <?php echo $subscription['approved']; ?></td>
            <td style="vertical-align: middle;" rowspan="2">
                <a data-callback="/CorporateHealthPrograms/subscriptions_intervention/<?php echo $subscription['id'];?>/1" class="btn-default btn page_ajaxify">See Status of Program</a>
            </td>
        </tr>
        <tr>    
            <td style="text-indent: 20px; padding-bottom: 20px; width: 20%;">Declined Enrollment: <?php echo $subscription['declined']; ?></td>
            <td style="padding-bottom: 20px; ">Pending Enrollment: <?php echo $subscription['pending']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div id="intervention-modal" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>