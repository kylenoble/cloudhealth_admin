<style>
    .progress_upload { 
        position:relative; 
        width:100%; 
        border: 1px solid #ddd; 
        padding: 1px; 
        border-radius: 3px; 
        display: inline-block;
    }
    .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
    .percent { 
        position:absolute; 
        display:inline-block; 
        top:0px; 
        left:48%; 
        font-weight: bold;
        font-size: 14px;
    }
    .file-field {width: 50%}
    .progress {
        width: 50%; 
        display: inline-block;
        vertical-align: bottom;
        margin-left: auto;
        margin-right: auto;
    }
   
    .delivery-status{
        width: 100%;
        padding: 3px;
        font-size: 11px;
        color: #fff;
        text-align: center;
        font-weight: bold;
        margin-left: auto;
        margin-right: auto;
    }

    #email-sending-modal.modal, div#consent-list-modal, div#employee-enrollment-modal{
        max-height: 90% !important;
        height: 100% !important;
        top: 5% !important;
    }
    
    .modal-content {
        padding: 20px 40px !important;
    }
    
    div#consent-list-modal{
        width: 1200px;
    }
    
    div#employee-enrollment-modal{
        width: 100%;
    }
    
    .small-note.no-close{
        display: inline-block;
        text-align: left;
        width: 80%;
    }
    
</style>
<div id="company_registration" class="col s12">
    <div class="compay-registration col s12 mt-4">
        <table>
            <tbody>
                <?php if (!empty($this->session->flashdata('success_msg'))) { ?>
                    <tr>
                        <td>
                            <?php echo _error_message('success', $this->session->flashdata('success_msg')); ?>
                        </td>
                    </tr> 
                <?php } ?>
                <tr> 
                    <td>A. Business details</td>
                    <td>
                        <a class="waves-effect waves-light btn page_ajaxify" data-callback="/organizations/update/<?php echo $company_id ?>" parent-callback="organizations/index" href="<?php echo base_url() ?>/organizations/update/<?php echo $company_id ?>">Update</a>							
                    </td>
                </tr>
                <tr>
                    <td>
                        B. Consent to Enrollment
                        <br />
                        <a class="waves-effect " href="/assets/templates/Consent_to_enrollment_format.csv" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Download Template</a><br />
                        <a class="waves-effect consent-list" style="color: #00bcd4;font-weight: bold;font-size: 13px;">See Report</a>
                    </td>
                    <td>
                        <!--<div class='alert-info upload-message'></div>-->
                        
                        <form id="uploadfile" method="post" enctype="multipart/form-data">

                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>File</span>
                                    <input type="file" name='employee_consent_file' id='employee_consent_file'>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Choose Employee List Consent file">
                                </div>
                            </div>

                            <a class="waves-effect waves-light btn upload-employees-consent-file">Upload</a>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>C. Employee Enrollment (List and Tagging)</td>
                    <td>
                        <div class='alert-info upload-message'></div>
                        <form id="uploadimage" method="post" enctype="multipart/form-data">

                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>File</span>
                                    <input type="file" name='employee_file' id='employee_file'>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Choose Employee List and Tagging file">
                                </div>
                            </div>

                            <a class="waves-effect waves-light btn upload-employees">Upload</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="update-users" id="update-users" /><label for="update-users"> Update Existing</label>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>D. HMO Utilization Reports <span style="font-style: italic">(Not yet available)</span></td>
                    <td> </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
<div id="email-sending-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5 class="small-note">Sending consent email to the following employees:</h5>
        <div class="progress_upload hidden">
            <div class="bar"></div >
            <div class="percent">0%</div >
        </div>
        <div id="dvCSV">
            <table id='email-consent' class='stripe row-border order-column' width='100%' cellspacing='0'>
                <thead>
                    <tr>
                        <th>Row#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <span class="small-note no-close">Please do not close window while sending of email consent is ongoing.</span>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<div id="consent-list-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<div id="employee-enrollment-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5 class="small-note">Sending welcome email to the following employees:</h5>
        <div class="progress_upload hidden">
            <div class="bar"></div >
            <div class="percent">0%</div >
        </div>
        <div>
            <table id='email-upload' class='stripe row-border order-column' width='100%' cellspacing='0'>
                <thead>
                    <tr>
                        <th>Row#</th>
                        <th>Email Address</th>
                        <th>Full Name</th>
                        <th>Birthdate</th>
                        <th>Gender</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Employee ID</th>
                        <th>Job Grade</th>
                        <th>Risk Status</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <span class="small-note no-close">Please do not close window while enrollment is ongoing.</span>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.modal').modal({
            dismissible: false
        });
        
        $('#email-consent').DataTable({
            scrollY:        400,
            scrollX:        true,
            scrollCollapse: true,
            bPaginate: false,
            "columnDefs": [
                {"className": "dt-center", "targets": [0, 2, 3]}
            ]
        });
        
        $('#email-upload').DataTable({
            scrollY:        400,
            scrollX:        true,
            scrollCollapse: true,
            bPaginate: false,
            "columnDefs": [
                {"className": "dt-center", "targets": [0, 3, 4, 5, 6, 7, 8, 9, 10]}
            ]
        });
        
        $('.consent-list').on('click', function() {
            $.get(SITEROOT + '/healthprofile/getConsentList', function(response){
                $("#consent-list-modal").modal("open")
                $("#consent-list-modal .modal-content").html(response)
            })
        });
        
        $(".modal-close").on("click", function(){
            $("div.file-path-wrapper input:text").val("")
        })
    })
</script>