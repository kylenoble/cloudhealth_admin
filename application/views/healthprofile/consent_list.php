<style>
    label {
        font-weight: bold;
    }
    
    #table1{
        width: 80%;
    }
    
    #table1 tbody tr td:first-child(3n+3) {
        text-align: center;
        font-weight: bold;
        width: 10%;
    }
    
    #table1 tbody tr td:first-child, #table1 tbody tr td:nth-child(4) {
        text-align: left;
        text-indent: 20px;
        font-weight: bold;
        background-color: #f4f5f8;
        width: 25%;
    }
    
    #table1 tbody tr td:nth-child(2), #table1 tbody tr td:nth-child(5) {
        text-align: center;
        width: 10%;
    }
    
    .reload_btn{
        margin-left: 30px !important;
    }
    
    .failed{
        font-weight: bold;
        color: red;
        font-size: 14px;
    }
    
    .sent{
        font-weight: bold;
        color: green;
        font-size: 14px;
    }
    
    .custom-toolbar h6{
        display: inline;
        float: left;
        text-transform: uppercase;
        font-weight: bold;
    }
</style>
<div class="content-wrapper">
    <table class="cyan-table" id="table1">
        <tr>
            <td>Total Emails Sent</td>
            <td><?php echo $total_emails_sent;?></td>
            <td><a class="waves-effect show_list" data-status="Total Emails Sent" data-status-filter="total" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
            <td>Total Respondents </td>
            <td><?php echo $total_respondents;?></td>
            <td><a class="waves-effect show_list" data-status="Total Respondents" data-status-filter="respondents" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
        </tr>
        <tr>
            <td>Success</td>
            <td><?php echo $success;?></td>
            <td><a class="waves-effect show_list" data-status="Success" data-status-filter="sent" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
            <td>Consented to Enrollment</td>
            <td><?php echo $consented_to_enrollment;?></td>
            <td><a class="waves-effect show_list" data-status="Consented to Enrollment" data-status-filter="yes" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
        </tr>
        <tr>
            <td>Failed</td>
            <td><?php echo $failed;?></td>
            <td><a class="waves-effect show_list" data-status="Failed" data-status-filter="failed" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
            <td>Refused Enrollment</td>
            <td><?php echo $refused_enrollment;?></td>
            <td><a class="waves-effect show_list" data-status="Refused Enrollment" data-status-filter="no" style="color: #00bcd4;font-weight: bold;font-size: 13px;">Show List</a></td>
        </tr>
    </table>
    <table id='consentlist-table' class='stripe row-border order-column' width='100%' cellspacing='0'>
        <thead>
            <tr>
                <th>Full name (First Middle Last)</th>
                <th>Email Address</th>
                <th>Birthdate (mm/dd/yyyy)</th>
                <th>Gender (Male/Female/Trans)</th>
                <th>Branch</th>
                <th>Department</th>
                <th>Employee ID</th>
                <th>Job Grade</th>
                <th>Risk Status(1 - High Value/VIP employees regardless of health status; 2 - High risk employees (people with previous medical condition, surgeries etc); 0 - none of the above)</th>
                <th>Email Delivery Status</th>
                <th>Consent Answer</th>
                <th>Date Uploaded</th>
                <th>Date Updated</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data as $rec):?>
            <tr class="<?php echo $rec['is_agreed'] == 'yes' ? ($rec['registered'] ? '' : 'unenrolled') : '' ?>">
                <td><?php echo $rec['name']; ?></td>
                <td class="center"><?php echo $rec['email']; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="center <?php echo strtolower($rec['is_sent']); ?>"><?php echo $rec['is_sent']; ?></td>
                <td class="center" data-search="<?php echo ($rec['is_agreed'] <> null ? 'consent|'.$rec['is_agreed'] : ''); ?>"><?php echo $rec['is_agreed']; ?></td>
                <td class="center" data-order="<?php echo $rec['date_uploaded']?>"><?php echo $rec['uploaded']; ?></td>
                <td class="center" data-order="<?php echo $rec['date_updated']?>"><?php echo $rec['updated']; ?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.html5.min.js"></script>

<script>
$(document).ready(function(){
    $(document).prop('title', 'CloudHealthAsia Enrollment');
    var table = $('#consentlist-table').DataTable({
        scrollY:        400,
        scrollX:        true,
        scrollCollapse: true,
        "order": [[ 4, "asc" ]],
        "paging": false,
        dom: '<"custom-toolbar">Bfrtip',
        "columnDefs": [
            {
                "targets": [ 2,3,4,5,6,7,8 ],
                "visible": false
            }
        ],
        buttons: [{ 
            extend: 'csv', 
            text: 'Download list',
            title: '<?php echo $company_name, '-ConsentReport-', date("YMd");?>',
            className: 'download',
            exportOptions: {
                columns: [0, 1]
            }
        },
        { 
            extend: 'csv', 
            text: 'Download list for enrollment',
            title: '<?php echo $company_name, '-EnrollmentList-', date("YMd");?>',
            className: 'enrollment',
            exportOptions: {
                columns: [1,0,2,3,4,5,6,7,8]
            }
        },
        { 
            extend: 'csv', 
            text: 'Download list for enrollment (unenrolled only)',
            title: '<?php echo $company_name, '-EnrollmentList-', date("YMd");?>',
            className: 'enrollment-unenrolled',
            exportOptions: {
                columns: [1,0,2,3,4,5,6,7,8],
                rows: '.unenrolled'
            }            
        }
        ]
    });
    
    table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
    table.buttons().enable();
    if(table.page.info().end == 0){
        table.buttons().disable();
    }
    if($(".unenrolled").length == 0){
        table.button(2).disable();
    }

    $("a.show_list").click(function(){
        var status = $(this).data("status")
        var status_filter = $(this).data("status-filter")
        $(".custom-toolbar").html("<h6>Status: "+status+"</h6>");
        
        //reset search
        table.columns().search("").draw();
        var searchTerm = status_filter;
        if(status_filter == 'respondents'){
            table.column(10).search("consent", true, false).draw();
            
            table.button(".download").nodes().removeClass('hidden');
            table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
            
        }else if(status_filter == 'total'){
            table.columns().search("").draw();
            
            table.button(".download").nodes().removeClass('hidden');
            table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
            
        }else if(status_filter == 'sent' || status_filter == 'failed'){
            //sent & failed
            //column 2
            table.column(9).search(searchTerm, true, false).draw();
            
            table.button(".download").nodes().removeClass('hidden');
            table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
        }else if(status_filter == 'yes' || status_filter == 'no'){
            //yes & no
            //column 3
            table.column(10).search(searchTerm, true, false).draw();
            if(status_filter == 'yes'){
                table.buttons(".enrollment, .enrollment-unenrolled").nodes().removeClass('hidden');
                table.button(".download").nodes().addClass('hidden');
                
                if('<?php echo $cnt_registered?>' === '0'){
                    table.button(".enrollment-unenrolled").disable();
                }
            }else{
                table.button(".download").nodes().removeClass('hidden');
                table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
            }
        }else{
            table.button(".download").nodes().removeClass('hidden');
            table.buttons(".enrollment, .enrollment-unenrolled").nodes().addClass('hidden');
        }
        table.button( 0 ).enable();
        if(table.page.info().end == 0){
            table.buttons().disable();
        }
    })
})
</script>