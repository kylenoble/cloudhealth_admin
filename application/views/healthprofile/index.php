<?php _load_js($js); ?> 
<!-- START CONTENT -->
<section id="content">
    <div class="content_bg"></div>
    <!--start container-->
    <div class="container container-fluid" id="healthprofile_content">
        <div class="row">
            <ul class="myTabs">
                <li class="active"><a onclick="showPage(this)" id="company_registration">COMPANY REGISTRATION</a></li>
                <li><a onclick="showPage(this)" id="summary_and_analysis" >SUMMARY AND ANALYSIS</a></li>
                <li><a id="employee_health_activities" onclick="showPage(this)">EMPLOYEE HEALTH ACTIVITIES</a></li>
            </ul>
            <div class="page-content" id="content_view"></div>
        </div>
    </div>
</section>
<!-- END CONTENT -->
<script>
    var default_page = "<?php echo ($default_page ? '#' . $default_page : '#company_registration'); ?>";
    $(document).ready(function(){
        showPage(default_page);
    });
    
    function showPage(obj){
        var page = $(obj).attr("id");
        var url = SITEROOT + '/healthprofile/' + page;
        show_waitMe(jQuery('body'));
        $.get(url, function (response) {
            $("#healthprofile_content ul li").removeClass('active');
            $(obj).closest('li').addClass('active');
            hide_waitMe();
            $("#content_view").html(response);
        });
    }

</script>