<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<div class="s12 m12">
    <div class="title small-tabs active no-hover">HEALTH AND WELLNESS PLAN</div>
    <table id="analysis" class="stripe row-border order-column" width="100%" cellspacing="0">
        <thead>
            <tr>
                <td>Date/Time Generated</td>
                <td>MD In-Charge</td>
                <td>CHI MD Report</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($CHI_reports as $report):
                $filename = explode("_", $report['file_name']); ?>
            <tr>
                <td data-order="<?php echo strtotime($report['date_created']);?>"><?php echo date('F j, Y h:iA', strtotime($report['date_created']));?></td>
                <td><?php echo $report['md_incharge'];?></td>
                <td><a href="<?php echo CHA_URL;?>/static/reports/CHI/<?php echo $report['file_name'];?>" target="_blank"><?php echo $report['report_name'];?></a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#analysis').DataTable({
        "oLanguage": {
            "sSearch": "Search by MD In-Charge"
        },
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "searchable": false, "targets": [1] }
        ],
        "sDom": 'l<"toolbar">frtip'
    });
    
    $("div.toolbar").html("<b>Doctor's recommendation/analysis report</b>");
    $("#analysis_length").hide();
    $("div.toolbar").addClass('left');
})
</script>