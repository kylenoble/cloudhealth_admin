<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
  header("location: dashboard");
}
?>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/login.js" ></script>


      <!-- Start Page Loading -->
      <div id="loader-wrapper">
          <div id="loader"></div>
          <div class="loader-section section-left"></div>
          <div class="loader-section section-right"></div>
      </div>
      <!-- End Page Loading -->
      <div id="login-page" class="row">
          <div class="col s12 z-depth-4 card-panel">
             
                  <div class="row">
                      <div class="input-field col s12 center">
                          <img width="180px" src="<?php echo base_url(); ?>assets/images/logo/cha_logo_with_text.png" alt="CloudHealthAsia" class="responsive-img">
                      </div>
                  </div>
				  <div class="login-box">
				   <?php if($success_msg) { ?>
				  <?php echo _error_message('success',$success_msg);?>
				   <?php } ?>

				  <?php if(validation_errors()) { ?>
				   <div class="alert alert-warning">
					<?php echo validation_errors(); ?>
				   </div>
				  <?php } ?>

				  <?PHP if(isset($error)): ?>
					<?php echo $error;?>
				  <?php endif ?>

				  <?php  echo form_open('login',array('id' => 'vip-user-login-form','class'=>'login-form')); ?>

						  <div class="row margin">
							  <div class="input-field col s12">
								  <i class="material-icons prefix pt-5">person_outline</i>
								  <?php echo form_input(array('id' => 'username', 'name' => 'username', 'placeholder' => '')); ?>
								  <label for="username" class="center-align">Email Address</label>
							  </div>
						  </div>

						   <div class="row margin">
							  <div class="input-field col s12">
								  <i class="material-icons prefix pt-5">lock_outline</i>
								  <?php echo form_password(array('id' => 'pass', 'name' => 'pass', 'placeholder' => '')); ?>
								  <label for="password">Password</label>
							  </div>
						 </div>
						
						 <div class="row">
							  <div class="col s12 m12 l12 ml-2 mt-3">
								  <input type="checkbox" id="remember-me" />
								  <label for="remember-me">Remember me</label>
							  </div>
						</div>
						
						<div class="row">
							  <div class="input-field col s12">
                                                              <input type="submit" class="btn waves-effect waves-light col s12" value="Login" />
							  </div>
						</div>

						<div class="row">
							  <div class="input-field col s12 m12">
                                                              <p class="margin right-align medium-small"><a href="<?php echo base_url()?>login/forgot_password">Forgot password?</a></p>
							  </div>
					  </div>


				<?php echo form_close(); ?>
				  </div>
                  
                  
          </div>
      </div>

      <!-- ================================================
    Scripts
    ================================================ -->
      <!-- jQuery Library -->
      <script type="text/javascript" src="<?php echo base_url();?>/assets/vendors/jquery-3.2.1.min.js"></script>
      <!--materialize js-->
      <script type="text/javascript" src="<?php echo base_url();?>/assets/js/materialize.min.js"></script>
      <!--scrollbar-->
      <script type="text/javascript" src="<?php echo base_url();?>/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
      <script type="text/javascript" src="<?php echo base_url();?>/assets/js/plugins.js"></script>
 

