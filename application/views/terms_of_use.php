<h3>CloudHealthAsia</h3>
<h3>Terms of Use Agreement</h3>
<h3 style="font-style: italic;">Acceptance</h3>
<p>
<span class="small-note">It is important that you read all the following terms and conditions carefully</span>. This Terms of Use Agreement (“Agreement”) is a legal agreement between you and CloudHealthAsia (“Owner”), the owner and operator of this website (the “Website”). It states the terms and conditions under which you may access and use the website and all written and other materials displayed or made available through the website (“content”). Your use of the <span class="small-note">CloudHealthAsia</span> website constitutes your acceptance of these terms and conditions of use. If you have not already done so, please take some time to familiarize yourself with the terms. By accessing the site, you are agreeing to be bound by these site terms of use, all applicable laws and regulations and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, do not use this site. The Owner may revise this Agreement at any time by updating this posting. Use of the website after such changes are posted and will signify your acceptance of these revised terms. You should visit this page periodically to review this Agreement.
</p>

<h3 style="font-style: italic;">Medical Emergency.</h3>
<p>
Do NOT use this website for medical emergencies. If you have a medical emergency, call a physician or a qualified health care provider, or CALL 911 immediately. Under no circumstances should you attempt self-treatment based on anything you have seen or read on this website. This website is not intended for acute care management.
</p>

<h3 style="font-style: italic;">No physician-patient relationship</h3>
<p>
Unless you already have a consult with one of our functional medicine doctors, under no circumstance shall it be that the medical information displayed in this website establish a physician-patient relationship. Overview of your health profile without an actual consult is considered general information.
</p>

<h3 style="font-style: italic;">Patient portal services</h3>
<p>
If you are an existing patient, <span class="small-note">CloudHealthAsia</span> may allow you to communicate with your physician(s) by video conferencing or by some other electronic means. Any use or disclosure of personal information provided by you shall be in accordance with our privacy policy.
</p>

<h3 style="font-style: italic;">Indemnity</h3>
<p>
You agree to indemnify, defend, and hold harmless <span class="small-note">CloudHealthAsia</span>, its physicians, directors, employees, agents, licensors and their respective successors , from and against any and all claims, demands, liabilities, costs or expenses whatsoever, including , without limitation, legal fees and disbursements , resulting directly or indirectly from (i) your breach of any of the terms and conditions of this Agreement; (ii) your access to, use, misuse, reliance upon, or to time or; (iii) your use of, reliance on, publication, communication, distribution, uploading, or downloading of anything (including Content) on or from the website.
</p>

<h3 style="font-style: italic;">Use of the Website</h3>
<p>
<span class="small-note">CloudHealthAsia</span> authorizes you to access and use our website for your personal noncommercial use in accordance with the terms and conditions of this Agreement.
</p>

<h3>Disclaimer and Copyright</h3>
<h3 style="font-style: italic;">Copyright</h3>
All information, text, documents, materials, graphics, photography, designs, logos, layouts, icons and computer codes (collectively, “content”) of this site is (and shall continue to be) owned exclusively by the <span class="small-note">CloudHealthAsia</span> and is protected under applicable copyrights, patents, trademarks, and/or other proprietary intellectual property rights. The copying, redistribution, use or publication of any such content, or any part of this site, are prohibited. Under no circumstances will you acquire any ownership rights or other interest in any content by or through your use of this site.
</p>

<h3 style="font-style: italic;">Disclaimer</h3>
<p>
The information on this site is provided solely for users’ general knowledge and is provided “as is”. <span class="small-note">CloudHealthAsia</span> hereby grants you a limited, non-transferable, nonexclusive right to access and use of the site solely for your personal use to find out information about your health and our business. This authorizes you to view and the materials on the site solely for your personal, non-commercial use. You may not modify, copy, distribute, transmit, display, publish, or create derivative works from the Information or use the Information for any commercial purpose whatsoever. There is no warranty, representation or guarantee with respect to the accuracy, timeliness or completeness of the information. <span class="small-note">CloudHealthAsia</span> does not warrant that the site and/or the information will be free from errors, defects, program limitations, viruses or other harmful components or that the site and the information will be accessible and perform in accordance with your expectations. The use of the site is at your own risk.
</p>