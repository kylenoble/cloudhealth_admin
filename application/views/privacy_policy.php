<h3>CloudHealthAsia Privacy Policy</h3>
<h3><em>About our privacy policy</em></h3>
<p>
    CloudHealthAsia recognizes the importance of keeping the personal information that you entrust to us private and confidential. This policy outlines how your personal information is handled and how CloudhealthAsia protects and uses the information that you provide through our platform.
</p>
<p>
    CloudHealthAsia is committed to following the general principles and rules of data privacy protection in the Philippines. We recognize are responsibilities under the Republic Act No. 10173 , also known as the Data Privacy Act of 2012, with respect to the data it collects, records, organizes, updates, uses and consolidates from its clients. By using our platform and submitting personal information through our platform, the user expressly consents to the collection, use and disclosure of the user’s personal information in accordance with this privacy policy.
</p>
<ol type = 'I'>
    <li>Information we collect
        <p>
            We receive information from your company health manager and what you directly provide to us. In this privacy policy, we refer to all of this as the ‘information’.
            The following are the information your company and you as the user provide to us.</p>
        <p>From the Company: <br />
        <ul>
            <li>Full Name</li>
            <li>Birth date</li>
            <li>Gender</li>
            <li>Email Address</li>
            <li>Company related details (Branch, Department, Job Grade or Level)</li>
        </ul>
        </p>

        <p>From the data collection and health survey in the platform: <br />
        <ul>
            <li>Ethnicity</li>
            <li>Civil Status</li>
            <li>Health information (Past diagnoses, current symptoms, anthropometrics, lifestyle habits) pertinent to the creation of a health profile</li>
        </ul>
        </p>
    </li>

    <li>How we use the information
        <p> We use all the information to:</p>
        <ol>
            <li>Generate the user’s individual health profile that would be used in creating his personalized health plan </li>
            <li>Generate corporate aggregate reports that would be used in creating the appropriate health and wellness plan for your company. </li>
            <li>Generate epidemiological data for in-house researches and analysis following research protocols. </li>
            <li>Perform analysis on how you use our platform</li>
            <li>Provide and communicate with you during the implementation of your health plan.</li>
            <li>Fulfill your requests regarding our services</li>
            <li>Respond to your inquiries</li>
            <li>Provide technical support for the services we offer</li>
            <li>Enforce the legal terms that govern your use of our platform and/or for the purposes for which you provided the Information</li>
            <li>Prevent fraudulent or potentially illegal activities on or through our platform</li>
            <li>Protect the safety of our users</li>
            <li>Establish, exercise or defend legal claims</li>
        </ol>
        <br />
    </li>

    <li class="lists">How we process the information
        <p>
            We collect, record, organize, store, update/modify, retrieve, consult, user or consolidate your personal information during your enrollment in the system and to our services. The Personal data obtained from CloudHealthAsia is entered and stored within CloudHealth’s information and communication system and will only be accessible to the patient and the CloudHealthAsia team (Doctors, system administrator and medical practitioners).
        </p>
    </li>

    <li class="lists">Information sharing and disclosure
        <p>
            Information CloudHealthAsia collects from its clients concerning their health will be kept strictly confidential and secure at all times.
        </p>
        <p>
            We may disclose Information in accordance with the purposes stated above as follows:
        </p>
        <ul>
            <li>To other medical teams and service providers where health information is needed for epidemiological data or any other researches, provided your personal information shall be held under strict confidentiality and shall only be used for the declared purpose. </li>
            <li>To the technical support team in processing support requests</li>
            <li>To the sales and operations team when fulfilling orders and user requests</li>
            <li>To our host provider who maintains our databases</li>
            <li>To the system administrator in cases of need to investigate, prevent or take action regarding potentially illegal activities involving potential threats to any person or us, the services or violations of our policies or the law</li>
            <li>In response to legal process, for example, information is subpoenaed by Court or similar requests.</li>
            <li>With third parties in order to investigate, prevent or take action regarding potentially illegal activities, violations of our policies.</li>
        </ul>
    </li>

    <li class="lists">Complaints, concerns and questions
        <p>
            For complaints, concerns and questions regarding the processing of your personal information, please contact us at client.services@cloudhealthasia.com
        </p>
    </li>

    <li class="lists">Your rights as the data subject
        <p>
            As the provider of the information, you have the following rights under the Data Privacy Act:
        </p>
        <ul>
            <li>The right to access your personal information</li>
            <li>The right to make corrections to your personal information</li>
            <li>The right to object to the processing of your personal information</li>
            <li>The right to suspend, withdraw or order the blocking, removal or destruction of your personal information</li>
            <li>The right to be informed on the reasons for the disclosure of the personal information</li>
            <li>The right to be informed of the existence of processing of your personal information, the manner by which such data were processed and the names and addresses of the recipients of the personal information</li>
            <li>The right to damages</li>
            <li>The right to obtain from the personal information controller a copy of data undergoing processing in an electronic or structured format</li>
            <li>The right to file a complaint before the National Privacy Commission</li>
        </ul>
    </li>

    <li class="lists">Updates and Effective Date
        <p>
            CloudHealthAsia reserves the right to update this Privacy Policy. We will however make sure to notify you about the material changes either by sending an email message to your registered email address or through a notification or announcement posted in our platform. We then encourage you to regularly check and review this policy so you are always informed on what information we collect, how we use it, and with and whom we share it.
        </p>
    </li>

</ol>