<style>
    .collapsible-body{
        padding-top: 5px;
        padding-bottom: 5px;
    }
</style>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">

            <div class="title small-tabs active no-hover">COMPANY DETAILS</div>
            <div class="row right" style="margin-top: 10px; margin-right: 10px; margin-bottom: 5px;">
                <div class="col-sm-8">                
                    <a data-callback="/organizations/index" class="btn-default btn page_ajaxify" parent-callback="organizations/index">Back</a>
                </div>
            </div>
            <div class="container-fluid">

                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row view-orgnization">
                        <div class="col-sm-12">

                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">

                                <div class="company-view form-horizontal">

                                    <div class="form-group">
                                        <?php echo form_label('Company Name:', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8" style='padding-top:5px'>
                                            <?php echo $result['company_name']; ?>
                                        </div>
                                        <?php echo form_label('Company Code:', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8" style='padding-top:5px'>
                                            <?php echo $result['company_code']; ?>
                                        </div>
                                        <?php echo form_label('Location:', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8" style='padding-top:5px'>
                                            <?php echo $result['location'] ? $result['location'] : '-'; ?>
                                        </div>
                                        <?php echo form_label('Account Officer-In-Charge:', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8" style='padding-top:5px'>
                                            <?php echo $result['contact'] ? $result['contact'] : '-'; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="company_branches">
                                            <div class="title">Branches and Departments</div>
                                            <div class="clearfix"></div>
                                            <ul class="collapsible col-sm-8" data-collapsible="accordion">
                                                <?php if(count($branches) == 0) :?>
                                                <li>
                                                    <div class="collapsible-header">No department</div>
                                                </li>
                                                <?php endif; ?>
                                                <?php foreach($branches as $branch) :?>
                                                <li>
                                                    <div class="collapsible-header" id="<?php echo $branch['id'];?>"><i class="material-icons">place</i><?php echo $branch['br_name'];?></div>
                                                    <div class="collapsible-body" id="departments_list-<?php echo $branch['id'];?>"></div>
                                                </li>
                                                <?php endforeach;?>
                                                <?php if(!empty($depts_no_branch)) :?>
                                                <li>
                                                    <div class="collapsible-header" id="0"><i class="material-icons">place</i><strong class="small-note">Show Departments</strong></div>
                                                    <div class="collapsible-body" id="departments_list-0"></div>
                                                </li>
                                                <?php endif;?>
                                            </ul>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12" style="margin-top: 15px;">                
                                                <a href="/organizations/update/<?php echo $result['company_id']?>" class="btn-default btn page_ajaxify" data-callback="/organizations/update/<?php echo $result['company_id']?>" parent-callback="/organizations/view/<?php echo $result['company_id']?>" >EDIT</a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="title small-tabs active no-hover">HR USERS</div>
                            <div class="row right" style="margin-top: 10px; margin-bottom: 5px;">
                                <div class="col-sm-12">                
                                    <a class="btn btn-info add_user active page_ajaxify" data-callback="users/add/<?php echo $result['company_id']?>" parent-callback="organizations/view" id="users_actions"  href="javascript:void(0)">Add User</a>
                                </div>
                            </div>
                            <?php if ($this->session->flashdata('email_sending_error') != '') {
                                ?>
                                <div class="alert alert-warning alert-dismissable" style="margin: 0;visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                                    <?php echo $this->session->flashdata('email_sending_error'); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <?php echo _error_message('danger'); ?>
                            <table id='adminusers_table' class="stripe row-border order-column" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Email Address</td>
                                        <td>Default Password</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($hr_users as $row) : ?>
                                        <tr>
                                            <td><?php echo $row['name'] ?></td>
                                            <td><?php echo $row['email'] ?></td>
                                            <td><?php echo ($row['status'] == null && $row['date_modified'] == null) ? $row['password'] : '-'; ?></td>
                                            <td>
                                                <?php echo _actions_icon('/users/update/' . $row['id'], 'edit', 'page_ajaxify', '', 'organizations/view/'.$result['company_id']); ?>&nbsp;
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                            
                            <div class="title small-tabs active no-hover">EMPLOYEE LIST</div>
                            <div id="account_view" style="margin-bottom: 60px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    $(document).ready(function(){
        $('div#company_branches .collapsible').collapsible();

        $('#adminusers_table').DataTable({
            scrollY:        400,
            scrollX:        true,
            scrollCollapse: true,
            "order": [[ 0, "asc" ]]
        });
        
        $("div#company_branches .collapsible-header").on('click', function(){
            var branch_id = $(this).attr("id");
            $.get(SITEROOT + '/organizations/get_depts_perBranch/<?php echo $result['company_id']; ?>/' + branch_id, function(html){
                var list = "<span>";
                if(html.length == 0){
                    list += "<div style='font-style: italic'>No department</div>";
                }else{
                    for(var i=0; i < html.length; i++){
                        list += "<div style='font-style: italic'> - " + html[i]['dept_name'] + "</div>";
                    }
                }
                list += "</span>";
                $("#departments_list-" + branch_id).html(list);
                
            }, 'json');
        });
        //set last param to 1 = view only; no access to revoke 
        $.get(SITEROOT + '/account/users/<?php echo $result['company_id'];?>/1', function(result){
            $("#account_view").html(result)
            
            $('div#account_view table#users').DataTable({
                scrollY:        400,
                scrollX:        true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 2
                },
                "oLanguage": {
                    "sSearch": "Search by Name or Employee ID" //search
                },
                "order": [[ 9, "desc" ]],
                "columnDefs": [
                    { "searchable": false, "targets": [2,3,4,5,6,7,8,9,10] }
                ],
                dom: 'Bfrtip',
                buttons: [{ 
                    extend: 'pdf', 
                    text: 'Export to PDF',
                    title: 'List of Registered Employees: (<?php echo $result['company_name']; ?>)',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11]
                        }
                    }
                ]
            });
        });
        
        $(document).on('click', '.unlock-survey-form', function(){
            var selected = $("input:checkbox[name^=checkMe_]:checked");
            if(selected.length > 0){
                if(confirm("Are you sure you want to unlock the Health Survey forms to the selected users?")){
                    var userids = [];
                    $(selected).each(function(){
                        var tmp_id = ($(this).attr("id")).split("_")
                        userids.push(tmp_id[1]);
                    });

                    if(userids.length > 0){
                        $.post(SITEROOT + '/account/survey_unlock', {userids: userids}, function(response){
                            if(response){
                                var data = {};
                                data.callback = '/organizations/view/<?php echo $result['company_id']?>';
                                page_ajaxify(data);
                            }else{
                                alert('An error was encountered. Please contact your system administrator.');
                            }
                        });
                    }
                }
            }else{
                alert("Please select user(s) with LOCKED Health Survey Forms")
            }
        });

        $(document).on('click', "input:checkbox#checkAllLocked", function(){
            var select_all = $(this).prop("checked");
            $("input:checkbox[id^=checkMe_]").each(function(index){
                if(select_all){
                    $(this).prop("checked", true);
                }else{
                    $(this).prop( "indeterminate" , false );
                    $(this).prop( "checked" , false );
                }
            });
        });
    });
    
</script>