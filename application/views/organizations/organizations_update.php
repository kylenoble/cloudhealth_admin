<?php $logo = $result['logo'];
if($logo != null){
    // Remove 0x from the string start
    $logo = substr($logo, 2);

    // Convert hexadecimal string to binary
    $bin = hex2bin($logo);

    // Convert binary to base64
    $base64_image = $bin;
    //get mime-type
    $imgdata = base64_decode($base64_image);
    $f = finfo_open();
    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
}

?>
<style>
    .ui-state-highlight {
        height:25px !important;
    }
    
    .control-label{
        line-height: 3rem;
    }
    
    .form-control{
        margin: 0 !important;
    }
    
    .logo-wrapper{
        height: 50px;      /* equals max image height */
/*        width: auto;*/
        white-space: nowrap; /* this is required unless you put the helper span closely near the img */
        text-align: center; margin: 1em 0;
    }
    
    .helper {
        display: inline-block;
        height: 100%;
        vertical-align: middle;
    }
    
    .logo-wrapper img{
        vertical-align: middle;
        max-height: 50px;
        max-width: 220px;
    }
</style>
<?php _load_js($js); ?>
<input type="hidden" id="is_hr" value="<?php echo ($this->session->userdata['logged_in']['role'] == Users_model::HR ? true : false)?>"/>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">

            <div class="title">UPDATE BUSINESS DETAILS</div>

            <div class="container-fluid">

                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row update-orgnization">
                        <div class="col-sm-12">

                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">

                                <?php echo _error_message('danger'); ?>

                                <?php echo form_open_multipart(base_url() . 'organizations/update', array('class' => 'form-horizontal vip-organization-update', 'id' => 'vip-organization-update')); ?>

                                <input type='hidden' name='id' id='id' value="<?php echo $result['id'] ?>">	
                                <div class="panel-body">
                                    <div class="form-group">
                                        <?php echo form_label('Business Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'org_name', 'name' => 'org_name', 'class' => 'form-control', 'value' => $result['company_name'])); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Business Address', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'location', 'name' => 'location', 'class' => 'form-control', 'value' => $result['location'])); ?>
                                        </div>
                                    </div>

                                    <?php if ($this->session->userdata['logged_in']['role'] == Users_model::HR): ?>
                                    <div class="form-group">
                                        <?php echo form_label('Business Email<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => trim($email))); ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <?php echo form_label('Account Officer-in-Charge', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'contact', 'name' => 'contact', 'class' => 'form-control', 'value' => $result['contact'])); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?php echo form_label('Company Logo', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-4 file-field input-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" name='company_logo' id='company_logo'>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Select company logo file">
                                            </div>
                                        </div>
                                        <?php if($logo != null) :?>
                                        <div class="col-sm-4 logo-wrapper">
                                            <span class="helper"></span>
                                            <img src="data:<?php echo $mime_type;?>;base64,<?php echo $base64_image;?>" />
                                        </div>
                                        <?php endif;?>
                                    </div>

                                    <div class="clearfix"><hr/></div>
                                        <div class="form-group">
                                            <?php echo form_label('Add Branches', '', array('class' => 'col-sm-3 control-label')); ?>
                                            <div class="col-sm-8">
                                                <div id="add-action">
                                                    <?php
                                                    if (count($branches) > 0) {
                                                        $i = 0;
                                                        foreach ($branches as $branch) {
                                                            $branchname = $branch['br_name'];
                                                            $branchaddress = $branch['location'];
                                                            $branchcontact = $branch['contact'];
                                                            ?>
                                                            <div class="row m-b-15">
                                                                <input type="hidden" value="<?php echo $branch['id']?>" name="branches_id[]" />
                                                                <span class="col-sm-3"><?php echo form_input(array('name' => 'branch_name[]', 'placeholder' => 'Branch Name', 'value' => $branchname, 'class' => 'form-control')); ?></span>
                                                                <span  class="col-sm-3"><?php echo form_input(array('name' => 'branch_address[]', 'placeholder' => 'Branch Address', 'value' => $branchaddress, 'class' => 'form-control')); ?></span>
                                                                <span class="col-sm-3"><?php echo form_input(array('name' => 'branch_contact[]', 'placeholder' => 'Contact Number', 'value' => $branchcontact, 'class' => 'form-control')); ?></span>
                                                                <?php if ($i != 0) { ?>
                                                                             <!-- <span class="remove-btn col-sm-1"><a href="javascript:void(0);" class="remove_button" title="Remove field"><span class="ti ti-trash"></span></a></span> -->
                                                                <?php } ?>
                                                            </div>
                                                            <?php
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0);" class="add_more" title="Add More">
                                                        <img src="<?php echo base_url(); ?>assets/images/add-more.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"><hr/></div>

                                        <div class="form-group" style='padding-top:10px;padding-bottom:10px'>
                                            <?php echo form_label('Add Departments', '', array('class' => 'col-sm-3 control-label')); ?>
                                            <div class="col-sm-8">
                                                <div id="add-action-department">
                                                    <?php
                                                    if (count($departments) > 0) {
                                                        foreach ($departments as $dept) {
                                                            $deptname = $dept['dept_name'];
                                                            ?>
                                                            <div class="row m-b-15">
                                                                <input type="hidden" value="<?php echo $dept['id']?>" name="departments_id[]" />
                                                                <span class="col-sm-4">
                                                                    <select class="form-control" name="branch_id[]" placeholder="Select Branch">
                                                                        <option value=''> - </option>
                                                                        <?php foreach($branchList as $br) :?>
                                                                        <option value='<?php echo $br['id']?>' <?php echo ($br['id'] == $dept['branch_id'] ? 'selected' : '');?>><?php echo $br['br_name']?></option>
                                                                        <?php endforeach;?>
                                                                    </select>
                                                                </span>
                                                                <span class="col-sm-4"><?php echo form_input(array('name' => 'dept_name[]', 'placeholder' => 'Department Name', 'value' => $deptname, 'class' => 'form-control')); ?></span>
                                                                <!--<span class="remove-btn col-sm-1"><a href="javascript:void(0);" class="remove_button" title="Remove field"><span class="ti ti-trash"></span></a></span>-->
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0);" class="add_more_dept" title="Add More">
                                                        <img src="<?php echo base_url(); ?>assets/images/add-more.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                </div>



                                <?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <?php //echo form_submit(array('id' => 'submit', 'value' => 'Save','class' =>'btn btn-primary')); ?>
                                                <a href='javascript:void(0)' class="btn-primary btn edit-org-save">Save</a>
                                                <?php if ($this->session->userdata['logged_in']['role'] == Users_model::HR) : ?>
                                                    <a data-callback="/healthprofile" parent-callback="healthprofile/index" class="btn-default btn page_ajaxify">Cancel</a>
                                                <?php else: ?>
                                                    <a data-callback="/organizations/view/<?php echo $result['id'] ?>" parent-callback="organizations/index" class="btn-default btn page_ajaxify">Cancel</a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
