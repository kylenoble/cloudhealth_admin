<style>
    .ui-state-highlight {
        height:25px !important;
    }
    .panel .panel-body{
        padding: 16px;
    }
    
    .control-label {
        margin-bottom: 20px;
    }
    
    input.form-control {
        height: 2rem !important;
    }
    
    #number_of_users{
        width: 100px;
        margin-left: 20px;
        text-align: center;
    }
</style>

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="title small-tabs active no-hover">ENROLL NEW COMPANY</div>

            <div class="container-fluid">

                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row add-orgnization">
                        <div class="col-sm-12">

                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">

                                <?php echo _error_message('danger'); ?>

                                <?php echo form_open_multipart(base_url() . 'organizations/add', array('class' => 'form-horizontal vip-organization-add', 'id' => 'vip-organization-add')); ?>

                                <div class="panel-body">
                                    <div class="form-group">
                                        <?php echo form_label('Company Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'org_name', 'name' => 'org_name', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo form_label('Account Officer In Charge', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'account_officer', 'name' => 'account_officer', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?php echo form_label('Username/Email Address', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?php echo form_label('NDA Attachment', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8 file-field">
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" name='nda_attach' id='nda_attach'>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload NDA attachment">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?php echo form_label('Maximum number of users', '', array('class' => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-8">
                                            <input type="checkbox" checked name="allowed_number_of_users[0]" id="allowed_number_of_users" /><label for="allowed_number_of_users"> Unlimited</label>
                                            <input type="number" step="1" oninput="this.value=this.value.replace(/[^1-9]/g,'');" min="1" placeholder="Input value" class="form-control hidden" value="" name="allowed_number_of_users[1]" id="number_of_users" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <input type="checkbox" name="enroll_to_trial" id="enroll_to_trial" /><label for="enroll_to_trial"> Enroll company to CHI Trial</label>
                                        </div>
                                    </div>

                                </div>

                                <?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">

                                                <a href='javascript:void(0)' class="btn-primary btn add-org-save">Enroll</a>
                                                <a data-callback="/organizations" parent-callback="organizations/index" class="btn-default btn page_ajaxify">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
$(document).ready(function(){
    $("#allowed_number_of_users").click(function(){
        if(!$(this).is(":checked")){
            $("#number_of_users").removeClass("hidden")
        }else{
            $("#number_of_users").addClass("hidden").val("")
        }
    })
})
</script>