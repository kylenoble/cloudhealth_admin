<style>
    a {
        cursor: pointer;
    }
</style>
<?php _load_js($js); ?>
<div class="static-content-inner manage-organizations ">
    <div class="static-content">
        <div class="page-content">
            <div class="title small-tabs active no-hover">LIST OF ENROLLED COMPANIES</div>

            <div class="container-fluid">

                <?php if ($this->session->flashdata('organization_add_msg') != '') {
                    ?>
                    <div class="alert alert-info alert-dismissable" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                        <?php echo $this->session->flashdata('organization_add_msg'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php if ($this->session->flashdata('email_sending_error') != '') {
                    ?>
                    <div class="alert alert-warning alert-dismissable" style="margin: 0;visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
                        <?php echo $this->session->flashdata('email_sending_error'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php echo _error_message('danger'); ?>
                
                <div class="input-group-btn right">
                    <a class="btn btn-info add_company active page_ajaxify" data-callback="organizations/add" parent-callback="organizations/index">Enroll New Company</a>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <table id='clientlist_table' class="stripe row-border order-column" width="100%" cellspacing="0">
                            <thead>
                            <tr>   
                                <th>Company Name</th>
                                <th>Account Id</th>
                                <th>Account Officer-in-Charge</th>
                                <th>Company Code</th>
                                <th>NDA</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($companies as $key => $company) {
                                ?>

                                <tr>                                               
                                    <td style='text-align: center'><?php echo addslashes($company['company_name']); ?></td>
                                    <td style='text-align: center'><?php echo addslashes($company['id']); ?></td>
                                    <td><?php echo $company['contact'] ? addslashes($company['contact']) : '-'; ?></td>
                                    <td style='text-align: center'><?php echo addslashes($company['code']); ?></td> 
                                    <td style='text-align: center'>
                                        <?php if($company['with_pending_nda']):?>
                                        <a class="btn btn-small page_ajaxify" id="nda-verified_<?php echo $company['id']?>" data-callback="organizations/NDAverified">NDA Ok</a>
                                        <?php else: echo '-'; endif;?>
                                    </td> 
                                    <td style='text-align: center'>
                                        <a alt="See Details" title="See Details" onclick="viewCompanyDetails(this)" class="company_details"  data-callback="/organizations/view/<?php echo $company['id']?>" parent-callback="organizations/index">
                                            <i class="material-icons dp48">info_outline</i> 
                                        </a>
                                    </td>
                                </tr>

                            <?php }
                            ?>
                                </tbody>

                        </table>

                    </div>

                </div>

            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    $(document).ready(function(){
        $('#clientlist_table').DataTable({
            "order": [[ 0, "asc" ]],
            dom: 'Bfrtip',
            buttons: [{ 
                extend: 'pdf', 
                text: 'Export to PDF',
                title: 'List of Enrolled Companies (as of '+ getDate(new Date()) + ")",
                exportOptions: {
                    columns: [0, 1, 2, 3]
                    }
                }
            ]
        });
        
        $("#clientlist_table").on("click", "a[id^=nda-verified]", function(e){
            e.preventDefault();
            e.stopPropagation();
            
            var tmp_id = ($(this).attr("id")).split("_");
            var data = {};
            data.callback = $(this).attr("data-callback");
            data.form_data = {x: tmp_id[1]};
            var response = vip_ajax(data);
            if (response.result.success) {
                var data = {};
                data.callback = 'organizations/index';
                page_ajaxify(data);
            } else {
                alert(response.result.errors);
            }
        });
    })
    function viewCompanyDetails(obj){
        var data = {};
        data.callback = $(obj).attr("data-callback");
        page_ajaxify(data);
    }
</script>