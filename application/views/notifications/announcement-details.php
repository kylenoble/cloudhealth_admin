<?php 
$is_email = false;
if(isset($is_email)){
    $is_email = true;
}?>
<label id="modal-datetime" <?php echo ($is_email ? 'style="font-size: 15px; color: #9e9e9e;"' : '')?> ><?php echo $datetime;?></label>
<h5 id="announcement-header" <?php echo ($is_email ? 'style="font-size: 1.64rem;line-height: 110%;margin: 0.82rem 0 0.656rem 0; font-weight:400"' : '')?> ><?php echo $header?></h5>
    <pre <?php echo ($is_email ? 'style="font-family:Arial,Helvetica,sans-serif;font-size:15px;"' : '')?> ><?php echo $message?></pre>
