<style>
    .dataTable tbody tr:hover{
        cursor: pointer;
    }
    
    .unread {
        color: blue;
        font-weight: bold;
    }
    
    .modal p{
        margin-top: 40px;
    }
    
    #announcement-table_wrapper.dataTables_wrapper{
        margin: 0 !important;
    }
    
    pre{
        font-family: inherit;
    }
</style>
<div class="static-content-inner manage-users ">
    <div class="static-content">
        <div class="page-content">
            <?php if(isset($announcements)) : ?>
            <div class="title small-tabs active no-hover">ANNOUNCEMENTS</div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <table id='announcement-table' class="hover stripe row-border" width="100%" cellspacing="0">
                            <thead>
                            <tr>                         
                                <th>Date</th>
                                <th>Sender</th>
                                <th>Subject</th>
                            </tr>
                            </thead>
                            <tbody>
                        <?php foreach($announcements as $rec) :
                            $is_read = false;
                            if($rec['read_by'] != null){
                                $read_by = json_decode($rec['read_by'], true);
                                $my_userid = $this->session->userdata['logged_in']['userid'];
                                foreach($read_by as $val){
                                    if($val['type'] == 'HR' && $val['id'] == $my_userid){
                                        $is_read = true;
                                    }
                                }
                            }
                            ?>
                                <tr id="a-<?php echo $rec['id']?>" data-read="<?php echo $is_read;?>" class="<?php echo ($is_read ? '' : 'unread')?>">
                                    <td class="center"><span class='hidden'><?php echo strtotime($rec['date_created']);?></span><?php echo date('M j, Y g:i:s A', strtotime($rec['date_created'])); ?></td>
                                    <td class="center"><?php echo $rec['name'];?></td>
                                    <td><?php echo addslashes($rec['title']); ?></td>
                                </tr>
                        <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="title small-tabs active no-hover">NOTIFICATIONS</div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <table id='notifications-table' class="hover stripe row-border" width="100%" cellspacing="0">
                            <thead>
                            <tr>                         
                                <th>Date</th>
                                <th>Sender</th>
                                <th>Subject</th>
                            </tr>
                            </thead>
                            <tbody>
                        <?php foreach($my_notifications as $notif) :
                            ?>
                                <tr id="<?php echo $notif['id']?>" data-read="<?php echo $notif['is_read'];?>" class="<?php echo ($notif['is_read'] == 't' ? '' : 'unread')?>">                                               
                                    <td class="center"><span class='hidden'><?php echo strtotime($notif['date_created']);?></span><?php echo date('M j, Y g:i:s A', strtotime($notif['date_created'])); ?></td>
                                    <td class="center"><?php echo $notif['name'];?></td>
                                    <td><?php echo addslashes($notif['subject']); ?></td>
                                </tr>
                        <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<div id="notification-text" class="modal">
    <input type="hidden" value="" id="open-notif" />
    <div class="modal-content">
        <label id="modal-datetime"></label>
        <h5 id="notification-header"></h5>
        <p id="modal-text"></p>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<div id="announcement-text" class="modal modal-fixed-footer">
    <input type="hidden" value="" id="open-announcement" />
    <div class="modal-content">
        
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.modal').modal({
            dismissible: false
        });
        $('#notifications-table, #announcement-table').DataTable({
            "order": [[ 0, "desc" ]],
            bFilter: false
        });
        
        $('#notifications-table').on('click', 'tbody tr', function() {
            var id = $(this).attr('id');
            var is_read = $(this).data("read")
            $.post(SITEROOT + '/notifications/showNotificationMessage/', {id: id, is_read: is_read}, function(response){
                $("#notification-text #modal-datetime").text(response.datetime);
                $("#notification-text #modal-text").html(response.message);
                if(response.header !== ''){
                    $("#notification-text #notification-header").html(response.header);
                }
                $("#open-notif").val(id);
                $('#notification-text').modal('open');
            }, 'json');
        });
        
        $("#notification-text .modal-close").on('click', function(){
            var id = $("#open-notif").val()
            if($("#notifications-table").find("tbody tr#"+id).removeClass("unread")){
                $("#notifications-table").find("tbody tr#"+id).data("read", "t")
                $("#notification-text #open-notif").val("")
            }
        })
        
        $('#announcement-table').on('click', 'tbody tr', function() {
            var tmp_id = ($(this).attr('id')).split("-");
            var id = tmp_id[1]
            var is_read = $(this).data("read")
            $.post(SITEROOT + '/notifications/showAnnouncementDetails/', {id: id, is_read: is_read}, function(response){
                $("#announcement-text .modal-content").html(response);
                $("#open-announcement").val(id);
                $('#announcement-text').modal('open');
            });
        });
        
        $("#announcement-text .modal-close").on('click', function(){
            var id = $("#announcement-text #open-announcement").val()
            if($("#announcement-table").find("tbody tr#a-"+id).removeClass("unread")){
                $("#announcement-table").find("tbody tr#a-"+id).data("read", "t")
                $("#announcement-text #open-announcement").val("")
            }
        })

    });
</script>