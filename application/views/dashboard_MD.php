<style>
    div.picker select{
        width: auto !important;
    }
    
    .datepicker {
        height: 2rem !important;
        width: 150px !important;
        text-align: center;
    }
    
    #filterDiv .filter_header{
        display: inline-block;
    }
    
    #filterDiv label{
        width: 80px;
    }
    
    svg > g > g:last-child { 
        pointer-events: none;
    }
    
    div.google-visualization-tooltip { 
        pointer-events: none;
        width: 500px;
    }
    
    div.google-visualization-tooltip table thead { 
        background-color: #00bcd4;
        color: #fff;
        font-weight: bold;
    }
    
    div.google-visualization-tooltip table thead th  { 
        color: #fff;
        padding: 4px;
    }
    
    #filterDiv select{
        width: 70%;
    }
    
    #piechart, #piechart_outcomes, #barchart, #linechart{
        float: left;
    }

    #piechart_outcomes{
        margin-top: 50px !important;
    }
    
    .title{
        text-shadow: 1px 1px #fff;
        display: block;
        color: #000;
    }
    
    .custom-legend {
        display: inline-block;
        margin-left: 20px;
        padding-top: 20px;
    }
    
    .no-data{
        display: flex;
        color: rgb(255, 255, 255);
        font-size: 16px;
        text-transform: capitalize;
        height: 100px;
        -webkit-box-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        align-items: center;
        background: rgb(82, 97, 127);
        flex: 1 1 0%;
    }

    label[for="show_only_uniques"] {
        margin-right: 20px;
    }
    
    [type="checkbox"] + label::before, [type="checkbox"]:not(.filled-in) + label::after{
        left: 10px !important;
    }
</style>
<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
} else {
    header("location: login");
}
?>
<!-- START CONTENT -->
<section id="content">
    <!--start container-->
    <div class="container container-fluid" style="padding: 0; margin-left: 0;">
        <h5 class="title">Appointments Booked vs Approved, Cancelled, Pending, Rescheduled</h5>
        <div class="row">
            <div class="tab s5" id="filterDiv">
                <div class="col s5" style="border: 1px solid rgb(27, 117, 187);">
                    <div class="filter_header">Filter by</div>
                    <form id="analytics-filter" name="analytics-filter">
                        <div style="padding-left: 15px; margin: 5px 0;">
                            <?php if(admin_role() == Users_model::MD):?>
                            <div>
                                <label>Company</label>
                                <select id="company" name="company">
                                    <option value="0">ALL</option>
                                    <?php foreach ($companies as $company) : ?>
                                        <option value="<?php echo $company['id'] ?>"><?php echo $company['company_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php endif;?>
                            <div>
                                <label>MD Name</label>
                                <select id="doctors" name="doctors" <?php echo (count($doctors) > 0 ? "" : "disabled"); ?>>
                                    <option value='0'>ALL</option>
                                    <?php foreach ($doctors as $md) { ?>
                                        <option value='<?php echo $md['user_id'] ?>'><?php echo $md['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div>
                                <label>Schedule</label>
                                <input placeholder="From" type="text" class="datepicker" name="schedule_from" id="schedule_from" /> - 
                                <input placeholder="To" type="text" class="datepicker" name="schedule_to" id="schedule_to" />
                            </div>
                        </div>
                        <a class="btn right view-analytics" style="margin-bottom: 10px;">View</a>
                    </form>
                </div>
            </div>
            <div class="custom-legend">
                <p>Legend</p>
                <svg height="120" width="150">
                    <circle cx="10" cy="10" r="10" fill="#71B2CA" />
                    <text x="30" y="15">Approved</text>
                    <circle cx="10" cy="40" r="10" fill="#9C9E9E" />
                    <text x="30" y="45">Cancelled</text>
                    <circle cx="10" cy="70" r="10" fill="#E1AF29" />
                    <text x="30" y="75">Rescheduled</text>
                    <circle cx="10" cy="100" r="10" fill="#D77F1A" />
                    <text x="30" y="105">Pending</text>
                </svg> 
            </div>
        </div>
        <div class="row" style="padding-left: 30px !important;">
            <div class="s5" id="piechart" ></div>
            <div class="s5" id="barchart">
                <p style="position: absolute; right: 10%; margin: 0;">
                    View by &nbsp;&nbsp;&nbsp;
                    <input type="radio" id="date" name="viewby" checked /><label for="date">Appointment Date</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="md" name="viewby" /><label for="md">Doctor</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="company_rdo" name="viewby" /><label for="company_rdo">Company</label>
                </p>
                <div style="position: relative; margin-top: 30px">
                    <div class="chart" style="margin-top: 20px; padding-bottom: 10px;" id="analytics_info"></div>
                    <div class="chart " style="margin-top: 20px; padding-bottom: 10px;" id="md_analytics_info"></div>
                    <div class="chart" style="margin-top: 20px; padding-bottom: 10px;" id="co_analytics_info"></div>
                </div>
            </div>
        </div>
        <h5 class="title">Patient Outcomes & Status</h5>
        <div class="row">
            <div class="s5" id="filterDiv">
                <div class="col s5" style="border: 1px solid rgb(27, 117, 187);">
                    <div class="filter_header">Filter by</div>
                    <form id="patient-outcomes-filter" name="patient-outcomes-filter">
                        <div style="padding-left: 15px; margin: 5px 0;">
                            <?php if(admin_role() == Users_model::MD):?>
                            <div>
                                <label>Company</label>
                                <select id="company_filter" name="company_filter">
                                    <option value="0">ALL</option>
                                    <?php foreach ($companies as $company) : ?>
                                        <option value="<?php echo $company['id'] ?>"><?php echo $company['company_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php endif;?>
                            <div>
                                <label>MD Name</label>
                                <select id="doctors_filter" name="doctors_filter" <?php echo (count($doctors) > 0 ? "" : "disabled"); ?>>
                                    <option value='0'>ALL</option>
                                    <?php foreach ($doctors as $md) { ?>
                                        <option value='<?php echo $md['user_id'] ?>'><?php echo $md['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <a class="btn right view-patient-outcomes" style="margin-bottom: 10px;">View</a>
                    </form>
                </div>
            </div>
            <div class="custom-legend" style="padding-top: 0;">
                <p style="margin-top: 0; margin-bottom: 5px;">Legend</p>
                <svg height="110" width="500">
                    <circle cx="10" cy="10" r="10" fill="#72a153" />
                    <text x="30" y="15">Improving</text>
                    <circle cx="10" cy="40" r="10" fill="#71b2ca" />
                    <text x="30" y="45">Not Improving</text>
                    <circle cx="10" cy="70" r="10" fill="#dfaf29" />
                    <text x="30" y="75">For Immediate follow up</text>
                    <circle cx="10" cy="100" r="10" fill="#d77f1a" />
                    <text x="30" y="105">Follow up Next Month</text>
                    <circle cx="10" cy="100" r="10" fill="#9c9e9e" />
                    <text x="30" y="135">Stalled or needs Recovery</text>
                </svg> 
            </div>
        </div>
        <div class="row" style="padding-left: 30px !important; ">
            <div id="piechart_outcomes" class="s5"></div>
            <div class="s5" id="linechart">
                <p style="position: absolute; margin: 0;" id="linechart_filter">
                    Select Date Range: 
                    <input placeholder="From" type="text" name="date_from" style="width: 80px" id="date_from" /> - 
                    <input placeholder="To" type="text" name="date_to" style="width: 80px" id="date_to" />
                    <input type="checkbox" name="show_only_uniques" id="show_only_uniques" />
                    <label for="show_only_uniques">Show Only Uniques</label>
                    <a class="btn btn-small reload-line-outcomes">Refresh Chart</a>
                </p>
                <div style="position: relative; margin-top: 40px;" id="linechart_outcomes"></div>
            </div>
        </div>       
    </div>
</section>
<!-- END CONTENT -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php _load_js($js); ?> 