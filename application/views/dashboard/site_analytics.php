<div>
    <p>
        Total Number of registered users: <b><?php echo $total_users;?></b><br/>
        Total Number of employees who opted not to answer the health survey: <b><?php echo $no_answer;?></b><br/>
        Total Number of employees who do not want to disclose their identity: <b><?php echo $anonymous;?></b><br/>
        Today's Unique System Login (<?php echo date('M d, Y');?>): <b><?php echo $unique_login_today, ' (', $pct_unique_login_today, '%)';?></b><br/>
        Total Unique System Login: <b><?php echo $total_unique_login, ' (', $pct_total_unique_login, '%)';?></b><br/>
        Dashboard Logins <b><?php echo $total_dashboard_logins, ' (', $pct_dashboard_login, '%)'; ?> </b>
    </p>
    
    <b>Health Survey Status</b>
    
    <p>
        Total Respondents: <b><?php echo $total_users_atleast1_complete; ?> </b><br/>
        Incomplete: <b><?php echo $incomplete; ?> </b><br/>
        Complete (not yet submitted): <b><?php echo $complete; ?></b> <br/>
        Submitted: <b><?php echo $total_users_with_submitted; ?></b> <br/>
        Compliance rate: <b><?php echo $compliance_rate; ?>%</b>
    </p>

    <div id="compliance_report">
        <table id='compliance' class="stripe row-border order-column" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                    <th>Branch</th>
                    <th>Department</th>
                    <th>Personal</th>
                    <th>Health Issues</th>
                    <th>Health Goals</th>
                    <th>Readiness</th>
                    <th>Weight</th>
                    <th>Detox</th>
                    <th>Movement</th>
                    <th>Nutrition</th>
                    <th>Sleep</th>
                    <th>Stress</th>
                    <th>Muscle, Energy, Heart</th>
                    <th>Eyes, Ears, Nose</th>
                    <th>Head and Mind</th>
                    <th>Mouth, Throat, Lungs</th>
                    <th>Other</th>
                    <th>Weight and Digestion</th>
                    <th>Overall Status</th>
                    <th>Health Survey Status</th>
                    <th>Last Login</th>
                    <th>Last Survey Update</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($report_data) > 0) {  ?>
                <?php foreach($report_data as $data) : ?>
                <tr style="<?php echo ($data['overallStatus'] == "Incomplete" ? "color: red;" : "")?>">
                    <td><?php echo $data['name']; ?></td>
                    <td><?php echo $data['employee_id']; ?></td>
                    <td><?php echo $data['branch']; ?></td>
                    <td><?php echo $data['department']; ?></td>
                    <td><?php echo $data['personal']; ?></td>
                    <td><?php echo $data['health_issues']; ?></td>
                    <td><?php echo $data['health_goals']; ?></td>
                    <td><?php echo $data['readiness']; ?></td>
                    <td><?php echo $data['weight']; ?></td>
                    <td><?php echo $data['detox']; ?></td>
                    <td><?php echo $data['movement']; ?></td>
                    <td><?php echo $data['nutrition']; ?></td>
                    <td><?php echo $data['sleep']; ?></td>
                    <td><?php echo $data['stress']; ?></td>
                    <td><?php echo $data['activity']; ?></td>
                    <td><?php echo $data['eyes_ears_nose']; ?></td>
                    <td><?php echo $data['head_and_mind']; ?></td>
                    <td><?php echo $data['mouth_throat_lungs']; ?></td>
                    <td><?php echo $data['other']; ?></td>
                    <td><?php echo $data['weight_and_digestion']; ?></td>
                    <td style="text-align: center"><?php echo $data['overallStatus']; ?></td>
                    <td style="text-align: center"><?php echo $data['survey_status']; ?></td>
                    <td style="text-align: center"><?php echo $data['last_login'] ? date('Y-m-d h:i A', strtotime($data['last_login']) + (8*3600)) : ''; ?></td>
                    <td style="text-align: center"><?php echo $data['last_survey_update'] ? date('Y-m-d h:i A', strtotime($data['last_survey_update'])) : ''; ?></td>
                </tr>
                <?php endforeach;?>
                <?php }?>
            </tbody>
        </table>

    </div>
</div>
<script>
    $(document).ready(function(){
        var today = "<?php echo date("YMd");?>";
        $('#compliance').DataTable({
            scrollY:        400,
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 1
            },
            "oLanguage": {
                "sSearch": "Search by Name or Employee ID" //search
            },
            "order": [[ 0, "asc" ]],
            "columnDefs": [
                { "searchable": false, "targets": [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23] }
            ],
            dom: 'Bfrtip',
            buttons: [{ 
                extend: 'excel', 
                text: 'Export to Excel',
                title: COMPANY_NAME+' Compliance - ' + today,
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0, 1, 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
                    }
                }
            ]
        });
    })
</script>