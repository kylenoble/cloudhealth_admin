<table id="table1" class="fixed">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Mean</th>
            <th>Median</th>
            <th>Counts of Users</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Weight</td>
            <td><?php echo $upstream_table_data['mean_data']['weight'];?> lbs</td>
            <td><?php echo $upstream_table_data['median_data']['weight'];?> lbs</td>
            <td><?php echo $upstream_table_data['weight_cnt'];?></td>
        </tr>
        <tr>
            <td>Height</td>
            <td><?php echo $upstream_table_data['mean_data']['height'];?> in</td>
            <td><?php echo $upstream_table_data['median_data']['height'];?> in</td>
            <td><?php echo $upstream_table_data['height_cnt'];?></td>
        </tr>
        <tr>
            <td>BMI</td>
            <td><?php echo $upstream_table_data['mean_data']['bmi'];?></td>
            <td><?php echo $upstream_table_data['median_data']['bmi'];?></td>
            <td class="wideTd">
                Below healthy average: <strong><?php echo $upstream_table_data['bmi_cnt_below'];?></strong><br />
                Healthy average: <strong><?php echo $upstream_table_data['bmi_cnt_avg'];?></strong><br />
                Above healthy average: <strong><?php echo $upstream_table_data['bmi_cnt_above'];?></strong><br />
                Further above healthy average: <strong><?php echo $upstream_table_data['bmi_cnt_further'];?></strong><br />
            </td>
        </tr>
        <tr>
            <td>Waist</td>
            <td><?php echo $upstream_table_data['mean_data']['waist']; ?> in</td>
            <td><?php echo $upstream_table_data['median_data']['waist']; ?> in</td>
            <td><?php echo $upstream_table_data['waist_cnt']; ?></td>
        </tr>
        <tr>
            <td>Hip</td>
            <td><?php echo $upstream_table_data['mean_data']['hip']; ?> in</td>
            <td><?php echo $upstream_table_data['median_data']['hip']; ?> in</td>
            <td><?php echo $upstream_table_data['hip_cnt']; ?></td>
        </tr>
        <tr>
            <td>WHR</td>
            <td><?php echo $upstream_table_data['mean_data']['whr']; ?></td>
            <td><?php echo $upstream_table_data['median_data']['whr']; ?></td>
            <td><?php echo $upstream_table_data['whr_cnt']; ?></td>
        </tr>
        <tr>
            <td>Overall Stress Score</td>
            <td><?php echo $upstream_table_data['mean_data']['stress']; ?></td>
            <td><?php echo $upstream_table_data['median_data']['stress']; ?></td>
            <td><?php echo $upstream_table_data['stress_cnt']; ?></td>
        </tr>
        <tr>
            <td># of Hours of Sleep</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td class="wideTd">
                More than 10 hours : <strong><?php echo $upstream_table_data['sleep_cnt_more_than_10']; ?></strong><br />
                8 to 10 hours : <strong><?php echo $upstream_table_data['sleep_cnt_8_to_10']; ?></strong><br />
                6 to 8 hours : <strong><?php echo $upstream_table_data['sleep_cnt_6_to_8']; ?></strong><br />
                Less than 6 hours : <strong><?php echo $upstream_table_data['sleep_cnt_less_than_6']; ?></strong>
            </td>
        </tr>
        <tr>
            <td>Number of users who are undergoing therapy</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_stress_therapy_respondents'] > 0 ? round(($upstream_table_data['total_stress_therapy']/$upstream_table_data['total_stress_therapy_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_stress_therapy_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Number of users who are undergoing counseling</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_stress_counseling_respondents'] > 0 ? round(($upstream_table_data['total_stress_counseling']/$upstream_table_data['total_stress_counseling_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_stress_counseling_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Exposed to Tobacco smoke</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_tobaccoExposure_respondents'] > 0 ? round(($upstream_table_data['total_tobaccoExposure']/$upstream_table_data['total_tobaccoExposure_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_tobaccoExposure_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Exposed to Caffeine</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_caffeineexposure_respondents'] > 0 ? round((($upstream_table_data['total_caffeineexposure_respondents']-$upstream_table_data['total_caffeineexposure'])/$upstream_table_data['total_caffeineexposure_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_caffeineexposure_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Chemically-dependent</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_chemical_respondents'] > 0 ? round(($upstream_table_data['total_chemical']/$upstream_table_data['total_chemical_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_chemical_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Exercises atleast 150mins per week</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_exercise_respondents'] > 0 ? round(($upstream_table_data['total_exercise']/$upstream_table_data['total_exercise_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_exercise_respondents']; ?>)</td>
        </tr>
        <tr>
            <td>Uses sleeping aids</td>
            <td class="empty">&nbsp;</td>
            <td class="empty">&nbsp;</td>
            <td><?php echo $upstream_table_data['total_sleeping_respondents'] > 0 ? round(($upstream_table_data['total_sleeping']/$upstream_table_data['total_sleeping_respondents']) * 100) : 0; ?>% (n=<?php echo $upstream_table_data['total_sleeping_respondents']; ?>)</td>
        </tr>
    </tbody>
</table>