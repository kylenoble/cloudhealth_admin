<?php
function renderTableData($data, $type, $total_respondents = 0) {
    if (count($data) == 0) {
        return '
        <tbody>
          <tr>
            <td colSpan="3"> - </td>
          </tr>
        </tbody>
      ';
    }
    $row = "";
    foreach ($data as $i => $item) {
        $rank = $i+1;
        $row .= "<tr>
            <td class='rank'>#" . ($rank) . "</td>
            <td>{$item['name']}</td>
            <td>";
        switch ($type) {
            case 'stresssource':
                $row .= $item['total'] > 0 ? number_format($item['total'], 2, '.', '') : 0;
                break;
//                case 'msqTopAffectedSystems':
//                  return (floatval(item.total) > 0 ? ((floatval(item.total)/this.state.msqTopAffectedSystems_total_respondents) * 100).toFixed(2) : 0) . '%';
//                case 'prevalentSymptoms':
//                  return (floatval(item.total) > 0 ? ((floatval(item.total)/this.state.prevalentSymptoms_total_respondents) * 100).toFixed(2) : 0) . '%';
            default:
                $row .= ($total_respondents > 0 ? number_format(($item['total'] / $total_respondents) * 100, 2, '.', '') : 0) . '%';
        }
        $row .= "</td>
        </tr>";
    }
    return "<tbody>
      {$row}
      </tbody>";
}
?>

<table>
    <thead>
        <tr>
            <?php if($table == 'foodchoices') :?>
            <th colSpan="3">Top Food Choices (n=<?php echo $total_respondents; ?>)</th>
            <?php elseif ($table == 'stresssource') :?>
            <th colSpan="2">Top Sources of Stress (n=<?php echo $total_respondents; ?>)</th>
            <th>Mean Score</th>
            <?php elseif ($table == 'systemrank') :?>
            <th colSpan="3">
                Top Organ Systems Affected (n=<?php echo $total_respondents; ?>)
                <br />
                <i style="text-transform: none">
                    (Note: Number corresponds to counts of users who score at least 10 in each system.)
                </i>
            </th>
            <?php elseif ($table == 'symptoms') :?>
            <th colSpan="3">Top Most Prevalent Symptoms (n=<?php echo $total_respondents; ?>)</th>
            <?php endif;?>
        </tr>
    </thead>
<?php echo renderTableData($data, $table, $total_respondents); ?>
</table>

