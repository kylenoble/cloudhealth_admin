<?php
if (!isset($this->session->userdata['logged_in']['userid']) && !isset($this->session->userdata['userid'])) {
        header("location: login");
}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/login.js" ></script>


<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">

        <div class="row" style="margin-bottom: 0;">
            <div class="input-field col s12 center">
                <img style="width: 200px;" src="<?php echo base_url(); ?>assets/images/logo/cha_logo_with_text.png" alt="CloudHealthAsia" class="responsive-img">
                <div><?php echo (isset($title_change_password) ? $title_change_password : '') ?></div>
            </div>
        </div>
        <div class="login-box">
            <?php if (isset($success_msg) && $success_msg) { ?>
                    <?php echo _error_message('success', $success_msg); ?>
            <?php } ?>

            <?php if (validation_errors()) { ?>
                    <div class="alert alert-warning">
                        <?php echo validation_errors(); ?>
                    </div>
            <?php } ?>

            <?PHP if (isset($error)): ?>
                    <?php echo $error; ?>
            <?php endif ?>

            <?php echo form_open('login/change_password/'.$initial, array('id' => 'vip-user-reset-form', 'class' => 'login-form')); ?>

            <?php if(!$initial) :?>
<!--            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">vpn_key</i>
                    <?php echo form_password(array('id' => 'password_new', 'name' => 'password_new')); ?>
                    <label for="password_new" class="center-align">New Password</label>
                </div>
            </div>-->
            <?php endif;?>
            
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">vpn_key</i>
                    <?php echo form_password(array('id' => 'password_new', 'name' => 'password_new')); ?>
                    <label for="password_new" class="center-align">New Password</label>
                </div>
            </div>
            
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">vpn_key</i>
                    <?php echo form_password(array('id' => 'cpassword_new', 'name' => 'cpassword_new')); ?>
                    <label for="cpassword_new" class="center-align">Confirm New Password</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <?php echo form_submit(array('id' => 'submit', 'value' => 'Update Password', 'class' => 'btn waves-effect waves-light col s12')); ?>
                </div>
                <?php if(!$initial) :?>
                <div class="input-field col s12">
                    <a class="btn-default btn waves-effect waves-light col s12 page_ajaxify" href="<?php echo base_url()?>/dashboard" >Back to Dashboard</a>
                </div>
                <?php endif;?>
            </div>

            <?php echo form_close(); ?>
        </div>

    </div>
</div>

<!-- ================================================
Scripts
================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/jquery-3.2.1.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/materialize.min.js"></script>
<!--scrollbar-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugins.js"></script>