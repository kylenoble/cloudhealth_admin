<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
} else {
    header("location: login");
}
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php _load_js($js); ?>       
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START CONTENT -->
<section id="content">
    <div class="content_bg"></div>
    <!--start container-->
    <div class="container container-fluid">
        <div class="row">
            <?php
            if (admin_role() == Users_model::HR) {
                if($logo != null){
                    // Remove 0x from the string start
                    $logo = substr($logo, 2);

                    // Convert hexadecimal string to binary
                    $bin = hex2bin($logo);

                    // Convert binary to base64
                    $base64_image = $bin;
                    //get mime-type
                    $imgdata = base64_decode($base64_image);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
                }
                ?>
                <div>
                    <?php if($logo != null):?>
                    <h4>
                        <img style="width: 150px;margin-right: 10px;" src="data:<?php echo $mime_type;?>;base64,<?php echo $base64_image;?>" />
                            <?php echo $company_name; ?>
                    </h4>
                    <?php else:?>
                    <h4 class="dashboard-title"><?php echo $company_name; ?></h4>
                    <?php endif;?>
                    <div class="small-tabs-container">
                        <div class="small-tabs active" id="site_analytics_tab"><a onclick="showMe('site_analytics')">SITE ANALYTICS</a></div>
                        <div class="small-tabs" id="hr_graphs_tab"><a onclick="showMe('hr_graphs')">CORPORATE HEALTH INDEX</a></div>
                    </div>
                </div>
            <?php } ?>
            <div id="site_analytics">
                <?php $this->load->view('dashboard/site_analytics', $data) ?>
            </div>
            <div id="hr_graphs" style="display: none;">
                <div class="col-sm-8 col-md-8 dashboard-tabs" style='padding-top:25px;'>
                    <div class="tab col s6">
                        <a class="active dashboard-tab-color demographics" href='javascript:void(0)'>DISTRIBUTION OF RESPONDENTS</a>
                    </div>
                    <div class="tab col s6">
                        <a href='javascript:void(0)' class="dm dashboard-tab-color">CHRONIC HEALTH ISSUES</a>
                    </div>
                    <div class="tab col s6">
                        <a class='rtfm dashboard-tab-color' href='javascript:void(0)'>READINESS TO CHANGE</a>						
                    </div>
                    <div class="tab col s6">
                        <a href='javascript:void(0)' class="msq dashboard-tab-color">SYMPTOM REVIEW</a>
                    </div>
                    <div class="tab col s6">
                        <a href='javascript:void(0)' class="hsahs dashboard-tab-color">HEALTH STATUS</a>
                    </div>
                    <div class="tab col s6">
                        <a href='javascript:void(0)' class="upstream dashboard-tab-color">EARLY MARKERS OF HEALTH IMBALANCES</a>
                    </div>
                </div>
                <div class="tab col s4" id="filterDiv">
                    <div style="border: 1px solid rgb(27, 117, 187);">
                        <div class="filter_header">Filter by</div>
                        <div style="padding-left: 15px; margin: 5px 0;">
                            <div>
                                <label>Business Area</label>
                                <select id="branch" name="branch" <?php echo (count($branches) > 0 ? "" : "disabled"); ?>>
                                    <option value=''>All</option>
                                    <?php foreach ($branches as $branch) { ?>
                                        <option value='<?php echo $branch['branch'] ?>'><?php echo $branch['branch'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div>
                                <label>Department</label>
                                <span id="department_container">
                                    <select id="department" name="department" <?php echo (count($departments) > 0 ? "" : "disabled"); ?>>
                                        <option value=''>All</option>
                                        <?php foreach ($departments as $dept) { ?>
                                            <option value='<?php echo $dept['department'] ?>'><?php echo $dept['department'] ?></option>
                                        <?php } ?>
                                    </select>
                                </span>
                            </div>
                            <div>
                                <label>Job Grade</label>
                                <select id="job_grade" <?php echo (count($job_grades) > 0 ? "" : "disabled"); ?> name="job_grade">
                                    <option value="">All</option>
                                    <?php foreach ($job_grades as $row) { ?>
                                        <option value='<?php echo $row['job_grade'] ?>'><?php echo $row['job_grade'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div>
                                <label>Status</label>
                                <select id="high_risk" name="high_risk">
                                    <option value="">All</option>
                                    <option value="1">High Value</option>
                                    <option value="2">High Risk</option>
                                    <option value="0">Others</option>
                                </select>
                            </div>
                            <div id="age_group_filter">
                                <label>Age Group</label>
                                <select id="age_group" name="age_group">
                                    <option value="">All</option>
                                    <option value="0 - 9">0 - 9</option>
                                    <option value="10 - 19">10 - 19</option>
                                    <option value="20 - 29">20 - 29</option>
                                    <option value="30 - 39">30 - 39</option>
                                    <option value="40 - 49">40 - 49</option>
                                    <option value="50 - 59">50 - 59</option>
                                    <option value="60 - 69">60 - 69</option>
                                    <option value="70 - 79">70 - 79</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='graphs'>
                    <div id="demographics" class="col s12">
                        <div class="col s12 mt-4" style='margin-top: 15px !important; margin-bottom: 100px;'>
                            <div class="chart_title">Age Distribution of Registered Employees</div>
                            <p>
                                Total number of registered employees: <strong id="total_registered"></strong><br />
                                Actual number of respondents: <strong id="total_respondents"></strong><br />
                            </p>
                            <div id='bar_chart'></div>
                        </div>
                    </div>
                    <div id="dm" class="col s12 hidden">
                        <div id='health_chart'></div>
                    </div>
                    <div id="rtfm" class="col s12 hidden">
                        <div style='padding-top:10px;background-color:white'>
                            <div style='padding:5px'>
                                Filter By: 
                                <a href='javascript:void(0)' class='rtfm_branch'>Business Area</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href='javascript:void(0)' class='rtfm_dept'>Department</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href='javascript:void(0)' class='rtfm_jobgrade'>Job Grade</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href='javascript:void(0)' class='rtfm_status'>Status</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href='javascript:void(0)' class='rtfm_age'>Age Group</a> 
                            </div>
                            <div> </div>
                            <div></div>
                        </div>
                        <div id="readiness_chart"></div>
                    </div>
                    <div id="msq" class="col s12 hidden">
                        <div id="msq_scattered_chart"></div>  
                        <div id="msq_score_chart" class='hidden'></div>
                        <div style='padding-top:0 10px; margin-bottom: 60px;'>
                            <div style='padding:0 10px'>
                                <div style='padding:0 10px'><a href='javascript:void(0)' class='msq-scattered hidden'>SHOW MSQ SCORES</a> 
                                    <div style='padding:0 10px'>
                                        <p class="small-note">
                                            Optimal: <span id="optimal_cnt"></span> (<span id="optimal_pct"></span>)<br/>
                                            Mild Toxicity: <span id="mild_cnt"></span> (<span id="mild_pct"></span>)<br/>
                                            Moderate Toxicity: <span id="moderate_cnt"></span> (<span id="moderate_pct"></span>)<br/>
                                            Severe Toxicity: <span id="severe_cnt"></span> (<span id="severe_pct"></span>)
                                        </p>
                                        <a href='javascript:void(0)' class='msq-scores'>SHOW COUNTS OF USERS WITH MSQ SCORES >10 PER SYSTEM</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="hsahs" class="col s12 hidden" style="margin-bottom: 100px;">
                        <div>
                            <div id='health_scattered_chart'></div>
                            <div id='health_pie_chart'></div>
                        </div>
                        <div id='health_bar_chart'></div>
                    </div>
                    <div id="upstream" class="col s12 hidden content-wrapper">
                        <div id="upstream_tables"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CONTENT -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
</div>
<!-- END WRAPPER -->
</div>
<!-- END MAIN -->