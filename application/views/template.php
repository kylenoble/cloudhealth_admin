<?php $page = $this->router->fetch_class();
if (!$this->input->is_ajax_request()) {
  if (isset($this->session->userdata['logged_in']['userid']) && $page != "login") {
    $this->load->view('templates/header');
    if(admin_role() == Users_model::MD){
        $this->load->view('templates/leftmenu_MD', (isset($button_vars) ? $button_vars : null));
    }else{
        $this->load->view('templates/leftmenu', (isset($button_vars) ? $button_vars : null));
    }
  }
  else {
    $this->load->view('templates/header-login');
  }
}
?>
<?php if (!$this->input->is_ajax_request() && isset($this->session->userdata['logged_in']['userid'])) { ?>

<?php } ?>
<div class="static-content-wrapper region-content" style="margin-bottom: 100px">
<?php 

$this->load->view($v);?>

<?php if (!$this->input->is_ajax_request() && isset($this->session->userdata['logged_in']['userid'])) { ?>
  
<?php } ?>
  </div>
<?php
if (!$this->input->is_ajax_request() && $page != "login") {
  $this->load->view('templates/footer');
}
?>
