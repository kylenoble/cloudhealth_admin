<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<style>
    .dataTables_wrapper {
        margin-bottom: 100px;
    }
    
    .dataTable tbody tr:hover{
        cursor: pointer;
    }
</style>
<?php $this->load->model('TIckets_model', 'tickets');?>
<table id='tickets_table' class="stripe row-border order-column" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Ticket #</th>
            <th>Concern</th>
            <th>Subject</th>
            <th>Status</th>
            <th>Requester</th>
            <th>Company</th>
            <th>Requested</th>
            <th>Updated</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($tickets as $ticket): 
            $ticket_replies = $this->tickets->getTicketLastUpdate($ticket['ticket_number']);
            $last_update = '-';
            if($ticket_replies && $ticket_replies['last_update']){
                $last_update = date('M d, Y h:i A', strtotime($ticket_replies['last_update']));
            }
            ?>
            <tr id="ticket-<?php echo $ticket['id']?>">
                <td class="center"><?php echo $ticket['ticket_number']; ?></td>
                <td class="center"><?php echo $ticket['concern']; ?></td>
                <td class="center"><?php echo $ticket['subject']; ?></td>
                <td class="center"><div class=""><?php echo $ticket['status']; ?></div></td>
                <td class="center"><?php echo $ticket['fullname']; ?></td>
                <td class="center"><?php echo $ticket['company_name'];?></td>
                <td class="center"><?php echo date('M d, Y h:i A', strtotime($ticket['date_created'])); ?></td>
                <td class="center"><?php echo $last_update; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div id="tickets-modal" class="modal modal-fixed-footer"></div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function () {
        $('#tickets_table').DataTable({
            scrollCollapse: true,
            scrollY: 400,
            scrollX: true,
            dom: 'Bfrtip',
            buttons: [{ 
                extend: 'excel', 
                text: 'Export to Excel',
                title: 'User Support Tickets as of ' + getDate(new Date()),
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0, 1, 2,3,4,5]
                    }
                }
            ]
        });
        
        $('.modal').modal({
            dismissible: false
        });
        
        $('#tickets_table').on('click', 'tbody tr', function() {
            var tmp_id = ($(this).attr('id')).split("-");
            var id = tmp_id[1];
            $.get(SITEROOT + '/dashboard/showTicketInfo/' + id, function(response){
                $('#tickets-modal').html(response);
                $('#tickets-modal').modal('open');
            });
        });
    });
</script>