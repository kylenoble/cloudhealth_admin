<style>
    textarea {
        width: 95%;
        height: 100%;
        margin: 5px 0 0 20px;
        border: 1px solid gray;
        padding: 10px;
    }
    
    .modal .modal-footer{
        height: auto;
    }
    
    .reply-div{
        border-top: 1px solid gray;
        margin: 30px 0 0 30px;
        padding: 10px;
    }
    
    .reply-div p{
        margin: 0;
        padding-top: 15px;
    }
    
    .modal-content span {
        color: #9e9e9e;
        font-size: 0.8em;
    }
    
    .modal.modal-fixed-footer .modal-content{
        height: calc(100% - 100px) !important;
        overflow-y: scroll;
    }
</style>
<?php $statusList = array(
    'Open' => 'Open', 'Pending' => 'Pending', 'Closed' => 'Closed', 'Resolved' => 'Resolved',
    );?>
<?php echo form_open(base_url() . 'dashboard/replyToTicket', array('id' => 'ticketReply-form')); ?>
<input type="hidden" value="<?php echo $ticket_info['id']?>" id="x" name="x" />
<input type="hidden" value="<?php echo $ticket_info['status'];?>" id="old_status" name="old_status" />
<div class="modal-content">
    <div class="alert alert-danger hidden" style="text-align: left;"></div>
    <h5 tabindex="1">Ticket#: <span style="color: blue"><?php echo $ticket_info['ticket_number'] ?></span> - <?php echo $ticket_info['subject'];?></h5>
    <p><?php echo $ticket_info['message'];?></p>
    <span>Requested: <?php echo date('M d, Y h:i A', strtotime($ticket_info['date_created']));?> | <?php echo $ticket_info['fullname'];?> (Company: <?php echo ($ticket_info['company_name'] != null && $ticket_info['company_id'] == 0 ? $ticket_info['company_name'] : $this->Users_model->getCompanyNameByID($ticket_info['company_id']));?>)</span>
    <div class="reply-div">
        <?php foreach($ticket_replies as $reply) :
            $user = $this->Users_model->get_adminDetails(array('id'=>$reply['repliedBy_userid']), 'name');
            ?>
        <p><?php echo $reply['reply_message'];?></p>
        <span>Replied: <?php echo date('M j, Y h:i:s A', strtotime($reply['datetime_reply']));?> | <?php echo $user['name'];?></span>
        <?php endforeach;?>
    </div>
</div>
<div class="modal-footer">
    <div style="float: left;width: 70%;text-align: left;">
        <textarea name="reply" id="reply"></textarea>
    </div>
    <div style="float: left;width: 30%;text-align: center;">
        Status: 
        <?php echo form_dropdown('status', $statusList, $ticket_info['status'], 'style = "width: auto;"')?>
        <br />
        <a class="modal-action waves-effect waves-red btn reply">Reply</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>
<?php echo form_close(); ?>
<script>
$(document).ready(function(){
    $(document).on('click', ".reply", function(e){
        e.preventDefault();
        e.stopPropagation();
        $("#reply").removeAttr('style');
        if($("#reply").val() != ""){
            $(this).attr('disabled', true)
            var data = jQuery("#ticketReply-form").serializeArray();
            $.post(SITEROOT + '/dashboard/replyToTicket', data, function(response){
                $(".alert-danger").addClass("hidden")
                if(response.result.success == 1){
                    var reply = "<p>"+response.result.message+"</p>";
                    reply += "<span class='small'>Replied: "+response.result.replied.date+" | "+response.result.replied.user+"</span>";
                    $(reply).hide().prependTo(".modal-content .reply-div").fadeIn(1000);
                    var position = $(".modal-content .reply-div p:first").position().top + $(".modal-content").scrollTop();
                    $(".modal-content").animate({scrollTop: position});
                    $("#reply").val("")
                    $(".reply").attr('disabled', false)

                    //add refresh class to close btn
                    $(".modal-close").addClass("refresh");
                }else{
                    $(".alert-danger").text(response.result.errors)
                    $(".alert-danger").removeClass("hidden")
                }
            }, 'json')
        }else{
            $("#reply").css({'border-color': 'red'})
        }
    })
})
</script>