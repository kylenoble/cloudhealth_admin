<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
        header("location: dashboard");
}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/login.js" ></script>


<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">

        <div class="row" style="margin-bottom: 0;">
            <div class="input-field col s12 center">
                <img src="<?php echo base_url(); ?>assets/images/logo/cha_logo_with_text.png" alt="CloudHealthAsia" class="responsive-img" style="max-width: 180px !important;">
                <div>
                    <p style="font-style: italic;">Enter your email address to reset your password.</p>
                </div>
            </div>
        </div>
        <div class="login-box">
            <?php if (isset($success_msg) && $success_msg) { ?>
                    <?php echo _error_message('success', $success_msg); ?>
            <?php } ?>

            <?php if (validation_errors()) { ?>
                    <div class="alert alert-warning">
                        <?php echo validation_errors(); ?>
                    </div>
            <?php } ?>

            <?PHP if (isset($error)): ?>
                    <?php echo $error; ?>
            <?php endif ?>

            <?php echo form_open('login/forgot_password', array('id' => 'vip-user-fpass-form', 'class' => 'forgot-form')); ?>

            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">email</i>
                    <?php echo form_input(array('id' => 'registered_email', 'name' => 'registered_email')); ?>
                    <label for="registered_email" class="center-align">Email Address</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <?php echo form_submit(array('id' => 'submit', 'value' => 'Reset Password', 'class' => 'btn waves-effect waves-light col s12')); ?>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m12">
                    <p class="margin right-align medium-small"><a href="<?php echo base_url()?>">Go to Login Page</a></p>
                </div>
            </div>

            <?php echo form_close(); ?>
        </div>

    </div>
</div>

<!-- ================================================
Scripts
================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/jquery-3.2.1.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/materialize.min.js"></script>
<!--scrollbar-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/plugins.js"></script>