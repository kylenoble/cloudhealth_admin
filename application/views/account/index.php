<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
	$userid = ($this->session->userdata['logged_in']['userid']);
} else {
	header("location: login");
}
?>
<div class="static-content-inner">

    <div class="static-content" id="account_management" style="margin-bottom: 100px;">
        <ul class="myTabs">
            <li class="active"><a onclick="showPage(this)" id="access_management" >ACCESS MANAGEMENT</a></li>
            <li><a onclick="showPage(this)" id="subscriptions">SUBSCRIPTIONS AND RENEWALS</a></li>
            <li><a onclick="showPage(this)" id="approvals">APPROVALS</a></li>
        </ul>
        <div class="page-content" id="content_view"></div>
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    var default_page = "<?php echo ($default_page ? '#'.$default_page : '#access_management'); ?>";
    $(document).ready(function(){
        showPage(default_page);
    });
    
    function showPage(obj){
        var url = SITEROOT + '/account/users';
        var page = $(obj).attr("id");
        if(page == "subscriptions"){
            url = SITEROOT + '/account/subscriptions_and_renewals';
        }
        if(page == "approvals"){
            url = SITEROOT + '/account/approvals';
        }
        show_waitMe(jQuery('body'));
        $.get(url, function (response) {
            $("#account_management ul li").removeClass('active');
            $(obj).closest('li').addClass('active');
            hide_waitMe();
            $("#content_view").html(response);
        });
    }

</script>