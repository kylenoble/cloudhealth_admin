<style>
    .dataTables_wrapper {
        padding-right: 20px;
    }
</style>
<div class="title">Program Enrollment Summary</div>
<div class="clearfix"></div>
<table class="striped centered" id="summary">
    <thead>
        <tr>
            <th>Name of Program</th>
            <th>Total Enrolled</th>
            <th>Total Approved</th>
            <th>Total Declined</th>
            <th>Total Pending</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($summary as $rec) :?>
        <tr>
            <td><?php echo $rec['subscription_name']; ?></td>
            <td><?php echo $rec['enrolled']; ?></td>
            <td><?php echo $rec['approved'] ?></td>
            <td><?php echo $rec['declined']; ?></td>
            <td><?php echo $rec['pending']; ?></td>
        </tr><?php endforeach; ?>
    </tbody>
</table>
<div class="clearfix"></div>
<div class="title">PENDING APPROVAL</div>
<div class="clearfix"></div>
<table class="striped centered" id="pending">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Department</th>
            <th>Program Enrolled</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($pending as $rec) :?>
        <tr>
            <td><?php echo ($rec['is_anonymous'] == 'f' ? $rec['employee_id'] : 'Anonymous'); ?></td>
            <td><?php echo ($rec['is_anonymous'] == 'f' ? $rec['name'] : 'Anonymous'); ?></td>
            <td><?php echo $rec['branch']; ?></td>
            <td><?php echo $rec['department']; ?></td>
            <td><?php echo $rec['program_enrolled']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div class="clearfix"></div>
<div class="title">APPROVED ENROLLMENT</div>
<div class="clearfix"></div>
<table class="striped centered" id="approved">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Department</th>
            <th>Program Enrolled</th>
            <th>Date/Time of Approval</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($approved as $rec) :?>
        <tr>
            <td><?php echo $rec['employee_id'], ($rec['is_anonymous'] == 'f' ? '' : ' *'); ?></td>
            <td><?php echo $rec['name'], ($rec['is_anonymous'] == 'f' ? '' : ' *'); ?></td>
            <td><?php echo $rec['branch']; ?></td>
            <td><?php echo $rec['department']; ?></td>
            <td><?php echo $rec['program_enrolled']; ?></td>
            <td><?php echo $rec['date_approved']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div class="clearfix"></div>
<div class="title">DECLINED ENROLLMENT</div>
<div class="clearfix"></div>
<table class="striped centered" id="declined">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Department</th>
            <th>Program Enrolled</th>
            <th>Date/Time Declined</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($declined as $rec) :?>
        <tr>
            <td><?php echo ($rec['is_anonymous'] == 'f' ? $rec['employee_id'] : 'Anonymous'); ?></td>
            <td><?php echo ($rec['is_anonymous'] == 'f' ? $rec['name'] : 'Anonymous'); ?></td>
            <td><?php echo $rec['branch']; ?></td>
            <td><?php echo $rec['department']; ?></td>
            <td><?php echo $rec['program_enrolled']; ?></td>
            <td><?php echo $rec['date_declined']; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
$(document).ready(function() {

    $('#approved, #pending, #declined').DataTable({
        "oLanguage": {
            "sSearch": "Search by Name or Employee ID" //search
        },
        "columnDefs": [
            { "searchable": false, "targets": [2,3,4] },
        ],
        dom: 'Bfrtip',
        buttons: [{ 
            extend: 'pdf', 
            text: 'Export to PDF',
            title: 'List of '+$(this).attr('id')+' enrollment: ('+ COMPANY_NAME+')',
            orientation: 'landscape'
        }]
    });
    
    $('#summary').DataTable({
        "order": [[ 0, "asc" ]],
        "bFilter": false,
        dom: 'Bfrtip',
        buttons: [{ 
            extend: 'pdf', 
            text: 'Export to PDF',
            title: 'Program Enrollment Summary: ('+ COMPANY_NAME+')',
            orientation: 'landscape'
        }]
    });
})
</script>