<?php if(admin_role() == Users_model::HR) :?>
<div class="title">LIST OF USERS AND ACCESS</div>
<?php endif;?>
<style>
    .unlock-survey-form{
        padding: 2px 10px;
    }
</style>
<div class="container-fluid">
    <table id='users' class="stripe row-border order-column" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Employee ID</th>
                <th>Name</th>
                <th>Branch</th>
                <th>Department</th>
                <th>Email Address</th>
                <th>Job Grade</th>
                <th>Status</th>
                <th>Gender</th>
                <th>Date Registered</th>
                <th>Date Revoked</th>
                <?php if(!$view_only) :?>
                <th>Access</th>
                <?php endif;?>
                <th>Last Login</th>
                <th>Last Survey Update</th>
                <th>Survey Status</th>
                <?php if(admin_role() == Users_model::HR):?>
                <th class="unlock-chkbox">
                    <input type="checkbox" name="checkAllLocked" id="checkAllLocked" title="Select/Deselect Locked Responses" />
                    <label for="checkAllLocked" title="Check to select all users with locked Health Survey Form"></label>
                    <a href='javascript:void(0)' title="Click to unlock selected" class="btn-primary btn unlock-survey-form">Unlock</a>
                </th>
                <?php endif;?>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($users as $key => $user) {
                $survey_status = '-';
                $survey_status_unlock = '';
                if($user['healthsurvey_status'] !== null){
                    $survey_status = $user['healthsurvey_status'];
                    if($survey_status == 'Locked' && $user['status'] != '3'){
                        $survey_status_unlock = '<input type="checkbox" name="checkMe_'. $user['id'] .'" id="checkMe_'. $user['id'] .'" /><label for="checkMe_'. $user['id'] .'" title="Check to unlock Health Survey Form"></label>';
                    }
                }
                ?>
                <tr>
                    <td><?php echo addslashes($user['employee_id']); ?></td>
                    <td>
                        <?php if(admin_role() == Users_model::HR && $user['status'] != '3'):?>
                        <a href="/account/update/<?php echo $user['id'];?>" class="page_ajaxify" data-callback="/account/update/<?php echo $user['id'];?>">
                            <?php echo addslashes($user['name']); ?>
                        </a>
                        <?php else: ?>
                        <?php echo addslashes($user['name']); ?>
                        <?php endif;?>
                    </td>
                    <td><?php echo addslashes($user['branch']); ?></td>
                    <td><?php echo addslashes($user['department']); ?></td>
                    <td><?php echo addslashes($user['email']); ?></td>
                    <td style="text-align: center"><?php echo $user['job_grade']; ?></td>
                    <td style="text-align: center"><?php echo $user['high_risk']; ?></td>
                    <td><?php echo $user['gender']; ?></td>
                    <td><?php echo date('Y/m/d h:iA', strtotime($user['created_at'])); ?></td>
                    <td style="text-align: center"><?php echo ($user['date_revoked'] != null ? date('Y/m/d h:iA', strtotime($user['date_revoked'])) : '-'); ?></td>
                    <?php if(!$view_only) :?>
                    <td style="text-align: center">
                        <?php if($user['status'] == '3'){
                            echo '<span class="ti ti-lock"></span>';
                        }else{
                            $class = 'ti-unlock';
                            $access = ' Revoke';
                            echo '<a href="Javascript:void(0);" id="'.$user['id'].'" class="user-access">'
                                . '<span class="ti ti-unlock"></span> Revoke</a>';
                        } ?>
                    </td>
                    <?php endif;?>
                    <td style="text-align: center">
                        <?php echo $user['last_login'] ? date('Y-m-d h:i A', strtotime($user['last_login']) + (8*3600)) : ''; ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo $user['last_survey_update'] ? date('Y-m-d h:i A', strtotime($user['last_survey_update'])) : ''; ?>
                    </td>
                    <td style="text-align: center;"><?php echo $survey_status;?></td>
                    <?php if(admin_role() == Users_model::HR):?>
                    <td style="text-align: center;"><?php echo $survey_status_unlock;?></td>
                    <?php endif;?>
                </tr>
            <?php }
            ?>
        </tbody>
    </table>
</div>
<?php if(admin_role() == Users_model::HR) :?>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/account.js"></script>
<?php endif;?>