<style>
    form{
        padding: 20px;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }
</style>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="title">UPDATE USER INFO</div>
            <div class="container-fluid">
                <div data-widget-group="group1" class="ui-sortable">
                    <div class="row user-update">
                        <div class="col-sm-12">
                            <div data-widget='{"draggable": "false"}' class="panel panel-midnightblue">
                                <?php echo _error_message('danger'); ?>
                                <?php echo form_open(base_url() . 'account/update', array('id' => 'useraccount-update')); ?>
                                <input type='hidden' name='id' id='id' value="<?php echo $id ?>">	

                                <div class="form-group">
                                    <?php echo form_label('Employee ID<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'employee_id', 'name' => 'employee_id', 'class' => 'form-control', 'value' => $employee_id)); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control', 'value' => $name)); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Email<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => $email)); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Gender', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php $genderList = array('Female' => 'Female', 'Male' => 'Male');
                                            echo form_dropdown('gender', $genderList, $gender, 'class="form-control select-2 select2-hidden-accessible"'); 
                                        ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Date of Birth<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('id' => 'dob', 'name' => 'dob', 'class' => 'form-control datepicker', 'value' => $dob)); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo form_label('Branch', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php 
                                        //add empty option for branch
                                        $branchList = array_merge(array('' => ''), $branchList);
                                        echo form_dropdown('branch', $branchList, $branch, 'class="form-control select-2 select2-hidden-accessible"'); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Department', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php 
                                        //add empty option for department
                                        $departmentList = array_merge(array('' => ''), $departmentList);
                                        echo form_dropdown('department', $departmentList, $department, 'class="form-control select-2 select2-hidden-accessible"'); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Job Grade', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('job_grade', $job_gradeList, $job_grade, 'class="form-control select-2 select2-hidden-accessible"'); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo form_label('Status', '', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php $high_riskList = array('0' => 'Others', '1' => 'High Value', '2' => 'High Risk');
                                            echo form_dropdown('high_risk', $high_riskList, ($high_risk == null || $high_risk == '0' ? '0' : $high_risk), 'class="form-control select-2 select2-hidden-accessible"'); 
                                        ?>
                                    </div>
                                </div>
                                
<?php echo form_close(); ?>

                                <div class="clearfix"></div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">&nbsp;</div>
                                            <div class="col-sm-8">
                                                <a href='javascript:void(0)' class="btn-primary btn edit-account-save">Save</a>
                                                <a data-callback="/account" class="btn-default btn page_ajaxify" parent-callback="account/index">Cancel</a>
                                            </div>
                                        </div>  <!-- form group -->
                                    </div><!-- row -->
                                </div><!-- panel footer -->
                            </div><!-- panel -->
                        </div><!-- col 12 -->
                    </div><!-- add user -->
                </div><!-- ui s -->
            </div><!-- container -->
        </div>
        <!-- page-content -->
    </div>
    <!-- static-content -->
</div>
<!-- static-content-wrapper -->
<script>
$(document).ready(function(){
    $(".edit-account-save").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        data.callback = 'account/save';
        data.form_data = null;
        data.form_data = jQuery("#useraccount-update").serializeArray();
     
        var response = vip_ajax(data);
        if (response.result.success == 1) {
            data.callback = 'account/index';            
            page_ajaxify(data);
        } else {
            $(".alert-danger").removeClass('hidden');
            $(".alert-danger").html(response.result.errors);
        }
    })
    
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });
})
</script>