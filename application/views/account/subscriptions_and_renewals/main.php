<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<style>
    .modal-content {
        /*height: auto !important;*/
    }
    
    #employeelist.modal-content{
        padding: 0 !important;
        max-height: calc(100vh - 400px) !important;
    }
    
    #enroll-confirm .modal-content{
        padding: 0 !important;
        max-height: calc(100vh - 330px) !important;
    }
    
    .modal-header {
        margin: 20px;
    }
    
    #enroll-confirm .modal-footer{
        height: 110px;
        padding-right: 20px;
    }
    
    /* adjusts second modal's opacity'*/
    .modal-overlay ~ .modal-overlay{
        opacity: 0 !important;
    }
    
    #renewal-form.modal {
        width: 35%;
    }
    
    #renewal-form.modal .modal-content {
        padding-top: 0;
    }
    
    #renewal_remarks{
        height: 100px;
    }
    
    span#package-name{
        font-size: 15px;
        color: #00bcd4;
        font-weight: bold;
        font-style: italic;
    }
    
    select {
        height: 30px;
        display: inline;
        width: 180px; 
        max-width: 300px;
    }
    
    fieldset{
        border-radius: 10px;
        width: 100%;
    }
    
    fieldset > div {
        display: inline-block;
        width: 24%;
    }
    
    table#package-subscriptions td{
        padding: 3px;
    }
    
    div#employeelist{
        height: 50%;
        overflow: auto;
    }
    
    a{
        cursor: pointer;
    }
    
    .static-content-wrapper{
        margin-bottom: 0 !important;
    }

</style>
<?php _load_js($js); ?> 

<div id="active-subscriptions-container"></div>
<div class="title">SUBSCRIBE TO PACKAGES</div>
<div class="clearfix"></div>
<?php $disabled = '';
if(!$with_report) :
$disabled = 'disabled'; ?>
<div class="alert alert-warning" style="margin-bottom: 0;padding: 10px;">
    You may enroll your employees to our programs as soon as the medical team sends in their analysis and recommendation.
</div>
<?php endif;?>

<div class="container-fluid">
    <table class="display responsive nowrap" cellspacing="0" id="package-subscriptions">
        <thead>
            <tr>
                <th></th>
                <th>Programs</th>
                <th>Details</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($all_programs as $program):?>
            <tr>
                <td><h6><?php echo $program['is_basic'] ? 'BASIC PROGRAMS' : 'OTHERS'?></h6></td>
                <td><?php echo $program['subscription_name'];?></td>
                <td style="text-align: center;">
                    <a data-code="<?php echo $program['shortcode'];?>" data-view="details" title="View Details">View Details</a>
                </td>
                <td>
                    <a class="waves-effect waves-light btn btn-small" <?php echo $disabled;?> <?php echo (!$program['enrolled'] ? 'disabled' : '');?> data-target="package-enroll-form" name="<?php echo $program['shortcode'];?>" id="<?php echo $program['shortcode'];?>" data-subscription="<?php echo $program['company_subscription_id'];?>" data-package-name="<?php echo $program['subscription_name'];?>" data-package-type="<?php echo $program['is_basic'] ? 'Basic' : 'telemed'?>">Enroll Employees</a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>

<!-- SUBSCRIPTION INFO -->
<div id="subscription-info-modal" class="modal modal-fixed-footer">
    <div class="modal-content"></div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
    </div>
</div>

<!-- EMPLOYEE ENROLLMENT FORM -->
<div class="modal modal-fixed-footer" id="package-enroll-form">
    <?php echo form_open(base_url() . 'account/enroll_to_package', array('id' => 'form-enroll')); ?>
    <div class="modal-header">
        <p>List of employees recommended for <span class="small-note" id="package-name"></span></p>
        <fieldset id="filters">
            <legend class="small-note" style="padding: 0 5px;">Filter By</legend>
            <div class="doh_filter hidden">
                <label>Determinants of Health: </label>
                <select name="doh" id="doh" placeholder="Determinants of Health">
                    <option value="">- ALL -</option>
                    <option value="weightscore">Weight</option>
                    <option value="nutrition">Nutrition</option>
                    <option value="detox">Detox</option>
                    <option value="exercise">Movement</option>
                    <option value="sleep">Sleep</option>
                    <option value="stress">Stress</option>
                </select>
            </div>
            <div>
                <label>Health Status: </label>
                <select name="health_status" id="health_status" placeholder="Health Status">
                    <option value="">- ALL -</option>
                    <option value="Optimal">Optimal</option>
                    <option value="Suboptimal">Suboptimal</option>
                    <option value="Neutral">Neutral</option>
                    <option value="Compromised">Compromised</option>
                    <option value="Alarming">Alarming</option>
                </select>
            </div>
            <div>
                <label>Job Grade: </label>
                <select name="job_grade" id="job_grade_filter" placeholder="Job Grade">
                    <option value="" selected>- ALL -</option>
                    <?php foreach($jobgrades as $jobgrade) :?>
                    <option value="<?php echo $jobgrade['job_grade'];?>"><?php echo $jobgrade['job_grade'];?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div>
                <label>Risk Status: </label>
                <select name="risk_status" id="risk_status_filter" placeholder="Risk Status">
                    <option value="" selected>- ALL -</option>
                    <option value="High Value">High Value</option>
                    <option value="High Risk">High Risk</option>
                    <option value="Others">Others</option>
                </select>
            </div>
        </fieldset>
    </div>
    <div class="modal-content" id="employeelist">
        
    </div>
    
    <div class="modal-footer">
        <a class="waves-effect waves-light btn enroll-confirm-btn" data-target="enroll-confirm">Enroll</a>
        <a id="cancel_enroll_form" class="btn-default btn-flat">Cancel</a>
    </div>
    <?php echo form_close(); ?>
</div>

<!-- EMPLOYEE ENROLLMENT CONFIRMATION WINDOW -->
<div id="enroll-confirm" class="modal modal-fixed-footer">
    <div class="modal-header">You are enrolling the following employees to the program <span class="small-note" id="package-name-confirm"></span>: </div>
    <div class="modal-content">
        <div id="confirm-list"></div>
    </div>
    <div class="modal-footer">
        <p>Total number of employees enrolled: <span class="small-note" id="confirm-list-cnt"></span></p>
        <span class="small-note">Do you wish to continue?</span> &nbsp;&nbsp;
        <a class="modal-action waves-effect waves-green btn enroll-to-package">Yes, enroll these employees</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    </div>
</div>

<!-- SUBSCRIPTION RENEWAL REQUEST FORM -->
<div id="renewal-form" class="modal">
    <input type="hidden" id="company_enrollment_id" />
    <div class="modal-header">Subscription Renewal Request for <span class="small-note" id="renewal-package-name"></span>: </div>
    <div class="modal-content">
        <label>Notes: </label>
        <textarea name="renewal_remarks" id="renewal_remarks" rows="4" cols="50"></textarea>
    </div>
    <div class="modal-footer">
        <a class="modal-action waves-effect waves-green btn send-renewal">Confirm</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function(){
    var groupColumn = 0;
    $("#package-subscriptions").DataTable({
        "columnDefs": [
            {"visible": false, "targets": [groupColumn, 0]}
        ],
        bFilter: false,
        "paging":   false,
        "bInfo" : false,
        "ordering": false,
        "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;

            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr class="group"><td colspan="4">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
    
    $('.modal').modal({
        dismissible: false
    });
    
    $("#active-subscriptions-container").load(SITEROOT + '/account/getActiveSubscriptions', function(){
     
        /****************************
        *   CANCEL SUBSCRIPTION    *
        ****************************/
       $("#active-subscriptions").find("a[id^=cancel]").each(function(){
           $(this).on("click", function(){
               var tmp_id = ($(this).attr('id')).split("-");
               var company_enrollment_id = tmp_id[1];
               var package_name = $("#active-subscriptions").find("td#"+company_enrollment_id).text();
               if(confirm("Are you sure you want to cancel your subscription in "+package_name+"?")){
                   $.get(SITEROOT + '/account/cancelSubscription/' + company_enrollment_id, function(){
                        var data = {};
                        data.callback = '/account';
                        data.form_data = {page: 'subscriptions'};
                        page_ajaxify(data);
                   })
               }
           });
       });
              
        /****************************
        *    RENEW SUBSCRIPTION    *
        ****************************/
        $("#active-subscriptions").find("a[id^=renew]").each(function(){
            $(this).on("click", function(){
               var tmp_id = ($(this).attr('id')).split("-");
               var company_enrollment_id = tmp_id[1];
               var package_name = $("#active-subscriptions").find("td#"+company_enrollment_id).text();
               
                $('#renewal-form').modal('open');
                $("#renewal-package-name").text(package_name);
                $("#company_enrollment_id").val(company_enrollment_id);
                $("#renewal_remarks").val("");
           });
       });
       
        $('.send-renewal').click(function(e){
            $.post(SITEROOT + '/account/renewSubscription', {id: $("#company_enrollment_id").val(), notes: $("#renewal_remarks").val()}, function(){
                var data = {};
                data.callback = '/account';
                data.form_data = {page: 'subscriptions'};
                page_ajaxify(data);

                $(".modal-overlay").remove()
                $("body").removeAttr('style');
            });
        });
    })
    
    /****************************
     *  SUBSCRIBE TO PACKAGES   *
     ****************************/
    var program_code = null;
    var company_subscription_id = 0;
    $('table#package-subscriptions tbody a[data-target="package-enroll-form"]').click(function(){
        var package_name = $(this).attr("data-package-name");
        $("#employeelist").html("");

        if($(this).data('package-type') != 'Basic'){
            //TELEMEDICINE
            $(".doh_filter").removeClass("hidden");
        }else{
            //BASIC PROGRAMS
            $(".doh_filter").addClass("hidden")
        }
        $('#package-enroll-form').modal('open');
        $("#package-name").text(package_name);
        $("div#package-enroll-form #filters select").val("");
        var data = {}
        data['program_code'] = $(this).attr('name');
        data['company_subscription_id'] = $(this).data('subscription');
        program_code = $(this).attr('name');
        company_subscription_id = $(this).data('subscription');
        showEmployeeList(data);
    });
    
    $(document).on('click', '#cancel_enroll_form', function(){
        $('#package-enroll-form').modal('close');
    });

    $('#filters select#health_status, #filters select#doh, #filters select#job_grade_filter, #filters select#risk_status_filter').change(function(){
        onFilterChange();
    });
    
    function onFilterChange(){
        var filter_data = {}
        var filter = null
        if($("#health_status").val() !== "" ){
            filter = $("#health_status").attr('name')
            filter_data[filter] = $("#health_status").val()
        }
        if($("#job_grade_filter").val() !== "" ){
            filter = $("#job_grade_filter").attr('name')
            filter_data[filter] = $("#job_grade_filter").val()
        }
        if($("#risk_status_filter").val() !== "" ){
            filter = $("#risk_status_filter").attr('name')
            filter_data[filter] = $("#risk_status_filter").val()
        }
        if($("#doh").val() !== "" ){
            filter = $("#doh").attr('name')
            filter_data[filter] = $("#doh").val()
        }
        filter_data['program_code'] = program_code;
        filter_data['company_subscription_id'] = company_subscription_id;
        showEmployeeList(filter_data);
    }
    
    function showEmployeeList(data){
        show_waitMe($("#employeelist"));
        $("#package-enroll-form").find(".alert-warning").remove();
        $.post(SITEROOT + '/account/showEmployeeList', data, function(result){
            $("#employeelist").html(result);
            hide_waitMe();
        });
        
    }
    
    $("#enroll-confirm .enroll-to-package").click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var data = {};
        data.callback = '/account/enroll_to_package';
        data.form_data = null;
        data.form_data = jQuery("#form-enroll").serializeArray();
        
        var response = vip_ajax(data);
        if (response.result.success > 0) {
            var data1 = {};
            data1.callback = '/account';
            data1.form_data = {page: 'subscriptions'};
            page_ajaxify(data1);
            $('#enroll-confirm').modal('close');
            $('#package-enroll-form').modal('close');
            $(".modal-overlay").remove()
            $("body").removeAttr('style');
        } else {
            $("#enroll-confirm").modal("close")
            var warning = '<div class="alert alert-warning" style="margin-bottom: 0;padding: 10px;">'+response.result.errors+'</div>';
            $("#package-enroll-form .modal-header").after(warning);
        }
    })
    
    $('#package-subscriptions a[data-view="details"]').click(function(){
        $.get(SITEROOT + '/account/showSubscriptionDetails/' + $(this).data("code"), function(response){
            $('#subscription-info-modal').modal('open');
            $('#subscription-info-modal .modal-content').html(response)
        });
    });
});
</script>