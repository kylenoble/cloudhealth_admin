<div class="title">SUBSCRIPTIONS</div>
<div class="clearfix"></div>
<table class="striped centered" id="active-subscriptions">
    <thead>
        <tr>
            <th>Service Name</th>
            <th>Date of Subscription</th>
            <th>Expiration Date</th>
            <th>Number of Slots</th>
            <th>Enrollees</th>
            <th>Status</th>
            <th>Options</th>
        </tr>
    </thead>

    <tbody>
    <?php foreach ($subscriptions as $program) :
        $expired = false;
        if($program['subscription_end'] < date('Y-m-d', time()) && ($program['status'] != 'Completed' && $program['status'] != 'Cancelled')){
            $expired = true;
        }
            ?>
        <tr style="<?php echo ($expired ? 'color: red' : '')?>">
            <td id="<?php echo $program['company_enrollment_id']; ?>"><?php echo $program['subscription_name']; ?></td>
            <td><?php echo date('M j, Y', strtotime($program['subscription_start'])); ?></td>
            <td><?php echo $program['subscription_end'] ? date('M j, Y', strtotime($program['subscription_end'])) : '-' ?></td>
            <td class="center"><?php echo ($program['allowed_number_of_users'] == 0 ? 'Unlimited' : $program['allowed_number_of_users']);?></td>
            <td class="center">
                <a data-code="<?php echo $program['program_code'];?>" data-view="enrollees" title="View List" data-id="<?php echo $program['company_enrollment_id'];?>">View List</a>
            </td>
            <td><?php echo $program['status'];?></td>
            <td>
                <?php if($program['status'] == 'Ongoing') :?>
                <a href="Javascript:void(0);" id="cancel-<?php echo $program['company_enrollment_id']; ?>">Cancel</a>
                &nbsp;&nbsp;
                <a href="Javascript:void(0);" id="renew-<?php echo $program['company_enrollment_id']; ?>">Renew</a>
                <?php else: ?>
                -
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="clearfix"></div>
<script>
$(document).ready(function(){
    /****************************
     *   ACTIVE SUBSCRIPTIONS   *
     ****************************/
    $("#active-subscriptions").DataTable({
        "order": [[4, "desc"]],
        bFilter: false,
        dom: 'Bfrtip',
        buttons: [{
                extend: 'pdf',
                text: 'Export to PDF',
                title: 'Active Subscriptions: (' + COMPANY_NAME + ')',
                orientation: 'landscape',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }
        ]
    });
    
    $('#active-subscriptions a[data-view="enrollees"]').click(function(){
        show_waitMe(jQuery('body'));
        $.get(SITEROOT + '/account/showSubscriptionEnrollees/' + $(this).data("id"), function(response){
            $('#subscription-info-modal').modal('open');
            $('#subscription-info-modal .modal-content').html(response)
            hide_waitMe();
        });
    });

});
</script>