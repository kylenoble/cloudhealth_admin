<table class="striped centered" id="enrolled_employee_table">
    <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Department</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($enrolled_employees as $employee) : 
            $employee_id = $employee['employee_id'];
            $employee_name = trim($employee['name']);
            if(!$is_CHI && $employee['status'] == 'Approved'){
                $employee_id .= ($employee['is_anonymous'] == 't' ? ' *' : '');
                $employee_name .= ($employee['is_anonymous'] == 't' ? ' *' : '');
            }else{
                $employee_id = ($employee['is_anonymous'] == 't' ? 'Anonymous' : $employee_id);
                $employee_name = ($employee['is_anonymous'] == 't' ? 'Anonymous' : $employee_name);
            }
            ?>
            <tr>
                <td><?php echo $employee_id; ?></td>
                <td><?php echo $employee_name; ?></td>
                <td><?php echo $employee['branch']; ?></td>
                <td><?php echo $employee['department']; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>
<script>
$(document).ready(function(){
    /****************************
     *   ACTIVE SUBSCRIPTIONS   *
     ****************************/
    $("#enrolled_employee_table").DataTable({
        "order": [[0, "asc"]],
        bFilter: false,
        dom: 'Bfrtip',
        buttons: [{
                extend: 'pdf',
                text: 'Export to PDF',
                title: COMPANY_NAME + ' - <?php echo $subscription['subscription_name'], ' - Enrolled Employee List'?>',
                orientation: 'landscape',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }
        ]
    });

});
</script>