<style>
    .disabled{
        color: #ccc;
    }
    #employee_table{
        margin: 20px 0 40px 0;
    }
</style>
<input type="hidden" value="<?php echo $program_code;?>" id="program_code" name="program_code" />
<table class="striped centered" id="employee_table">
    <thead>
        <tr>
            <th><input type="checkbox" name="checkAll" id="checkAll" /><label for="checkAll"> </label></th>
            <th>Employee ID</th>
            <th>Employee Name</th>
            <th><?php echo ($doh == null ? 'OVERALL Health Status' : 'Health Status')?></th>
            <th>Job Grade</th>
            <th>Risk Status</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($employees as $employee) :?>
        <tr class="<?php echo $employee['enrolled'], ' ', $employee['enrolled_other_package']; ?>">
            <td><input type="checkbox" <?php echo $employee['enrolled'], ' ', $employee['enrolled_other_package'];?> name="check[<?php echo $employee['user_id'];?>]" id="check-<?php echo $employee['user_id'];?>" /><label for="check-<?php echo $employee['user_id'];?>"></label></td>
            <td><?php echo ($employee['status'] == Users_model::$ANONYMOUS ? 'Anonymous' : $employee['employee_id']), ($employee['enrolled'] != '' || $employee['enrolled_other_package'] != '' ? ' (Already enrolled)' : '');?></td>
            <td><?php echo ($employee['status'] == Users_model::$ANONYMOUS ? 'Anonymous' : $employee['name']);?></td>
            <td><?php echo $employee['health_status']//, ' - ', $employee[$doh];?></td>
            <td><?php echo $employee['job_grade'];?></td>
            <td><?php echo $employee['high_risk'];?></td>
        </tr>
        <?php endforeach;?>
    </tbody>

</table>