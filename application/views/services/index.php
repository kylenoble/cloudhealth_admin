<?php
if (isset($this->session->userdata['logged_in']['userid'])) {
    $userid = ($this->session->userdata['logged_in']['userid']);
    $is_md = (admin_role() == Users_model::MD);
} else {
    header("location: login");
}
?>
<style>
    ul.myTabs li {
        width: 33.3%;
    }
</style>
<div class="static-content-inner">

    <div class="static-content" id="services" style="margin-bottom: 100px;">
        <ul class="myTabs">
            <li class="active"><a onclick="showPage(this)" id="queuing">QUEUING</a></li>
            <li class="<?php echo $is_md ? 'hidden' : ''?>"><a onclick="showPage(this)" id="marketplace">MARKETPLACE</a></li>
            <li><a onclick="showPage(this)" id="user_access_management" >USER AND ACCESS MANAGEMENT</a></li>
        </ul>
        <div class="page-content" id="content_view"></div>
    </div><!-- static-content -->
</div><!-- static-content-wrapper -->
<script>
    var default_page = "<?php echo ($default_page ? '#' . $default_page : '#queuing'); ?>";
    $(document).ready(function () {
        $(".myTabs li").css('width', 97 / ($(".myTabs").find("li:visible").length) + '%')
        showPage(default_page);
    });

    function showPage(obj) {
        var page = $(obj).attr("id");
        var url = SITEROOT + '/services/' + page;
        var subpage = {}
        show_waitMe(jQuery('body'));
        if(page == 'queuing' && '<?php echo $sub_page?>' != ''){
            subpage = {page: '<?php echo $sub_page?>'}
        }
        $.post(url, subpage, function (response) {
            $("#services ul li").removeClass('active');
            $(obj).closest('li').addClass('active');
            hide_waitMe();
            $("#content_view").html(response);
        });
    }

</script>