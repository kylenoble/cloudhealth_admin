<div class="alert alert-success hidden" style="margin:0"></div>
<table id='administrators_table' class="stripe row-border order-column" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Status</th>
            <?php if($type == 'administrators') :?>
            <th>Department</th>
            <?php else:?>
            <th>Company</th>
            <?php endif;?>
            <th>Name</th>
            <th>Access Level</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<script>
    $(document).ready(function () {
        var user_type = '<?php echo $type?>';
        var oTable = $('#administrators_table').DataTable({
            "ajax": SITEROOT + '/Services/getAllUsers/<?php echo $type?>',
            "order": [[0, "asc"]],
            dom: '<"custom-toolbar">frtip',
            "displayLength": 10,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"},
                {
                    "targets": 0,
                    "visible": false,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        var status = 'all';
                        if(row.status == 1){
                            status = 'active';
                        }else if(row.status == null || row.status == '') {
                            status = 'newly created';
                        }else if(row.status == 3){
                            status = 'revoked';
                        }
                        return status
                    }
                },
                {
                    "targets": 1,
                    "visible": true,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        if(user_type == 'hr'){
                            return row.company_name
                        }else{
                            var department = 'CHA Admin'
                            if(row.role == 3){
                                department = 'Medical'
                            }else if(row.role == 4) {
                                department = 'Sales'
                            }else if(row.role == 5) {
                                department = 'Ops'
                            }else if(row.role == 6) {
                                department = 'Finance'
                            }
                            return department
                        }
                    }
                },
                {
                    "targets": 2,
                    "visible": true,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        return '<span id="admin-name_'+row.id+'">' + row.name + '</span>'
                    }
                },
                {
                    "targets": 3,
                    "visible": true,
                    "searchable": false,
                    data: function(){
                        return 'Default'
                    }
                },
                {
                    "targets": 4,
                    "visible": true,
                    "searchable": false,
                    data: function(row, type, val, meta){
                        var html = ' - '
                        if(row.role > 2){
                            html = '<a class="btn btn-small revoke" id="revoke-access_'+row.id+'" >Revoke Admin Access</a>'
                        }else if(row.role == 2){
                            html = '<a class="btn btn-small revoke" id="revoke-access_'+row.id+'" >Revoke Access</a>'
                        }
                        if(row.status == 3){
                            html = row.date_revoked
                        }
                        return html
                    }
                }
            ]
        });

        var status = 'Status: <select id="user-status"><option value="all" selected>ALL</option>'
        status += '<option value="newly created">New</option>'
        status += '<option value="active">Active</option>'
        status += '<option value="revoked">Revoked</option></select>'
        $(".custom-toolbar").html(status)
        
        if(user_type == 'administrators'){
            var department = '&nbsp;&nbsp;&nbsp;Department: <select id="user-department"><option value="all" selected>ALL</option>'
            department += '<option value="CHA Admin">CHA Admin</option>'
            department += '<option value="Medical">Medical</option>'
            department += '<option value="Sales">Sales</option>'
            department += '<option value="Ops">Ops</option>'
            department += '<option value="Finance">Finance</option></select>'
            $(".custom-toolbar").append(department)
        }else{
            $.getJSON(SITEROOT + '/Organizations/ajax_companies_select', function(html){
                $(".custom-toolbar").append('&nbsp;&nbsp;&nbsp;'+ html.active_companies)
                
                if(user_type == 'hr'){
                    $("div#administrators_table_wrapper select#active-companies").on("change", function(){
                        oTable.columns().search("").draw()
                        oTable.column(1).search( $(this).val() ).draw()
                        $("input[type=search]").val("")
                        $("select#user-status").val("all")
                    })
                }
            })
        }

        $("select#user-status").on("change", function(){
            oTable.columns().search("").draw()
            var last_col_text = 'Action'
            if($(this).val() == 'revoked'){
                last_col_text = 'Date Revoked';
            }
            $("table").find("thead th:last").text(last_col_text)
            if($(this).val() != 'all'){
                oTable.column(0).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
            $("select#user-department").val("all")
            $("select#active-companies").val("")
        })
        
        $("select#user-department").on("change", function(){
            oTable.columns().search("").draw()
            if($(this).val() != 'all'){
                oTable.column(1).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
            $("select#user-status").val("all")
        })

        $(document).off().on('click', "a.revoke", function(){
            if(confirm("Are you sure you want to revoke user's admin access?")){
                show_waitMe(jQuery('body'));
                var tmp_id = ($(this).attr("id")).split("_")
                $.get(SITEROOT + '/Services/delete_admin/' + tmp_id[1], function(response){
                    oTable.ajax.reload(function () {
                        $("select#user-status").val("revoked").trigger("change")
                        $("select#active-companies").val("")
                    });
                    hide_waitMe();
                    var admin_name = $("span#admin-name_" + tmp_id[1]).text()
                    $(".alert-success").html("<strong>" + admin_name + "</strong>'s admin access was successfully revoked.").removeClass("hidden")
                    hide_waitMe();
                    $('html,body').scrollTop(0);
                })
            }
        })
        
    })
</script>