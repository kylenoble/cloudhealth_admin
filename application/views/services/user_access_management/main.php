<style>
    .small-tabs{
        width: 13.9%;
        background-color: gray;
    }
</style>
<?php
$is_admin = (admin_role() == Users_model::ADMIN);
?>
<div class="small-tabs-container <?php echo (!$is_admin ? 'hidden': '')?>" style="margin-bottom: 30px;">
    <div class="small-tabs active">
        <a onclick="showMyPage(this)" id="corporate_users" data-method="getCorporateUsers">Corporate Users</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="administrators" data-method="getUsers">Administrators</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="hr" data-method="getUsers">HR</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="medical" data-method="getUsers">Medical</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="sales" data-method="getUsers">Sales</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="ops" data-method="getUsers">Ops</a>
    </div>
    <div class="small-tabs">
        <a onclick="showMyPage(this)" id="finance" data-method="getUsers">Finance</a>
    </div>
</div>
<div id="access-subcontent-view"></div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".small-tabs").css('width', 97 / ($(".small-tabs-container").find(".small-tabs:visible").length) + '%')
        showMyPage("<?php echo ($default_page ? '#' . $default_page : '#corporate_users'); ?>");
    });

    function showMyPage(obj, is_refresh = false) {
        var method = $(obj).data('method')
        var url = SITEROOT + '/services/' + method + '/' + $(obj).attr("id");
        if (url !== null) {
            if (!is_refresh) {
                show_waitMe(jQuery('body'));
            }
            $.get(url, function (response) {
                $("div.small-tabs-container div.small-tabs").removeClass('active');
                $(obj).closest('div.small-tabs').addClass('active');
                hide_waitMe();
                $("#access-subcontent-view").html(response);
            });
        }
    }
</script>