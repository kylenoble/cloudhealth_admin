<style>
    table h6{
        display: inline-block;
        min-width: 100px;
    }
    
    table .list {
        margin-left: 30px;
    }
    
    #usersList-modal.modal{
        max-height: 85% !important;
        height: 85%;
        width: 95%;
    }
    
    #usersList-modal .modal-content{
        padding: 0 20px !important;
    }
    table tbody tr td a{
        cursor: pointer;
    }
    input[type="number"]{
        margin: 0 !important;
        width: 60px !important;
        height: 2rem !important;
        text-align: center;
    }
    
    a[id^="save-slot_"]{
        margin: 0;
    }
</style>
<table id='corporate_users_table' class="stripe row-border order-column" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Company</th>
            <th>ID</th>
            <th>Subscription</th>
            <th>Allowed Slots</th>
            <th>Slots Used</th>
            <th>Available Slots</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data as $rec) { ?>
            <tr>
                <td>
                    <h6 class="left"><?php echo $rec['company_name'] ?></h6>
                    <a class="btn list" data-x="<?php echo $rec['company_id'];?>" data-xx="<?php echo $rec['company_name'];?>">See List of Users</a>
                </td>
                <td><?php echo $rec['id']; ?></td>
                <td><?php echo $rec['subscription_name'] ?></td>
                <td class="center">
                    <input type="hidden" id="initial-allowed_number_of_users-<?php echo $rec['id']; ?>" value="<?php echo $rec['allowed_number_of_users'];?>" />
                    <span id="slot-text_<?php echo $rec['id']; ?>">
                        <span>
                        <?php echo ((int)$rec['allowed_number_of_users'] == 0 ? 'Unlimited' : $rec['allowed_number_of_users']);?>
                        </span>
                        <a id="update-slot_<?php echo $rec['id']; ?>" title="Update">
                            <i class="material-icons" style="vertical-align: middle;">edit</i>
                        </a>
                    </span>
                    <span class="hidden" id="edit-slot_<?php echo $rec['id']; ?>">
                        <input type="checkbox" <?php echo ($rec['allowed_number_of_users'] == 0 ? 'checked' : '');?> id="allowed_number_of_users-<?php echo $rec['id']; ?>" data-x="<?php echo $rec['id']; ?>" /><label for="allowed_number_of_users-<?php echo $rec['id']; ?>"> Unlimited</label><br />
                        <input type="number" step="1" min="1" class="form-control <?php echo $rec['allowed_number_of_users'] == 0 ? 'hidden' : '';?>" value="<?php echo $rec['allowed_number_of_users']?>" id="number_of_users-<?php echo $rec['id']; ?>" />
                        <a id="save-slot_<?php echo $rec['id'];?>" class="hidden save-slot" title="Save Changes">
                            <i class="material-icons" style="vertical-align: middle;">save</i>
                        </a>
                    </span>
                    <a id="cancel_<?php echo $rec['id'];?>" class="hidden" title="Cancel Update">
                        <i class="material-icons" style="vertical-align: middle;">cancel</i>
                    </a>
                </td>
                <td class='center' id="used-slots_<?php echo $rec['id'];?>"><?php echo $rec['used_slots'];?></td>
                <td class='center' id="available-slots_<?php echo $rec['id']?>"><?php echo $rec['available_slots'];?></td>
            </tr>
<?php } ?>
    </tbody>

</table>
<div id="usersList-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <div id="form-content"></div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-red btn-flat">Close</a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/buttons.html5.min.js"></script>
<script>
    $(document).ready(function () {
        var groupColumn = 0;
        $('#corporate_users_table').DataTable({
            "columnDefs": [
                {"visible": false, "targets": [groupColumn, 1]}
            ],
            "bInfo" : false,
            "order": [[groupColumn, 'asc'], [1, 'asc']],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                );

                        last = group;
                    }
                });
            }
        })
        
        $('.modal').modal({
            dismissible: false
        });
        
        $(".list").click(function(){
            var company_id = $(this).data("x")
            var company_name = $(this).data("xx")
            getUsers(company_id, company_name)
        })
        
        $("a[id^=update-slot_]").click(function(){
            var tmp_id = ($(this).attr("id")).split("_")
            $("#slot-text_" + tmp_id[1]).addClass("hidden");
            $("#edit-slot_" + tmp_id[1]).removeClass("hidden");
            $("#cancel_" + tmp_id[1]).removeClass("hidden");
            $("#save-slot_"+tmp_id[1]).addClass("hidden");
            
            var initial_slot = $("#initial-allowed_number_of_users-"+tmp_id[1]).val();
            $("#number_of_users-"+tmp_id[1]).val(initial_slot)
        })
        
        $("[id^=cancel_]").click(function(){
            var tmp_id = ($(this).attr("id")).split("_")
            $("#edit-slot_" + tmp_id[1]).addClass("hidden");
            $("#slot-text_" + tmp_id[1]).removeClass("hidden");
            $(this).addClass("hidden")
        })
        
        $("[id^=number_of_users-]").on("change", function(){
            var tmp_id = ($(this).attr("id")).split("-");
            if($("#initial-allowed_number_of_users-"+tmp_id[1]).val() != $(this).val()){
                $("#save-slot_"+tmp_id[1]).removeClass("hidden");
                $("#cancel_"+tmp_id[1]).removeClass("hidden");
            }else{
                $("#save-slot_"+tmp_id[1]).addClass("hidden");
                var initial_slot = $("#initial-allowed_number_of_users-"+tmp_id[1]).val();
                $("#number_of_users-"+tmp_id[1]).val(initial_slot);
            }
        })
        
        $("a[id^=save-slot_]").click(function(){
            var tmp_id = ($(this).attr("id")).split("_")
            var data = {
                ce_id: tmp_id[1],
                number_of_users: $("#number_of_users-" +tmp_id[1]).val(),
                allowed_number_of_users: $("#allowed_number_of_users-" + tmp_id[1]).val()
            }
            $.post(SITEROOT + "/Services/updateSlot", data, function(response){
                var text = 'Unlimited';
                var value = 0;
                var available = '-';
                if( ! $("#allowed_number_of_users-" + tmp_id[1]).is(":checked")){
                    text = $("#number_of_users-" +tmp_id[1]).val()
                    value = $("#number_of_users-" +tmp_id[1]).val()
                }
                
                $("#slot-text_"+ tmp_id[1] + " span").text(text)
                $("#initial-allowed_number_of_users-" + tmp_id[1]).val(value)
                if($("#initial-allowed_number_of_users-" + tmp_id[1]).val() > 0){
                    available = parseInt(value) - parseInt($("#used-slots_" + tmp_id[1]).text());
                }
                $("#available-slots_"+tmp_id[1]).text(available)
                
                console.log(text, "   ", $("#used-slots_" + tmp_id[1]).text())
                $("#cancel_" + tmp_id[1]).trigger("click")
            })
        })
        
        $("[id^=allowed_number_of_users-]").click(function(){
            var tmp_id = ($(this).attr("id")).split("-")
            $("#save-slot_"+tmp_id[1]).removeClass("hidden")
            if(!$(this).is(":checked")){
                $("#number_of_users-"+tmp_id[1]).removeClass("hidden")
                if($("#initial-allowed_number_of_users-"+tmp_id[1]).val() == 0){
                    $("#save-slot_"+tmp_id[1]).removeClass("hidden")
                    $("#cancel_"+tmp_id[1]).removeClass("hidden")
                }else{
                    $("#save-slot_"+tmp_id[1]).addClass("hidden")
                    $("#number_of_users-"+tmp_id[1]).val($("#initial-allowed_number_of_users-"+tmp_id[1]).val())
                }
            }else{
                $("#number_of_users-"+tmp_id[1]).addClass("hidden").val("")
                if($("#initial-allowed_number_of_users-"+tmp_id[1]).val() > 0){
                    $("#save-slot_"+tmp_id[1]).removeClass("hidden")
                    $("#cancel_"+tmp_id[1]).removeClass("hidden")
                }else{
                    $("#save-slot_"+tmp_id[1]).addClass("hidden")
                }
            }
        })
        
        $(document).off().on('click', '#usersList-modal #form-content #users tbody a.user-access', function(){
            var userid = $(this).attr('id');
            var company_id = $("#usersList-modal #form-content").data("company_id");
            var company_name = $("#usersList-modal #form-content").data("company_name");
            if(confirm("This will revoke the access of the user and delete his files in the system. Do you wish to continue?")){
                $.post(SITEROOT + '/account/revoke', {userid: userid}, function(response){
                    if(response){
                        getUsers(company_id, company_name)
                    }else{
                        alert('An error was encountered. Please contact your system administrator.');
                    }
                });
            }
            return false;
        });
        
        function getUsers(company_id, company_name){
            show_waitMe(jQuery('body'))
            $.get(SITEROOT + '/account/users/' + company_id, function(html){
                $("#usersList-modal #form-content").data("company_id", company_id)
                $("#usersList-modal #form-content").data("company_name", company_name)
                $("#usersList-modal #form-content").html(html)
                $("#usersList-modal").modal("open")
                hide_waitMe()
                $('#usersList-modal #form-content table#users').DataTable({
                    "bInfo" : true,
                    scrollY:        400,
                    scrollX:        true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 2
                    },
                    "oLanguage": {
                        "sSearch": "Search by Name or Employee ID" //search
                    },
                    "order": [[ 9, "desc" ]],
                    "columnDefs": [
                        { "searchable": false, "targets": [2,3,4,5,6,7,8,9,10] }
                    ],
                    dom: 'Bfrtip',
                    buttons: [{ 
                        extend: 'pdf', 
                        text: 'Export to PDF',
                        title: 'List of Registered Employees: ('+company_name+')',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                            }
                        }
                    ]
                });
                
            })
        }
        
    })
</script>