<style>
    .alert{
        margin-bottom: 0;
    }
    
    .modal.modal-fixed-footer{
        height: 80%;
    }
    
    .modal{
        max-height: 80%;
    }
    
    table thead th:last-child{
        width: 20%;
    }
    
    strong {
        font-weight: bold;
    }
</style>
<div class="alert alert-success hidden"></div>
<div class="right">
    <a class="btn btn-info enroll-btn active" href="javascript:void(0)">Enroll</a>
</div>

<div class="row">
    <div class="col-sm-12">

        <table id='mpusers_table' class="stripe row-border order-column" width="100%" cellspacing="0">
            <thead>
                <tr>   
                    <th>Status</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Access Level</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>

    </div>

</div>
<div id="new-user-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <div id="form-content"></div>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn enroll-user" data-type="<?php echo $type;?>">Enroll</a>
        <a class="modal-close waves-effect waves-red btn-flat">Close</a>
    </div>
</div>
<script>
    $(document).ready(function () {
       
        var oTable = $('#mpusers_table').DataTable({
            "ajax": SITEROOT + '/Services/getAllUsers/<?php echo $type?>',
            "order": [[0, "asc"]],
            dom: '<"custom-toolbar">frtip',
            "displayLength": 10,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"},
                {
                    "targets": 0,
                    "visible": false,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        var status = 'all';
                        if(row.status == 1){
                            status = 'active';
                        }else if(row.status == null || row.status == '') {
                            status = 'newly created';
                        }else if(row.status == 3){
                            status = 'revoked';
                        }
                        return status
                    }
                },
                {
                    "targets": 1,
                    "visible": false,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        return row.type
                    }
                },
                {
                    "targets": 2,
                    "visible": true,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        return '<span id="name-'+row.id+'">' + row.name + '</span>'
                    }
                },
                {
                    "targets": 3,
                    "visible": true,
                    "searchable": true,
                    data: function(row, type, val, meta){
                        return row.company
                    }
                },
                {
                    "targets": 4,
                    "visible": true,
                    "searchable": false,
                    data: function(){
                        return 'Default'
                    }
                },
                {
                    "targets": 5,
                    "visible": true,
                    "searchable": false,
                    data: function(row, type, val, meta){
                        var html = ''
                        if(row.status == 3){
                            html = row.date_revoked
                        }else{
                            html = '<a class="btn btn-small" onclick="editUser(this)" data-x="'+row.id+'">Edit</a>'
                            html += '&nbsp;<a class="btn btn-small" id="delete-user_'+row.id+'" >Revoke Access</a>'
                            if(row.type == 'Doctor'){
                                var btn = '<a class="btn-small btn promote" id="promote-user_'+row.id+'" >Promote to Admin</a>'
                                if(row.is_admin == 't'){
                                    btn = '<a class="btn-small btn promote" disabled >Promoted to Admin</a>';
                                }
                                html += '&nbsp;' + btn
                            }
                        }
                        return html
                    }
                }
            ]
        });
        
        $('.dataTables_filter input').unbind().bind('keyup', function() {
            var searchTerm = this.value.toLowerCase()
            oTable.rows().search(searchTerm, true, false).draw();
        })

        $('.modal').modal({
            dismissible: false,
            complete: function() {
                //check if table needs to be reloaded
                if($("#new-user-modal").is(":hidden") && $("#new-user-modal").hasClass("refresh")){
                    oTable.ajax.reload();
                }
            } // Callback for Modal close
        });

        $(".enroll-btn").click(function(){
            $(".alert-danger").html("");
            $(".alert-danger").addClass('hidden');
            $.get(SITEROOT + '/Services/new_user/<?php echo $type;?>', function(response){
                $('#new-user-modal .modal-content div#form-content').html(response);
                $(".enroll-user").text("ENROLL")
                $('#new-user-modal').modal('open');
            });
        });
        
        var status = '<span class="status">Status: <select id="user-status"><option value="all" selected>ALL</option>'
        status += '<option value="newly created">New</option>'
        status += '<option value="active">Active</option>'
        status += '<option value="revoked">Revoked</option></select></span>'
        $(".custom-toolbar").html(status)

        $("select#user-status").on("change", function(){
            oTable.columns().search("").draw()
            var last_col_text = 'Action'
            if($(this).val() == 'revoked'){
                last_col_text = 'Date Revoked';
            }
            $("table").find("thead th:last").text(last_col_text)
            if($(this).val() != 'all'){
                oTable.column(0).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
        })
        
        var companies = '&nbsp;&nbsp;&nbsp;Company: <select id="medical-companies"><option value="all" selected>ALL</option>'
        companies += '<option value="LifeScience">LifeScience</option>'
        companies += '<option value="Independent Contractor">Independent Contractor</option>'
        companies += '<option value="Others">Others</option></select>'
        $(".custom-toolbar .status").append(companies)

        $("select#user-status").on("change", function(){
            oTable.columns().search("").draw()
            var last_col_text = 'Action'
            if($(this).val() == 'revoked'){
                last_col_text = 'Date Revoked';
            }
            $("table").find("thead th:last").text(last_col_text)
            if($(this).val() != 'all'){
                oTable.column(0).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
        })
        
        var user_types = '&nbsp;&nbsp;&nbsp;User: <select id="medical-user-type"><option value="all" selected>ALL</option>'
        user_types += '<option value="Doctor">Doctor</option>'
        user_types += '<option value="Nurse">Nurse Practitioner</option>'
        user_types += '<option value="Nutritionist">Nutritionist-Dietitian</option>'
        user_types += '<option value="Kinesiologist">Kinesiologist</option></select>'
        $(".custom-toolbar .status").append(user_types)

        $("select#user-status").on("change", function(){
            oTable.columns().search("").draw()
            var last_col_text = 'Action'
            if($(this).val() == 'revoked'){
                last_col_text = 'Date Revoked';
            }
            $("table").find("thead th:last").text(last_col_text)
            if($(this).val() != 'all'){
                oTable.column(0).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
        })

        $("select#medical-companies").on("change", function(){
            oTable.columns().search("").draw()
            if($(this).val() != 'all'){
                if($(this).val() == 'Others'){
                    oTable.column(3).search("^((?!Independent|LifeScience).)*$", true).draw()
                }else
                    oTable.column(3).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
            $("select#user-status, select#medical-user-type").val("all")
        })
        
        $("#medical-user-type").on("change", function(){
            oTable.columns().search("").draw()
            if($(this).val() != 'all'){
                oTable.column(1).search( $(this).val() ).draw()
            }
            $("input[type=search]").val("")
            $("select#user-status, select#medical-companies").val("all")
        })

        $(document.body).off().on("click", "a[id^=promote-user_]", function(){
            if(confirm("Are you sure you want to promote user to admin?")){
                show_waitMe(jQuery('body'));
                var tmp_id = ($(this).attr('id')).split("_")
                var this_button = $(this)
                $.post(SITEROOT + '/Services/promoteToAdmin', {id: tmp_id[1]}, function(response){
                    if(response.result.success == 1){
                        var md_name = $("span#name-" + tmp_id[1]).text()
                        $(".alert-success").html("<strong>" + md_name + "</strong> was successfully promoted to Admin.").removeClass("hidden")
                        $(this_button).text("PROMOTED TO ADMIN").attr("disabled", true);
                        hide_waitMe();
                        $('html,body').scrollTop(0);
                    }
                }, 'json')
            }
        })
        
        $(document.body).on('click', "a[id^=delete-user]", function(){
            if(confirm("Are you sure you want to delete the record?")){
                var tmp_id = ($(this).attr("id")).split("_")
                $.get(SITEROOT + '/Services/delete_user/' + tmp_id[1], function(){
                    oTable.ajax.reload(function () {
                        $("select#user-status").val("revoked").trigger("change")
                        $("select#medical-companies, select#medical-user-type").val("all")
                    });
                });
            }
            
        })

    })
    
    function editUser(obj){
        show_waitMe(jQuery('body'));
        $(".alert-danger").html("");
        $(".alert-danger").addClass('hidden');
        $.get(SITEROOT + '/Services/edit_user/' + $(obj).data("x"), function(response){
            $('#new-user-modal .modal-content div#form-content').html(response);
            $(".enroll-user").text("UPDATE")
            hide_waitMe();
            $('#new-user-modal').modal('open');
        });
    }
</script>