<style>
    form{
        padding: 20px;
    }
    select.select2-hidden-accessible.initialized{
        display: none;
    }
    input, select {
        height: inherit !important;
        padding: 0 !important;
    }
    
    input#company_name::placeholder {
        color: red;
        font-style: italic;
    }
    .title {
        padding: 3px 10px;
    }
</style>
<div class="title"><?php echo $title; ?></div>
<div>
    <?php echo form_open(base_url() . 'services/enrollUser', array('id' => 'enroll_form')); ?>
    <input type="hidden" name="x" id="x" value="<?php echo (!$is_new ? $user['id'] : 0) ?>" />
    <div class="form-group">
        <?php echo form_label('Type', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php $medical_user_types = array(
                'Doctor' => 'Doctor', 
                'Nutritionist' => 'Nutritionist-Dietitian',
                'Nurse' => 'Nurse Practitioner', 
                'Kinesiologist' => 'Kinesiologist'
                );
                echo form_dropdown('type', $medical_user_types, (!$is_new ? $user['type'] : null), 'class="form-control select-2 select2-hidden-accessible" style="margin-bottom: 20px"'); 
            ?>
        </div>
    </div>
        
    <div class="form-group">
        <?php echo form_label('Name<span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'form-control required', 'value' => (!$is_new ? $user['name'] : null))); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Email Address <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class' => 'form-control required', 'value' => (!$is_new ? $user['email'] : null))); ?>
        </div>
    </div>
           
    <div class="form-group">
        <?php echo form_label('Gender', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php $genderList = array('Female' => 'Female', 'Male' => 'Male');
                echo form_dropdown('gender', $genderList, (!$is_new ? $gender : null), 'class="form-control select-2 select2-hidden-accessible" style="margin-bottom: 20px"'); 
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo form_label('Company <span class="required_true">*</span>', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php $companyList = array('' => '', 'LifeScience' => 'LifeScience', 'Independent Contractor' => 'Independent Contractor', 'Others' => 'Others');
            $selected = '';
            if(!$is_new){
                if(in_array($user['company'], $companyList)){
                    $selected = $user['company'];
                }else{
                    $selected = 'Others';
                }
            }
                echo form_dropdown('company', $companyList, $selected, 'class="form-control select-2 select2-hidden-accessible required" id="company" style="margin-bottom: 20px"'); 
            ?>
        </div>
    </div>
        
    <div class="company_name form-group <?php echo (!$is_new && $user['company'] != '' && !in_array($user['company'], $companyList) ? '' : 'hidden');?>">
        <?php echo form_label('', '', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-8">
            <?php echo form_input(array('id' => 'company_name', 'name' => 'company_name', 'class' => 'form-control', 'value' => (!$is_new ? $user['company'] : null), 'placeholder' => 'Enter Company Name')); ?>
        </div>
    </div>
        
    <div class="clearfix"></div>
    <?php echo form_close(); ?>
</div>
<!-- static-content-wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script>
    $(document).ready(function () {
        var date = new Date()
        date.setFullYear(date.getFullYear() - 18)
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 100, // Creates a dropdown of 15 years to control year,
            max: date,
            today: '',
            clear: 'Clear',
            close: 'Ok',
            format: 'mmm dd, yyyy',
            closeOnSelect: false // Close upon selecting a date,
        });

        $("#company").change(function(){
            if($(this).val() === 'Others'){
                $(".company_name").removeClass("hidden");
                $("#company_name").focus();
            }else{
                $(".company_name").addClass("hidden")
                $("#company_name").val("");
            }
        })
        
        $(document).off().on("click", ".enroll-user", function(){
            show_waitMe(jQuery('body'));
            var data = $("#enroll_form").serializeArray();
            var url = SITEROOT + '/Services/enrollUser';
            var is_update = false
            if($("#enroll_form").find("input#x").val() > 0){
                url = SITEROOT + '/Services/updateUser';
                is_update = true
            }
            $.post(url, data, function(response){
                if(response.result.success == 1){
                    $("#new-user-modal").addClass("refresh")
                    hide_waitMe();
                    $('.modal').modal('close');
                    var text = "A new user was successfully enrolled.";
                    if(is_update){
                        text = "User record was successfully updated.";
                    }
                    $(".alert-success").text(text).removeClass("hidden")
                } else {
                    hide_waitMe();
                    $(".alert-danger").removeClass('hidden');
                    $(".alert-danger").html(response.result.errors);
                }
            }, 'json');
        })

    });
</script>