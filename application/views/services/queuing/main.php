<div class="small-tabs-container" style="margin-bottom: 30px;">
    <div class="small-tabs active"><a onclick="showMyPage(this)" id="company" data-method="getCompanyQueue">Company Queue</a></div>
    <div class="small-tabs"><a onclick="showMyPage(this)" id="patient" data-method="getPatientQueue">Patient Queue</a></div>
<div id="user-subcontent-view"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        showMyPage("<?php echo ($default_page ? '#'.$default_page : '#company'); ?>");
        /*
         * COMPANY QUEUE
         */
        $(document).off().on('click', ".assign-company-queue", function(){
            $(".alert-danger").html("");
            $(".alert-danger").addClass('hidden');
            //check if with selected lead
            if($("input:checkbox[id^=chkLead_]:checked").length > 0){
                show_waitMe(jQuery('body'));
                var data = $("#assignMD-form").serializeArray();
                $.post(SITEROOT + '/Services/assignMedicalPrac', data, function(response){
                    hide_waitMe();
                    $('#assignMD-modal').modal('close');
                    showMyPage('#company', true)
                });
            }else{
                $(".alert-danger").html("Please select a medical team lead.");
                $(".alert-danger").removeClass('hidden');
            }
        });
        
        /*
         *  PATIENT QUEUE
         */
        $(document).on('click', ".cancel-appointment", function(){
            $(".alert-danger").html("");
            $(".alert-danger").addClass('hidden');
            if(confirm("Are you sure you want to cancel this appointment?")){
                var aid = $(this).data('aid')
                show_waitMe(jQuery('body'));
                $.getJSON(SITEROOT + '/Services/cancelAppointment/' + aid, function(response){
                    if(response.result.success == 1){
                        showMyPage('#patient', true)
                    }else{
                        hide_waitMe();
                        $(".alert-danger").html(response.result.error);
                        $(".alert-danger").removeClass('hidden');
                    }
                });
            }
        })
        
        $(document).on('click', ".reassign-md", function(){
            var aid = $(this).data('aid')
            var data = {};
            data.callback = 'services/reassignMDform/' + aid;
            data.form_data = null;
            page_ajaxify(data);
        });
        
        $(document).on('click', ".resched-appointment", function(){
            var aid = $(this).data('aid')
            var data = {};
            data.callback = 'services/reschedform/' + aid;
            data.form_data = null;
            page_ajaxify(data);
        });
    });

    function showMyPage(obj, is_refresh = false){
        var method = $(obj).data('method')
        var url = SITEROOT + '/services/' + method;
        if (url !== null) {
            if(!is_refresh){
                show_waitMe(jQuery('body'));
            }
            $.get(url, function (response) {
                $("div.small-tabs-container div.small-tabs").removeClass('active');
                $(obj).closest('div.small-tabs').addClass('active');
                hide_waitMe();
                $("#user-subcontent-view").html(response);
            });
        }
    }
</script>