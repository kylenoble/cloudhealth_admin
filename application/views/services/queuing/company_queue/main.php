<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<style>
    .dataTables_wrapper{
        margin: 0 !important;
    }
    
    .alert{
        margin-bottom: 0;
    }
    
    .modal.modal-fixed-footer{
        height: 80%;
    }
    
    .modal{
        max-height: 80%;
    }
    
    table thead th:last-child{
        width: 20%;
    }
</style>

<div class="row">
    <div class="col-sm-12">

        <table id='company_queue_table' class="stripe row-border order-column" width="100%" cellspacing="0">
            <thead>
                <tr>   
                    <th>Company</th>
                    <th>Account ID</th>
                    <th>Account Officer-In-Charge</th>
                    <th>Enrollment Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($companies as $rec) { ?>
                    <tr>                                               
                        <td style='text-align: center'><?php echo $rec['company_name']?></td>
                        <td style='text-align: center'><?php echo $rec['id']; ?></td>
                        <td style='text-align: center'><?php echo $rec['contact']; ?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime($rec['date_created'])?>"><?php echo date('F j, Y', strtotime($rec['date_created']));?></td>
                        <td style='text-align: center' >
                            <a class='btn assign-md' href='Javascript:void(0)' data-cid="<?php echo $rec['id']?>"><?php echo (in_array($rec['id'], array_column($companies_with_assigned, 'company_id')) ? 'SHOW ASSIGNED MD' : 'ASSIGN MD');?></a>
                        </td>
                    </tr>
<?php } ?>
            </tbody>
        </table>

    </div>

</div>
<div id="assignMD-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <div id="form-content"></div>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn assign-company-queue">Assign</a>
        <a class="modal-close waves-effect waves-red btn-flat">Close</a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
       
        $('.modal').modal({
            dismissible: false
        });
        
        $('#company_queue_table').DataTable({
            scrollCollapse: true,
            scrollY: 400,
            scrollX: true,
            dom: 'Bfrtip'
        });

        $("#company_queue_table").on("click", ".assign-md", function(){
            $(".alert-danger").html("");
            $(".alert-danger").addClass('hidden');
            var cid = $(this).data('cid')
            $.getJSON(SITEROOT + '/Services/assignMDform/' + cid, function(response){
                $('#assignMD-modal .modal-content div#form-content').html(response.html);
                $('#assignMD-modal').modal('open');
                if(response.is_edit){
                    $(".assign-company-queue").text("UPDATE");
                }else{
                    $(".assign-company-queue").text("ASSIGN");
                }
            });
        });
        
        

    })
</script>