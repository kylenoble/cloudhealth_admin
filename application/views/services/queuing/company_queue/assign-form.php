<style>
    th{
        padding: inherit;
        color: inherit;
        border: inherit;
        text-align: inherit;
    }
    
    .modal{
        width: 35% !important;
    }
</style>
<?php echo form_open(base_url() . 'services/assignMedicalPrac', array('id' => 'assignMD-form')); ?>
<input type='hidden' name='cid' id='cid' value="<?php echo $company_id ?>">
<table class="highlight">
    <thead>
        <tr>
            <th style="text-align: center;"></th>
            <th>Name</th>
            <th>Type</th>
            <th style="text-align: center;">Lead?</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($medical_practitioners as $user):
            $assign_checked = false;
            $lead_checked = false;
            
            if(in_array($user['id'], array_column($data, 'user_id'))){
                $assign_checked = true;
                $pos = array_search($user['id'], array_column($data, 'user_id'));
                if($data[$pos]['is_lead'] == 't'){
                    $lead_checked = true;
                }
            }
            ?>
        <tr>
            <td style="text-align: center;">
                <input type="checkbox" <?php echo ($assign_checked ? 'checked' : '');?> name="chkAssign[<?php echo $user['id'];?>]" data-type="<?php echo $user['type'];?>" id="chkAssign_<?php echo $user['id'];?>" /><label for="chkAssign_<?php echo $user['id'];?>"></label>
            </td>
            <td><?php echo trim($user['name']);?></td>
            <td><?php echo $user['type'];?></td>
            <td style="text-align: center;">
                <input type="checkbox" <?php echo ($lead_checked ? 'checked' : '');?> name="chkLead[<?php echo $user['id'];?>]" <?php echo ($lead_checked ? '' : 'disabled');?> data-type="<?php echo $user['type'];?>" id="chkLead_<?php echo $user['id'];?>" /><label for="chkLead_<?php echo $user['id'];?>"> </label>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php echo form_close();?>
<script>
    $(document).ready(function(){

        $("input:checkbox[id^=chkLead_]").click(function(){
            var chkLeadMD_length = $("input:checkbox[id^=chkLead_][data-type='Doctor']:checked").length;
            if(chkLeadMD_length === 1){
                $("input:checkbox[id^=chkLead_]:not(:checked)").prop("disabled", true);
            }else{
                $("input:checkbox[id^=chkLead_][data-type='Doctor']").each(function(){
                    var tmp_id_1 = ($(this).attr("id")).split("_");
                    if($("#chkAssign_" + tmp_id_1[1]).is(":checked")){
                        $(this).prop("disabled", false);
                    }
                })
            }
            
        });
        
        $("input:checkbox[id^=chkAssign_]").click(function(){
            var tmp_id = ($(this).attr("id")).split("_");
            $("input#chkLead_" + tmp_id[1]).prop("disabled", true).prop("checked", false);
            if($(this).is(":checked")){
                var chkLeadMD_length = $("input:checkbox[id^=chkLead_][data-type='Doctor']:checked").length;
                if($("input#chkLead_" + tmp_id[1]).data("type") == "Doctor" && chkLeadMD_length == 0){
                    $("input#chkLead_" + tmp_id[1]).prop("disabled", false);
                }else{
                    $("input#chkLead_" + tmp_id[1]).prop("disabled", true);
                }
            }
            else{
                if($(this).data("type") == "Doctor"){
                    $("input#chkLead_" + tmp_id[1]).prop("checked", false);
                    $("input:checkbox[id^=chkAssign_][data-type='Doctor']:checked").each(function(){
                        var my_tmp_id = ($(this).attr("id")).split("_")
                        $("input#chkLead_" + my_tmp_id[1]).prop("disabled", false)
                    })
                }
            }
        });
    })
</script>