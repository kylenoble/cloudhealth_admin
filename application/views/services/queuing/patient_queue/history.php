<table class="cyan-table">
    <thead>
        <tr>
            <th>Date</th>
            <th>Action</th>
            <th>Details</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($history as $rec):?>
        <tr>
            <td><?php echo date('F j, Y h:i A', strtotime($rec['history_created_date']));?></td>
            <td><?php echo $rec['action'];?></td>
            <td style="text-align: left; padding-left: 5px">
                Appointment Date & Time: <?php echo date('M d, Y h:i A', strtotime($rec['appointment_date']))?><br />
                MD Requested: <?php echo $rec['md_name'];?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>