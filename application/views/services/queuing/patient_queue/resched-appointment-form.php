<link href="<?php echo base_url(); ?>assets/css/themes/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css" type="text/css" rel="stylesheet">
<style>
    div.ui-datepicker{
        font-size: 20px;
    }
    
    select{
        width: auto;
        height: 30px;
        padding: 0;
    }
    #datepicker-wrap {
	float: left;
	border: 1px solid #aaa;
	border-radius: 4px;
	padding-top: 3px;
    }
    #datepicker-wrap h3 {
        margin: 0;
        padding-left: 3px;
    }
    #datepicker .ui-datepicker {
        border: none;
        border-top-right-radius: 0;
        border-top-left-radius: 0;
    }
    
    h6{
        font-weight: bold;
    }
    
    .title{
        width: 100% !important;
    }
    
    /* adjusts second modal's opacity'*/
    .modal-overlay ~ .modal-overlay{
        opacity: 0 !important;
    }
    
    .ui-state-highlight{
        border:1px solid #d3d3d3 !important;
        background: #d7ebf9 url("images/ui-bg_glass_80_d7ebf9_1x400.png") 50% 50% repeat-x !important;
    }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
        border:1px solid #aaaaaa !important;
        background: #3baae3 url("images/ui-bg_glass_50_3baae3_1x400.png") 50% 50% repeat-x !important;
    }
    
    .ui-state-default{
        color: #000 !important;
        text-align: center;
    }
    
</style>
<div class="row">
    <div class="col s10 details">
        <div class="col s5">
            <ul class="collection">
                <li class="collection-item">
                    <label>Patient Details: </label>
                    <p>
                        Employee ID: <b><?php echo $data['employee_id'];?></b><br />
                        Name: <b><?php echo $data['employee_name'];?></b><br />
                        Company: <b><?php echo $data['company'];?></b>
                    </p>
                </li>
            </ul>
        </div>
        <div class="col s6">
            <ul class="collection">
                <li class="collection-item">
                    <label>Appointment Details: </label>
                    <p>
                        Date & Time: <b><?php echo date('M d, Y g:i A', strtotime($data['datetime']))?></b><br />
                        MD Requested: <b><?php echo $data['md_name'];?></b><br />
                        Concern: <b><?php echo $data['concern'];?></b>
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s2" style="padding-top: 10px;">
        <a class="btn-default btn back" href="Javascript:void(0)">Back</a>
    </div>
</div>
<div class="row">
    <div class="col s4">
        <div id="datepicker-wrap">
            <h6>Select an Appointment Date</h6>
            <div id="datepicker"></div>
        </div>
    </div>
    <div class="col s8" id="schedule">
        <div class="title">Availability of <span class="small-note"><?php echo $data['md_name'];?></span> on <span class="small-note available-date"><?php echo date('F d, Y', strtotime($data['datetime']));?></span></div>
        <div>
            <span class="small-note">Note: Time slot(s) in red is already booked; time slot(s) in blue is the patient's booked appointment.</span>
        </div>
        <table id="schedule-table">
            <thead>
            <th>Time Schedule</th>
            <th># of Appointments</th>
            <th>Available Slots</th>
            <th>Time Slots</th>
            <th>Action</th>
            </thead>
        </table>
    </div>
</div>
<div id="confirm-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <div id="form-content" class="row"></div>
        <div class="row">
            <div class="col s12">
                <p>Dr. <?php echo $data['md_name'];?> will be notified through email on the new appointment date.</p>
                <h5 id="new-date"></h5>
                <p class="full red hidden" style="text-align: center;color: #fff; font-weight: bold;">Please note that selected time slot is already full. If you wish to continue, click CONFIRM.</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn resched-appointment" data-u="">Confirm</a>
        <a class="modal-close waves-effect waves-red btn-flat">Close</a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.timepicker.js"></script>
<script>
$( function() {
    var appointment_date = '<?php echo date("Y-m-d", strtotime($data['datetime']))?>';
    var oTable = $('#schedule-table').DataTable({
        "order": [[1, "asc"]],
        iDisplayLength: 10,
        bFilter: false,
        "language": {
            "loadingRecords": "&nbsp;",
            "processing": "Loading...",
        },
        processing: true,
        ajax: {
            url: SITEROOT + 'Services/ajax_getAvailableMDs/<?php echo $data['id']?>',
            dataType: 'json',
            type: 'GET'
        },
        "columnDefs": [
            {"className": "dt-center", "targets": "_all"},
            {
                "targets": 0,
                "visible": true,
                data: function(row, type, val, meta){
                    return row.start_time + ' - ' + row.end_time
                }
            },
            {
                "targets": 1,
                "visible": true,
                data: function(row, type, val, meta){
                    return row.no_of_appointments
                }
            },
            {
                "targets": 2,
                "visible": true,
                data: function(row, type, val, meta){
                    var available = row.available_slots
                    if(row.available_slots == 0){
                        available = '<span class="small-note" style="color: red">FULL</span>';
                    }
                    return available
                }
            },
            {
                "targets": 3,
                "visible": true,
                data: function(row, type, val, meta){
                    return row.available_time_slots
                }
            },
            {
                "targets": 4,
                "visible": true,
                data: function(row, type, val, meta){
                    return '<a class="btn btn-small resched" id="set-sched_'+row.id+'" >Set</a>'
                }
            }
        ]
    });

    $('.modal').modal({
        dismissible: false
    });

    $.get(SITEROOT + '/Services/ajax_getMDAvailableDates/<?php echo $data['userid']?>', function(result){
        var result_arr = $.parseJSON(result)
        console.log(result_arr)
        var availableDates = function(date) {
            var currDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            for(var i = 0; i < result_arr.length; i++){
                if($.inArray(currDate, result_arr) == -1) {
                    return [false];
                }
            }
            return [true, 'ui-highlight ui-state-default ui-state-active', ''];
        }
        $( "#datepicker" ).datepicker({
            minDate: new Date(new Date().getTime() + 2*24*60*60*1000),
            maxDate: '+2M',
            onSelect: function(dateText, inst) {
                appointment_date = $(this).val();
                $(".available-date").text(appointment_date)
                console.log(appointment_date)
                oTable.ajax.url(SITEROOT + 'Services/ajax_getAvailableMDs/<?php echo $data['id'] ?>/' +  encodeURIComponent(appointment_date)).load();
            },
            dateFormat: 'MM dd, yy',
            defaultDate: '<?php echo date('F d, Y', strtotime($data['datetime'])); ?>',
            beforeShowDay: availableDates
        });
    });
    
    
    
    $('.dataTables_filter input').unbind().bind('keyup', function() {
        var searchTerm = this.value.toLowerCase()
        oTable.rows().search(searchTerm, true, false).draw();
    })
    
    $(document).off().on('click', '.resched', function(){
        $(".full").addClass("hidden")
        $("#form-content").html("")
        $("#new-date").html("")
        var tmp_id = ($(this).attr("id")).split("_")
        var id = tmp_id[1]
        var timeslot = $("#timeslot-" + id + " option:selected");
        var time = $(timeslot).text()
        show_waitMe(jQuery('body'));
        $("#form-content").html(
                $(".details").clone().removeClass("s10").addClass("s12")
                )
                .find("div.s6").removeClass("s6").addClass("s7")
        $("#new-date").html("New Appointment Date & Time: "+(new Date(appointment_date)).toDateString()+" "+time)
        $("#confirm-modal").modal("open")
        $(".resched-appointment").attr("data-u", id)
        if($(timeslot).data("full") == "1"){
            $(".full").removeClass("hidden").slideDown("slow")
        }
        hide_waitMe();
    })
    
    $(document).on('change', 'select[id^=timeslot-]', function(){
        if($("option:selected", this).attr('data-full') == 1){
            alert("This slot is already FULL.")
        }
    })
    
    $(document).on('click', '.resched-appointment', function(){
        show_waitMe(jQuery('body'));
        var data = $("#form-" + $(this).data("u")).serializeArray();
        $.post(SITEROOT + '/services/ajax_reschedAppointment', data, function(response){
            if(response.result.success == 1){
                $(".btn.back").trigger('click')
                $(".modal-overlay").remove()
                $("body").removeAttr('style');
            }else{
                hide_waitMe();
                $(".alert-danger").text(response.result.error)
            }
        }, 'json')
    })
        
    $(".btn.back").click(function(){
        var data = {};
        data.callback = '/services';
        data.form_data = {page: 'queuing', subpage: 'patient'};
        page_ajaxify(data);
    })
    
    
} );
</script>