<link href="<?php echo base_url(); ?>assets/css/themes/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet">
<style>

    
    div#details, div#schedule{
        float: left;
        vertical-align: top;
    }
    
    #details {
        width: 300px;
        margin-right: 10px;
    }
    
    #schedule{
        width: 800px;
    }
    
    select{
        width: 150px;
        height: 30px;
        padding: 0;
    }
    
    .title{
        width: 100% !important;
    }
</style>
<a class="btn-default btn back" href="Javascript:void(0)">Back</a>
<div class="row">
    <div class="col s3 details">
        <ul class="collection">
            <li class="collection-item">
                <label>Patient Details: </label>
                <p>
                    Employee ID: <b><?php echo $data['employee_id'];?></b><br />
                    Name: <b><?php echo $data['employee_name'];?></b><br />
                    Company: <b><?php echo $data['company'];?></b>
                </p>
            </li>
            <li class="collection-item">
                <label>Appointment Details: </label>
                <p>
                    Date: <b><?php echo date('M d, Y h:iA', strtotime($data['datetime']))?></b><br />
                    MD Requested: <b><?php echo $data['md_name'];?></b><br />
                    Concern: <b><?php echo $data['concern'];?></b>
                </p>
            </li>
        </ul>
    </div>
    <div class="col s9 schedule">
        <div class="title">Available Doctors on: <span class="small-note"><?php echo date('F d, Y', strtotime($data['datetime']));?></span></div>
        <div>
            <span class="small-note">Note: Time slot(s) in red is already booked; time slot(s) in blue is the patient's booked appointment.</span>
        </div>
        <table id="schedule-table">
            <thead>
            <th>MD Name</th>
            <th>Time Slots</th>
            <th># of Appointments</th>
            <th>Available Slots</th>
            <th>Action</th>
            </thead>

            <tbody>
                <?php foreach($doctors_data as $rec):?>
                <tr>
                    <td id="mdname_<?php echo $rec['id'];?>"><?php echo $rec['name']?></td>
                    <td class="center"><?php echo $rec['available_time_slots']?></td>
                    <td class="center"><?php echo $rec['no_of_appointments']?></td>
                    <td class="center"><?php echo $rec['available_slots']?></td>
                    <td class="center">
                        <a class="btn btn-small reassign" id="set-sched_<?php echo $rec['id'];?>" >ASSIGN</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div id="confirm-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <div id="form-content" class="row" style="margin-bottom: -8px;">
            <div class="col s6 details">
                <ul class="collection">
                    <li class="collection-item">
                        <label>Patient Details: </label>
                        <p>
                            Employee ID: <b><?php echo $data['employee_id'];?></b><br />
                            Name: <b><?php echo $data['employee_name'];?></b><br />
                            Company: <b><?php echo $data['company'];?></b>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col s6 details">
                <ul class="collection">
                    <li class="collection-item">
                        <label>Appointment Details: </label>
                        <p>
                            Date: <b><?php echo date('M d, Y h:iA', strtotime($data['datetime']))?></b><br />
                            MD Requested: <b><?php echo $data['md_name'];?></b><br />
                            Concern: <b><?php echo $data['concern'];?></b>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p>Patient, old and new doctors will be notified through email on the following changes:</p>
                <h5>New assigned MD: <span id="new-md"></span></h5>
                <h5>New Schedule: <span id="new-date"></span></h5>
                <p class="full red hidden" style="text-align: center;color: #fff; font-weight: bold;">Please note that selected time slot is already FULL. If you wish to continue, click CONFIRM.</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-light btn reassign-md" data-u="">Confirm</a>
        <a class="modal-close waves-effect waves-red btn-flat">Close</a>
    </div>
</div>
<script>
$( function() {
    $('.modal').modal({
        dismissible: false
    });
    
    $('select[id^=timeslot-]').on('change', function(){
        if($("option:selected", this).attr('data-full') == 1){
            alert("This slot is already FULL.")
        }
    })

    $(".btn.back").click(function(){
        var data = {};
        data.callback = '/services';
        data.form_data = {page: 'queuing', subpage: 'patient'};
        page_ajaxify(data);
    })
    
    $('#schedule-table').DataTable({
        "order": [[0, "desc"]]
    });
    
    $(document).off().on('click', '.reassign', function(){
        $(".full").addClass("hidden")
        $("#new-md").text("")
        $("#new-date").text("")
        var tmp_id = ($(this).attr("id")).split("_")
        var id = tmp_id[1]
        var timeslot = $("#timeslot-" + id + " option:selected");
        var time = $(timeslot).text()
        var md_name = $("#mdname_"+id).text()
        $("#new-md").text(md_name)
        $("#new-date").text(time)
        show_waitMe(jQuery('body'));
        $("#confirm-modal").modal("open")
        $(".reassign-md").attr("data-u", id)
        if($(timeslot).data("full") == "1"){
            $(".full").removeClass("hidden").slideDown("slow")
        }
        hide_waitMe();
    })
    
    $(document).on('click', '.reassign-md', function(){
        show_waitMe(jQuery('body'));
        var data = $("#form-" + $(this).data("u")).serializeArray();
        $.post(SITEROOT + '/services/ajax_reassignMD', data, function(response){
            if(response.result.success == 1){
                $(".btn.back").trigger('click')
                $(".modal-overlay").remove()
                $("body").removeAttr('style');
            }else{
                hide_waitMe();
                $(".alert-danger").text(response.result.error)
            }
        }, 'json')
    })
} );
</script>