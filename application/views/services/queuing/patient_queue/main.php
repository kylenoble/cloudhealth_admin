<link href="<?php echo base_url(); ?>assets/css/themes/datatables/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/themes/datatables/custom.css" type="text/css" rel="stylesheet">
<style>
    .dataTables_wrapper{
        margin: 0 !important;
    }
    
    .alert{
        margin-bottom: 0;
    }
    
    table a:hover{
        color: #00bcd4;
    }
    
    table {
        margin-bottom: 100px;
    }
    
    .with_history {
        background-color: #daf8f9 !important;
    }
    
    .modal-header {
        margin: 20px;
    }
    
    .modal-content {
        padding-top: 0;
    }
    
    tr.cancelled td:not(:last-of-type) {
        text-decoration: line-through;
    }
    
    .dataTables_wrapper{
        margin-bottom: 60px !important;
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger hidden" style="text-align: left;"></div>
        <table id='patient_queue_table' class="stripe row-border" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Appointment Date<br />Requested</th>
                    <th>Date & Time Requested</th>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>MD Requested</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($data as $rec) {
                    $cancelled_class = "";
                    if($rec['status'] == null){
                        if($rec['active'] == 't'){
                            $status = 'Approved';
                        }else{
                            $status = 'For Approval';
                        }
                    }else{
                        $status = ($rec['status'] == 1 ? 'Completed' : ($rec['status'] == 2 ? 'Re-scheduled' : 'Cancelled'));
                        if($rec['status'] == 3){
                            $cancelled_class = "cancelled";
                        }
                    }
                    ?>
                    <tr class="<?php echo ($rec['is_modified'] == 't' ? 'with_history' : ''), ' ', $cancelled_class ?> ">
                        <td><?php echo $rec['company'];?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime($rec['datetime'])?>"><?php echo date('M d, Y h:i A', strtotime($rec['datetime']))?></td>
                        <td style='text-align: center' data-order="<?php echo strtotime($rec['date_requested'])?>"><?php echo date('M d, Y h:i A', strtotime($rec['date_requested'])); ?></td>
                        <td style='text-align: center'><?php echo $rec['employee_id']; ?></td>
                        <td style='text-align: center'><?php echo $rec['employee_name'];?></td>
                        <td style='text-align: center'><?php echo $rec['md_name'];?></td>
                        <td style='text-align: center'><?php echo $status;?></td>
                        <td style='text-align: center' >
                            <?php //reassigning of MD is only applicable to not-yet completed or cancelled appointments
                            if(($rec['status'] != 1)
                                   && strtotime($rec['datetime']) > strtotime("+2 days")
                                    ) : ?>
                            <?php if($rec['status'] != 3):?>
                            <a href="javascript:void(0)" class='reassign-md' title="Re-assign MD" data-aid="<?php echo $rec['id']?>">Re-assign MD</a>
                            <br />
                            <a class='cancel-appointment' href='Javascript:void(0)' title="Cancel Appointment" data-aid="<?php echo $rec['id']?>">Cancel Appointment</a>
                            <br />
                            <a class='resched-appointment' title="Re-schedule Appointment" href='Javascript:void(0)' data-aid="<?php echo $rec['id']?>">Re-schedule Appointment</a>
                            <?php if($rec['is_modified'] == 't') :?>
                            <br />
                            <?php 
                            endif;
                            endif;?>
                            <?php endif;?>
                            <?php if($rec['is_modified'] == 't') :?>
                            <a class='view-history' title="View Appointment History" href='Javascript:void(0)' data-aid="<?php echo $rec['id']?>">Appointment History</a>
                            <?php endif;?>
                        </td>
                    </tr>
<?php } ?>
            </tbody>
        </table>

    </div>

    <div id="history-modal" class="modal modal-fixed-footer">
        <div class="modal-header">
            <h5>Appointment History</h5>
        </div>
        <div class="modal-content"></div>
        <div class="modal-footer">
            <a class="modal-close waves-effect waves-red btn-flat">Close</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
       
        $('.modal').modal({
            dismissible: false
        });
        
        var oTable = $('#patient_queue_table').DataTable({
            "order": [[2, "desc"]],
            dom: '<"custom-toolbar">frtip',
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": true
                }
            ]
        });

        $.getJSON(SITEROOT + '/Organizations/ajax_companies_select', function(html){
            $(".custom-toolbar").html(html.active_companies)

            $.getJSON(SITEROOT + '/Services/getAppointmentStatus_select', function(html){
                $(".custom-toolbar").append(html.status)
            })
        })
        
        $(document).on("change", "select#active-companies, select#appointment-status", function(){
            oTable.search( $(this).val() ).draw()
            $("input[type=search]").val("")
        })
        
        $(document).on("keyup", "input[type=search]", function(){
            if($(this).val() != ""){
                $("select#active-companies").val("")
                $("select#appointment-status").val("")
            }
        })
        
        $('.view-history').off().on('click', function(){
            $(".modal-content").html("")
            $.get(SITEROOT + '/services/ajax_getAppointmentHistory/' +$(this).data("aid"), function(response){
                $(".modal-content").html(response)
            })
            $("#history-modal").modal("open");
        })

    })
</script>