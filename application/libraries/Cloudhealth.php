<?php

class Cloudhealth {

    public function load_script($path, $echo = false) {
        $path = base_url() . $path;
        $js = '<script>';
        $js .= 'if(LoadedScripts.indexOf("' . $path . '") == -1) { ';
        $js .= 'jQuery.getScript("' . $path . '"); ';
        $js .= 'LoadedScripts.push("' . $path . '"); ';
        $js .= '}';
        $js .= '</script>';
        if ($echo)
            echo $js;
        else
            return $js;
    }

    public function load_style($path, $echo = false) {
        $path = base_url() . $path;
        $js = '<script>';
        $js .= 'if(LoadedStyle.indexOf("' . $path . '") == -1) { ';
        $js .= '$("head").append("<link rel=\"stylesheet\" href=\"' . $path . '\" type=\"text/css\">"); ';
        $js .= 'LoadedStyle.push("' . $path . '"); ';
        $js .= '}';
        $js .= '</script>';
        if ($echo)
            echo $js;
        else
            return $js;
    }
    
    function send_welcome_email($data = array()) {
        $html = get_instance()->load->view('templates/email', $data, true);

        get_instance()->load->library('email');
        get_instance()->email->from('client.services@cloudhealthasia.com', 'CloudHealthAsia');
        get_instance()->email->to(trim($data['email']));
        get_instance()->email->subject('Welcome to CloudHealthAsia');
        get_instance()->email->message($html);
        
        if(isset($data['attachment'])){
            //attach files
            get_instance()->email->attach($data['attachment']);
        }
        $sent = get_instance()->email->send();
//        $sent = true;
//echo $html;die;
        return $sent;
    }
    
    function send_consent_email($info = array()) {
        $data['data'] = $info;
        $data['page'] = 'consent';
        $html = get_instance()->load->view('templates/email', $data, true);

        get_instance()->load->library('email');
        get_instance()->email->from('client.services@cloudhealthasia.com', 'CloudHealthAsia');
        get_instance()->email->to(trim($info['email']));
        get_instance()->email->subject('Consent: CloudHealthAsia Enrollment');
        get_instance()->email->message($html);
        
        //attach files
        get_instance()->email->attach(TEMPLATE_DIR.'CloudHealthAsia Privacy Policy.pdf');
        get_instance()->email->attach(TEMPLATE_DIR.'CloudHealthAsia Terms of Use.pdf');
        $sent = get_instance()->email->send();
//        $sent = true;
//echo $html;die;
        return $sent;
    }
    
    function send_notification_email($data = array(), $is_update = false, $individual = false) {
        $ci = get_instance();
        $sent = false;
        $html = $ci->load->view('templates/email', $data, true);
//        echo $html;die;
//var_dump($data);die;
        $ci->load->library('email');
        if($individual){
            foreach($data['emails'] as $email){
                $ci->email->from('client.services@cloudhealthasia.com', 'CloudHealthAsia');
                $ci->email->message($html);
                if($is_update){
                    $ci->email->subject('Notification Update from CloudHealthAsia');
                }else{
                    $ci->email->subject('New Notification from CloudHealthAsia');
                }
                $ci->email->to($email);
                $sent = $ci->email->send();
            }
        }else{
            $ci->email->from('client.services@cloudhealthasia.com', 'CloudHealthAsia');
            $ci->email->message($html);
            if($is_update){
                $ci->email->subject('Notification Update from CloudHealthAsia');
            }else{
                $ci->email->subject('New Notification from CloudHealthAsia');
            }
            $ci->email->bcc($data['emails']);
            $sent = $ci->email->send();
        }
//        $sent = false;
//echo $html;die;
        if(!$sent){
            return $ci->email->print_debugger(array('body'));
        }
        return $sent;
    }
    
    function getHealthStatusByScore($score){
        if ($score >= 0 && $score < 3) {
            return 'Optimal';
        } else if ($score >= 3.0 && $score < 5) {
            return 'Suboptimal';
        } else if ($score >= 5.0 && $score < 6) {
            return 'Neutral';
        } else if ($score >= 6 && $score < 8.6) {
            return 'Compromised';
        } else {
            return 'Alarming';
        }
    }
    
    function getScoreByHealthStatus($status){
        switch($status){
            case 'Optimal':
                return array(1, 3);
            case 'Suboptimal':
                return array(3, 5);
            case 'Neutral':
                return array(5, 6);
            case 'Compromised':
                return array(6, 8.6);
            case 'Alarming':
                return array(8.6, 10);
        }
    }
    
    public function random_password() {
        $random_LC = substr(str_shuffle("abcdefghjkmnpqrstuvwxyzabcdefghjkmnpqrstuvwxyz"), 0, 3);
        $random_UC = substr(str_shuffle("ABCDEFGHJKLMNPQRSTUVWXYZABCDEFGHJKLMNPQRSTUVWXYZ"), 0, 3);
        $random_NC = substr(str_shuffle("123456789123456789"), 0, 3);
        
        $random_PW = substr(str_shuffle("$random_LC$random_UC$random_NC"), 0, 8);

        $rand = rand(10, 100);
        $password_number = $random_PW . $rand;
        $password = ucfirst($password_number);
        $password_crypt = password_hash($password, PASSWORD_BCRYPT);
        
        return array('plain' => $password, 'crypt' => $password_crypt, 'rand' => $random_PW);
    }
    
    function _getUserType(){
        $ci = get_instance();
        $users_model = $ci->Users_model;
        switch($ci->session->userdata['logged_in']['role']){
            case $users_model::ADMIN:
                return 'admin';
            case $users_model::HR:
                return 'hr';
            case $users_model::MD:
                return 'md';
            case $users_model::SALES:
                return 'md';
            case $users_model::OPS:
                return 'md';
            case $users_model::FINANCE:
                return 'md';
        }
    }
    
    function sendNotification($subject = null, $message = null, $recipients = array()){
        $ci = get_instance();

        $sender_name = $ci->session->userdata['logged_in']['name'];
        $sender_id = $ci->session->userdata['logged_in']['userid'];
        $sender_type = $this->_getUserType();
        $sender = array('type' => $sender_type, 'id' => $sender_id, 'name' => $sender_name);

        $notification_data = [];
        foreach($recipients as $recipient){
            $notification_data[] = array(
                'sender' => json_encode($sender),
                'recipient' => json_encode($recipient),
                'subject' => $subject,
                'message' => $message,
                'date_created' => date(DateTime::ISO8601)
            );
        }
        return $ci->notif->sendNotification($notification_data);
    }
    
    function getNotifications(){
        $notification = array();
        $ci = get_instance();
        //check report
        $with_new_report = $ci->reports->checkReport();
        
        if((int)$with_new_report['count'] > 0){
            $notification['with_notification']['page'][] = 'account';
        }
        
        //check if with expired subscription(s)
        $with_renewal_or_expired = $ci->subscription->checkForRenewalAndExpiration();
        if((int)$with_renewal_or_expired['count'] > 0){
            $notification['with_notification']['page'][] = 'corporate_health_programs';
        }
        
        //check notifications
        $userid = $ci->session->userdata['logged_in']['userid'];
        $company_enrolled_date = $ci->session->userdata['logged_in']['company_enrolled_date'];
        $notifications = $ci->notif->getNotificationsByUser($userid, $type='admin', 'f', $company_enrolled_date);
        
        if(count($notifications) > 0){
            $notification['with_notification']['page'][] = 'notifications';
        }
        
        if(admin_role() == Users_model::HR){
            $ci->load->model('Announcements_model', 'announcements');
            //check announcements
            $admin_id = $ci->Users_model->getSuperAdminID();
            $my_where['created_by'] = $admin_id['id'];
            $my_where["'HR' = ANY (string_to_array(audience, ', '))"] = null;
            $my_where['e.date_created >= '] = $company_enrolled_date;
            $announcements = $ci->announcements->getAll($my_where);

            if(count($announcements) > 0){
                $unread_announcements = count($announcements);
                foreach($announcements as $rec){
                    if($rec['read_by'] != null){
                        $read_by = json_decode($rec['read_by'], true);
                        $my_userid = $ci->session->userdata['logged_in']['userid'];
                        $is_read = false;
                        foreach($read_by as $val){
                            if($val['type'] == 'HR'){
                                if($val['id'] == $my_userid){
                                    $is_read = true;
                                }
                            }
                        }
                        if($is_read){
                            $unread_announcements--;
                        }
                    }
                    
                }
                if($unread_announcements > 0){
                    $notification['with_notification']['page'][] = 'notifications';
                }
            }
        }
        
        
        return $notification;
    }
    
    function _saveToUserActivities($action = null, $detals = array()){
        $ci = get_instance();
        
        $data = array(
            'action' => $action,
            'details' => json_encode($detals)
        );
        
        return $ci->activity->log($data);
    }

}
