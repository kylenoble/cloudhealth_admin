<?php

class DataConsent_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function add($data = array()){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        //delete if exists
        $this->db->where('company_id', $company_id);
        $this->db->where('trim(email)', trim($data['email']));
        if($this->db->delete('data_consent')){
            $data['date_uploaded'] = date("Y-m-d H:i:s");
            $data['company_id'] = $company_id;
            return $this->db->insert('data_consent', $data);
        }
    }
    
    function update($email = null, $data = array()){
        $this->db->where('trim(email)', trim($email));
        return $this->db->update('data_consent', $data);
    }
    
    function getAllPerCompany($company_id=0){
        $this->db->where('company_id', $company_id);
        $this->db->select("email, name, date_uploaded, to_char((date_uploaded), 'Mon DD, YYYY HH12:MIPM') as uploaded, date_updated, to_char((date_updated at time zone 'PHT'), 'Mon DD, YYYY HH12:MIPM') as updated, CASE WHEN is_sent = TRUE THEN 'SENT' WHEN is_duplicate = TRUE THEN 'FAILED (DUPLICATE)' ELSE 'FAILED' END AS is_sent, is_agreed");
        $query = $this->db->get('data_consent');
        
        return $query->result_array();
    }
    
    function getCount($company_id){
        $this->db->where('company_id', $company_id);
        $this->db->select("count(*) as total_emails_sent,
            COALESCE(SUM(CASE WHEN is_sent = true THEN 1 ELSE 0 END)) as success,
            COALESCE(SUM(CASE WHEN is_sent = false THEN 1 ELSE 0 END)) as failed,
            COALESCE(SUM(CASE WHEN is_agreed IS NOT NULL THEN 1 ELSE 0 END)) as total_respondents,
            COALESCE(SUM(CASE WHEN is_agreed = 'yes' THEN 1 ELSE 0 END)) as consented_to_enrollment,
            COALESCE(SUM(CASE WHEN is_agreed = 'no' THEN 1 ELSE 0 END)) as refused_enrollment");

        $query = $this->db->get('data_consent');

        return $query->row_array();
    }
    
    function userConsentedYES($email){
        $this->db->where('trim(email)', trim($email));
        $this->db->where('is_agreed', 'yes');
        if($this->db->count_all_results('data_consent') > 0){
            return true;
        }
        return false;
    }
}
