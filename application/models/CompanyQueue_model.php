<?php

class CompanyQueue_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function save_batch($data=array()){
        $this->db->insert_batch('company_queue', $data);
    }
    
    function getAll($where=array(), $is_active=true, $cols=null){
        if($cols != null){
            $this->db->select($cols);
        }
        $this->db->where($where);
        $query = $this->db->get('company_queue');
        
        return $query->result_array();
    }
    
    function delete($company_id=0){
        $this->db->where('company_id', $company_id);
        $this->db->delete('company_queue');
    }
    
    function getAssignedDoctors($where = array()){
        $this->db->where($where);
        $this->db->select('name, user_id');
        $this->db->from('company_queue');
        $this->db->join('users', 'users.id = user_id', 'LEFT');
        $query = $this->db->get();
        
        return $query->result_array();
    }

}
