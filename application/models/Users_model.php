<?PHP

class Users_model extends CI_Model {
    static $NEW_USER = null;
    static $ACTIVE = '1';
    static $PENDING_NDA = '2';
    static $REVOKED = '3';
    static $ANONYMOUS = '4';
    
    const ADMIN = 1;
    const HR = 2;
    const MD = 3;
    const SALES = 4;
    const OPS = 5;
    const FINANCE = 6;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();

    }

  public function adminList($role = null, $active_only = false, $company_id = 0) {
        $this->db->select('a.id, role, email, password, name, company_id, status, date_revoked, a.password, a.date_modified');
	$this->db->from('admin as a');
        if($role == $this::HR){
            $this->db->select('company_name');
            $this->db->join('companies as c', 'c.id = a.company_id', 'left');
            $this->db->where('c.is_active', true);
        }else{
            $role_arr = array(
                $this::ADMIN, $this::MD, $this::SALES, $this::OPS, $this::FINANCE 
            );
            $this->db->where_in('role', $role_arr);
            $this->db->where('is_group_head', true);
        }
        if($company_id > 0){
            $this->db->where('company_id', $company_id);
        }
        if($active_only){
            $this->db->group_start();
            $this->db->where('a.status != ', Users_model::$REVOKED);
            $this->db->or_where('a.status', null);
            $this->db->group_end();
        }
	$query = $this->db->get();
	$result = $query->result_array();

	return $result;
  }

    public function add_user($data) {
          $password = $this->cloudhealth->random_password();
          $d = array();
          $d['company_id'] = $data['company_id'];
          $d['email'] = $data['email'];
          $d['name'] = $data['name'];
          $d['role'] = $data['role'];
          $d['date_created'] = date('Y-m-d H:i:s');
          $d['password'] = $password['plain'];

          $this->db->insert('admin', $d);
          return array('password' => $d['password']);
    }

    public function promoteMD_to_admin($user= array(), $exists = false) {
        
        $data['md_company_name'] = trim($user['company']);
        $data['email'] = trim($user['email']);
        $data['name'] = trim($user['name']);
        $data['role'] = $this::MD;
        $data['is_group_head'] = true;
        $data['password'] = $user['password'];
        $data['status'] = null;

        if($exists){
            $data['date_modified'] = date('Y-m-d H:i:s');
            $this->db->where('id', $exists['id']);
            $this->db->update('admin', $data);
            return $exists['id'];
        }else{
            $data['date_created'] = date('Y-m-d H:i:s');
            $this->db->insert('admin', $data);
            return $this->db->insert_id();
        }
    }

  public function update_user($data,$id, $send_email=false) {

	$d = array();
	$d['email'] = $data['email'];
	$d['name'] = $data['name'];
	$d['role'] = $data['role'];
        $d['date_modified'] = date('Y-m-d H:i:s');
        if($send_email){    //email address was modified
            $password = $this->cloudhealth->random_password();
            $d['password'] = $password['plain'];
            $d['status'] = null;
        }
	 
	$this->db->where('id', $id);
	$this->db->update('admin', $d);
        if($send_email){
            return array('new_password' => $d['password']);
        }
	$result = $this->db->affected_rows();
	return $result;
  
  }

  public function get_userdetails($data) {
	$id = $data['id'];
	$this->db->select('*')
		->from('admin')
		->where('id', $id);	
	$query = $this->db->get();
	$result = $query->row_array();
	return $result;
  }

  public function delete_user($data) {
	$id = $data['id'];
	$this->db->where('id', $id);
    $del=$this->db->delete('admin');   
    return $del;
  }

  public function validate_email($email, $user_id=0) {	
	$this->db->select('id')
		->from('users')
		->where('UPPER(email)', strtoupper($email));
        if($user_id > 0){
            $this->db->where('id != ', $user_id);
        }
	$query = $this->db->get();
	$result = $query->row_array();	
	return $result['id'];
  }

  public function add_employee($data) {
	$d = array();
	$d['email'] = $data['email'];
	$d['password'] = $data['password'];
	$d['name'] = $data['name'];
	$d['company'] = $data['company'];
	$d['created_at'] = date('Y-m-d H:i:s');
	$d['branch'] = $data['branch'];
	$d['department'] = $data['department'];
	$d['employee_id'] = $data['employee_id'];
	$d['job_grade'] = $data['job_grade'];
	$d['high_risk'] = $data['high_risk'];
	
	$this->db->insert('users', $d);
	$user_id = $this->db->insert_id();

	//insert to new_ansers
	$answers = array();
	$answers['question_id'] = '1';
	$answers['user_id'] = $user_id;
	$answers['value'] = $data['dob'];
	$this->db->insert('new_answers', $answers);
	$ques_id = $this->db->insert_id();

	//insert to new_ansers
	$gender = array();
	$gender['question_id'] = '2';
	$gender['user_id'] = $user_id;
	$gender['value'] = $data['gender'];
	$this->db->insert('new_answers', $gender);
	$quesid = $this->db->insert_id();

    return $user_id;
		
  }
  
    public function patientList($company_id=0) {
        $company_name = $this->getCompanyNameByID($company_id);
        $sql = "select id, email, u.name, branch, department, employee_id, status, healthsurvey_status,
            CASE WHEN job_grade IS NULL OR job_grade = '' THEN '-' ELSE job_grade END AS job_grade,
            CASE WHEN high_risk = 1 THEN 'High Value' WHEN high_risk = 2 THEN 'High Risk' ELSE '-' END AS high_risk,
            MAX(gender) AS gender, MAX(created_at) AS created_at, 
            MAX(date_revoked) AS date_revoked,
            (SELECT MAX(created_date) FROM user_activities WHERE user_id=u.id AND action='Login') AS last_login,
            (SELECT MAX(created_date) FROM new_answers WHERE user_id=u.id and question_id > 10) AS last_survey_update
            FROM users u
            LEFT JOIN (
                SELECT DISTINCT ON (user_id, question_id) user_id,
                CASE WHEN question_id = 2 THEN value END AS gender 
                FROM new_answers 
                WHERE question_id = 2
                ORDER BY user_id, question_id, created_date DESC    
            ) t
            ON u.id = t.user_id
            WHERE type='Patient'
            AND company = '{$company_name}'
            GROUP BY id, email, u.name, branch, department, employee_id, status, healthsurvey_status, high_risk
            ORDER BY id desc";

        $query = $this->db->query($sql);
	$result = $query->result_array();

	return $result;
    }
    
    function getCompanyNameByID($id = 0){
        $this->db->select('company_name')
                ->from('companies')
                ->where('id', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result['company_name'];
    }
    
    public function update_access($userid = 0) {
        $d = array();
        $d['status'] = 3;   //  revoked access status
        $d['date_revoked'] = date('Y-m-d H:i:s');

        $this->db->where('id', $userid);
        $this->db->update('users', $d);
        $result = $this->db->affected_rows();
        return $result;
    }
    
    public function getUserInfoById($userid = 0, $col=null) {
        $this->db->where('id', $userid);
        $query = $this->db->get('users');
        $result = $query->row_array();
        if($col != null){
            return trim($result[$col]);
        }
        return $result;
    }
    
    public function getUserCountPerCompany($company_name=null, $data=array()){
        if($company_name != null){
            $this->db->where('trim(company)', trim($company_name));
        }
        if (!empty($data)) {
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->join('companies c', 'trim(company) = company_name', 'LEFT');
        $this->db->where('c.is_active', true);
        $this->db->where('(users.status != 3 OR users.status IS NULL)', null);
        $this->db->where('type', 'Patient');
        return $this->db->count_all_results('users');
    }
    
    public function getUserCountPerCompanyPerStatus($company_name=null, $status=null, $check_login = false){
        $this->db->select('count(*)');
        if($status == null){
            $this->db->where('status IS NULL');
        }else{
            $this->db->where('status', $status);
        }
        $this->db->where('trim(company)', trim($company_name));
        if($check_login){
            $this->db->where("id IN (select distinct(user_id) from account_logs where activity_name ='login')");
        }
        $query = $this->db->get('users');
        $result = $query->row_array();
        return $result['count'];
    }
    
    public function update_userInfo($data=array(),$id=0) {
        $data['date_modified'] = date('Y-m-d H:i:s');
	$this->db->where('id', $id);
	$this->db->update('users', $data);
	$result = $this->db->affected_rows();
	return $result;
    }
    
    public function getPersonalInfoFromAnswers($userid=0, $question_id=0, $col=null){
        $this->db->select($col);
        $this->db->where('user_id', $userid);
        $this->db->where('question_id', $question_id);
        $this->db->order_by('created_date', 'desc');
        $query = $this->db->get('new_answers');
        if($query->num_rows() > 0){
            $result = $query->row_array();
            return $result['value'];
        }
        return null;
    }
    
    function saveDOB($dob = null, $userid=0, $update=true){
        $data['value'] = date('m/d/Y', strtotime($dob));
        if($update){
            $this->db->where('user_id', $userid);
            $this->db->where('question_id', 1);
            $this->db->update('new_answers', $data);
        }else{
            $data['question_id'] = 1;
            $data['user_id'] = $userid;
            $this->db->insert('new_answers', $data);
        }
        
	$result = $this->db->affected_rows();
	return $result;
    }
    
    function saveGender($gender, $userid=0, $update=true){
        $data['value'] = $gender;
        if($update){
            $this->db->where('user_id', $userid);
            $this->db->where('question_id', 2);
            $this->db->update('new_answers', $data);
        }else{
            $data['question_id'] = 2;
            $data['user_id'] = $userid;
            $this->db->insert('new_answers', $data);
        }
        
	$result = $this->db->affected_rows();
	return $result;
    }
    
    public function validate_AdminEmail($email, $user_id=0) {
	$this->db->select('id, name')
		->from('admin')
		->where('UPPER(email)', strtoupper($email));
        if($user_id > 0){
            $this->db->where('id != ', $user_id);
        }
	$query = $this->db->get();
	return $query->row_array();	
    }
    
    public function update_admin($data, $id) {
        $data['date_modified'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        $this->db->update('admin', $data);
        return $this->db->affected_rows();
    }

    public function get_adminDetails($where = array(), $cols = null) {
        if($cols == null){
            $cols = 'admin.id, email, admin.name, role, company_id, status';
        }
	$this->db->select($cols)
		->from('admin')
		->where($where);
        if($cols != null){
            if(strpos($cols, 'role')){
                $this->db->join('group_profiles as gp', 'role = gp.id', 'LEFT');
            }
            if(strpos($cols, 'company_name')){
                $this->db->join('companies as c', 'company_id = c.id', 'LEFT');
            }
        }
	$query = $this->db->get();
        return $query->row_array();
    }
    
    public function getRoles($is_active = true) {
        $roles = array();
        $this->db->select('id, name')
                ->from('group_profiles')
                ->where('is_active', $is_active)
                ->where('id > ', 1);    //all except super admin
        $query = $this->db->get();
        $result = $query->result_array();
        if(count($result) > 0){
            foreach($result as $role){
                $roles[$role['id']] = $role['name'];
            }
        }
        return $roles;
    }
    
    function getUserCountPerType($type = null, $company = null, $check_created_date = array(), $is_active = true){
        if($type != null){
            $this->db->where('type', $type);
        }
        if($company != null){
            $this->db->where('trim(company)', $company);
        }
        if(!empty($check_created_date)){
            $this->db->group_start();
            $this->db->where('created_at >=', $check_created_date['start']);
            $this->db->where('created_at <=', $check_created_date['end']);
            $this->db->group_end();
        }
        if($is_active === true){
            $this->db->where('(status != 3 OR status IS NULL)', NULL);
        }
        return $this->db->count_all_results('users');
    }
    
    public function get_users_branches($company) {
        $this->db->select('DISTINCT(branch) as branch');
        $this->db->from('users as u');
        if ($company != '') {
            $this->db->where('company', $company);
        }
        $this->db->where('branch!=', '');
        $this->db->order_by('branch');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_users_departments($company) {
        $this->db->select('DISTINCT(department) as department');
        $this->db->from('users as u');
        if ($company != '') {
            $this->db->where('company', $company);
        }
        $this->db->where('department!=', '');
        $this->db->order_by('department');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    function updateStatus($data = array(), $userids = array()){
        $this->db->where_in('id', $userids);
        return $this->db->update('users', $data);
    }
    
    function getSuperAdminId(){
        $this->db->select('id, name')
                ->from('admin')
                ->where('role', $this::ADMIN);
        
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function getAdminUsersByCompany($company_id = 0, $where = array(), $cols = null) {
        if($cols == null){
            $cols = 'id, email, name, role, company_id, status';
        }
	$this->db->select($cols)
		->from('admin')
		->where('company_id', $company_id);
        if(!empty($where)){
            $this->db->where($where);
        }
	$query = $this->db->get();
        return $query->result_array();
    }
    
    function getEnrolleeList($where=null, $company_id=0){
        $sql = "SELECT CASE WHEN status = 4 THEN 1 ELSE 0 END AS is_anonymous, employee_id, name, branch, department FROM users u LEFT JOIN companies c ON trim(c.company_name) = trim(u.company) WHERE c.id = {$company_id} {$where}";
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function getUsersByType($type=null, $is_active = false, $all_non_patient = false){
        $this->db->select('id, email, name, company, created_at, type, employee_id, status, date_revoked, is_admin');
        if(!$all_non_patient && $type != null){
            $this->db->where('type', $type);
        }else{
            $this->db->where('type !=', 'Patient');
        }
        if($is_active){
            $this->db->group_start();
            $this->db->where('status !=', $this::$REVOKED);
            $this->db->or_where('status', null);
            $this->db->group_end();
        }
        $query = $this->db->get('users');

        return $query->result_array();
    }
    
    function add_mpuser($data=array()){
//        $data['status'] = $this::$ACTIVE;
        $this->db->insert('users', $data);
	return $this->db->insert_id();
    }
    
    function getEnrolleeCount($where=null, $company_name=null){
        $sql = "SELECT COUNT(id) FROM users WHERE company = '{$company_name}' {$where}";
        
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    public function revoke_admin($userid = 0) {
        $d = array();
        $d['status'] = 3;   //  revoked access status
        $d['date_revoked'] = date('Y-m-d H:i:s');

        $this->db->where('id', $userid);
        $this->db->update('admin', $d);
        $result = $this->db->affected_rows();
        return $result;
    }
    
    function getActiveUserEmailsByType($user_types = array(), $company_name=null){
        $this->db->distinct();
        $this->db->select('TRIM(email) AS email');
        $this->db->from('users');
        $this->db->where_in('type', $user_types);
        $this->db->where('status != ', Users_model::$REVOKED);
        $this->db->where('status IS NOT NULL ', null);
        if($company_name != null){
            $this->db->where('trim(company)', $company_name);
        }
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    function getActiveAdminEmailsByRole($admin_roles = array(), $company_id=0){
        $this->db->distinct();
        $this->db->select('TRIM(email) AS email');
        $this->db->from('admin');
        $this->db->where_in('role', $admin_roles);
        $this->db->where('status', $this::$ACTIVE);
        if($company_id > 0){
            $this->db->where('company_id', $company_id);
        }
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    public function update_userByEmail($old_email, $new_email, $new_password) {
        $data['date_modified'] = date('Y-m-d H:i:s');
        $data['email'] = $new_email;
        $data['password'] = $new_password;
	$this->db->where('UPPER(email)', strtoupper($old_email));
	$this->db->update('users', $data);
	$result = $this->db->affected_rows();
	return $result;
    }
    
    function getUserDataByEmail($email=null, $col=null){
        $this->db->where('trim(UPPER(email))', trim(strtoupper($email)));
        $query = $this->db->get('users');
        $result = $query->row_array();
        if($col != null){
            return trim($result[$col]);
        }
        return $result;
    }
    
    function getUserAdminByToken($token = null){
        $this->db->select('id, email, password, is_admin');
        $this->db->where('admin_access_token', $token);
        $this->db->where('is_admin', true);
        
        $query = $this->db->get('users');
        
        return $query->row_array();
    }
    
    function updatePatientsPerCompanyName($company_name=0, $data=array(), $addtl_where=array()){
        $this->db->where('company', $company_name);
        $this->db->where('type', 'Patient');
        if(!empty($addtl_where)){
            $this->db->where($addtl_where);
        }
        $this->db->update('users', $data);
    }
    
    function getAllUsersPerCompany($company_name = null){
        $this->db->where('trim(company)', trim($company_name));
        $query = $this->db->get('users');
        
        return $query->result_array();
    }
    
    function countAdminPerStatus($status, $company_id){
        $this->db->where('status', $status);
        $this->db->where('company_id', $company_id);
        
        return $this->db->count_all_results('admin');
    }
}
?>