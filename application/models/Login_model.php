<?PHP
class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function auth_user($data) {

        $email = $data['email'];

        $password = base64_decode($data['pass']);

        $fromMD_dashboard = (isset($data['fromMD_dashboard']) || false);
        $this->db->select('*');
        $this->db->where('lower(email)', strtolower($email));
//	$this->db->where('password', $password);

        $this->db->group_start();
        $this->db->where('status !=', Users_model::$REVOKED);
        $this->db->where('status !=', Users_model::$PENDING_NDA);
        $this->db->or_where('status', null);
        $this->db->group_end();

        $query = $this->db->get('admin');

        $result = $query->row_array();
        
        if($result){
            return $this->_validatePassword($result, $password, $fromMD_dashboard);
        }
    }

    private function _validatePassword($admin, $password, $fromMD_dashboard = false){
        if($admin['role'] == Users_model::MD && $fromMD_dashboard == false){
            if (password_verify($password, $admin['password'])) {
                return $admin;
            }
        }else{
            if($admin['password'] == $password){
                return $admin;
            }
        }
    }
}
