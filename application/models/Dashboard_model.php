<?PHP
error_reporting(E_ERROR | E_WARNING | E_PARSE);

class Dashboard_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_users_age($company_name = '', $data = array()) {
        $sql_where = null;
        if (!empty($data)) {
            $where = [];
            foreach($data as $col=>$val){
                $where[] = " {$col} = '{$val}'";
            }
            $sql_where = " AND ".implode(" AND ", $where);
        }
        $sql = "select gender, sum(case when age <= 9 then 1 else 0 end) as zero,
                sum(case when age between 10 and 19 then 1 else 0 end) as ten,
                sum(case when age between 20 and 29 then 1 else 0 end) as twenty,
                sum(case when age between 30 and 39 then 1 else 0 end) as thirty,
                sum(case when age between 40 and 49 then 1 else 0 end) as forty,
                sum(case when age between 50 and 59 then 1 else 0 end) as fifty,
                sum(case when age between 60 and 69 then 1 else 0 end) as sixty,
                sum(case when age between 70 and 79 then 1 else 0 end) as seventy
                from (
                select user_id, u.name, max(case when a.question_id=2 then value end) as Gender,
                max(case when a.question_id=1 then date_part('year',age(value::date)) end) as age
                from users u
                left join new_answers a ON a.user_id=u.id and question_id < 3
                where company='{$company_name}' and user_id is not null and (u.status != 3 OR u.status IS NULL)
                and type = 'Patient'
                {$sql_where}
                group by user_id, u.name
                ) t
                group by gender";
        

        $query = $this->db->query($sql);
        $results = $query->result_array();
        
        $return = array();
        foreach($results as $row){
            $return[$row['gender']] = array(
                'zero' => (int) $row['zero'],
                'ten' => (int) $row['ten'],
                'twenty' => (int) $row['twenty'],
                'thirty' => (int) $row['thirty'],
                'forty' => (int) $row['forty'],
                'fifty' => (int) $row['fifty'],
                'sixty' => (int) $row['sixty'],
                'seventy' => (int) $row['seventy']
            );
        }
        return $return;
    }

    public function get_users_health($company_name = null, $data = array()) {
        $this->db->select('distinct ON (a.user_id) a.user_id, a.value');
        $this->db->from('new_answers a');
        $this->db->join('new_questions q', 'a.question_id = q.id', 'left');
        $this->db->join('users u', 'a.user_id=u.id', 'left');
        $this->db->where('a.question_id', '53');
        $this->db->where('company', $company_name);
        $this->db->where('healthsurvey_status', 'Locked');
        
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = a.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->order_by('a.user_id, created_date desc');
        $query = $this->db->get();
        $results = $query->result_array();
        $r = null;
        $r['none'] = $r['stroke'] = $r['cancer'] = $r['weight'] = $r['hypertension'] = $r['heart'] = $r['chronic'] = 0;
        foreach ($results as $res) {
            $value = $res['value'];

            if(preg_match('/weight/i', $value) == 1){
                $r['weight'] = $r['weight'] + 1;
            }
            if(preg_match('/cancer/i', $value) == 1){
                $r['cancer'] = $r['cancer'] + 1;
            }
            if(preg_match('/hypertension/i', $value) == 1){
                $r['hypertension'] = $r['hypertension'] + 1;
            }
            if(preg_match('/stroke/i', $value) == 1){
                $r['stroke'] = $r['stroke'] + 1;
            }
            if(preg_match('/heart/i', $value) == 1){
                $r['heart'] = $r['heart'] + 1;
            }
            if(preg_match('/chronic/i', $value) == 1){
                $r['chronic'] = $r['chronic'] + 1;
            }
            if(preg_match('/diabetes/i', $value) == 1){
                $r['diabetes'] = $r['diabetes'] + 1;
            }
        }
        ksort($r);
        return $r;
    }

    public function get_users_readiness($company_name = null, $filter = null) {
        if ($filter != null) {
            if ($filter == 'high_risk') {
                $this->db->select("CASE WHEN u.high_risk = 1 THEN 'High Value' WHEN u.high_risk = 2 THEN 'High Risk' ELSE 'Others' END as name");
            }else if ($filter == 'age') {
                $this->db->select("age_group as name");
                $this->db->join('user_age_group uag', 'uag.user_id=r.user_id', 'LEFT');
            } else {
                $this->db->select("CASE WHEN {$filter} IS NULL OR {$filter} = '' THEN 'Others' ELSE initcap({$filter}) END as name");
            }
        }
        $this->db->select('sum(CASE WHEN total_per_user >= 21 THEN 1 ELSE 0 END) AS ready, sum(CASE WHEN total_per_user < 21 THEN 1 ELSE 0 END) AS not_ready, count(r.user_id) AS total_cnt');
        $this->db->from('readiness_graph_data r');
        $this->db->join('users u', 'u.id=r.user_id', 'LEFT');
        if ($company_name != '') {
            $this->db->where('company', $company_name);
        }
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->group_by('1');
        $this->db->order_by('1');
        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }

    public function get_users_health_score($company_name = null, $data = array(), $return_status = false, $program_code = null) {
        $healthscore_col = 'healthscore';
        if($data['doh'] != null){
            $healthscore_col = $data['doh'];
        }
        unset($data['doh']);

        $this->db->select("employee_id, h.user_id, name, high_risk, job_grade, status, {$healthscore_col}, survey_count");
        $this->db->from('healthscore_graph_data h');
        $this->db->where('company', $company_name);
        $this->db->where('healthsurvey_status', 'Locked');
        if (!empty($data)) {
            if($return_status){ //  used in employee enrollment to subscriptions
                foreach($data as $col => $value){
                    if($col == 'default'){
                        $range = $this->cloudhealth->getScoreByHealthStatus($value);
                        $this->db->where($healthscore_col.' >= ', (float) $range[0]);
                        if($value != 'Alarming'){
                            $this->db->where($healthscore_col.' < ', (float) $range[1]);
                        }
                    }else{
                        if ($col == 'high_risk' && $program_code == 'TeleMed') {
                            if($value == 'Others'){
                                $value = 0;
                            }elseif($value == 'High Value'){
                                $value = 1;
                            }else{
                                $value = 2;
                            }
                        }
                        $this->db->where($col, $value);
                    }
                }
            }else{
                // used in CHI graph
                foreach($data as $col => $value){
                    if($col == 'default'){
                        $range = $this->cloudhealth->getScoreByHealthStatus($value);
                        $this->db->where('healthscore >= ', (float) $range[0]);
                        if($value != 'Alarming'){
                            $this->db->where('healthscore < ', (float) $range[1]);
                        }
                    }
                    if ($col != 'default' && $value != null) {
                        if ($col == 'high_risk') {
                            if($value == '0'){
                                $value = 'Others';
                            }elseif($value == '1'){
                                $value = 'High Value';
                            }else{
                                $value = 'High Risk';
                            }
                        }
                        $this->db->where($col, $value);
                    }
                }
                if(array_key_exists('age_group', $data)){
                    $this->db->join('user_age_group uag','uag.user_id = h.user_id', 'LEFT');
                }
            }
        }
        $query = $this->db->get(); 
//        echo $this->db->last_query();die;
        $results = $query->result_array();

        if($return_status){
            return $results;
        }
        
        $r = null;
        $r['healthscore'] = array();
        $r['color'] = array();
        $r['counts']['optimal'] = $r['counts']['suboptimal'] = $r['counts']['neutral'] = $r['counts']['compromised'] = $r['counts']['alarming'] = $r['counts']['incomplete'] = 0;

        foreach ($results as $res) {
            $survey_count = $res['survey_count'];
            $score = (float) $res['healthscore'];
            $r['healthscore'][] = $score;

            if($survey_count >= '6'){
                if ($score >= 0 && $score < 3) {
                    $r['counts']['optimal'] = $r['counts']['optimal'] + 1;
                    $r['color'][] = '#72A153';
                } else if ($score >= 3.0 && $score < 5) {
                    $r['counts']['suboptimal'] = $r['counts']['suboptimal'] + 1;
                    $r['color'][] = '#71B2CA';
                } else if ($score >= 5.0 && $score < 6) {
                    $r['counts']['neutral'] = $r['counts']['neutral'] + 1;
                    $r['color'][] = '#E1AF29';
                } else if ($score >= 6 && $score < 8.6) {
                    $r['counts']['compromised'] = $r['counts']['compromised'] + 1;
                    $r['color'][] = '#D77F1A';
                } else {
                    $r['counts']['alarming'] = $r['counts']['alarming'] + 1;
                    $r['color'][] = '#FF0000';
                }
            }else{
                $r['counts']['incomplete'] = $r['counts']['incomplete'] + 1;
                $r['color'][] = '#9C9E9E';
            }
        }

        ksort($r);
        return $r;
    }

    public function get_msq_scattered($company_name = null, $data = array()) {

        $sql_where = null;
        $join = null;
        if (!empty($data)) {
            $where = [];
            if(array_key_exists('age_group', $data)){
                $join = 'LEFT JOIN user_age_group uag ON uag.user_id = t.user_id';
            }
            foreach($data as $col=>$val){
                $where[] = " {$col} = '{$val}'";
            }
            $sql_where = " AND ".implode(" AND ", $where);
        }

        $SQL = "SELECT employee_id, branch, department, job_grade, high_risk, t.user_id, value 
            FROM (
                SELECT DISTINCT ON (user_id) user_id, value FROM summary a WHERE a.name='msq' ORDER BY user_id, created_date desc) t 
            LEFT JOIN users u ON t.user_id = u.id 
            {$join}
            WHERE company = '" . $company_name . "' $sql_where AND healthsurvey_status='Locked'";

        $query = $this->db->query($SQL);

        $results = $query->result();

        $r['healthscore'] = array();
        $r['color'] = array();
        $r['counts'] = array();

        $optimal = 0;
        $mild = 0;
        $moderate = 0;
        $severe = 0;

        foreach ($results as $key => $row) {

            $score = $row->value;

            $r['healthscore'][] = $score;

            if ($score < 10) {
                $optimal++;
                $r['color'][] = '#72a153'; //  green=>optimal     
            } else if ($score <= 50) {
                $mild++;
                $r['color'][] = '#f7e118';  //  yellow=>mild
            } else if ($score <= 100) {
                $moderate++;
                $r['color'][] = '#d77f1a';  //  orange=>moderate
            } else {
                $severe++;
                $r['color'][] = '#FF0000';  //  red=>severe
            }
        }

        $r['counts'] = array(
            'optimal' => $optimal,
            'mild' => $mild,
            'moderate' => $moderate,
            'severe' => $severe,
            'total' => count($results)
        );

        return $r;
    }

    function getSurveyStatus($company_name = null, $data=array()) {
        $this->db->select("user_id, SUM(CASE WHEN survey_status='Complete' THEN 1 ELSE 0 END) AS completed_survey");
        $this->db->from('user_surveystatus_pergroup s');
        
        $sql_where = null;
        if (!empty($data)) {
            $where = [];
            foreach($data as $col=>$val){
                $where[] = " {$col} = '{$val}'";
            }
            $sql_where = " AND ".implode(" AND ", $where);
        }
        $this->db->where("user_id IN (SELECT id from users where company='{$company_name}' {$sql_where})");
        $this->db->group_by('user_id');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function getSurveyStatusReportData($company_name = null) {
        $sql = "select u.name, u.employee_id, u.branch, u.department, u.healthsurvey_status,
        MAX(CASE WHEN s.group = 'personal' THEN survey_status END) AS personal,
        MAX(CASE WHEN s.group = 'health' THEN survey_status END) AS health_issues,
        MAX(CASE WHEN s.group = 'Health Goals' THEN survey_status END) AS health_goals,
        MAX(CASE WHEN s.group = 'readiness' THEN survey_status END) AS readiness,
        MAX(CASE WHEN s.group = 'body' THEN survey_status END) AS Weight,
        MAX(CASE WHEN s.group = 'intake' THEN survey_status END) AS Detox,
        MAX(CASE WHEN s.group = 'movementAndExercise' THEN survey_status END) AS Movement,
        MAX(CASE WHEN s.group = 'nutrition' THEN survey_status END) AS Nutrition,
        MAX(CASE WHEN s.group = 'sleep' THEN survey_status END) AS Sleep,
        MAX(CASE WHEN s.group = 'stress' THEN survey_status END) AS Stress,
        MAX(CASE WHEN s.group = 'Activity' THEN survey_status END) AS Activity,
        MAX(CASE WHEN s.group = 'Eyes, Ears, Nose' THEN survey_status END) AS eyes_ears_nose,
        MAX(CASE WHEN s.group = 'Head and Mind' THEN survey_status END) AS head_and_mind,
        MAX(CASE WHEN s.group = 'Mouth, Throat, Lungs' THEN survey_status END) AS mouth_throat_lungs,
        MAX(CASE WHEN s.group = 'Other' THEN survey_status END) AS Other,
        MAX(CASE WHEN s.group = 'Weight and Digestion' THEN survey_status END) AS weight_and_digestion,
        (select max(created_date) from user_activities where user_id=u.id and action='Login') as last_login,
        (select max(created_date) from new_answers where user_id=u.id and question_id > 10) as last_survey_update
        from user_surveystatus_pergroup s
        right join users u ON s.user_id = u.id
        where company='{$company_name}' and u.type='Patient' and (u.status != 3 or u.status is null)
        group by name, employee_id, branch, department, healthsurvey_status, u.id
        order by name, branch, department";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    function getUpstreamData($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = a.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->where('company', $company_name);
        $query = $this->db->get('upstream_manifestations a');
        $result = $query->result_array();

        return $result;
    }

    function getFoodChoices($company_name = null, $data = array()) {
        $sql_where = null;
        $join = null;
        if (!empty($data)) {
            $where = [];
            if(array_key_exists('age_group', $data)){
                $join = 'LEFT JOIN user_age_group uag ON uag.user_id = a.user_id';
            }
            foreach($data as $col=>$val){
                $where[] = " {$col} = '{$val}'";
            }
            $sql_where = " AND ".implode(" AND ", $where);
        }
        $sql = "select s.token as name, count(*) as total
            from (
            select distinct on (a.user_id, question_id) value
            from new_answers a
            left join users u on u.id=a.user_id
            {$join}
            where question_id=134
            and company = '{$company_name}'
            and healthsurvey_status = 'Locked'
            {$sql_where}
            order by a.user_id, question_id, created_date desc
            ) t, unnest(string_to_array(t.value, ',')) s(token)
            group by name
            order by total desc";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    function getStressSourceRank($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = a.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('label as name, avg(total_score) as total');
        $this->db->where('company', $company_name);
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->group_by('name');
        $this->db->order_by('total desc');
        $query = $this->db->get('upstream_stress');
        $result = $query->result_array();

        return $result;
    }

    function getMSQSystemRank($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = a.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('msq_system as name, count(total_score) as total');
        $this->db->from('msqsystem_ranks a');
        $this->db->where('company', $company_name);
        $this->db->where('total_score >= 10');
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->group_by('msq_system');
        $this->db->order_by('total', 'desc');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function getPrevalentSymptoms($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = a.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('label as name, count(value::numeric) as total');
        $this->db->where('company', $company_name);
        $this->db->where('(value::numeric) > ', 0);
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->join('users u', 'u.id=user_id', 'LEFT');
        $this->db->group_by('label');
        $this->db->order_by('total desc');
        $query = $this->db->get('prevalent_symptoms');
        $result = $query->result_array();
        
        return $result;
    }

    public function get_users_msq_score($company_name = null, $data = array()) {
        if ($company_name != '') {
            $this->db->where('company', $company_name);
        }
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = msq.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->from('msqscores_persystem msq');
        $query = $this->db->get();
        $results = $query->result_array();

        $r = null;
        $r['nose_score'] = $r['digestive_score'] = $r['lungs_score'] = $r['mouth_and_throat_score'] = $r['energy_score'] = $r['joints_and_muscles_score'] = $r['heart_score'] = $r['emotions_score'] = $r['head_score'] = $r['mind_score'] = $r['ears_score'] = $r['eyes_score'] = $r['other_score'] = $r['skin_score'] = $r['weight_score'] = 0;
        foreach ($results as $res) {

            if ($res['nose_score'] > 10) {
                $r['nose_score'] = $r['nose_score'] + 1;
            }

            if ($res['digestive_score'] > 10) {
                $r['digestive_score'] = $r['digestive_score'] + 1;
            }

            if ($res['lungs_score'] > 10) {
                $r['lungs_score'] = $r['lungs_score'] + 1;
            }

            if ($res['mouth_and_throat_score'] >= 10) {
                $r['mouth_and_throat_score'] = $r['mouth_and_throat_score'] + 1;
            }

            if ($res['energy_score'] > 10) {
                $r['energy_score'] = $r['energy_score'] + 1;
            }

            if ($res['joints_and_muscles_score'] > 10) {
                $r['joints_and_muscles_score'] = $r['joints_and_muscles_score'] + 1;
            }

            if ($res['heart_score'] > 10) {
                $r['heart_score'] = $r['heart_score'] + 1;
            }

            if ($res['emotions_score'] > 10) {
                $r['emotions_score'] = $r['emotions_score'] + 1;
            }

            if ($res['head_score'] > 10) {
                $r['head_score'] = $r['head_score'] + 1;
            }

            if ($res['mind_score'] > 10) {
                $r['mind_score'] = $r['mind_score'] + 1;
            }

            if ($res['ears_score'] > 10) {
                $r['ears_score'] = $r['ears_score'] + 1;
            }

            if ($res['skin_score'] > 10) {
                $r['skin_score'] = $r['skin_score'] + 1;
            }

            if ($res['other_score'] > 10) {
                $r['other_score'] = $r['other_score'] + 1;
            }

            if ($res['weight_score'] > 10) {
                $r['weight_score'] = $r['weight_score'] + 1;
            }

            if ($res['eyes_score'] > 10) {
                $r['eyes_score'] = $r['eyes_score'] + 1;
            }
        }

        return $r;
    }

    function getTotalRespondents($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = u.id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->where('company', $company_name);
        return $this->db->count_all_results('users');
    }

    function getTotalStressRespondents($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = u.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('count(distinct(u.user_id)) as count');
        $this->db->where('company', $company_name);
        $this->db->from('upstream_stress u');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getTotalMSQSystemRespondents($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = msq.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('count(distinct(user_id)) as count');
        $this->db->where('company', $company_name);
        $this->db->from('msqsystem_ranks msq');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getTotalSymptomsRespondents($company_name = null, $data = array()) {
        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = ps.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }
        $this->db->select('count(distinct(ps.user_id)) as count');
        $this->db->where('company', $company_name);
        $this->db->join('users u', 'u.id=ps.user_id', 'LEFT');
        $this->db->from('prevalent_symptoms ps');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    public function get_users_doh($company_name = null, $data = array()) {

        $this->db->select("name, round(AVG(value::numeric), 2) * 2 AS mean_value, array_to_string(ARRAY_AGG((value::numeric * 2) order by value::numeric), ',') AS median_values");
        $this->db->from('filtered_summary_view');
        $name = array('sleep', 'weightScore', 'nutrition', 'exercise', 'stress', 'detox');

        $this->db->where_in('name', $name);

        if ($company_name != '') {
            $this->db->where('company', $company_name);
        }

        if (!empty($data)) {
            if(array_key_exists('age_group', $data)){
                $this->db->join('user_age_group uag','uag.user_id = filtered_summary_view.user_id', 'LEFT');
            }
            foreach($data as $col => $value){
                $this->db->where($col, $value);
            }
        }

        $this->db->where('healthsurvey_status', 'Locked');
        $this->db->group_by('name');
        $this->db->order_by('name');

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }
    
    public function get_users_health_score_by_userid($userid = 0) {
        $this->db->select('healthscore');
        $this->db->from('healthscore_graph_data');
        $this->db->where('user_id', $userid);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row['healthscore'];
    }
    
    function getUserCountPerSurveyStatus($company_name=null, $data=array(), $survey_status = 'Locked'){
        if ($company_name != null) {
            $this->db->where('trim(company)', trim($company_name));
        }
        if (!empty($data)) {
            foreach ($data as $col => $value) {
                $this->db->where($col, $value);
            }
        }
        if($survey_status == 'Active'){
            $this->db->where_in('healthsurvey_status', array('Locked', 'Active'));
        }else{
            $this->db->where('healthsurvey_status', $survey_status);
        }
        $this->db->where('(status != 3 OR status IS NULL)', NULL);
        return $this->db->count_all_results('users');
    }

}