<?php

class Consults_Model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function getConsults($filter = array()) {
        $where = '';
        if($filter['company'] && $filter['company'] !== ''){
            $where .= " AND company = '{$filter['company']}'";
        }
        if($filter['doctor_id'] && $filter['doctor_id'] !== ''){
            $where .= " AND doctor_id = {$filter['doctor_id']}";
        }
        if($filter['consult_date_from'] && $filter['consult_date_from'] !== ''){
            $where .= " AND consult_date >= '{$filter['consult_date_from']}'";
        }
        if($filter['consult_date_to'] && $filter['consult_date_to'] !== ''){
            $where .= " AND consult_date < '{$filter['consult_date_to']} 23:59:59'";
        }
        $sql = "select c.id, c.employee_id, c.doctor_id, c.status, c.details, c.consult_date, to_char((c.consult_date at time zone 'PHT'), 'Mon YYYY') as consult_date_month_year,
                c.last_updated, c.patient_id, c.concern, u.name,
                (select age from user_age_group where user_id = c.patient_id ) as age, 
                (select value from new_answers where question_id = 2 and user_id = c.patient_id  order by created_date desc limit 1) as gender, 
                (select name from users where id = c.doctor_id) as md_incharge, u.name
                from consultations c 
                left join users u on u.id = c.patient_id
                where 1=1 {$where}
                order by employee_id, c.id asc";
        
        $query = $this->db->query($sql);
//echo $sql;die;
        return $query->result_array();
    }

}
