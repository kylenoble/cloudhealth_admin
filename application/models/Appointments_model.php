<?php

class Appointments_model extends CI_Model {
    const STATUS_COMPLETED = 1;
    const STATUS_RESCHED = 2;
    const STATUS_CANCELLED = 3;
    
    function __construct() {
        parent::__construct();
    }
    
    function getActiveAppointments(){
        $this->db->select('a.id, datetime, active, a.status, date_requested, is_modified');
        $this->db->select('u1.name as md_name, u2.name as employee_name, u2.employee_id, patient_company_id, u2.company');
        $this->db->from('appointments a');
        $this->db->join('users u1', 'a.doctor_id = u1.id', 'LEFT');
        $this->db->join('users u2', 'a.patient_id = u2.id', 'LEFT');
        
        //get only from active companies
        $this->db->join('companies c', 'c.id = patient_company_id', 'LEFT');
        $this->db->where('c.is_active', true);
        
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getRow($appointment_id=0){
        $this->db->select('id, appointment_code, doctor_id, patient_id, room_name, active, status, concern, patient_company_id, date_rescheduled, datetime, date_requested, is_modified');
        $this->db->from('appointments');
        $this->db->where('id', $appointment_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function getAppointmentDetails($appointment_id=0){
        $this->db->select('a.id, datetime, active, date_requested, concern');
        $this->db->select('u1.id as userid, u1.name as md_name, u1.email as md_email, u2.id as patient_id, u2.name as employee_name, u2.employee_id, u2.company, u2.email as patient_email');
        $this->db->from('appointments a');
        $this->db->join('users u1', 'a.doctor_id = u1.id', 'LEFT');
        $this->db->join('users u2', 'a.patient_id = u2.id', 'LEFT');
        $this->db->where('a.id', $appointment_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function cancel($appointment_id=0){
        $this->db->set('status', $this::STATUS_CANCELLED);
        $this->db->set('is_modified', true);
        $this->db->where('id', $appointment_id);
        return $this->db->update('appointments');
    }
    
    function getAvailableMDByDate($date=null, $md_id = 0){
        $addtl_where = '';
        
        if($date != null){
            $addtl_where .= " cast((start_datetime at time zone 'PHT') as date) = DATE '{$date}'";
        }
        if($md_id > 0){
            if($date != null){
                $addtl_where .= " AND ";
            }
            $addtl_where .= " users.id = {$md_id}";
        }
        $query = $this->db->query("select s.id, users.id as userid, name, start_datetime at time zone 'pht' as start_date, start_datetime::time at time zone 'pht' as start_time, end_datetime at time zone 'pht' as end_date, end_datetime::time at time zone 'pht' as end_time, DATE_PART('hour', end_datetime - start_datetime) as no_of_hours FROM schedules s LEFT JOIN users ON users.id = user_id WHERE {$addtl_where}");

        return $query->result_array();
    }
    
    function getAppointmentsPerMDsched($md_id=0, $date=null, $start_time=null, $end_time=null){
        $where = '';
        if($date != null){
            $where .= " AND datetime::date = '{$date}'";
        }
        if($start_time <> null && $end_time <> null){
            $where .= " AND datetime::time at time zone 'UCT' BETWEEN '{$start_time}' AND '{$end_time}'";
        }
        $query = $this->db->query("select id, datetime at time zone 'PHT' as datetime FROM appointments WHERE doctor_id = {$md_id} {$where}");
        return $query->result_array();
    }
    
    function reschedule($id = 0, $data = null){
        $data['date_rescheduled'] = date(DateTime::ISO8601);
        $data['status'] = $this::STATUS_RESCHED;
        $data['active'] = true; //auto-approved
        $data['is_modified'] = true;
        $this->db->where('id', $id);
        return $this->db->update('appointments', $data);
    }
    
    function reassignMD($id = 0, $data = array()){
        $data['active'] = true; //auto-approved
        $data['is_modified'] = true;
        $this->db->where('id', $id);
        return $this->db->update('appointments', $data);
    }
       
    function getScheduleDetailsById($schedule_id=0){
        $this->db->select('id, user_id, start_datetime, end_datetime, title, status');
        $this->db->from('schedules');
        $this->db->where('id', $schedule_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function getAppointmentCountByStatus($company_id = 0, $md_id = 0, $schedule = array()){
        $this->db->select("coalesce(sum(CASE WHEN active=true and status=1 and date_rescheduled is null then 1 else 0 end), 0) as approved_completed, 
            coalesce(sum(CASE WHEN active=true and status is null and datetime < now() then 1 else 0 end), 0) as approved_missed, 
            coalesce(sum(CASE WHEN active=true and status is null and datetime > now() then 1 else 0 end), 0) as approved_pending,
            coalesce(sum(CASE WHEN status=3 then 1 else 0 end), 0) as cancelled,
            coalesce(sum(CASE WHEN active=true and status=1 and date_rescheduled is not null then 1 else 0 end), 0) as rescheduled_completed, 
            coalesce(sum(CASE WHEN active=true and status=2 and datetime < now() then 1 else 0 end), 0) as rescheduled_missed, 
            coalesce(sum(CASE WHEN active=true and status=2 and datetime > now() then 1 else 0 end), 0) as rescheduled_pending,
            coalesce(sum(CASE WHEN active=false and (status is null or status != 3) then 1 else 0 end), 0) as pending_for_approval");
        
        if($company_id > 0){
            $this->db->where('patient_company_id', $company_id);
        }
        if($md_id > 0){
            $this->db->where('doctor_id', $md_id);
        }
        
        if(!empty($schedule)){
            if($schedule['from'] <> null){
                $this->db->where('datetime >= ', $schedule['from']);
            }
            if($schedule['to'] <> null){
                $this->db->where('datetime <= ', $schedule['to']);
            }
        }
        
        $query = $this->db->get('appointments');
//        echo $this->db->last_query();
        return $query->row_array();
    }
    
    function analyticsData($company_id = 0, $md_id = 0, $schedule = array(), $filter_by=null){
        $order_by = 'datetime::date, ';
        $group_col = "datetime::date, to_char(to_timestamp (date_part('month', datetime)::text, 'MM'), 'TMMon') || date_part('day', datetime) || ' ''' ||(date_part('year', datetime)::integer % 100)";
        $where = ' WHERE 1=1 ';
        if($filter_by != null){
            $group_col = $filter_by;
            $order_by = '';
        }
        if($company_id > 0){
            $where .= " AND patient_company_id = {$company_id}";
        }
        if($md_id > 0){
            $where .= " AND doctor_id = {$md_id}";
        }
        
        if(!empty($schedule)){
            if($schedule['from'] <> null){
                $where .= " AND datetime::date >= '{$schedule['from']}'";
            }
            if($schedule['to'] <> null){
                $where .= " AND datetime::date <= '{$schedule['to']}'";
            }
        }
        $sql = "select {$group_col} column_select,
                coalesce(sum(CASE WHEN active=true and status=1 and date_rescheduled is null then 1 else 0 end), 0) as approved_completed, 
                coalesce(sum(CASE WHEN active=true and status is null and datetime < now() then 1 else 0 end), 0) as approved_missed, 
                coalesce(sum(CASE WHEN active=true and status is null and datetime > now() then 1 else 0 end), 0) as approved_pending,
                coalesce(sum(CASE WHEN status=3 then 1 else 0 end), 0) as cancelled,
                coalesce(sum(CASE WHEN active=true and status=1 and date_rescheduled is not null then 1 else 0 end), 0) as rescheduled_completed, 
                coalesce(sum(CASE WHEN active=true and status=2 and datetime < now() then 1 else 0 end), 0) as rescheduled_missed, 
                coalesce(sum(CASE WHEN active=true and status=2 and datetime > now() then 1 else 0 end), 0) as rescheduled_pending,
                coalesce(sum(CASE WHEN active=false and (status is null or status != 3) then 1 else 0 end), 0) as pending_for_approval,
                count(*) as total
                from appointments
                {$where}
                group by {$group_col}
                order by {$order_by} column_select ";
        
        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
    function getAppointmentCount($company_id = 0, $by_date = 'day', $unique_user_count = false){
        $addtl_where = '';
        if($company_id > 0){
            $addtl_where = " AND patient_company_id = {$company_id}";
        }
        $count_distinct = 'id';
        if($unique_user_count){
            $count_distinct = 'distinct(patient_id)';
        }
        $sql = "select date_trunc('{$by_date}', datetime), count({$count_distinct}) from appointments
                where (status IS NULL OR status != 3) {$addtl_where}
                group by date_trunc('{$by_date}', datetime)";
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }
    
    function saveToHistory($action, $appointment=array()){
        $history_data = array(
            'appointment_code' => $appointment['appointment_code'],
            'patient_id' => $appointment['patient_id'],
            'md_id' => $appointment['doctor_id'],
            'concern' => $appointment['concern'],
            'appointment_date' => $appointment['datetime'],
            'appointment_status' => $appointment['status'],
            'action' => $action
        );
        $this->db->insert('appointment_history', $history_data);
    }
    
    function getAppointmentHistory($appointment_code){
        $this->db->select("u.name as md_name, h.appointment_code, h.patient_id, h.md_id, h.concern, h.appointment_date, h.appointment_status, h.history_created_date, action");
        $this->db->from('appointment_history h');
        $this->db->join('users u', 'u.id = h.md_id', 'LEFT');
        $this->db->where('appointment_code', $appointment_code);
        $query = $this->db->get();
        
        return $query->result_array();
    }
}
