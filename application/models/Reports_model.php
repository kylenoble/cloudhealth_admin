<?php

class Reports_model extends CI_Model {
    static $FINAL = 1;
    static $REPORT_SENT = 2;
    
    function __construct() {
        parent::__construct();
    }
    
    function getReportByCompanyId($company_id, $status = null, $company_subscription_id = 0){
        $this->db->select('md_incharge, status, file_name, date_created, date_modified, report_name');
        $this->db->where('company_id', $company_id);
        if($company_subscription_id > 0){
            $this->db->where('company_subscription_id', $company_subscription_id);
        }
        if($status != null){
            $this->db->where('status', $status);
        }
        
        $query = $this->db->get('reports');

        return $query->result_array();
    }
    
    function checkReport(){
        $this->db->select('count(reports.id) as count');
        $this->db->from('reports');
        $this->db->join('subscription_employee_enrollment ee', 'company_subscription_id = ee.company_enrollment_id', 'LEFT');
        $this->db->join('companies', 'companies.id = reports.company_id', 'LEFT');
        $this->db->where('ee.id IS NOT NULL');
        $this->db->where('companies.is_active', true);
        
        $query = $this->db->get();
        
        return $query->row_array();
    }
    
    function checkIfReportSent($id){
        $this->db->where('company_subscription_id', $id);
        if($this->db->count_all_results('reports') > 0){
            return true;
        }
        return false;
    }
}
