<?php

class Announcements_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function save($data = array()) {
        $data['date_created'] = date(DateTime::ISO8601);
        $this->db->insert('announcements', $data);
        return $this->db->insert_id();
    }

    function update($id = 0, $data = array()) {
        $data['date_modified'] = date(DateTime::ISO8601);
        $this->db->where('id', $id);
        return $this->db->update('announcements', $data);
    }

    function getAll($where = array(), $is_active = true) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($is_active) {
            $this->db->where('e.status', 'Active');
        }
        $this->db->select("e.id, title, details, audience, a.name, e.status, e.created_by, e.date_created, read_by->>'data' as read_by");
        $this->db->from('announcements e');
        $this->db->join('admin a', 'a.id = e.created_by', 'LEFT');
        $this->db->order_by('e.id', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }

    function getRow($where = array()) {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('announcements');
        return $query->row_array();
    }
    
    function read($id=0, $read_by=null){
        if($read_by != null){
            $index = count(json_decode($read_by, true));
            $jsonb = array(
                "id" => $this->session->userdata['logged_in']['userid'],
                "type" => admin_role() == Users_model::ADMIN ? "Admin" : "HR",
                "status" => null,
                "date_read" => date("Y-m-d h:i:s A")
            );
            $sql = "UPDATE announcements "
                    . "SET read_by = jsonb_set(read_by, '{data, $index}', '". json_encode($jsonb)."') "
                    . "WHERE id = {$id}";

            return $this->db->query($sql);
        }else{
            $jsonb[] = array(
                "id" => $this->session->userdata['logged_in']['userid'],
                "type" => admin_role() == Users_model::ADMIN ? "Admin" : "HR",
                "status" => null,
                "date_read" => date("Y-m-d h:i:s A")
            );

            $data['read_by'] = json_encode(array("data" => $jsonb));
        
            $this->db->where('id', $id);
            return $this->db->update('announcements', $data);
        }
    }

    function getAnnouncementById($id=0, $is_read=false){
        $this->db->where('id', $id);
        $this->db->select("id, title, details, date_created, read_by->>'data' as read_by");
        $this->db->from('announcements');
        $query = $this->db->get();
        
        $result = $query->row_array();
        if(!$is_read){
            if($this->read($id, $result['read_by'])){
                return $query->row_array();
            }
        }else{
            return $query->row_array();
        }
        
    }
    

}
