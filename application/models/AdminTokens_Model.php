<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminTokens_Model
 *
 * @author AGC_User
 */
class AdminTokens_Model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function add($data = array()){
        $this->db->insert('md_admin_tokens', $data);
    }
    
    function delete($admin_id=0){
        $this->db->where('admin_id', $admin_id);
        $this->db->delete('md_admin_tokens');
    }
    
    function get($admin_id=0, $col=null){
        $this->db->where('admin_id', $admin_id);
        if($col != null){
            $this->db->select($col);
        }
        $query = $this->db->get('md_admin_tokens');
        return $query->row_array();
    }
}
