<?php

class Tickets_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function save($data = array()) {
        $this->db->insert('tickets', $data);
        return $this->db->insert_id();
    }

    function reply($data = array()) {
        $this->db->insert('ticket_replies', $data);
        
        $id = $this->db->insert_id();
        $q = $this->db->get_where('ticket_replies', array('id' => $id));
        return $q->row_array();
    }

    function getAll($where=array()){
        if(!empty($where)){
            $this->db->where($where);
        }

        $this->db->select('id, ticket_number, fullname, email_address, concern, subject, message, attachment_filename, status, date_created, company_name');
        $this->db->from('tickets');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getRow($where=array()){
        if(!empty($where)){
            $this->db->where($where);
        }
        $query = $this->db->get('tickets');
        return $query->row_array();
    }
    
    function getReplies($ticketNumber=null){
        $this->db->where('ticket_number', $ticketNumber);
        $this->db->select('id, ticket_number, reply_message, datetime_reply, repliedBy_userid');
        $this->db->from('ticket_replies');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getTicketLastUpdate($ticketNumber=null){
        $this->db->where('ticket_number', $ticketNumber);
        $this->db->select('MAX(datetime_reply) as last_update');
        $this->db->from('ticket_replies');
        $query = $this->db->get();

        return $query->row_array();
    }
    
    function updateTicket($id = 0, $data = array()){
        $this->db->where('id', $id);
        return $this->db->update('tickets', $data);
    }

}
