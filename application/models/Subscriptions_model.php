<?php

class Subscriptions_model extends CI_Model {
    const TRIAL_PROGRAM_CODE = 'CHIT';    //CHI Trial
    const TRIAL_PROGRAM_DURATION_MONTH = 3;
    const CHA_PROGRAM_CODE = 'CHIR';    //CHI Run
    const CHA_PROGRAM_DURATION_MONTH = 6;
    
    function __construct() {
        parent::__construct();
    }
    
    function getDefaultProgramCode(){
        return $this::CHA_PROGRAM_CODE;
    }
    
    function getTrialProgramCode(){
        return $this::TRIAL_PROGRAM_CODE;
    }
    
    function computeSubscriptionEnd($start, $code=null){
        if($code == null){
            $code = $this::TRIAL_PROGRAM_CODE;
        }
        $duration = $this::TRIAL_PROGRAM_DURATION_MONTH;
        if($code != $this::TRIAL_PROGRAM_CODE){
            $duration = $this::CHA_PROGRAM_DURATION_MONTH;
        }
        return date('Y-m-d', strtotime("+".$duration." months", strtotime($start)));
    }
    
    function companyEnroll($data = array()){
        $data['date_created'] = date(DateTime::ISO8601);
        $new = $this->db->insert('subscription_company_enrollment', $data);
        
        if($new && $data['program_code'] == 'TeleMed'){
            //enable telemedicine in the companies table
            $d = array();
            $d['telemedicine_enabled'] = true;
            $d['date_modified'] = date('Y-m-d H:i:s');

            $this->db->where('id', $data['company_id']);
            $this->db->update('companies', $d);
        }
        return $new;
    }
    
    function companySubscriptionUpdate($id=0, $data = array(), $subscription = array()){
        $data['date_modified'] = date(DateTime::ISO8601);
        if(!isset($data['subscription_end']) && $data['status'] == 'Completed'){
            $data['subscription_end'] = date('Y-m-d');
        }
        $this->db->where('id', $id);
        $update = $this->db->update('subscription_company_enrollment', $data);
        
        if($update && $subscription && $subscription['program_code'] == 'TeleMed'){
            //disable telemedicine in the companies table
            $d = array();
            $d['telemedicine_enabled'] = (($data['status'] == 'Ongoing' || $data['status'] == 'For Renewal') ? true : false);
            $d['date_modified'] = date('Y-m-d H:i:s');

            $this->db->where('id', $subscription['company_id']);
            $this->db->update('companies', $d);
        }
        
        return $update;
    }
    
    function getCompanySubscriptionById($cols, $id){
        $this->db->where('id', $id);
        $this->db->select($cols);
        $query = $this->db->get('subscription_company_enrollment');
        
        return $query->row_array();
    }
    
    function enrollEmployeeToSubscription($data = array()){
        return $this->db->insert_batch('subscription_employee_enrollment', $data);
    }
    
    function corporateHealthProgramsSummary(){
        $this->db->select("t.id as company_id, company_name, contact, telemedicine_enabled, sce.status, count(program_code) subscription_count");
        $this->db->from("companies t");
        $this->db->join("subscription_company_enrollment sce", "sce.company_id = t.id", "LEFT");
        $this->db->where("t.is_active", TRUE);
        $this->db->group_by("t.id, company_name, contact, telemedicine_enabled, sce.status");
        
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getSubscriptionsByCompanyId($company_id, $with_employee_cnt = false, $company_name = null){
        $this->db->select("a.id as company_enrollment_id, program_code, subscription_name, subscription_start, subscription_end, status, allowed_number_of_users, is_basic");
        if($with_employee_cnt){
            $this->db->select("CASE WHEN program_code IN ('". $this::TRIAL_PROGRAM_CODE ."', '". $this::CHA_PROGRAM_CODE ."') THEN (SELECT COUNT(id) FROM users where type='Patient' AND company='{$company_name}' and (status != 3 OR status IS NULL ) and created_at::date between a.subscription_start and a.subscription_end) ELSE count(user_id) END AS employees_count ");
            $this->db->join("subscription_employee_enrollment b", "b.company_enrollment_id = a.id", "LEFT");
            $this->db->group_by("a.id, program_code, subscription_name, subscription_start, subscription_end, status, allowed_number_of_users, is_basic");
        }else{
            $this->db->select('inclusions');
        }
        $this->db->from("subscription_company_enrollment a");
        $this->db->join("subscription_programs c", "a.program_code = c.shortcode", "LEFT");
        $this->db->where('company_id', $company_id);
        $this->db->where('is_active', true);
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getAllPrograms($except = array(), $except_CHI = false, $CHI_only = false){
        $this->db->select('subscription_name, shortcode, is_basic');
        $this->db->where('is_active', true);
        if(!empty($except)){
            $this->db->where_not_in('shortcode', $except);
        }else{
            if($CHI_only){
                $this->db->where_in('shortcode', array($this::CHA_PROGRAM_CODE, $this::TRIAL_PROGRAM_CODE));
            }
        }
        if($except_CHI == true){
            $this->db->where_not_in('shortcode', array($this::CHA_PROGRAM_CODE, $this::TRIAL_PROGRAM_CODE));
        }
        $this->db->order_by('id');
        $query = $this->db->get('subscription_programs');

        return $query->result_array();
    }
    
    function getEnrolledProgramById($id, $with_company_name=false){
        $this->db->select('e.id as enrollment_id, company_id, subscription_name, subscription_start, subscription_end, status, e.remarks, program_code, allowed_number_of_users, inclusions');
        $this->db->where('e.id', $id);
        $this->db->from('subscription_company_enrollment e');
        $this->db->join('subscription_programs p', 'p.shortcode = e.program_code', 'LEFT');
        if($with_company_name){
            $this->db->select('company_name');
            $this->db->join('companies c', 'c.id = e.company_id', 'LEFT');
        }
        $query = $this->db->get();

        return $query->row_array();
    }
    
    function getEnrolledCountPerProgram($company_id=0){
        $this->db->select("ce.id, subscription_name, count(user_id) as Enrolled,
            COALESCE(SUM(CASE WHEN enrollment_status='Pending' THEN 1 END), 0) as Pending,
            COALESCE(SUM(CASE WHEN enrollment_status='Approved' THEN 1 END), 0) as Approved,
            COALESCE(SUM(CASE WHEN enrollment_status='Declined' THEN 1 END), 0) as Declined");
        $this->db->from('subscription_company_enrollment ce');
        $this->db->join('subscription_employee_enrollment ee', 'ee.company_enrollment_id = ce.id', 'LEFT');
        $this->db->join('subscription_programs p', 'p.shortcode = ce.program_code', 'LEFT');
        $this->db->where_not_in('program_code', array($this::TRIAL_PROGRAM_CODE, $this::CHA_PROGRAM_CODE));
        $this->db->where('company_id', $company_id);
        $this->db->group_by('ce.id, subscription_name');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    function getEnrollmentPerStatus($company_id=0, $status = 'Pending'){
        $this->db->select("subscription_name as program_enrolled, ee.employee_id, u.name, u.branch, u.department, date_approved, date_declined, is_anonymous");
        $this->db->from('subscription_employee_enrollment ee');
        $this->db->join('subscription_company_enrollment ce', 'ee.company_enrollment_id = ce.id', 'LEFT');
        $this->db->join('subscription_programs p', 'p.shortcode = ce.program_code', 'LEFT');
        $this->db->join('users u', 'u.id = ee.user_id', 'LEFT');
        $this->db->where_not_in('program_code', array($this::TRIAL_PROGRAM_CODE, $this::CHA_PROGRAM_CODE));
        $this->db->where('company_id', $company_id);
        $this->db->where('ee.enrollment_status', $status);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    function checkForRenewalAndExpiration(){
        $sql = "SELECT COUNT(*) FROM subscription_company_enrollment ce "
                . "LEFT JOIN companies ON companies.id = ce.company_id "
                . "WHERE subscription_end::date < now()::date AND "
                . "(status != 'Completed' AND status != 'Cancelled') "
                . "AND companies.is_active = true";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    function countEnrolledPerCompany($company_id=0){
        $this->db->from('subscription_employee_enrollment ee');
        $this->db->join('subscription_company_enrollment ce', 'ee.company_enrollment_id = ce.id', 'LEFT');
        $this->db->where('company_id', $company_id);
        
        return $this->db->count_all_results();
    }
    
    function getEnrolledPerCompanyPackage($company_subscription_id=0, $no_event_yet=false, $program_code=null, $addtl_where=array()){
        $this->db->select('ee.id as employee_enrollment_id, user_id, trim(u.name) as name, ee.employee_id, branch, department, ee.is_anonymous, enrollment_status as status, email');
        $this->db->from('subscription_employee_enrollment ee');
        $this->db->join('users u', 'ee.user_id = u.id', 'LEFT');
        $this->db->where('company_enrollment_id', $company_subscription_id);
        
        if($program_code!= null && $no_event_yet === true){
            $this->db->join('user_program_intervention upi', "ee.id = upi.employee_enrollment_id AND program_code='{$program_code}'", 'LEFT');
            $this->db->where('upi.id is null');
        }
        if(!empty($addtl_where)){
            $this->db->where($addtl_where);
        }
        
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getActivePackages($company_id){
        $sql = "SELECT id company_enrollment_id, program_code FROM subscription_company_enrollment WHERE status = 'Ongoing' AND subscription_end::date >= now()::date AND company_id = '{$company_id}' ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function _getEnrolledProgramCodes($company_id, $active = false){
        if($active){
            $enrolled_packages = $this->getActivePackages($company_id);
        }else{
            $enrolled_packages = $this->getSubscriptionsByCompanyId($company_id);
        }
        $enrolled_program_codes = array();
        foreach ($enrolled_packages as $package) {
            $enrolled_program_codes[$package['company_enrollment_id']] = $package['program_code'];
        }
        return $enrolled_program_codes;
    }
    
    function getInfoByProgramcode($cols, $program_code, $company_id, $addtl_where=array()){
        $this->db->select($cols);
        $this->db->where('program_code', $program_code);
        $this->db->where('company_id', $company_id);
        if(!empty($addtl_where)){
            $this->db->where($addtl_where);
        }
        $query = $this->db->get('subscription_company_enrollment');
        
        return $query->row_array();
    }
    
    function getEmployeesPerProgram($company_enrollment_id=0, $where=array(), $program_code=null, $addtl_cols=null){
        $this->db->select('subscription_employee_enrollment.id, user_id, u.employee_id, name, is_anonymous, enrollment_status as status')
                ->from('subscription_employee_enrollment')
                ->where('company_enrollment_id', $company_enrollment_id);
        $this->db->join('users u', 'u.id = subscription_employee_enrollment.user_id', 'LEFT');
        
        if($addtl_cols != null){
            $this->db->select($addtl_cols);
        }
        if($program_code != null){
            $this->db->select('upi.id as upi_id, details');
            $this->db->join("user_program_intervention upi", "subscription_employee_enrollment.id=upi.employee_enrollment_id AND program_code = '{$program_code}'", 'LEFT');
            $this->db->order_by('upi_id', 'desc');
        }
        if(!empty($where)){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
//echo $this->db->last_query();die;
        return $query->result_array();
    }
    
    function getProgramInfoByCode($cols = null, $shortcode = null){
        if($cols == null){
            $cols = 'id, subscription_name, shortcode, price_per_plan, remarks, is_active, inclusions';
        }
        $this->db->select($cols);
        $this->db->from('subscription_programs');
        $this->db->where('shortcode', $shortcode);
        $query = $this->db->get();
        
        return $query->row_array();
    }

    function interventionExists($id=0){
        $this->db->where('employee_enrollment_id', $id);
        return $this->db->count_all_results('user_program_intervention');
    }

    function setInterventionAppointment($id=0, $data = array(), $do_update = false){
        if($do_update){
            $this->db->where('employee_enrollment_id', $id);
            return $this->db->update('user_program_intervention', $data);
        }
        return $this->db->insert('user_program_intervention', $data);
    }
    
    function getUserInfoBySubscriptionEnrollmentId($employee_subscription_id=0, $addtl_col=null){
        if($addtl_col != ''){
            $this->db->select($addtl_col);
        }
        $this->db->select('trim(u.name) as name, trim(email) as email, user_id, type');
        $this->db->from('subscription_employee_enrollment ee');
        $this->db->join('users u', 'ee.user_id = u.id', 'LEFT');
        $this->db->where('ee.id', $employee_subscription_id);
        $query = $this->db->get();
        
        return $query->row_array();
    }
    
    function getBasicProgramInterventionData($company_enrollment_id=0, $program_code=null, $status='Approved'){
        $sql = "select company_enrollment_id, details->>'health_talk' as health_talk, details->>'is_addon' as is_addon, details->>'event_id' as event_id, count(*) 
            from user_program_intervention upi
            left join subscription_employee_enrollment ee
            ON ee.id = upi.employee_enrollment_id
            where program_code = '{$program_code}'
            and company_enrollment_id = {$company_enrollment_id}
            and enrollment_status = '{$status}'
            group by company_enrollment_id, details->>'health_talk', details->>'is_addon', details->>'event_id'";
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }
    
    function getEnrolledPerCompanyBasicPackage($company_enrollment_id=0, $program_code=null, $htalk=null, $isaddon=null){
        $sql = "select ee.user_id, trim(u.name) as name, ee.employee_id, branch, department, ee.is_anonymous, details->>'status' as status
            from user_program_intervention upi
            left join subscription_employee_enrollment ee
            ON ee.id = upi.employee_enrollment_id
            left join users u
            ON u.id = ee.user_id
            where program_code = '{$program_code}'
            and company_enrollment_id = {$company_enrollment_id}
            and details->>'health_talk' = '{$htalk}'
            and details->>'is_addon' = '{$isaddon}' ";
        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
    function setBasicIntervention($data = array()){
        return $this->db->insert_batch('user_program_intervention', $data);
    }
    
    function setBasicEventID($id=0, $event_details=array()){
        $sql = "update user_program_intervention
            set details = details || '[". json_encode($event_details)."]'
            WHERE id = {$id}";
        $this->db->query($sql);
        return true;
    }
    
    function getEventIDindex($event_id=0, $program_code=null, $id=0){
        $sql = "select pos- 1 as elem_index from 
                user_program_intervention
                ,jsonb_array_elements(details) with ordinality arr(elem, pos)
                where program_code='{$program_code}'
                and elem->'event_id' = '{$event_id}'
                and id = {$id}";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    function setBasicEventIntervention($id=0, $index=0, $inclusions=null){
        $sql = "UPDATE user_program_intervention "
                . "SET details = jsonb_set(details, '{".$index.", inclusions}', "
                . "'".json_encode($inclusions)."', true) "
                . "WHERE id={$id}";
        $this->db->query($sql);
//        echo $this->db->last_query();
        return true;
        
    }
    
    function getBasicEventsStat($company_id=0){
        if($company_id > 0){
            $this->db->where('company_id', $company_id);
        }
        $this->db->select("COALESCE(SUM(CASE WHEN enrollment_status='Approved' THEN count ELSE 0 END), 0) AS \"approved_count\"");
        $this->db->select("COALESCE(SUM(CASE WHEN enrollment_status='Declined' THEN count ELSE 0 END), 0) AS \"declined_count\"");
        $this->db->from('health_events_attendance_statistics');
        
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function countUsedSlots($company_enrollment_id=0){
        $this->db->from('subscription_employee_enrollment ee');
        $this->db->join('subscription_company_enrollment ce', 'ee.company_enrollment_id = ce.id', 'LEFT');
        $this->db->where('ce.id', $company_enrollment_id);
        $this->db->where('enrollment_status != ', 'Declined');
        
        return $this->db->count_all_results();
    }

    function getSubscriptionSlots(){
        $this->db->select('ce.id, company_name, company_id, subscription_name, shortcode, subscription_end, subscription_start, allowed_number_of_users, ce.status');
        $this->db->from('subscription_company_enrollment ce');
        $this->db->join('subscription_programs p', 'ce.program_code = p.shortcode', 'LEFT');
        $this->db->join('companies c', 'ce.company_id=c.id', 'LEFT');
        $this->db->where('c.is_active', true);
        $this->db->where('ce.status != ', 'Cancelled');
        $this->db->where('p.is_active', true);
        $this->db->order_by('c.id, ce.id');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    function checkOngoingCHISubscription($company_id){
        $this->db->select('program_code, subscription_end, subscription_start, allowed_number_of_users');
        $this->db->where('company_id', $company_id);
        $this->db->where('status', 'Ongoing');
        $this->db->where_in('program_code', array($this->getTrialProgramCode(), $this->getDefaultProgramCode()));
        
        $query = $this->db->get('subscription_company_enrollment');

        return $query->row_array();
    }
    
    function getCompaniesWithBasicPrograms(){
        $sql="SELECT c.id, company_name
                FROM companies c
                where id in (
                select company_id from subscription_company_enrollment ce
                    left join subscription_programs p ON ce.program_code = p.shortcode 
                    where ce.status != 'Cancelled' 
                    AND p.is_active = TRUE 
                    AND is_basic = TRUE 
                )
                AND c.is_active = TRUE";
        
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }
    
    function event_is_sent($ceid=0, $user_id=0){
        $this->db->where('company_enrollment_id', $ceid);
        $this->db->where('user_id', $user_id);
        return $this->db->update('subscription_employee_enrollment', array('event_is_sent' => true));
    }
    
    function countEventNotSent($company_enrollment_id){
        $this->db->where('company_enrollment_id', $company_enrollment_id);
        $this->db->where('enrollment_status', 'Approved');
        $this->db->where('event_is_sent IS NULL', NULL);
        
        return $this->db->count_all_results('subscription_employee_enrollment');
    }
    
}
