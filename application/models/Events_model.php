<?php
/**
 * read_by column jsonb
 * status key:
 *      null = read
        1 = archived
        2 = joined
        3 = declined
 */
class Events_model extends CI_Model {
    const read = null;
    const archived = 1;
    const joined = 2;
    const declined = 3;

    function __construct() {
        parent::__construct();
    }

    function save($data = array()) {
        $data['date_created'] = date(DateTime::ISO8601);
        $this->db->insert('events', $data);
        return $this->db->insert_id();
    }

    function update($id = 0, $data = array()) {
        $data['date_modified'] = date(DateTime::ISO8601);
        $this->db->where('id', $id);
        return $this->db->update('events', $data);
    }

    function getAll($where=array(), $is_active = true, $admin_events=false, $is_health_talk = false){
        if(!empty($where)){
            $this->db->where($where);
        }
        if($is_active){
            $this->db->where('e.status != ', 'Deleted');
        }
        $this->db->select('e.id, e.name as event_name, venue, start_date, start_time, end_date, end_time, description, audience, a.name, e.status, e.created_by, health_talk, read_by, company_enrollment_id');
        $this->db->from('events e');
        $this->db->join('admin a', 'a.id = e.created_by', 'LEFT');
        if(!$admin_events){
            $this->db->join('companies c', "cast(audience as integer)=c.id and audience::text ~ '^\d+$'::text", 'LEFT');
            $this->db->where('c.is_active', true);
        }
        if($is_health_talk){
            $this->db->select('subscription_name');
            $this->db->join('subscription_company_enrollment ce', "ce.id=e.company_enrollment_id", 'LEFT');
            $this->db->join('subscription_programs p', "p.shortcode=ce.program_code", 'LEFT');
        }
        $this->db->order_by('e.id', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }
    
    function getRow($where=array()){
        if(!empty($where)){
            $this->db->where($where);
        }
        $query = $this->db->get('events');
        return $query->row_array();
    }
    
    function getBasicEvents($where=array(), $is_active = true){
        if(!empty($where)){
            $this->db->where($where);
        }
        if($is_active){
            $this->db->where('status', 'Active');
        }
        $this->db->from('events');
        $query = $this->db->get();

        return $query->result_array();
    }

}
