<?php

class Notifications_model extends CI_Model {
    
    function __construct(){
        parent::__construct();
    }
    
    function sendNotification($data = array()){
        return $this->db->insert_batch('notifications', $data);
    }
    
    function getNotificationsByUser($userid=0, $type=null, $read=null, $date_created = null){
        $role = (admin_role() == Users_model::HR ? 'HR' : 'Admin');
        $addtl_where = "";
        if($read !== null){
            $addtl_where .= " AND is_read = '{$read}'";
        }
        if($role == 'HR' && $date_created !== null){
            $addtl_where .= " AND date_created >= '{$date_created}'";
        }
        $sql = "select id, sender->>'name' as name, subject, date_created, is_read from notifications "
                . "where ((recipient->>'id' = '{$userid}' AND recipient->>'type' = '{$type}') OR "
                . "(recipient->>'id' = '". strtolower($role)."' AND recipient->>'type' = '{$role}')) "
                . "$addtl_where";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    
    function readNotification($notification_id=0){
        $data['is_read'] = true;
        $data['date_read'] = date(DateTime::ISO8601);
        $this->db->where('id', $notification_id);
        return $this->db->update('notifications', $data);
    }
    
    function getNotificationById($id=0, $is_read=null){
        $this->db->where('id', $id);
        $this->db->select('id, subject, message, is_read, date_created');
        $this->db->from('notifications');
        $query = $this->db->get();
        
        if($is_read == 'f'){
            if($this->readNotification($id)){
                return $query->row_array();
            }
        }else{
            return $query->row_array();
        }
        
    }
}
