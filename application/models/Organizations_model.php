<?PHP

class Organizations_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function organizations_list() {
        $this->db->select('*');
        $this->db->order_by('company_name');
        $this->db->where('is_active', true);
        $query = $this->db->get('companies');
        $results = $query->result_array();
        return $results;
    }

    public function get_organizations($is_active=true, $cols='company_name') {
        $this->db->select($cols);
        $this->db->order_by('company_name');
        if($is_active !== null){
            $this->db->where('is_active', $is_active);
        }
        $query = $this->db->get('companies');
        $results = $query->result_array();

        return $results;
    }

    public function add_org($data, $with_nda=false) {
        $code = $this->cloudhealth->random_password();
        $d = array();
        $d['company_name'] = trim($data['org_name']);
        $d['code'] = $code['rand'];
        $d['contact'] = trim($data['account_officer']);
        $d['date_created'] = date('Y-m-d H:i:s');
        
        $this->db->insert('companies', $d);
        $insert_id = $this->db->insert_id();

        //HR admin create credentials
        $password = $this->cloudhealth->random_password();
        $hr = array();
        $hr['date_created'] = date('Y-m-d H:i:s');
        $hr['password'] = $password['plain'];
        $hr['email'] = $data['email'];
        $hr['company_id'] = $insert_id;
        $hr['name'] = trim($data['org_name']);
        $hr['role'] = 2;
        
        if($with_nda){
            //set company as inactive by default until nda is returned -> done offline
            $hr['status'] = Users_model::$PENDING_NDA;
        }

        $this->db->insert('admin', $hr);
        return array('email' => $hr['email'], 'password' => $hr['password'], 'company_id' => $insert_id);
    }

    function exists($table = null, $where = array()){
        $this->db->where($where);
        return $this->db->count_all_results($table) > 0;
    }
    
    public function update_org($data, $id) {
        $d = array();
        $d['company_name'] = $data['org_name'];
        $d['location'] = $data['location'];
        $d['contact'] = $data['contact'];
        $d['date_modified'] = date('Y-m-d H:i:s');
        if(isset($data['logo'])){
            $d['logo'] = $data['logo'];
        }
        $this->db->where('id', $id);
        $this->db->update('companies', $d);
        $result = $this->db->affected_rows();

        if(isset($data['branch_name'])){
            $branches_id = $data['branches_id'];
            $arrBranchname = $data['branch_name'];
            $arrBranchAddress = $data['branch_address'];
            $arrBranchContact = $data['branch_contact'];
            
            foreach($branches_id as $key => $branch_id){
                $orgBranch = array();
                if($branch_id > 0){
                    //  update branch
                    $orgBranch['br_name'] = $arrBranchname[$key];
                    $orgBranch['location'] = $arrBranchAddress[$key];
                    $orgBranch['contact'] = $arrBranchContact[$key];
                    $orgBranch['date_modified'] = date('Y-m-d');
                    $this->update_branch($orgBranch, $branch_id);
                }else{
                    //save non-existing branch only
                    if(!$this->exists('cmpy_branches', array('LOWER(TRIM(br_name))' => strtolower(trim($arrBranchname[$key])), 'cmpy_id' => $id))){
                        //Insert branch
                        $orgBranch['cmpy_id'] = $id;
                        $orgBranch['br_name'] = trim($arrBranchname[$key]);
                        $orgBranch['location'] = $arrBranchAddress[$key];
                        $orgBranch['contact'] = $arrBranchContact[$key];
                        $orgBranch['date_added'] = date('Y-m-d');
                        $this->add_branch($orgBranch);
                    }
                }
            }
        }
        
        if(isset($data['dept_name'])){
            //Insert dept
            $departments_id = $data['departments_id'];
            $arrDept = $data['dept_name'];
            $arrBranch = $data['branch_id'];
            
            foreach($departments_id as $k => $dept_id){
                $orgDept = array();
                if($dept_id > 0){
                    $orgDept['cmpy_id'] = $id;
                    $orgDept['dept_name'] = $arrDept[$k];
                    $orgDept['date_modefied'] = date('Y-m-d');
                    if($arrBranch[$k] != ""){
                        $orgDept['branch_id'] = $arrBranch[$k];
                    }
                    $this->update_department($orgDept, $dept_id);
                }else{
                    //save non-existing branch only
                    if(!$this->exists('cmpy_departments', array('LOWER(TRIM(dept_name))' => strtolower(trim($arrDept[$k])), 'cmpy_id' => $id))){
                        $orgDept['cmpy_id'] = $id;
                        $orgDept['dept_name'] = $arrDept[$k];
                        $orgDept['date_added'] = date('Y-m-d');
                        if($arrBranch[$k] != ""){
                            $orgDept['branch_id'] = $arrBranch[$k];
                        }
                        $this->add_department($orgDept);
                    }
                }
            }
            
        }
        return $result;
    }

    public function add_department($data) {
        $this->db->insert('cmpy_departments', $data);
        //$insert_id = $this->db->insert_id();
        // return  $insert_id;
    }

    public function update_department($data, $dept_id) {
        $this->db->where('id', $dept_id);
        $result = $this->db->update('cmpy_departments', $data);
        return $result;
    }

    public function add_branch($data) {
        $this->db->insert('cmpy_branches', $data);
    }

    function update_branch($data, $branch_id){
        $this->db->where('id', $branch_id);
        $result = $this->db->update('cmpy_branches', $data);
        return $result;
    }

    public function get_company_details($data) {
        $company_id = $data['company_id'];
        $this->db->select('c.id as company_id, c.company_name, c.code as company_code, c.location, c.contact');
        $this->db->from('companies as c');
        $this->db->where('c.id', $company_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_company_details_by_id($id) {

        $this->db->select('id, code, company_name, location, contact, date_created, date_modified, is_active, telemedicine_enabled, logo');
        $this->db->from('companies as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_company_branches($company_id) {
        $this->db->select('id, br_name, location, contact');
        $this->db->from('cmpy_branches');
        if ($company_id != '') {
            $this->db->where('cmpy_id', $company_id);
        }
        $this->db->order_by('br_name');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_company_departments($company_id) {
        $this->db->select('id, dept_name, branch_id');
        $this->db->from('cmpy_departments');
        if ($company_id != '') {
            $this->db->where('cmpy_id', $company_id);
        }
        $this->db->order_by('dept_name');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_companyname_by_id($id) {

        $this->db->select('company_name')
                ->from('companies')
                ->where('id', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result['company_name'];
    }

    public function get_company_jobgrades($company) {
        $this->db->select('DISTINCT(job_grade) as job_grade');
        $this->db->from('users as u');
        if ($company != '') {
            $this->db->where('company', $company);
        }
        $this->db->where('job_grade!=', '');
        $this->db->order_by('job_grade');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_departments_per_branch($company, $branch) {
        $this->db->select('DISTINCT(department) as department');
        $this->db->from('users as u');
        if ($company != '') {
            $this->db->where('company', $company);
        }
        if ($branch != '') {
            $this->db->where('trim(branch)', trim($branch));
        }
        $this->db->where('department!=', '');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function companyName_exists($str) {
        $this->db->where('trim(lower(company_name))', trim(strtolower($str)));
        return $this->db->count_all_results('companies');
    }
    
    function getBranches($company_id=0){
        $this->db->select('id, br_name');
        $this->db->where('cmpy_id', $company_id);
        $this->db->order_by('br_name');
        $query = $this->db->get('cmpy_branches');
        $result = $query->result_array();
        return $result;
    }
    
    function getDepartments($company_id=0, $branch_id=null){
        $this->db->select('dept_name');
        $this->db->where('cmpy_id', $company_id);
        if($branch_id != null){
            $this->db->where('branch_id', $branch_id);
        }
        $this->db->order_by('dept_name');
        $query = $this->db->get('cmpy_departments');
        $result = $query->result_array();
        return $result;
    }
    
    function branch_exists($branch_name=null, $company_id=0){
        $this->db->select('trim(br_name) as branch_name');
        if($company_id > 0){
            $this->db->where('cmpy_id', $company_id);
        }
        $this->db->where('trim(lower(br_name))', trim(strtolower($branch_name)));
        $query = $this->db->get('cmpy_branches');
        return $query->row_array();
    }
    
    function department_exists($dept_name=null, $company_id=0){
        $this->db->select('trim(dept_name) as dept_name');
        if($company_id > 0){
            $this->db->where('cmpy_id', $company_id);
        }
        $this->db->where('trim(lower(dept_name))', trim(strtolower($dept_name)));
        $query = $this->db->get('cmpy_departments');
        return $query->row_array();
    }
    
    public function getDeptsPerBranch($company_id, $branch_id=0) {
        if($branch_id > 0){
            $this->db->where('branch_id', $branch_id);
        }else{
            $this->db->where('branch_id IS NULL');
        }
        if($company_id > 0){
            $this->db->where('cmpy_id', $company_id);
        }
        $this->db->select('dept_name');
        $this->db->order_by('dept_name');
        $query = $this->db->get('cmpy_departments');
        $results = $query->result_array();
        return $results;
    }
    
    function toggleTelemedicine($data = array()){
        $d = array();
        $d['telemedicine_enabled'] = ($data['status'] == '0' ? false : true);
        $d['date_modified'] = date('Y-m-d H:i:s');

        $this->db->where('id', $data['cid']);
        $this->db->update('companies', $d);
    }
    
    function company_analytics(){
        $this->db->select('company_name, c.id as company_id,
                    sum(case when status != 3 or status is null then 1 else 0 end) as total_enrolled,
                    sum(case when status in (1, 4) then 1 else 0 end) as total_dashboard_logins');
        $this->db->from('companies c');
        $this->db->join('users u', 'c.company_name=u.company', 'LEFT');
        $this->db->where('c.is_active is true', null);
        $this->db->where('u.type', 'Patient');
        $this->db->group_by('company_name, c.id');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

}
?>