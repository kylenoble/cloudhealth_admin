<?php

class User_activities extends CI_Model{
    var $table = 'user_activities';
    
    public function __construct() {
        parent::__construct();
    }

    public function log($data) {
        $data['user_id'] = (int)$this->session->userdata['logged_in']['userid'];
	$this->db->insert($this->table, $data);
	$insert_id = $this->db->insert_id();
        
	return $insert_id;
    }

    public function getTotalUniqueLogins($company_name=null, $date=null) {
        $sql = "select count(distinct(user_id))
                from user_activities
                left join companies c ON trim(details->> 'company') = company_name
                where action = 'Login' and user_id > 0 
                and c.is_active = true ";
        if($company_name != null){
            $sql .= "and details->> 'company' = '{$company_name}' ";
        }
        if($date != null){
            $sql .= "and (created_date + interval '8 hours')::date = '{$date}'";
        }
        $query = $this->db->query($sql);
	$result = $query->row_array();

        return $result;
    }
    
    public function getLoginGraphData($company_name=null) {
        $where = "";
        if($company_name != null && $company_name != 'undefined'){
            $where = " and details->> 'company' = '{$company_name}' ";
        }
        $sql = "select TO_CHAR(created_date::date, 'Mon dd') created_date, count(distinct(user_id))
                                    from user_activities
                                    left join companies c ON trim(details->> 'company') = company_name
                                    where action = 'Login' and user_id > 0
                                    {$where}
                                    and c.is_active = true
                                    and created_date > current_date - interval '10' day
                                    group by created_date::date 
                                    order by created_date::date
                                    ";

        $query = $this->db->query($sql);
	$result = $query->result_array();

        return $result;
    }
    
    function getAppointmentHistory($appointment_id = 0){
        $sql = "SELECT name AS action_by, action, details, created_date "
                . "FROM user_activities a "
                . "LEFT JOIN admin u ON u.id = a.user_id "
                . "WHERE action IN ('Re-assigned Appointment', 'Re-scheduled Appointment', 'Cancelled Appointment') "
                . "AND details->>'id' = '{$appointment_id}' "
                . "ORDER BY a.id desc";
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }

}
?>