<?php

if (!function_exists('admin_role')) {

    /**
     * get the array in pretty format
     *
     * @return array
     */
    function admin_role() {
        $role = get_instance()->session->userdata['logged_in']['role'];
        return $role;
    }

}

if (!function_exists('_is_admin')) {

    /**
     * get the array in pretty format
     *
     * @return array
     */
    function _is_admin() {

        if (!isset(get_instance()->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
    }

}

if (!function_exists('_get_query_args')) {

    /**
     * get the request string data
     *
     * @return array
     */
    function _get_query_args() {

        $r = $get = $post = array();
        if (isset($_GET)) {
            $get = $_GET;
        }

        if (isset($_POST)) {
            $post = $_POST;
        }

        $r = array_merge($get, $post);

        return $r;
    }

    function _format_SelectBox($data=array(), $index='id', $text=null){
        $return = array();
        foreach($data as $rec){
            $return[$rec[$index]] = $rec[$text];
        }
        return $return;
    }
}
?>