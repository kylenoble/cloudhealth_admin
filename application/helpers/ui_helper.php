<?php
if (!function_exists('_get_error')) {

  /**
   * get the array in pretty format
   *
   * @return array
   */
  function _get_error($message, $type = 'danger') {

    $str = '';
    if (isset($message)) :
      $str = '<div class="alert alert-' . $type . '" style="opacity: 1; display: block; transform: translateY(0px);">
            <div class="error">' . $message . '</div>
        </div>';

     endif;
    return $str;

  }
}

if (!function_exists('_error_message')) {

  function _error_message($type, $message = '') {

    $str = $this_show = '';
    if ($message == '') {
      $this_show = 'hidden';
    }
    $str = '<div class="error">
          <div class="alert alert-' . $type . ' ' . $this_show . '">' . $message . '</div>
        </div>';

    return $str;

  }
}

if (!function_exists('_actions_icon')) {

  function _actions_icon($url, $type, $class = 'click',$id = '1',$parent_menu = '') {

    $html = '';

    if ($type == 'view') {
      $html .= '<a href="' . $url . '" class="' . $class . '" id="' . $id . '" data-callback="' . $url . '" parent-callback="'.$parent_menu.'"><span class="ti ti-search"></span> </a>';
    }

    if ($type == 'edit') {
      $html .= '<a href="' . $url . '" class="' . $class . '" id="' . $id . '" data-callback="' . $url . '" parent-callback="'.$parent_menu.'"><span class="ti ti-pencil"></span> </a>';
    }

    if ($type == 'delete') {
      $html .= '<a href="' . $url . '" class="' . $class . '" id="' . $id . '" data-callback="' . $url . '" parent-callback="'.$parent_menu.'"><span class="ti ti-trash"></span> </a>';
    }

    if ($type == 'clone') {
      $html .= '<a href="' . $url . '" class="' . $class . '" id="' . $id . '" data-callback="' . $url . '" parent-callback="'.$parent_menu.'"><span class="ti ti-plus"></span> </a>';
    }
    return $html;

  }
}

if (!function_exists('_load_js')) {

  /**
   * function to add script
   *
   * @param array $js
   */
  function _load_js($js) {

    if ($js) :
      foreach ( $js as $val )
        echo $val;


    endif;

  }
}

if (!function_exists('send_welcome_mail')) {

	function send_welcome_mail($email,$password) {
		$html = 'Welcome to CloudHealth Asia, your road to optimum health is just a few steps away.<br>';

		$html.= 'You may now log-in at our website www.cloudhealthasia.com with the following details:<br>';

		$html.= 'Username: '.$email.'<br>';

		$html.= 'Password: '.$password.'<br>';

		$html.= 'This is a system generated email. Please do not reply. For inquiries please email at contact@merakisv.com <br>';
       
	    $html.= 'Thanks';

		$subject = 'Welcome to Cloudhealth Asia';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Cloudhealth Admin <contact@merakisv.com>' . "\r\n";

		// Mail it
		mail($email, $subject, $html, $headers);

	}
}

if ( ! function_exists('formatSelect'))
{
	/**
	 * Multi-select menu
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @param	mixed
	 * @return	string
	 */
	function formatSelect($data=array(), $col = null, $key_is_id = false)
	{
		$select = array();
                foreach($data as $key=>$row){
                    if($key_is_id){
                        $select[$key] = $row;
                    }else{
                        $select[$row[$col]] = $row[$col];
                    }
                }
                return $select;
	}
}
?>