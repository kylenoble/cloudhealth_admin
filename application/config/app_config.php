<?php
/** WEIGHT MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['WMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'nutrition_consult' => 'Nutrition Consult',
    'nutrition_follow_up' => 'Nutrition Follow-up',
    'fitness_consult' => 'Fitness Consult',
    'fitness_follow_up' => 'Fitness Follow-up',
    'bca' => 'BCA',
    'envirotox' => 'EnviroTox',
    'food_intolerance_test' => 'Food Intolerance Test',
    'add_ons' => array(
        'nutrition_sensor' => 'Nutrition Sensor',
        'toxo_sensor' => 'Toxo Sensor',
        'weight_sensor' => 'Weight Sensor',
        'stool_analysis' => 'Comprehensive Stool Analysis and Parasitology'
    )
);
/** WEIGHT MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['WMPB'] = array(
    'food_diary_analysis' => 'Food Diary Analysis',
    'fitness_workshop' => 'Fitness Workshop',
    'bca' => 'BCA',
    'health_talk' => 'Optimal Weight Management Program',
    'add_ons' => array(
        'food_detective' => 'Food Detective',
        'nutrition_sensor' => 'Nutrition Sensor',
        'toxo_sensor' => 'Toxo Sensor',
        'weight_sensor' => 'Weight Sensor',
        'stool_analysis' => 'Comprehensive Stool Analysis and Parasitology'
    )
);

/** NUTRITION MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['NMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'nutrition_consult' => 'Nutrition Consult',
    'nutrition_follow_up' => 'Nutrition Follow-up',
    'fitness_consult' => 'Fitness Consult',
    'fitness_follow_up' => 'Fitness Follow-up',
    'bca' => 'BCA',
    'organic_acids_test' => 'GPL Organic Acids Test',
    'food_intolerance_test' => 'Food Intolerance Test',
    'add_ons' => array(
        'nutrition_sensor' => 'Nutrition Sensor',
        'toxo_sensor' => 'Toxo Sensor',
        'weight_sensor' => 'Weight Sensor',
        'stool_analysis' => 'Comprehensive Stool Analysis and Parasitology'
    )
);
/** NUTRITION MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['NMPB'] = array(
    'health_talk' => array(
        'nutrition' => 'Nutrition Workshop', 
        'fitness' => 'Fitness Workshop'), 
    'food_diary_analysis' => 'Food Diary Analysis',
    'bca' => 'BCA',
    'facility_use' => 'Facility Use',
    'add_ons' => array(
        'addon_food_detective' => 'Food Detective',
        'addon_food_intolerance_test' => 'Food Intolerance Test',
        'nutrition_sensor' => 'Nutrition Sensor',
        'toxo_sensor' => 'Toxo Sensor',
        'weight_sensor' => 'Weight Sensor',
        'stool_analysis' => 'Comprehensive Stool Analysis and Parasitology'
    )
);

/** DETOXIFICATION MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['DMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'nutrition_consult' => 'Nutrition Consult',
    'nutrition_follow_up' => 'Nutrition Follow-up',
    'envirotox' => 'EnviroTox',
    'add_ons' => array(
        'iv_therapy' => 'Myers (Detox)',
        'hbot' => 'Hyperbaric Oxygen Therapy(HBOT)',
        'gpl_blood_test' => 'GPL Heavy Metals Blood Test',
        'heilen_low_laser' => 'Heilen Low Laser Light Therapy'
    )
);
/** DETOXIFICATION MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['DMPB'] = array(
    'health_talk' => 'Detox Workshop', 
    'add_ons' => array(
        'iv_therapy' => 'Myers (Detox)',
        'hbot' => 'Hyperbaric Oxygen Therapy(HBOT)',
        'gpl_blood_test' => 'GPL Heavy Metals Blood Test',
        'heilen_low_laser' => 'Heilen Low Laser Light Therapy'
    )
);

/** MOVEMENT MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['MMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'fitness_consult' => 'Fitness Consult',
    'regular_stability_ball' => 'Regular Stability Ball',
    'stress_recovery_monitor' => 'Stress Recovery Monitor',
    'physical_assessment' => 'Physical Assessment with Postural Analysis',
    'movement_screening' => 'Functional Movement Screening',
    'kinesis_sessions' => 'Kinesis Sessions',
    'bca' => 'BCA',
    'add_ons' => array(
        'athletic_sensor' => 'Athletic Sensor',
        'addon_organic_acids_test' => 'GPL Organic Acids Test',
        'addtl_kinesis' => 'Additional Kinesis Session',
        'premium_stability_ball' => 'Premium Stability Ball'
    )
);
/** MOVEMENT MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['MMPB'] = array(
    'bca' => 'BCA',
    'health_talk' => 'Movement Workshop', 
    'movement_equipment' => 'Movement Equipment',    
    'add_ons' => array(
        'athletic_sensor' => 'Athletic Sensor',
        'addon_organic_acids_test' => 'GPL Organic Acids Test',
        'addtl_kinesis' => 'Additional Kinesis Session',
        'premium_stability_ball' => 'Premium Stability Ball'
    )
);

/** SLEEP MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['SlMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'sleep_assessment' => 'Sleep Assessment',
    'hormones_comprehensive' => 'Hormones Comprehensive',
    'add_ons' => array(
        'amber_glasses' => 'Amber Colored Eye Glasses',
        'genova_comprehensive' => 'Genova Comprehensive Melatonin Profile',
        'mind_body_medicine_heartmath' => 'Mind Body Medicine or HeartMath Session'
    )
);
/** SLEEP MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['SlMPB'] = array(
    'health_talk' => 'Sleep Hygiene Education', 
    'add_ons' => array(
        'addon_sleep_assessment' => 'Sleep Assessment',
        'hormones_comprehensive' => 'Hormones Comprehensive',
        'amber_glasses' => 'Amber Colored Eye Glasses',
        'genova_comprehensive' => 'Genova Comprehensive Melatonin Profile',
        'mind_body_medicine_heartmath' => 'Mind Body Medicine or HeartMath Session'
    )
);

/** STRESS MANAGEMENT PROGRAM - PREMIUM  */
$config['intervention']['StMPP'] = array(
    'complete_health_profiling' => 'Complete Health Profiling', 
    'follow_up' => 'Follow-up',
    'mind_body_medicine' => 'Mind-Body Medicine',
    'heartmath_session' => 'Heart Math Session',
    'stress_ball' => 'Stress Ball',
    'add_ons' => array(
        'iv_therapy' => 'Myers (Detox)',
        'hbot' => 'Hyperbaric Oxygen Therapy(HBOT)',
        'gpl_blood_test' => 'GPL Heavy Metals Blood Test',
        'heilen_low_laser' => 'Heilen Low Laser Light Therapy'
    )
);
/** STRESS MANAGEMENT PROGRAM - BASIC  */
$config['intervention']['StMPB'] = array(
    'health_talk' => 'Mind-Body Medicine Workshop',
    'stress_ball' => 'Stress Ball',
    'add_ons' => array(
        'heartmath_session_1' => 'Heart Math Session (Individual)',
        'heartmath_session_10' => 'Heart Math Session (Group of 10)',
        'iv_therapy' => 'Myers (Detox)',
        'hbot' => 'Hyperbaric Oxygen Therapy(HBOT)',
        'gpl_blood_test' => 'GPL Heavy Metals Blood Test',
        'heilen_low_laser' => 'Heilen Low Laser Light Therapy'
    )
);