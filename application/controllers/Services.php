<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        $this->load->model('Organizations_model');
        $this->load->model('CompanyQueue_model', 'company_queue');
        $this->load->model('Appointments_model', 'appointments');
        $this->load->model('AdminTokens_Model', 'tokens');
    }
    
    function index() {
        $default_page = "queuing";
        $sub_page = '';
        if($this->input->post('page') != null){
            $default_page = $this->input->post('page');
        }
        if($this->input->post('subpage') != null){
            $sub_page = $this->input->post('subpage');
        }
        $d['default_page'] = $default_page;
        $d['sub_page'] = $sub_page;
        $d['v'] = 'services/index';
        $this->load->view('template', $d);
    }

    function queuing(){
        $default_page = "company";
        if($this->input->post('page') != null){
            $default_page = $this->input->post('page');
        }
        $d['default_page'] = $default_page;
        echo $this->load->view('services/queuing/main', $d, true);
    }
    
    function getCompanyQueue(){
        $d['companies'] = $this->Organizations_model->organizations_list();
        $d['user_label'] = 'Doctors';
        $d['companies_with_assigned'] = $this->company_queue->getAll(array(), true, 'distinct(company_id)');
        echo $this->load->view('services/queuing/company_queue/main', $d, true);
    }
    
    function assignMDform($company_id=0){
        $d['company_id'] = $company_id;
        $d['company_name'] = trim($this->Organizations_model->get_companyname_by_id($company_id));
        $d['medical_practitioners'] = $this->Users_model->getUsersByType(null, true, true);
        $d['data'] = $this->company_queue->getAll(array('company_id' => $company_id));
        echo json_encode(array(
            'html' => $this->load->view('services/queuing/company_queue/assign-form', $d, true),
            'is_edit' => (count($d['data']) > 0)
        ));
    }
    
    function assignMedicalPrac(){
        $post = $this->input->post();
        $userids = array_keys($post['chkAssign']);
        $company_id = $post['cid'];
        //delete old records
        $this->company_queue->delete($company_id);
        
        foreach($userids as $user){
            $data[] = array(
                'company_id' => $company_id,
                'user_id' => $user,
                'is_lead' => (key($post['chkLead']) == $user),
                'date_created' => date(DateTime::ISO8601)
            );
        }

        $this->company_queue->save_batch($data);
    }
    
    function getPatientQueue(){
        $d['data'] = $this->appointments->getActiveAppointments();
        echo $this->load->view('services/queuing/patient_queue/main', $d, true);
    }
    
    function getAppointmentStatus_select(){
        $status = array(
            'For Approval' => 'For Approval',
            'Approved' => 'Approved',
            'Completed' => 'Completed',
            'Re-scheduled' => 'Re-scheduled',
            'Cancelled' => 'Cancelled'
        );
        $html = "<select name='appointment-status' id='appointment-status' style='margin-left: 20px;'>";
        $html .= "<option selected value=''>Filter by Status</option>";
        foreach($status as $stat){
            $html .= "<option value='".$stat."'>".$stat."</option>";
        }
        $html .= "</select>";
        
        echo json_encode(array('status' => $html));
    }
    
    function reassignMDform($id=0){
        $data = $this->appointments->getAppointmentDetails($id);
        $d['data'] = $data;
        $doctors_data = $this->_getMDAvailabilityByDate(0, $data['datetime']);
        foreach($doctors_data as $i=>&$rec){
            $appointments_time = $this->appointments->getAppointmentsPerMDsched($rec['userid'], $rec['start_date'], $rec['start_time'], $rec['end_time']);
            $rec['available_time_slots'] = $this->_availableTimeRange($data['datetime'], $rec['start_time'], $rec['end_time'], $rec['id'], $appointments_time, $data['datetime'], $id);
            $rec['no_of_appointments'] = count($appointments_time);
            $rec['available_slots'] = (int) $rec['no_of_hours'] - count($appointments_time);
        }
        $d['doctors_data'] = $doctors_data;
        $this->load->view('services/queuing/patient_queue/reassignMD-form', $d);
    }
    
    function reschedform($id=0){
        $data = $this->appointments->getAppointmentDetails($id);
        $d['data'] = $data;
        $d['doctors_data'] = $this->_getMDAvailabilityByDate($data['userid'], $data['datetime']);
        $this->load->view('services/queuing/patient_queue/resched-appointment-form', $d);
    }
    
    private function _getMDAvailabilityByDate($md_id = 0, $date = null){
        if($date != null){
            $date = date('Y-m-d', strtotime($date));
        }
        return $this->appointments->getAvailableMDByDate($date, $md_id);
    }
    
    function ajax_getMDAvailableDates($md_id = 0){
        $available_dates = $this->_getMDAvailabilityByDate($md_id);
        $return_arr = [];
        foreach($available_dates as $date){
            $return_arr[] = date('Y-n-j', strtotime($date['start_date']));
        }
        
        echo json_encode($return_arr);
    }
    
    function ajax_getAvailableMDs($appointment_id=0, $date = null){
        $appointment_data = $this->appointments->getRow($appointment_id);
        $initial_appointment_date = $appointment_data['datetime'];
        if($date == null){
            $date = $appointment_data['datetime'];
        }else{
            $date = urldecode($date);
        }
        $data = $this->_getMDAvailabilityByDate($appointment_data['doctor_id'], $date);
        foreach($data as &$rec){
            $appointments_time = $this->appointments->getAppointmentsPerMDsched($rec['userid'], $rec['start_date'], $rec['start_time'], $rec['end_time']);
            $rec['no_of_appointments'] = count($appointments_time);
            $rec['start_time'] = date('h:i A', strtotime($rec['start_time']));
            $rec['end_time'] = date('h:i A', strtotime($rec['end_time']));
            $rec['available_slots'] = (int) $rec['no_of_hours'] - count($appointments_time);
            $rec['available_time_slots'] = $this->_availableTimeRange($date, $rec['start_time'], $rec['end_time'], $rec['id'], $appointments_time, $initial_appointment_date, $appointment_id);
        }
        $results['data'] = $data;
        echo json_encode($results);
    }
    
    private function _availableTimeRange($date, $start, $end, $index, $taken_slots, $initial_appointment_date, $appointment_id){
        $time_array = $this->create_time_range($start, $end);
        $date = date('Y-m-d', strtotime($date));
        $html = "<form id='form-{$index}'><input type='hidden' value='{$appointment_id}' name='x' />";
        $html .= "<input type='hidden' value='{$index}' name='s' />";
        $html .= "<select name='timeslot' id='timeslot-{$index}'>";
        $disabled = '';
        foreach($time_array as $time => $time_range){
            $style = '';
            $disabled = '';
            $is_full = false;
            $timestamp = date('Y-m-d H:i', strtotime("$date $time"));
            foreach($taken_slots as $slot){
                $slot_time = date('Y-m-d H:i', strtotime($slot['datetime']));
                if ($slot_time == $timestamp) {
                    if($slot['id'] == $appointment_id){
                        $style = 'color: blue;';
                        $disabled = 'disabled';
                    }else{
                        $style = 'color: red;';
                        $is_full = true;
                    }
                }
            }
            
            $html .= "<option data-full='{$is_full}' value='".$timestamp."' style='{$style}' {$disabled} >".$time_range ."</option>";
        }
        $html .= "</select>";
        $html .= "</form>";
        return $html;
    }
    
    function create_time_range($start, $end) {
        $interval = '1 hour';
        $format = '12';
        $startTime = strtotime($start);
        $endTime = strtotime($end);
        $returnTimeFormat = ($format == '12') ? 'g:i A' : 'G:i A';

        $current = time();
        $addTime = strtotime('+' . $interval, $current);
        $diff = $addTime - $current;

        $times = array();
        while ($startTime < ($endTime + ($startTime-$endTime)%3600)) {
            $times[date($returnTimeFormat, $startTime)] = date($returnTimeFormat, $startTime) . " - " . date($returnTimeFormat, $startTime+= $diff);
        }
        return $times;
    }
    
    function ajax_reschedAppointment(){
        $appointment_id = $this->input->post('x');
        $new_date = $this->input->post('timeslot');
        $appointment = $this->appointments->getRow($appointment_id);

	$new_date = DateTime::createFromFormat('Y-m-d H:i', $new_date)->format(DateTime::ISO8601);

        $r['result']['error'] = null;
        $r['result']['success'] = 0;

        $data['datetime'] = $new_date;
        if($this->appointments->reschedule($appointment_id, $data)){
            $r['result']['success'] = 1;
            
            //save history
            $this->appointments->saveToHistory('Rescheduled', $appointment);
            $this->_notifyAppointmentEmail($appointment_id, "Rescheduled");
        }else{
            $r['result']['error'] = 'An error was encountered. Please contact your system administrator.';
        }
        
        echo json_encode($r);
    }
    
    function ajax_reassignMD(){
        $appointment_id = $this->input->post('x');
        $schedule_id = $this->input->post('s');
        $new_date = $this->input->post('timeslot');
        $schedule = $this->appointments->getScheduleDetailsById($schedule_id);
        $appointment = $this->appointments->getRow($appointment_id);
        $old_mdid = $appointment['doctor_id'];
        
        $new_date = DateTime::createFromFormat('Y-m-d H:i', $new_date)->format(DateTime::ISO8601);
        
        $r['result']['error'] = null;
        $r['result']['success'] = 0;

        $new_data['doctor_id'] = $schedule['user_id'];
        
        //check if schedule is changed
        if(date('Y-m-d H:i', strtotime($new_date)) != date('Y-m-d H:i', strtotime($appointment['datetime']))){
            $new_data['date_rescheduled'] = date(DateTime::ISO8601);
            $new_data['status'] = Appointments_model::STATUS_RESCHED;
            $new_data['datetime'] = $new_date;
        }

        if($this->appointments->reassignMD($appointment_id, $new_data)){
            $r['result']['success'] = 1;
            
            //save history
            $this->appointments->saveToHistory('Reassigned', $appointment);
            $this->_notifyAppointmentEmail($appointment_id, "Reassign", $old_mdid);
        }else{
            $r['result']['error'] = 'An error was encountered. Please contact your system administrator.';
        }
        
        echo json_encode($r);
    }
    
    function ajax_getAppointmentHistory($appointment_id = 0){
        $appointment = $this->appointments->getRow($appointment_id);
        $history = $this->appointments->getAppointmentHistory($appointment['appointment_code']);
       
        $data['history'] = $history;
        echo $this->load->view('services/queuing/patient_queue/history', $data, true);
    }

    function cancelAppointment($appointment_id=0){
        $r['result']['success'] = 0;
        $appointment = $this->appointments->getRow($appointment_id);
        
        //update appointment status
        if ($this->appointments->cancel($appointment_id)) {
            $r['result']['success'] = 1;

            //save history
            $this->appointments->saveToHistory('Cancelled', $appointment);
            $this->_notifyAppointmentEmail($appointment_id, "Cancelled");
        } else {
            $r['result']['error'] = 'An error was encountered. Please contact your system administrator.';
        }

        echo json_encode($r);
        
    }
    
    private function _notifyAppointmentEmail($appointment_id, $title, $old_mdid = 0){
        $send_notif=true;
        //get appointment details
        $data = $this->appointments->getAppointmentDetails($appointment_id);

        $recipients = [];
        if($title == 'Reassign'){
            $subject = 'Appointment Details Update';
            $details = 'A new doctor was assigned to one of your booked appointment. To view the details, go to ';
        }else{
            $subject = $title.' Appointment';
            $details = 'A booked appointment has been '. strtolower($title).'. To view the appointment details, go to ';
        }

        //notify patient
        $recipients[] = array(
            'type' => 'Patient', 
            'id' => $data['patient_id'], 
            'name' => trim($data['employee_name'])
        );
        $details_px = "<b style='color: #59a5d8'>My Health Activities > Book Appointment > Appointment Request</b>.";
        if($this->cloudhealth->sendNotification($subject, $details.$details_px, $recipients)){
            //send email notification
            $email_data['title'] = $subject;
            $email_data['details'] = $details.$details_px;
            $email_recipients = array($data['patient_email']);
            $email_status = $this->_processEmailSending($email_data, $email_recipients, array());

            if(!$email_status){
                $r['result']['error'][] = $email_status;
            }
        }

        if($title == 'Cancelled'){
            //check if appointment is already approved
            //if so, send notification, else no need to notify MD
            if($data['active'] == 'f'){
                $send_notif=false;
            }
        }
        //reset recipient
        $recipients = [];

        //notify doctor
        $recipients[] = array(
            'type' => 'Doctor', 
            'id' => $data['userid'], 
            'name' => trim($data['md_name'])
        );
        if($title == 'Reassign'){
            $details = 'A new doctor was re-assigned to one of your booked appointment. To view the details, go to ';
        }
        $details_md = "<b style='color: #59a5d8'>My Schedule > Appointment Request.";
        if($send_notif && $this->cloudhealth->sendNotification($subject, $details.$details_md, $recipients)){
            //send email notification
            $email_data['title'] = $subject;
            $email_data['details'] = $details.$details_md;
            $email_recipients = array($data['md_email']);
            $email_status = $this->_processEmailSending($email_data, $email_recipients);

            if(!$email_status){
                $r['result']['error'][] = $email_status;
            }
        }
        
        if($old_mdid > 0){
            //reset recipient
            $recipients = [];

            $old_md_data = $this->Users_model->getUserInfoById($old_mdid);
            
            //notify doctor
            $recipients[] = array(
                'type' => 'Doctor', 
                'id' => $old_mdid, 
                'name' => trim($old_md_data['name'])
            );

            //notify old MD for re-assign MD module
            $subject = 'Cancelled Appointment';
            $details = 'A new doctor was re-assigned to one of your appointment with the following details: <br /><br />';
            $details .= "Appointment date and time: ".date('M d, Y h:iA', strtotime($data['datetime']))."<br />";
            $details .= "Patient Name: ".trim($data['employee_name']);
            if($this->cloudhealth->sendNotification($subject, $details, $recipients)){
                //send email notification
                $email_data['title'] = $subject;
                $email_data['details'] = $details;
                $email_recipients = array(trim($old_md_data['email']));
                $email_status = $this->_processEmailSending($email_data, $email_recipients);

                if(!$email_status){
                    $r['result']['error'][] = $email_status;
                }
            }
        }
    }
    
    private function _processEmailSending($data, $recipient_emails){
        $email_data['datetime'] = date('M j, Y g:i:s A', time());
        $email_data['message'] = $data['details'];
        $email_data['header'] = $data['title'];
        $email_data['is_email'] = true;
        $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
        $email_data['page'] = 'notification';
        $email_data['emails'] = array_unique($recipient_emails);
        $this->cloudhealth->send_notification_email($email_data, $is_update = false, $individual_sending = true);
    }
    
    function marketplace(){
        echo $this->load->view('templates/coming_soon', null, true);
    }
    
    function user_access_management(){
        $default_page = "corporate_users";
        if($this->input->post('page') != null){
            $default_page = $this->input->post('page');
        }
        $is_group_head = (isset($this->session->userdata['logged_in']['is_admin']) && $this->session->userdata['logged_in']['is_admin']);
        if(admin_role() == Users_model::MD && $is_group_head){
            $default_page = 'medical';
        }
        if(admin_role() == Users_model::SALES && $is_group_head){
            $default_page = 'sales';
        }
        if(admin_role() == Users_model::OPS && $is_group_head){
            $default_page = 'ops';
        }
        if(admin_role() == Users_model::FINANCE && $is_group_head){
            $default_page = 'finance';
        }
        $d['default_page'] = $default_page;
        echo $this->load->view('services/user_access_management/main', $d, true);
    }
    
    function promoteToAdmin(){
        $r['result']['success'] = 0;
        $post = $this->input->post();
        //check if user type is Doctor
        $user = $this->Users_model->getUserInfoById($post['id']);
        if($user['type'] == 'Doctor'){
            $r['result']['success'] = 1;
            $access_token = bin2hex(random_bytes(32));
            $user_data = array(
                'is_admin' => true,
                'admin_access_token' => $access_token
            );
            //update users table
            if($this->Users_model->update_userInfo($user_data, $post['id'])){
                //insert user to admin table if not yet exists
                $exists = $this->Users_model->validate_AdminEmail(trim($user['email']));
                $admin_id = $this->Users_model->promoteMD_to_admin($user, $exists);
                //insert new record in md_admin_tokens table
                $token_data = array(
                    'admin_id' => $admin_id,
                    'user_id' => $user['id'],
                    'token' => $access_token
                );
                $this->tokens->add($token_data);
            }
        }else{
            $r['result']['error'] = 'This feature is not yet available';
        }
        echo json_encode($r);
    }
    
    function getCorporateUsers(){
        $data = $this->subscription->getSubscriptionSlots();
        foreach($data as &$rec){
            if($rec['shortcode'] == $this->subscription->getTrialProgramCode() || $rec['shortcode'] == $this->subscription->getDefaultProgramCode()){
                $where = '';
                $subscription_end = ($rec['subscription_end'] ? $rec['subscription_end'] : date('Y-m-d'));
                if ($rec['shortcode'] == 'CHIT') {
                    $where .= " AND created_at::date <= '{$subscription_end}'";
                } else {
                    $where .= " AND created_at::date >= '{$rec['subscription_start']}'";
                }
                $used_slots = $this->Users_model->getEnrolleeCount($where, $rec['company_name']);
                $rec['used_slots'] = (int)$used_slots['count'];
            }else{
                $rec['used_slots'] = $this->subscription->countUsedSlots($rec['id']);
            }
            $rec['available_slots'] = ((int)$rec['allowed_number_of_users'] > 0 ? (int)$rec['allowed_number_of_users'] - (int)$rec['used_slots'] : '-');
        }
        $d['user_label'] = 'Corporate Users';
        $d['data'] = $data;
        echo $this->load->view('services/user_access_management/corporate_users', $d, true);
    }
    
    function updateSlot(){
        $post = $this->input->post();
        $allowed_number_of_users = 0;
        
        if(isset($post['number_of_users']) && (int)$post['number_of_users'] > 0){
            $allowed_number_of_users = $post['number_of_users'];
        }
        
        $this->subscription->companySubscriptionUpdate($post['ce_id'], array('allowed_number_of_users' => $allowed_number_of_users));
    }
    
    function getAllUsers($type = null){
        $data = array();

        if($type != null){
            switch($type){
                case "administrators":
                    $data = $this->Users_model->adminList();
                    break;
                case "hr":
                    $data = $this->Users_model->adminList(Users_model::HR);
                    break;
                
                case "medical":
                    $data = $this->Users_model->getUsersByType(null, $active_only = false, $all_non_patient = true);
                    break;
                case "sales":
                    $data = $this->Users_model->adminList(Users_model::SALES);
                    break;
                case "ops":
                    $data = $this->Users_model->adminList(Users_model::OPS);
                    break;
                case "finance":
                    $data = $this->Users_model->adminList(Users_model::FINANCE);
                    break;
            }
        }
        $results['data'] = $data;
        echo json_encode($results);
    }
    
    function getUsers($type = null){
        if($type != null && ($type == 'medical' || $type == 'administrators' || $type == 'hr')){
            switch($type){
                case "medical":
                    $view = 'mp_users';
                    break;
                default:
                    $view = 'administrators';
            }
            $d['type'] = $type;
            echo $this->load->view('services/user_access_management/'.$view, $d, true);
        }else{
            echo $this->load->view('templates/coming_soon', null, true);
        }
    }
    
    function new_user($type){
        $d['title'] = 'ENROLL A MEDICAL PRACTITIONER';
        $d['is_new'] = true;
        $d['type'] = ucfirst($type);
        echo $this->load->view('services/user_access_management/add_mpuser', $d, true);
    }
    
    function edit_user($id){
        $d['title'] = 'UPDATE DETAILS';
        $d['is_new'] = FALSE;
        $d['user'] = $this->Users_model->getUserInfoById($id);
        $gender = $this->Users_model->getPersonalInfoFromAnswers($id, 2, 'value');
        $d['gender'] = $gender;
        echo $this->load->view('services/user_access_management/add_mpuser', $d, true);
    }
    
    function enrollUser(){
        $post = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('company', 'Company', 'required');
        
        if($this->input->post('company') == 'Others'){
           $this->form_validation->set_rules('company_name', 'Company Name', 'required'); 
        }
        $this->form_validation->set_message('is_unique', 'Email Address already exists.');
        
        $r['result']['success'] = 0; // failed default status
        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $password = $this->cloudhealth->random_password();
            
            $d['email'] = trim($post['email']);
            $d['password'] = $password['crypt'];
            $d['name'] = trim($post['name']);
            $d['type'] = $post['type'];
            $d['company'] = $post['company'];
            if($post['company'] == 'Others'){
                $d['company'] = $post['company_name'];
            }

            if($userid = $this->Users_model->add_mpuser($d)){
                $gender = $post['gender'];
                $update=false;
                $this->Users_model->saveGender($gender, $userid, $update);
                
                //send email
                $email_data['page'] = 'welcome_medical_practitioner';
                $email_data['password'] = $password['plain'];
                $email_data['email'] = $d['email'];

                $this->cloudhealth->send_welcome_email($email_data);
            }
            $r['result']['success'] = 1;
        }

        echo json_encode($r);
    }
    
    function updateUser(){
        $data_string = _get_query_args();

        $this->form_validation->set_rules('dob', 'Date of Birth', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail['.$this->input->post('x').']');
        $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');

        $this->form_validation->set_rules('company', 'Company', 'required');
        
        if($this->input->post('company') == 'Others'){
           $this->form_validation->set_rules('company_name', 'Company Name', 'required'); 
        }
        
        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $id = $data_string['x'];
            $dob = $data_string['dob'];
            $gender = $data_string['gender'];
            
            //  check if email was changed; if so, send a welcome email
            $user_email = $this->Users_model->getUserInfoById($id, 'email');
            $send_email = false;
            if(trim($data_string['email']) != trim($user_email)){
                $send_email = true;
                //reset password
                $new_password = $this->cloudhealth->random_password();
                $data_string['password'] = $new_password['crypt'];
            }
            
            if($data_string['company'] == 'Others'){
                $data_string['company'] = $data_string['company_name'];
            }
            unset($data_string['x']);
            unset($data_string['dob']);
            unset($data_string['gender']);
            unset($data_string['company_name']);
            
            if($this->Users_model->update_userInfo($data_string, $id)){
                $this->Users_model->saveDOB($dob, $id);
                $this->Users_model->saveGender($gender, $id);
                
                if($send_email){
                    $email_data['page'] = 'welcome_user';
                    $email_data['password'] = $new_password['plain'];
                    $email_data['email'] = $data_string['email'];

                    $this->cloudhealth->send_welcome_email($email_data);
                }
            }

            $r['result']['success'] = 1;
        }

        echo json_encode($r);
    }
    
    function delete_user($id=0){
        $this->Users_model->update_access($id);
    }
    
    function delete_admin($id=0){
        $this->Users_model->revoke_admin($id);
        //check if user is MD
        //if MD, remove is_admin tag in users table and
        //delete access token record
        $data['id'] = $id;
        $admin = $this->Users_model->get_userdetails($data);
        if($admin['role'] != Users_model::HR){
            //update users table
            $token = $this->tokens->get($id, 'user_id');
            $d['is_admin'] = false;
            if($this->Users_model->update_userInfo($d, $token['user_id'])){
                $this->tokens->delete($id);
            }
        }
    }
    
    function uniqueEmail($email, $userid){
        if($this->Users_model->validate_email($email, $userid)){
            return false;
        }
        return true;
    }
}
