<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        $this->load->model('Organizations_model');
    }
    
    function index(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $data['data']['company_name'] = $this->Organizations_model->get_companyname_by_id($company_id);
        $data['v'] = 'account/index';
        $default_page = "access_management";
        if($this->input->post('page') != null){
            $default_page = $this->input->post('page');
        }
        $data['default_page'] = $default_page;
        $this->load->view('template', $data);
    }
    
    function users(){
        $view_only = false;
        if(admin_role() == Users_model::ADMIN){
            $company_id = $this->uri->segment(3);
            $view_only = (bool)$this->uri->segment(4);
        }else{
            $company_id = (int)$this->session->userdata['logged_in']['company_id'];
        }
        $users = $this->Users_model->patientList($company_id);

        $data['users'] = $users;
        $data['view_only'] = $view_only;
        echo $this->load->view('account/access_management/list', $data, true);
    }
    
    function revoke() {
        $userid = (int) $this->input->post('userid');
        if($this->Users_model->update_access($userid) > 0){
            echo true;
        }
        echo false;
    }
       
    function update($userid=0){
        $data = $this->Users_model->getUserInfoById($userid);
        $dob = $this->Users_model->getPersonalInfoFromAnswers($userid, 1, 'CAST(value AS date)');
        $gender = $this->Users_model->getPersonalInfoFromAnswers($userid, 2, 'value');
        $data['dob'] = ($dob != null ? date("d F, Y", strtotime($dob)) : '');
        $data['gender'] = $gender;

        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company = $this->Organizations_model->get_company_details_by_id($company_id);
        $branchList = $this->Organizations_model->getBranches($company_id);
        $departmentList = $this->Organizations_model->getDepartments($company_id);
        $jobgradeList = $this->Organizations_model->get_company_jobgrades($company['company_name']);
        
        $data['branchList'] = formatSelect($branchList, "br_name");
        $data['departmentList'] = formatSelect($departmentList, "dept_name");
        $data['job_gradeList'] = formatSelect($jobgradeList, "job_grade");
        $data['v'] = 'account/access_management/update';
        return $this->load->view('template', $data);
    }
    
    function save(){
        $data_string = _get_query_args();

        $this->form_validation->set_rules('employee_id', 'Employee ID', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail['.$this->input->post('id').']');
        $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $id = $data_string['id'];
            $dob = $data_string['dob'];
            $gender = $data_string['gender'];
            
            //  check if email was changed; if so, send a welcome email
            $user_email = $this->Users_model->getUserInfoById($id, 'email');
            $send_email = false;
            if(trim($data_string['email']) != trim($user_email)){
                $send_email = true;
                //reset password
                $new_password = $this->cloudhealth->random_password();
                $data_string['password'] = $new_password['crypt'];
            }
            
            unset($data_string['id']);
            unset($data_string['dob']);
            unset($data_string['gender']);
            
            if($this->Users_model->update_userInfo($data_string, $id)){
                $this->Users_model->saveDOB($dob, $id);
                $this->Users_model->saveGender($gender, $id);
                
                if($send_email){
                    $email_data['page'] = 'welcome_user';
                    $email_data['password'] = $new_password['plain'];
                    $email_data['email'] = $data_string['email'];

                    $this->cloudhealth->send_welcome_email($email_data);
                }
            }

            $r['result']['success'] = 1;
        }

        echo json_encode($r);
    }
    
    function uniqueEmail($email, $userid){
        if($this->Users_model->validate_email($email, $userid)){
            return false;
        }
        return true;
    }
    
    function subscriptions_and_renewals(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company = $this->Organizations_model->get_company_details_by_id($company_id);
        
        //  check notification
        $with_report = $this->reports->getReportByCompanyId($company_id, Reports_model::$REPORT_SENT);
        $d['with_report'] = ($with_report ? true : false);
        
        $enrolled_program_codes = $this->subscription->_getEnrolledProgramCodes($company_id, $active = true);
        
        $allprograms = $this->subscription->getAllPrograms(null, true);

        foreach($allprograms as &$program){
            $program['enrolled'] = false;
            $program['company_subscription_id'] = 0;
            if($company_subscription_id = array_search($program['shortcode'], $enrolled_program_codes)){
                $program['enrolled'] = true;
                $program['company_subscription_id'] = $company_subscription_id;
            }
        }
        $d['all_programs'] = $allprograms;
        $d['jobgrades'] = $this->Organizations_model->get_company_jobgrades(trim($company['company_name']));
        $d['js'][] = $this->cloudhealth->load_script('assets/js/account.js');
        
        $this->load->view('account/subscriptions_and_renewals/main', $d);
    }
    
    function getActiveSubscriptions(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $subscriptions = $this->subscription->getSubscriptionsByCompanyId($company_id);
        
        $d['subscriptions'] = $subscriptions;
        echo $this->load->view('account/subscriptions_and_renewals/active_subscriptions', $d, true);
    }
    
    function cancelSubscription($id){
        $update_data['status'] = 'Cancelled';
        $this->subscription->companySubscriptionUpdate($id, $update_data);
    }
    
    function validateSubscriptionCompletion($id){
        //check if subscription is CHI run
        //if CHI run, validate if already with report prior to completion
        $subscription = $this->subscription->getCompanySubscriptionById('program_code', $id);
        if($subscription['program_code'] === $this->subscription->getDefaultProgramCode()){
            $this->load->model('Reports_model', 'report');
            echo $this->report->checkIfReportSent($id);
        }else{
            echo true;
        }
    }
    
    function completeSubscription($id){
        $update_data['status'] = 'Completed';
        $this->subscription->companySubscriptionUpdate($id, $update_data);
    }
    
    function renewSubscription(){
        $data = $this->input->post();
        $update_data['status'] = 'For Renewal';
        $company_subscription = $this->subscription->getCompanySubscriptionById('program_code', $data['id']);
        if($this->subscription->companySubscriptionUpdate($data['id'], $update_data)){
            //send notification
            $receiver = $this->Users_model->getSuperAdminId();
            $subscription = $this->subscription->getProgramInfoByCode('subscription_name', $company_subscription['program_code']);
            $recipient[] = array('type' => 'admin', 'id' => $receiver['id'], 'name' => $receiver['name']);
            $subject = 'Program Renewal: '. $subscription['subscription_name'];
            $this->cloudhealth->sendNotification($subject, $data['notes'], $recipient);
        }
    }
    
    private function _getDOHbyProgramcode($program_code){
        switch($program_code){
            case 'DMPB':
                return 'detox';
            case 'SlMPB':
                return 'sleep';
            case 'StMPB':
                return 'stress';
            case 'NMPB':
                return 'nutrition';
            case 'MMPB':
                return 'exercise';
            case 'WMPB':
                return 'weightscore';
        }
    }
    
    function showEmployeeList(){
        $this->load->model('Dashboard_model');
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company = $this->Organizations_model->get_company_details_by_id($company_id);

        $health_status_only = true;
        $filters = $this->input->post();
        if($filters != null){
            $filter = array();
            $filter['doh'] = $this->_getDOHbyProgramcode($filters['program_code']);
            foreach($filters as $col => $value){
                if($col == 'risk_status'){
                    $filter['high_risk'] = $value;
                }
                if($col == 'health_status'){
                    $filter['default'] = $value;
                }
                if($col == 'job_grade'){
                    $filter['job_grade'] = $value;
                }
                if($col == 'doh' && $value != ''){
                    $filter['doh'] = $value;
                }
            }
        }
        $employee_list = $this->Dashboard_model->get_users_health_score(trim($company['company_name']), $filter, $health_status_only, $filters['program_code']);
        $is_enrolled = '';
        $enrolled_employee_list = array();
        
        $status = array('enrollment_status !=' => 'Declined');
        $enrolled_employees = $this->subscription->getEmployeesPerProgram($filters['company_subscription_id'], $status);
        foreach($enrolled_employees as $employee){
            $enrolled_employee_list[] = $employee['user_id'];
        }

        $is_enrolled_to_other_package = '';
        $enrolled_employees_to_other_package_list = array();
        $package = $this->subscription->getCompanySubscriptionById('program_code', $filters['company_subscription_id']);
        //check if basic or premium
        $other_package_type = (substr($package['program_code'], -1, 1) == 'P' ? 'B' : 'P');
        $other_package_code = substr($package['program_code'], 0, (strlen($package['program_code'])-1)).$other_package_type;
        $subscription = $this->subscription->getInfoByProgramcode('id', $other_package_code, $company_id);
        $enrolled_employees_to_other_package = $this->subscription->getEmployeesPerProgram($subscription['id'], $status);
        foreach($enrolled_employees_to_other_package as $employee){
            $enrolled_employees_to_other_package_list[] = $employee['user_id'];
        }

        foreach($employee_list as $key => &$employee){
            $healthscore = $filter['doh'] != null ? $employee[$filter['doh']] : $employee['healthscore'];
            $employee_list[$key]['health_status'] = $this->cloudhealth->getHealthStatusByScore((float) $healthscore);
            if(count($enrolled_employee_list) > 0){
                $is_enrolled = (in_array($employee['user_id'], $enrolled_employee_list) ? 'disabled' : '');
            }
            if(count($enrolled_employees_to_other_package_list) > 0){
                $is_enrolled_to_other_package = (in_array($employee['user_id'], $enrolled_employees_to_other_package_list) ? 'disabled' : '');
            }
            $employee_list[$key]['enrolled'] = $is_enrolled;
            $employee_list[$key]['enrolled_other_package'] = $is_enrolled_to_other_package;
        }
        $d['program_code'] = $filters['program_code'];
        $d['employees'] = $employee_list;
        $d['doh'] = $filter['doh'];
        $d['v'] = 'account/subscriptions_and_renewals/employee_list';
        $this->load->view('template', $d);
    }
    
    function enroll_to_package(){
        $this->load->model('Dashboard_model');
        $userids = $this->input->post('check');
        $program_code = $this->input->post('program_code');
        $company_id = $this->session->userdata['logged_in']['company_id'];
        
        $company_subscription = $this->subscription->getInfoByProgramcode('id, program_code, allowed_number_of_users', $program_code, $company_id);
        $r['result']['success'] = 0; // failed default status
        $notification_recipients = [];
        $notification_email_recipients = [];
        $data = [];
        //check allowed number of users vs enrollees
        $allowed_number_of_users = $company_subscription['allowed_number_of_users'];
        $used_slots = $this->subscription->countUsedSlots($company_subscription['id']);
        if($allowed_number_of_users > 0 && count($userids) > ($allowed_number_of_users - $used_slots)){
            $r['result']['errors'] = "You have reached the maximum allowed number of enrollees.";
        }else{
            foreach($userids as $userid => $on){
                $userinfo = $this->Users_model->getUserInfoById($userid);
                $health_score = $this->Dashboard_model->get_users_health_score_by_userid($userid);
                $data[] = array(
                    'company_enrollment_id' => $company_subscription['id'],
                    'user_id' => $userid,
                    'employee_id' => $userinfo['employee_id'],
                    'is_anonymous' => $userinfo['status'] == Users_model::$ANONYMOUS || false,
                    'health_status' => $this->cloudhealth->getHealthStatusByScore((float) $health_score),
                    'date_enrolled' => date(DateTime::ISO8601)
                );
                $notification_email_recipients[] = trim($userinfo['email']);
                $notification_recipients[] = array('type' => 'Patient', 'id' => $userid, 'name' => trim($userinfo['name']));
            }
            $this->subscription->enrollEmployeeToSubscription($data);
        }
        if(!empty($notification_recipients)){
            $r['result']['success']++;
            
            //send notification
            $subscription = $this->subscription->getProgramInfoByCode('subscription_name', $company_subscription['program_code']);
            $subject = 'Program Enrollment: ' . $subscription['subscription_name'];
            $details = "You have been enrolled to " . $subscription['subscription_name'];
            $details .= "<br />";
            $details .= "<br />Go to <span style='color: #59a5d8; font-weight: 500;'>My Health Activites > My Health Programs</span> to accept/decline enrollment.";
            
            if($this->cloudhealth->sendNotification($subject, $details, $notification_recipients)){
                $email_data['details'] = $details;
                $email_data['title'] = $subject;
                $email_data['emails'] = $notification_email_recipients;
                $this->_processEmailSending($email_data);
            }
        }
        echo json_encode($r);
    }
    
    private function _processEmailSending($data = array()){
        $email_data['datetime'] = date('M j, Y g:i:s A', time());
        $email_data['message'] = $data['details'];
        $email_data['header'] = $data['title'];
        $email_data['is_email'] = true;
        $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
        $email_data['page'] = 'notification';
        $email_data['emails'] = array_unique($data['emails']);
        $this->cloudhealth->send_notification_email($email_data, $is_update = false, $individual_sending = true);
    }
    
    function survey_unlock(){
        $userids = $this->input->post('userids');
        $data = array('healthsurvey_status' => 'Active');

        if(!empty($userids)){
            echo $this->Users_model->updateStatus($data, $userids);
        }
    }
    
    function approvals(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $d['summary'] = $this->subscription->getEnrolledCountPerProgram($company_id);
        $d['approved'] = $this->subscription->getEnrollmentPerStatus($company_id, 'Approved');
        $d['declined'] = $this->subscription->getEnrollmentPerStatus($company_id, 'Declined');
        $d['pending'] = $this->subscription->getEnrollmentPerStatus($company_id, 'Pending');
        
        $d['v'] = 'account/approvals/main';
        $this->load->view('template', $d);
    }
    
    function showSubscriptionDetails($shortcode=null){
        $package = $this->subscription->getProgramInfoByCode('subscription_name, inclusions', $shortcode);
        
        $html = "No details defined.";
        if($package){
            $inclusions = json_decode($package['inclusions'], true);
            if($shortcode == 'TeleMed'){
                $html = $inclusions['hr_details'];
            }else{
                $html = "<b>".$package['subscription_name'] ."</b> includes the following: <br /><br />";
                foreach($inclusions['description'] as $key=>$data){
                    if($data['name'] != ""){
                        $html .= "<b>".$data['name'] ."</b> <br/>";
                    }
                    if($data['list'] && !empty($data['list'])){
                        $html .= "<ul style='margin-top: 0;'>";
                        foreach($data['list'] as $list){
                            $html .= "<li>".$list['description']."</li>";
                        }
                        $html .= "</ul>";
                    }
                }
            }
        }
        echo $html;
    }
    
    function showSubscriptionEnrollees($company_enrollment_id=0, $no_event_yet=null){
        $is_CHI = false;
        
        //check if subscription is CHI run or CHI Trial
        $subscription = $this->subscription->getCompanySubscriptionById('company_id, program_code, subscription_end', $company_enrollment_id);
        if ($subscription['program_code'] == 'CHIT' || $subscription['program_code'] == 'CHIR') {
            $is_CHI = true;
            $where = '';
            $subscription_end = ($subscription['subscription_end'] ? $subscription['subscription_end'] : date('Y-m-d'));
            if ($subscription['program_code'] == 'CHIT') {
                $where .= " AND created_at::date <= '{$subscription_end}'";
            } else {
                //check if with completed CHIT
                $completedCHIT = $this->subscription->getInfoByProgramcode('subscription_end', 'CHIT', $subscription['company_id'], array('status' => 'Completed'));
                if (count($completedCHIT) > 0) {
                    $CHITsubscription_end = ($completedCHIT['subscription_end'] ? $completedCHIT['subscription_end'] : date('Y-m-d'));
                    $where .= " AND created_at::date > '{$CHITsubscription_end}'";
                } else {
                    $where .= " AND created_at::date <= '{$subscription_end}'";
                }
            }
            $d['enrolled_employees'] = $this->Users_model->getEnrolleeList($where, $subscription['company_id']);
        } else {
            $no_event_yet = ($no_event_yet == '1' ? true : false);
            $status = array('enrollment_status' => 'Approved');
            $d['enrolled_employees'] = $this->subscription->getEnrolledPerCompanyPackage($company_enrollment_id, $no_event_yet, $subscription['program_code'], $status);
        }
        
        $d['is_CHI'] = $is_CHI;
        $d['subscription'] = $this->subscription->getEnrolledProgramById($company_enrollment_id);
        echo $this->load->view('account/subscriptions_and_renewals/enrolled_employee_list', $d, true);
    }

    
}
