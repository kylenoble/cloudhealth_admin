<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        $this->load->model('Organizations_model');

    }
    /**
     * add()
     * Add organization form
     */
    public function add($company_id = 0) {
        $d['company'] = $this->Organizations_model->get_company_details_by_id($company_id);
        $d['company_id'] = $company_id;
        $d['roles'] = formatSelect($this->Users_model->getRoles(), null, true);
        $d['v'] = 'users/users_add';
        $this->load->view('template', $d);
    }

    /**
     * add_org_save()
     * Save organization form data
     * @param form data in array
     */
    public function add_user_save() {
        $data_string = _get_query_args();
       
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail');
        $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $new_user = $this->Users_model->add_user($data_string);
            if($new_user){
                $email_data['page'] = 'welcome_admin';
                $email_data['password'] = $new_user['password'];
                $email_data['email'] = $data_string['email'];

                $sent = $this->cloudhealth->send_welcome_email($email_data);
                if (!$sent) {
                    // mail not sent
                    $this->session->set_flashdata('email_sending_error', 'Sending of email failed. Please contact your system adminstrator.');
                }
            }
            $r['result']['success'] = 1;
        }
        echo json_encode($r);
    }
    
    /**
     * org_update()
     * Update organization form data
     * @param form data in array
     */
    public function user_update() {
        $data_string = _get_query_args();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail['.$this->input->post('id').']');
        $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $userid = $data_string['id'];
            //  check if email address was modified
            $send_email = false;
            $user_data_old = $this->Users_model->get_adminDetails(array('id' => $userid), 'email');
            if(strtolower(trim($user_data_old['email'])) != strtolower(trim($data_string['email']))){
                $send_email = true;
            }
            $updated_user = $this->Users_model->update_user($data_string, $userid, $send_email);
            if ($updated_user) {
                if($send_email){
                    //  send welcome email
                    $email_data['page'] = 'welcome_admin';
                    $email_data['password'] = $updated_user['new_password'];
                    $email_data['email'] = $data_string['email'];

                    $sent = $this->cloudhealth->send_welcome_email($email_data);
                    if (!$sent) {
                        // mail not sent
                        $this->session->set_flashdata('email_sending_error', 'Sending of email failed. Please contact your system adminstrator.');
                    }
                }
                $r['result']['success'] = 1;
            }
        }
        echo json_encode($r);
    }

    /**
     * update()
     * Edit organization form
     */
    public function update() {
        $id = $this->uri->segment('3');
        $data_string = array(
            'id' => $id
        );
        $user = $this->Users_model->get_userdetails($data_string);

        $d['v'] = 'users/users_update';
        $d['result'] = $user;
        $d['id'] = $id;
        $d['company'] = $this->Organizations_model->get_company_details_by_id($user['company_id']);
        $d['roles'] = formatSelect($this->Users_model->getRoles(), null, true);
        $this->load->view('template', $d);
    }

    /**
     * remove()
     * Remove organization and its logo
     */
    public function remove() {
        $id = $this->uri->segment('3');

        if ($id != '') {
            $data_string = array(
                'id' => $id
            );
            $this->Users_model->delete_user($data_string);
            $r['success'] = 1;
            echo json_encode($r);
        } else {
            $r['error'] = 1;
            echo json_encode($r);
        }
    }
    
    function uniqueEmail($email, $userid){
        if($this->Users_model->validate_AdminEmail($email, $userid)){
            return false;
        }
        return true;
    }

}
