<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Healthprofile extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        // Load url helper
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('email'); // Note: no $config param needed

        $this->load->model('Organizations_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index($page=null) {
        $d['v'] = 'healthprofile/index';
        $d['js'][] = $this->cloudhealth->load_script('assets/js/healthprofile.js');
        
        $default_page = "company_registration";
        if($page == null){
            $default_page = $this->input->post('page');
        }else{
            $default_page = $page;
        }
        $d['default_page'] = $default_page;
            
        $this->load->view('template', $d);
    }
    
    public function company_registration(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $data['company_id'] = $company_id;
        echo $this->load->view('healthprofile/company_registration', $data, true);
    }
    
    public function employee_health_activities(){
        $data['v'] = 'healthprofile/employee_health_activities/main';
        echo $this->load->view('template', $data, true);
    }
    
    public function active_subscriptions(){
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $data['company_id'] = $company_id;
        $data['active_subscriptions'] = $this->subscription->getEnrolledCountPerProgram($company_id);
        echo $this->load->view('healthprofile/employee_health_activities/active_subscriptions', $data, true);
    }

    public function upload_employees() {
        $r = array();
        $r['result']['errors'] = null;
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        
        $this->load->model('DataConsent_model', 'data_consent');
        
        if(!empty($_FILES)){
            if (0 < $_FILES['file']['error']) {
                $r['result']['errors'] = 'Error: ' . $_FILES['file']['error'] . '<br>';
                return;
            } else {
                $employee_upload = UPLOAD_DIR_USER . "" . $_FILES['file']['name'];
                // $employee_upload = UPLOAD_DIR_USER."sample_format_employees.csv";
                if (move_uploaded_file($_FILES['file']['tmp_name'], $employee_upload)) {

                    $ext = pathinfo($employee_upload, PATHINFO_EXTENSION);
                    $data_string = array();

                    if ($ext == 'csv' || $ext == 'txt') {

                        $file = file($employee_upload);

                        foreach ($file as $k) {
                            $csv[] = explode(',', $k);
                        }

                        if($this->input->post('is_update') == 'true'){
                            $this->_updateUsers($csv);
                        }else{
                            //check allowed number of users vs enrollees
                            $company_subscription = $this->subscription->getSubscriptionsByCompanyId($company_id);
                            $with_CHI_trial = false;
                            $with_CHI_Run = false;
                            $allowed_number_of_users = null;
                            if(in_array('CHIR', array_column($company_subscription, 'program_code'))){
                                $pos = array_search('CHIR', array_column($company_subscription, 'program_code'));
                                $allowed_number_of_users = $company_subscription[$pos]['allowed_number_of_users'];
                            }
                            $used_slots = $this->Users_model->getUserCountPerType('Patient', trim($company_name));
                            if($allowed_number_of_users > 0 && count($csv) > ($allowed_number_of_users - $used_slots)){
                                $r['result']['errors'] = "Unable to process. Available slot is not sufficient.";
                            }else{
                                $k = 0;
                                $validate_email = true;
                                $html = '';
                                foreach ($csv as $key=>$val) {
                                    if($key >= 1){
                                        $row = $key + 1;
                                        $email_address = trim(str_replace(" ", "", $val['0']));
                                        //check if user consented YES
                                        if($this->data_consent->userConsentedYES($email_address)){
                                            $validate_email = $this->validate_email($email_address);

                                            if ($validate_email && (count($val) >= 5)) {

                                                //validate date of birth value
                                                if($this->validateDate(str_replace("'", "", trim($val['2'])), 'n/j/Y') || $this->validateDate(trim($val['2']), 'm/d/Y') || $this->validateDate(trim($val['2']), 'm/d/y')){
                                                    //validate branch value
                                                    $branch = trim($val['4'], "\"\"");
                                                    if(($branch != "" && $this->Organizations_model->branch_exists($branch, $company_id)) || $branch == ""){
                                                        //validate department value
                                                        $department = trim($val['5'], "\"\"");
                                                        if(($department != "" && $this->Organizations_model->department_exists($department, $company_id)) || $department == ""){
                                                            //check if risk status has value
                                                            if(in_array(trim($val['8']), array('0', '1', '2'))){

                                                                $password = $this->cloudhealth->random_password();

                                                                //email,name,dob,gender,branch,department,employee_id
                                                                $data_string = array(
                                                                    'email' => $email_address,
                                                                    'company' => $company_name,
                                                                    'name' => $val['1'],
                                                                    'dob' => $val['2'],
                                                                    'gender' => $val['3'],
                                                                    'branch' => $branch,
                                                                    'department' => $department,
                                                                    'employee_id' => $val['6'],
                                                                    'password' => $password['crypt'],
                                                                    'job_grade' => $val['7'],
                                                                    'high_risk' => $val['8']
                                                                );

                                                                $user = $this->Users_model->add_employee($data_string);

                                                                if ($user) {
                                                                    $email_data['page'] = 'welcome_user';
                                                                    $email_data['password'] = $password['plain'];
                                                                    $email_data['email'] = $email_address;

                                                                    $this->cloudhealth->send_welcome_email($email_data);
                                                                }
                                                            }else{
                                                                $r['result']['errors'] = "Row ".$row .": Risk Status <b>" . $val['8']. "</b> is invalid.";
                                                            }
                                                        }else{
                                                            $r['result']['errors'] = "Row ".$row .": Department <b>" . $val['5']. "</b> does not exist in the database.";
                                                        }
                                                    }else{
                                                        $r['result']['errors'] = "Row ".$row .": Branch <b>" . $val['4']. "</b> does not exist in the database.";
                                                    }
                                                }else{
                                                    $r['result']['errors'] = "Row ".$row .": Date of Birth <b>" . $val['2']. "</b> is invalid. Please verify correct format (ie: month/day/year (ex: 12/31/1981)).";
                                                }
                                            } else {
                                                $r['result']['errors'] = "Row ".$row .": Email address <b>". $email_address. "</b> already exists!";
                                            }
                                        }else{
                                            $r['result']['errors'] = "Row ".$row .": User <b>". $val['1']. "</b> did not consent to enrollment.";
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $r['result']['errors'] = "Upload failed.Try again later.";
                    }
                } else {
                    $r['result']['errors'] = "Upload failed.Try again later.";
                }
            }

            echo json_encode($r);
            exit();
        }
    }
    
    public function upload_employeesFile() {
        $r = array();
        $data = array();
        $r['result']['errors'] = null;
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        
        $this->load->model('DataConsent_model', 'data_consent');
        
        if(!empty($_FILES)){
            if (0 < $_FILES['file']['error']) {
                $r['result']['errors'] = 'Error: ' . $_FILES['file']['error'] . '<br>';
                return;
            } else {
                $employee_upload = UPLOAD_DIR_USER . "" . $_FILES['file']['name'];
                // $employee_upload = UPLOAD_DIR_USER."sample_format_employees.csv";
                if (move_uploaded_file($_FILES['file']['tmp_name'], $employee_upload)) {

                    $ext = pathinfo($employee_upload, PATHINFO_EXTENSION);
                    $data_string = array();

                    if ($ext == 'csv' || $ext == 'txt') {

                        ini_set('auto_detect_line_endings',TRUE);
                        $handle = fopen($employee_upload, 'r');
                        while ($data = fgetcsv($handle)) {
                            $csv[] = $data;
                        }
                        fclose($handle);
                        ini_set('auto_detect_line_endings',FALSE);
//                        var_dump($csv);die;
//                        $file = file($employee_upload);

//                        foreach ($file as $k) {
//                            $csv[] = explode(',', $k);
//                        }

                        //check if file has values
                        if(count($csv) > 1){
                            if($this->input->post('is_update') == 'true'){
                                if($this->_updateUsers($csv) === false){
                                    $r['result']['errors'] = "Upload failed. Please check your file format.";
                                }
                            }else{
                                //check allowed number of users vs enrollees
                                $company_subscription = $this->subscription->checkOngoingCHISubscription($company_id);
                                if($company_subscription){
                                    $subscription_duration['start'] = $company_subscription['subscription_start'];
                                    $subscription_duration['end'] = $company_subscription['subscription_end'];
                                    $allowed_number_of_users = (int)$company_subscription['allowed_number_of_users'];
                                    //check used slots
                                    $used_slots = $this->Users_model->getUserCountPerType('Patient', trim($company_name), $subscription_duration);
                                    $remaining_slot = ($allowed_number_of_users - $used_slots);
                                    if($allowed_number_of_users > 0 && count($csv)-1 > $remaining_slot){
                                        $r['result']['errors'] = "The number of employees for enrollment exceeds the limit previously set. <br />Please contact the manager in-charge of your account. <br />Total allowable: ".$allowed_number_of_users."<br />Total remaining: ".$remaining_slot;
                                    }else{
                                        $k = 0;
                                        $validate_email = true;
                                        foreach ($csv as $key=>$val) {
                                            if($key >= 1){
                                                $row = $key + 1;
                                                $email_address = trim(str_replace(" ", "", $val['0']));
                                                //check if user consented YES
                                                if($this->data_consent->userConsentedYES($email_address)){
                                                    $validate_email = $this->validate_email($email_address);

                                                    if ($validate_email && (count($val) >= 5)) {

                                                        //validate date of birth value
                                                        if($this->validateDate(str_replace("'", "", trim($val['2'])), 'n/j/Y') || $this->validateDate(trim($val['2']), 'm/d/Y') || $this->validateDate(trim($val['2']), 'm/d/y')){
                                                            //validate gender values
                                                            if(preg_grep("/^{$val['3']}$/i", array('Male', 'Female', 'Trans', ''))){
                                                                //validate branch value
                                                                $branch = trim($val['4'], "\"\"");
                                                                if(($branch != "" && $branch_db = $this->Organizations_model->branch_exists($branch, $company_id)) || $branch == ""){
                                                                    //validate department value
                                                                    $department = trim($val['5'], "\"\"");
                                                                    if(($department != "" && $department_db = $this->Organizations_model->department_exists($department, $company_id)) || $department == ""){
                                                                        //check if risk status has value
                                                                        if(in_array(trim($val['8']), array('0', '1', '2'))){

                                                                            //email,name,dob,gender,branch,department,employee_id
                                                                            $data_string = array(
                                                                                'email' => $email_address,
                                                                                'company' => $company_name,
                                                                                'name' => $val['1'],
                                                                                'dob' => $val['2'],
                                                                                'gender' => ucfirst($val['3']),
                                                                                'branch' => $branch_db['branch_name'],
                                                                                'department' => $department_db['dept_name'],
                                                                                'employee_id' => $val['6'],
                                                                                'job_grade' => $val['7'],
                                                                                'high_risk' => trim(str_replace(" ", "", $val['8']))
                                                                            );
                                                                            $data_string['row'] = $row;
                                                                            $data[] = $data_string;
                                                                        }else{
                                                                            $r['result']['errors'] = "Row ".$row .": Risk Status <b>" . $val['8']. "</b> is invalid.";
                                                                        }
                                                                    }else{
                                                                        $r['result']['errors'] = "Row ".$row .": Department <b>" . $val['5']. "</b> does not exist in the database.";
                                                                    }
                                                                }else{
                                                                    $r['result']['errors'] = "Row ".$row .": Branch <b>" . $val['4']. "</b> does not exist in the database.";
                                                                }
                                                            }else{
                                                                $r['result']['errors'] = "Row ".$row .": Gender <b>" . $val['3']. "</b> is invalid.";
                                                            }
                                                        }else{
                                                            $r['result']['errors'] = "Row ".$row .": Date of Birth <b>" . $val['2']. "</b> is invalid. Please verify correct format (ie: month/day/year (ex: 12/31/1981)).";
                                                        }
                                                    } else {
                                                        $r['result']['errors'] = "Row ".$row .": Email address <b>". $email_address. "</b> already exists!";
                                                    }
                                                }else{
                                                    $r['result']['errors'] = "Row ".$row .": User <b>". $val['1']. "</b> did not consent to enrollment.";
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    $r['result']['errors'] = "Your account has no active subscription.<br />Please contact the manager in-charge of your account.";
                                }
                            }
                        }else{
                            $r['result']['errors'] = "File is empty. Please upload the correct file.";
                        }
                    } else {
                        $r['result']['errors'] = "Upload failed. Try again later.";
                    }
                } else {
                    $r['result']['errors'] = "Upload failed. Try again later.";
                }
            }

            $r['result']['data'] = $data;
            
            echo json_encode($r);
            exit();
        }
    }
    
    function employee_enrollment(){
        $data_string = $this->input->post();
        $password = $this->cloudhealth->random_password();
        $data_string['password'] = $password['crypt'];
        $user = $this->Users_model->add_employee($data_string);
        
        $sent = false;
        $color = 'red';
        $status = 'FAILED';

        if ($user) {
            $email_data['page'] = 'welcome_user';
            $email_data['password'] = $password['plain'];
            $email_data['email'] = $data_string['email'];

            $sent = $this->cloudhealth->send_welcome_email($email_data);
        }
        if($sent){
            $color = 'green';
            $status = 'SENT';
        }
        
        echo json_encode(array('status' => $status, 'color' => $color));
    }
    
    public function upload_employees_consent() {
        $post = $this->input->post();
        $sent = false;
        $color = 'red';
        $status = 'FAILED';
        
        $name = $post['name'];
        $email = $post['email'];
        
        $email_data['name'] = trim($name);
        $email_data['email'] = trim(str_replace(" ", "", $email));
        
        //check if already enrolled
        $already_enrolled = $this->Users_model->validate_email($email);
        if($already_enrolled){
            $status = 'DUPLICATE';
            $email_data['is_duplicate'] = true;
        }
        //store to DB
        $this->load->model('DataConsent_model', 'data_consent');

        if(!$already_enrolled && $this->data_consent->add($email_data)){
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
            $email_data['company_name'] = $company_name;
            $sent = $this->cloudhealth->send_consent_email($email_data);
            //update is_sent column
            if($sent){
                $sent_data['is_sent'] = $sent;
                $this->data_consent->update($email, $sent_data);
            }
        }
        if($sent){
            $color = 'green';
            $status = 'SENT';
        }
        
        echo json_encode(array('status' => $status, 'color' => $color));
    }
    
    function getConsentList($company_id=0){
        $this->load->model('DataConsent_model', 'data_consent');
        if($company_id == 0){
            $company_id = $this->session->userdata['logged_in']['company_id'];
        }
        $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        //get count of employees who are already enrolled in the system
        $cnt_registered = $this->Users_model->getUserCountPerCompany($company_name);
        $data_consent = $this->data_consent->getAllPerCompany($company_id);
        if($data_consent > 0){
            foreach($data_consent as &$rec){
                if($this->Users_model->validate_email(trim($rec['email']))){
                    $rec['registered'] = true;
                }else{
                    $rec['registered'] = false;
                }
            }
        }
        $count = $this->data_consent->getCount($company_id);
        $data['total_emails_sent'] = (int)$count['total_emails_sent'];
        $data['success'] = (int)$count['success'];
        $data['failed'] = (int)$count['failed'];
        $data['total_respondents'] = (int)$count['total_respondents'];
        $data['consented_to_enrollment'] = (int)$count['consented_to_enrollment'];
        $data['refused_enrollment'] = (int)$count['refused_enrollment'];
        
        $data['data'] = $data_consent;
        $data['cnt_registered'] = $cnt_registered;
        $data['company_name'] = $company_name;
        
        echo $this->load->view('healthprofile/consent_list', $data, true);
    }
    
    private function _updateUsers($file){
        if(!empty($file)){
            if($file[0][0] == 'old_email'){
                for($i=1; $i<count($file);$i++){
                    $old_email = trim($file[$i][0]);
                    $new_email = trim($file[$i][1]);
                    $password = $this->cloudhealth->random_password();
                    $new_password_crypt = $password['crypt'];
                    $new_password = $password['plain'];

                    if($this->Users_model->update_userByEmail($old_email, $new_email, $new_password_crypt) > 0){
                        //send credentials to new email address
                        $email_data['page'] = 'welcome_user';
                        $email_data['password'] = $new_password;
                        $email_data['email'] = $new_email;

                        $this->cloudhealth->send_welcome_email($email_data);
                    }
                }
            }else{
                return false;
            }
        }
    }

    public function validate_email($email) {
        $user_id = $this->Users_model->validate_email($email);

        if ($user_id > 0) {
            return false;
        } else {
            return true;
        }
    }

    function summary_and_analysis() {
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $d['CHI_reports'] = $this->reports->getReportByCompanyId($company_id, Reports_model::$REPORT_SENT);
        $d['v'] = 'healthprofile/summary_and_analysis';
        $this->load->view('template', $d);
    }
    
    function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
}
