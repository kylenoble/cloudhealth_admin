<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Organizations extends CI_Controller {
    private $is_admin = false;

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        $this->load->model('Organizations_model');
        if(admin_role() == Users_model::ADMIN){
            $this->is_admin = true;
        }
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $organization = $this->Organizations_model->organizations_list();
        $user = $this->Users_model;
        $status = Users_model::$PENDING_NDA;
        foreach($organization as &$org){
            $org['with_pending_nda'] = $user->countAdminPerStatus($status, $org['id']) > 0;
        }

        $d['companies'] = $organization;
        $d['js'][] = $this->cloudhealth->load_script('assets/js/organizations.js');
        $d['v'] = 'organizations/organizations';
        $this->load->view('template', $d);
    }

    /**
     * add()
     * Add organization form
     */
    public function add() {
        $d['v'] = 'organizations/organizations_add';
        $this->load->view('template', $d);
    }

    /**
     * add_org_save()
     * Save organization form data
     * @param form data in array
     */
    public function add_org_save() {
        $data_string = _get_query_args();
        $this->form_validation->set_rules('org_name', 'Company Name', 'required|callback_uniqueCompanyName');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail['.$this->session->userdata['logged_in']['userid'].']');
        $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');
        $this->form_validation->set_message('uniqueCompanyName', 'Company Name already exists.');
       
        $r['result']['success'] = 0; // failed default status

        $tmp_nda = null;
        //handle NDA file
        if ($_FILES['nda_attach']['name'] !== "") {
            //check file size
            $tmp_nda = UPLOAD_DIR . $_FILES['nda_attach']['name'];

            //store the file temporarily
            if (move_uploaded_file($_FILES['nda_attach']['tmp_name'], $tmp_nda)) {
                $filetype = mime_content_type($tmp_nda);
                $filesize = filesize($tmp_nda);

                $check_file_size_args = $filesize . '||' . 500000 . '||pdf';

                $this->form_validation->set_rules('nda_attach', 'NDA Attachment', "callback_check_file_size[" . $check_file_size_args . "]|callback_check_nda_filetype[" . $filetype . "]");
            }
        }
        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            //validate if allowed number of users is a valid number
            if(isset($data_string['allowed_number_of_users'][0]) && $data_string['allowed_number_of_users'][0] != 'on' && (int)$data_string['allowed_number_of_users'][1] === 0){
                $r['result']['errors'] = "Maximum number of users field accepts numbers only.";
            }else{
                $credentials = $this->Organizations_model->add_org($data_string, $tmp_nda);
                if($credentials){
                    $program_code = $this->subscription->getDefaultProgramCode();
                    $allowed_no_of_users = 0;
                    if(isset($data_string['enroll_to_trial'])){
                        $program_code = $this->subscription->getTrialProgramCode();
                    }

                    if(isset($data_string['allowed_number_of_users'][1]) && trim($data_string['allowed_number_of_users'][1]) != ''){
                        $allowed_no_of_users = $data_string['allowed_number_of_users'][1];
                    }
                    //  auto-enroll company to CHI program
                    $subscription_data = array(
                        'company_id'    =>  $credentials['company_id'],
                        'program_code'  =>  $program_code,
                        'subscription_start' => date('Y-m-d'),
                        'subscription_end' => $this->subscription->computeSubscriptionEnd(date('Y-m-d')),
                        'date_created'  =>  date('Y-m-d h:i:s'),
                        'allowed_number_of_users' => $allowed_no_of_users
                    );
                    $this->subscription->companyEnroll($subscription_data);

                    //  send welcome email to new user
                    $email_data['page'] = 'welcome_admin';
                    $email_data['attachment'] = $tmp_nda;
                    $email_data['email'] = $credentials['email'];
                    
                    if($tmp_nda){
                        $email_data['with_nda'] = true;
                        $email_data['oic'] = $data_string['account_officer'];
                    }else{
                        $email_data['password'] = $credentials['password'];
                    }

                    $sent = $this->cloudhealth->send_welcome_email($email_data);

                    //remove nda temp file
                    if ($tmp_nda) {
                        unlink($tmp_nda);
                    }
                    
                    //send system notification on the auto-enrollment of CHA Trial/Run
                    $this->sendNotif($program_code, $credentials['company_id']);
                    if (!$sent) {
                        // mail not sent
                        $this->session->set_flashdata('email_sending_error', 'Sending of email failed. Please contact your system adminstrator.');
                    }
                }

                $r['result']['success'] = 1;
                $this->session->set_flashdata('organization_add_msg', 'New company enrolled successfully. Please click on detail icon to view HR admin credentials.');
            }
        }
        echo json_encode($r);
    }
    
    function sendNotif($program_code, $company_id){
        $subscription = $this->subscription->getProgramInfoByCode('subscription_name', $program_code);
        $subject = 'New Subscription: ' . $subscription['subscription_name'];
        $details = "A new subscription has been added to your account";
        $details .= "<br />";
        $details .= "<br />Go to <b class='info_breadcrumbs'>Manage My Account > Subscriptions and Renewals</b> to view the list of subscriptions.";

        $users_model = $this->Users_model;
        $users_to_notify = $this->Users_model->getAdminUsersByCompany($company_id, array('role' => $users_model::HR), 'id, name');
        $recipients = [];
        foreach ($users_to_notify as $user) {
            $recipients[] = array(
                'type' => 'admin',
                'id' => $user['id'],
                'name' => trim($user['name'])
            );
        }

        $this->cloudhealth->sendNotification($subject, $details, $recipients);
    }

    function uniqueCompanyName($str){
        if($this->Organizations_model->companyName_exists($str) > 0){
            return false;
        }
        return true;
    }

    function check_file_size($i, $args){
        list($filesize, $allowed_filesize, $type) = explode('||', $args);
        if($filesize > $allowed_filesize){
            if($type == 'pdf'){
                $this->form_validation->set_message('check_file_size', 'File size is too large. File must not exceed 500KB');
            }else{
                $this->form_validation->set_message('check_file_size', 'File size is too large. File must not exceed 1MB or 1024KB');
            }
            return false;
        }
        return true;
    }
    
    function check_logo_filetype($i, $filetype){
        $allowed = array(
            image_type_to_mime_type(IMAGETYPE_BMP), 
            image_type_to_mime_type(IMAGETYPE_PNG), 
            image_type_to_mime_type(IMAGETYPE_JPEG),
            image_type_to_mime_type(IMAGETYPE_GIF));
        
        if(!in_array($filetype, $allowed)){
            $this->form_validation->set_message('check_logo_filetype', 'Please check file type of the company logo. Allowed file types are: jpg/jpeg, svg, bmp, png.');
            return false;
        }
        return true;
    }
    
    function check_nda_filetype($i, $filetype){

        if($filetype != 'application/pdf'){
            $this->form_validation->set_message('check_nda_filetype', 'Please check file type of the NDA attachment. Allowed file type is PDF.');
            return false;
        }
        return true;
    }
    
    /**
     * org_update()
     * Update organization form data
     * @param form data in array
     */
    public function org_update() {
        $data_string = _get_query_args();
        $logo_binary = null;

        $this->form_validation->set_rules('org_name', 'Company Name', 'required');
        if(!$this->is_admin){
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_uniqueEmail['.$this->session->userdata['logged_in']['userid'].']');
            $this->form_validation->set_message('uniqueEmail', 'Email address already exists.');
        }
        $r['result']['success'] = 0; // failed default status

        //handle logo file
        if (!empty($_FILES)) {
            //check file size
            $tmp_logo = UPLOAD_DIR . "" . $_FILES['company_logo']['name'];
            //store the file temporarily
            if (move_uploaded_file($_FILES['company_logo']['tmp_name'], $tmp_logo)) {
                $filetype = mime_content_type($tmp_logo);
                $filesize = filesize($tmp_logo);

                $check_file_size_args = $filesize.'||1024000||image';
                
                $this->form_validation->set_rules('company_logo', 'Company Logo', "callback_check_file_size[".$check_file_size_args." ]|callback_check_logo_filetype[".$filetype."]");
                //get image bytes
                $logo_binary = file_get_contents($tmp_logo);

                //remove file after getting its binary data
                if ($logo_binary) {
                    unlink($tmp_logo);
                }
                $data_string['logo'] = base64_encode($logo_binary);
            }
        }

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $orgid = $data_string['id'];
            if(!$this->is_admin){
                $userid = $this->session->userdata['logged_in']['userid'];
                //check if email was modified
                if (strtolower(trim($this->session->userdata['logged_in']['email'])) != strtolower(trim($data_string['email']))) {
                    //  update admin table
                    $admin_data = array('email' => $data_string['email']);
                    $this->Users_model->update_admin($admin_data, $userid);
                }
            }
            $this->Organizations_model->update_org($data_string,$orgid);
            $this->session->set_flashdata('success_msg', 'Business Details Updated Successfully.');
			
            $r['result']['success'] = 1;	
           
        }

        echo json_encode($r);
    }

    /**
     * update()
     * Edit organization form
     */
    public function update() {
        $orgid = $this->uri->segment('3');
        $d['js'] = array();

        $organization = $this->Organizations_model->get_company_details_by_id($orgid);
        
        $branches = $this->Organizations_model->get_company_branches($orgid);
        $departments = $this->Organizations_model->get_company_departments($orgid);
        $d['branches'] = $branches;
        $d['departments'] = $departments;
        $d['branchList'] = $this->Organizations_model->getBranches($orgid);
       
        if(!$this->is_admin){
            $userid = $this->session->userdata['logged_in']['userid'];
            $user = $this->Users_model->get_adminDetails(array('id' => $userid), 'email');
            $d['email'] = trim($user['email']);
        }else{
            $d['js'][] = $this->cloudhealth->load_script('assets/js/healthprofile.js');
        }
        $d['v'] = 'organizations/organizations_update';
        $d['result'] = $organization;
        $d['orgid'] = $orgid;
        $this->load->view('template', $d);
        
    }

    /**
     * view()
     * View organization details
     */
    public function view() {

        $company_id = $this->uri->segment('3');
        if($company_id == null){
            $company_id = $this->input->post('company_id');
        }
        $data_string = array(
            'company_id' => $company_id
        );

        $company_detail = $this->Organizations_model->get_company_details($data_string);
        $activeHR_users = $this->Users_model->adminList(Users_model::HR, $active_only = true, $company_id);

        $d['result'] = $company_detail;
        $d['hr_users'] = $activeHR_users;
        $d['company_id'] = $company_id;
        $d['branches'] = $this->Organizations_model->get_company_branches($company_id);
        $d['depts_no_branch'] = $this->Organizations_model->getDeptsPerBranch($company_id);
        $d['v'] = 'organizations/organizations_view';
        $this->load->view('template', $d);
    }

    /**
     * remove()
     * Remove organization and its logo
     */
    public function remove() {

        $type = $this->uri->segment('3');
        $orgid = $this->uri->segment('4');

        if ($orgid != '') {

            if ($type == 'logo') {
                $data_string = array(
                    'orgid' => $orgid,
                    'type' => $type
                );
                $url = 'organizations/org_remove';
                $result = api_curl($url, $data_string, 'POST');

                if ($result->form_data->status == 1) {
                    $this->session->set_flashdata('organization_add_msg', 'Organization logo removed successfully.');
                    redirect(base_url() . 'organizations/update/' . $orgid);
                } else {
                    $this->session->set_flashdata('organization_add_msg', $result->form_data->error);
                }
            } else if ($type == 'org') {
                $data_string = array(
                    'orgid' => $orgid,
                    'type' => $type
                );
                $url = 'organizations/org_remove';
                $result = api_curl($url, $data_string, 'POST');

                if ($result->form_data->status == 1) {
                    $this->session->set_flashdata('organization_add_msg', 'Organization deleted successfully.');

                    //redirect(base_url() . 'organizations');
                    echo json_encode($result);
                } else {

                    $this->session->set_flashdata('organization_add_msg', $result->form_data->error);
                    //redirect(base_url() . 'organizations');
                    echo json_encode($result);
                }
            }
        }
    }
    
    function uniqueEmail($email, $userid){
        if($this->Users_model->validate_AdminEmail($email, $userid)){
            return false;
        }
        return true;
    }
    
    function getBranches($company_id){
        $branchList = $this->Organizations_model->getBranches($company_id);
        echo json_encode($branchList);
    }
    
    function get_depts_perBranch(){
        $orgid = $this->uri->segment('3');
        $branch_id = $this->uri->segment('4');
        echo json_encode($this->Organizations_model->getDeptsPerBranch($orgid, $branch_id));
    }
    
    function ajax_companies_select(){
        $organizations = $this->Organizations_model->organizations_list();
        $html = "<select name='active-companies' id='active-companies'>";
        $html .= "<option selected value=''>Filter by Company</option>";
        foreach($organizations as $org){
            $html .= "<option value='".$org['company_name']."'>".$org['company_name']."</option>";
        }
        $html .= "</select>";
        
        echo json_encode(array('active_companies' => $html));
    }
    /**
     * Admin > Client List > NDA OK
     * Activates company HR account
     * Sends HR credentials to nominated email address
     */
    function NDAverified(){
        $r['result']['success'] = 0; // failed default status
        $company_id = $this->input->post('x');
        
        //get admin users
        $admin_users = $this->Users_model->getAdminUsersByCompany($company_id, null, 'id, email, name, password');
        //activates hr account
        $data['status'] = Users_model::$NEW_USER;
        foreach($admin_users as $admin){
            if($this->Users_model->update_admin($data, $admin['id'])){
                // Sends HR credentials to nominated email address
                //  send welcome email to new user
                $email_data['page'] = 'welcome_admin';
                $email_data['email'] = $admin['email'];
                $email_data['password'] = $admin['password'];

                if($this->cloudhealth->send_welcome_email($email_data)){
                    $r['result']['success'] = 1;
                }else{
                    $r['result']['errors'] = "An error was encountered. Please contact your system administrator.";
                }
            }
        }
        echo json_encode($r);
    }

}
