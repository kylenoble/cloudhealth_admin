<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CorporateHealthPrograms extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
    }
    
    function index() {
        $data = $this->subscription->corporateHealthProgramsSummary();
        $d['summary'] = $this->_cleanSummary($data);
        $d['v'] = 'corporatehealthprograms/summary';
        $this->load->view('template', $d);
    }
    
    function _cleanSummary($summary = array()){
        $result = [];
        foreach($summary as $row){
            if (!isset($result[$row['company_id']])) {
                $result[$row['company_id']] = array
                    (
                    'company_id' => $row['company_id'],
                    'company_name' => $row['company_name'],
                    'contact' => $row['contact'],
                    'subscription_count' => 0,
                    'Ongoing' => 0,
                    'Completed' => 0
                );
            }
            $result[$row['company_id']]['subscription_count'] += $row['subscription_count'];
            $result[$row['company_id']]['Ongoing'] += ($row['status'] == 'Ongoing' ? (int)$row['subscription_count'] : 0);
            $result[$row['company_id']]['Completed'] += ($row['status'] == 'Completed' ? (int)$row['subscription_count'] : 0);
        }
        return $result;
    }
    
    function subscriptions($company_id){
        $this->load->model('Organizations_model', 'organization');
        
        $company_name = $this->organization->get_companyname_by_id($company_id);
        $d['company_name'] = $company_name;
        $d['company_id'] = $company_id;
        $d['subscriptions'] = $this->subscription->getSubscriptionsByCompanyId($company_id, true, $company_name);
        $d['v'] = 'corporatehealthprograms/subscriptions/index';
        $this->load->view('template', $d);
    }
    
    function subscriptions_new($company_id){
        $this->load->model('Organizations_model', 'organization');
        $company_name = $this->organization->get_companyname_by_id($company_id);
        $d['company_name'] = $company_name;
        $d['company_id'] = $company_id;
        $enrolled_program_codes = $this->subscription->_getEnrolledProgramCodes($company_id);
        $programs = $this->subscription->getAllPrograms($enrolled_program_codes, false, true);
        $formatted_programs = array();
        foreach($programs as $program){
            $formatted_programs[$program['shortcode']] = $program['subscription_name'];
        }
        $d['programs'] = $formatted_programs;
        $d['callback'] = 'subscriptions_add';
        $d['title'] = 'ENROLL TO NEW SUBSCRIPTION';
        $d['v'] = 'corporatehealthprograms/subscriptions/add';
        $this->load->view('template', $d);
    }
    
    function subscriptions_add(){
        $data_string = _get_query_args();
        $this->form_validation->set_rules('program_code', 'Program Name', 'required');
        $this->form_validation->set_rules('subscription_start', 'Start of Subscription', 'required');
        $this->form_validation->set_rules('subscription_end', 'End of Subscription', 'required');

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            //validate if allowed number of users is a valid number
            if(isset($data_string['allowed_number_of_users'][0]) && $data_string['allowed_number_of_users'][0] != 'on' && (int)$data_string['allowed_number_of_users'][1] === 0){
                $r['result']['errors'] = "Maximum number of users field accepts numbers only.";
            }else{
                $allowed_no_of_users = 0;
                if(isset($data_string['enroll_to_trial'])){
                    $program_code = $this->subscription->getTrialProgramCode();
                }

                if(isset($data_string['allowed_number_of_users'][1]) && trim($data_string['allowed_number_of_users'][1]) != ''){
                    $allowed_no_of_users = $data_string['allowed_number_of_users'][1];
                }
                $data_string['allowed_number_of_users'] = $allowed_no_of_users;
                if($this->subscription->companyEnroll($data_string)){
                    //send notification to company
                    $subscription = $this->subscription->getProgramInfoByCode('subscription_name', $data_string['program_code']);
                    $subject = 'New Subscription: ' . $subscription['subscription_name'];
                    $details = "A new subscription has been added to your account";
                    $details .= "<br />";
                    $details .= "<br />Go to <span style='color: #59a5d8; font-weight: 500;'>Manage My Account > Subscriptions and Renewals</span> to view the list of subscriptions.";

                    $users_model = $this->Users_model;
                    $users_to_notify = $this->Users_model->getAdminUsersByCompany($data_string['company_id'], array('role' => $users_model::HR), 'id, name');
                    $recipients = [];
                    foreach($users_to_notify as $user) {
                        $recipients[] = array(
                            'type' => 'admin', 
                            'id' => $user['id'], 
                            'name' => trim($user['name'])
                        );
                    }

                    $this->cloudhealth->sendNotification($subject, $details, $recipients);

                    $r['result']['success'] = 1;
                    $this->session->set_flashdata('subscription_add_msg', 'A new subscription was enrolled successfully.');
                }
            }
        }
        echo json_encode($r);
    }
    
    function subscriptions_edit($enrollment_id){
        $program = $this->subscription->getEnrolledProgramById($enrollment_id);
        
        $this->load->model('Organizations_model', 'organization');
        $company_name = $this->organization->get_companyname_by_id($program['company_id']);
        $d['company_name'] = $company_name;
        $d['company_id'] = $program['company_id'];
        $d['program_info'] = $program;
        $d['callback'] = 'subscriptions_update';
        $d['with_unlock_option'] = ($program['program_code'] == $this->subscription->getDefaultProgramCode());
        $d['title'] = 'UPDATE SUBSCRIPTION';
        $d['v'] = 'corporatehealthprograms/subscriptions/add';
        $this->load->view('template', $d);
    }
    
    function subscriptionReportExists(){
        $id = $this->input->post('enrollment_id');
        $subscription = $this->subscription->getCompanySubscriptionById('program_code', $id);
        if($subscription['program_code'] === $this->subscription->getDefaultProgramCode()){
            $this->load->model('Reports_model', 'report');
            return $this->report->checkIfReportSent($id);
        }else{
            return true;
        }
    }
    
    function checkStatus($value){
        if(in_array($value, array('Completed', 'Ongoing', 'Expired', 'Cancelled', 'For Renewal'))){
            return true;
        }
        return false;
    }
    
    function subscriptions_update(){
        $data_string = _get_query_args();
        $this->form_validation->set_rules('subscription_start', 'Start of Subscription', 'required');
        $this->form_validation->set_rules('subscription_end', 'End of Subscription', 'required');
        $this->form_validation->set_rules('status', 'Status', 'callback_checkStatus');
        $this->form_validation->set_message('checkStatus', 'Invalid Status');
//        if($data_string['status'] == 'Completed'){
//            //check if with report before tagging the subscription as completed
//            $this->form_validation->set_rules('status', 'Status', 'callback_subscriptionReportExists');
//            $this->form_validation->set_message('subscriptionReportExists', 'Subscription cannot be set to complete. Awaiting MD report.');
//        }

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            if(!isset($data_string['allowed_number_of_users'][0]) && $data_string['allowed_number_of_users'][1] == ''){
                $r['result']['errors'] = "Maximum number of users field is required.";
            }else{
                //validate if allowed number of users is a valid number
                if(!isset($data_string['allowed_number_of_users'][0]) && (int)$data_string['allowed_number_of_users'][1] === 0){
                    $r['result']['errors'] = "Maximum number of users field accepts numbers only.";
                }else{
                    $id = $data_string['enrollment_id'];
                    unset($data_string['enrollment_id']);

                    $allowed_no_of_users = 0;
                    if(!isset($data_string['allowed_number_of_users'][0])){
                        $allowed_no_of_users = $data_string['allowed_number_of_users'][1];
                    }
                    
                    //check status if previous status is for renewal
                    $subscription = $this->subscription->getEnrolledProgramById($id);
                    $old_val_allowed_users = $subscription['allowed_number_of_users'];
                    $send_notif = false;
                    $details = '';
                    if((int)$old_val_allowed_users != (int)$allowed_no_of_users){
                        $send_notif = true;
                        $subject = 'Program Update: '. $subscription['subscription_name'];
                        $details = "The maximum allowed number of users for enrollment has been modified. To view details, go to <span style='color: #59a5d8; font-weight: 500;'>Manage My Account > Subscriptions and Renewals > Subscriptions</b>.";
                    }
                    $data_string['allowed_number_of_users'] = $allowed_no_of_users;

                    $unlock_all_survey = false;
                    if($data_string['status'] == 'Ongoing' && isset($data_string['unlock_all'])){
                        $unlock_all_survey = true;
                    }
                    unset($data_string['unlock_all']);
                    
                    if($subscription && $this->subscription->companySubscriptionUpdate($id, $data_string, $subscription)){
                        $company_subscription = $this->subscription->getEnrolledProgramById($id);
                        if($subscription['status'] == 'For Renewal'){
                            //send notification to company
                            $send_notif = true;
    
                            $subject = 'Program Renewal: '. $subscription['subscription_name'];
                            $details = "Subscription Start: ". date('d M, Y', strtotime($company_subscription['subscription_start']));
                            $details .= "<br />Subscription End: ". date('d M, Y', strtotime($company_subscription['subscription_end']));
                        }
                        if($send_notif){
                            $users_model = $this->Users_model;
                            $users_to_notify = $this->Users_model->getAdminUsersByCompany($company_subscription['company_id'], array('role' => $users_model::HR), 'id, name');
                            $recipients = [];
                            foreach($users_to_notify as $user) {
                                $recipients[] = array(
                                    'type' => 'admin', 
                                    'id' => $user['id'], 
                                    'name' => trim($user['name'])
                                );
                            }
                            $this->cloudhealth->sendNotification($subject, $details, $recipients);
                        }
                        if($unlock_all_survey){
                            $this->load->model('Organizations_model', 'organization');
                            //unlocks all employees
                            $company_name = $this->organization->get_companyname_by_id($company_subscription['company_id']);
                            $users_data = array('healthsurvey_status' => 'Active');
                            $this->Users_model->updatePatientsPerCompanyName($company_name, $users_data, array('healthsurvey_status' => 'Locked'));
                        }
                    }
                    $r['result']['success'] = 1;
                    $this->session->set_flashdata('subscription_add_msg', 'Subscription details were updated successfully.');
                }
            }
        }
        echo json_encode($r);
    }
    
    function subscriptions_intervention($company_enrollment_id=0, $from_HR=false, $table_only=false){
        $program = $this->subscription->getEnrolledProgramById($company_enrollment_id);
        
        $is_basic = false;
        //check if package is basic
        if(substr(strtoupper($program['program_code']), -1) == 'B'){
            $is_basic = true;
        }
        
        $this->load->model('Organizations_model', 'organization');
        $company_name = $this->organization->get_companyname_by_id($program['company_id']);
        $d['company_name'] = $company_name;
        $d['company_id'] = $program['company_id'];
        
        if($program){
            $code = $program['program_code'];

            $d['program_name'] = $program['subscription_name'];
            $d['program_code'] = $code;
            $d['company_enrollment_id'] = $company_enrollment_id;

            $status = array('enrollment_status' => 'Approved');
            $d['enrolled_employees'] = $this->subscription->getEmployeesPerProgram($company_enrollment_id, $status, $code);
        }
        $inclusions_tmp = json_decode($program['inclusions'], true);
        $inclusions_healthtalk = $inclusions_tmp['description']['health_talk']['list'];
        $inclusions_activity = (isset($inclusions_tmp['description']['activity']) ? $inclusions_tmp['description']['activity']['list'] : array());
        $inclusions_addons = $inclusions_tmp['description']['addons']['list'];

        $activities = [];
        $add_ons = [];
        foreach($inclusions_addons as $addon){
            $add_ons[] = $addon['name'];
        }
        foreach($inclusions_activity as $inclusion){
            $activities[] = $inclusion['name'];
        }
        $d['with_add_ons'] = $add_ons;

        //check if with event already
        $this->load->model('Events_model', 'events');
        $events = $this->events->getAll(array('company_enrollment_id' => $company_enrollment_id));
        if($events){
            $inclusions_healthtalk = [];
            foreach($events as $event){
                unset($event['name']);
                $event['name'] = $event['event_name'];
                $inclusions_healthtalk[$event['id']] = $event;
            }
        }
        $d['with_event'] = !empty($events);
        $d['inclusions_healthtalk'] = $inclusions_healthtalk;
        $d['inclusions'] = $activities;
        $d['js'] = array($this->cloudhealth->load_script('assets/js/dropdown_multiple_checkbox.js'));
        $view = 'corporatehealthprograms/subscriptions/intervention_basic';
        
        $d['from_hr'] = $from_HR || (admin_role() == Users_model::HR);
        $d['table_only'] = $table_only; //used by events > health talks > view enrollee list
        $this->load->view($view, $d);
    }

    function setProgramSchedule(){
        $post = $this->input->post();
        $program_code = $post['program_code'];
        $data = $post['data'];
        $employee_enrollment_id = $post['xx'];
        $params = array();
        parse_str($data, $params);

        $return['result']['success'] = 0;
        
        //check if program code exists
        if($program_code != null){
            
            $do_update = false;
            $json_values = array();
            
            if($this->subscription->interventionExists($employee_enrollment_id) > 0){
                //update
                $do_update = true;
            }
            
            foreach($params as $name=>$val){
                $date = (isset($val['date']) ? date('Y-m-d', strtotime($val['date'])) : null);
                $time = (isset($val['time']) ? $val['time'] : null);
                if($name != 'add_ons'){
                    $json_values[] = array(
                        'name' => $name,
                        'status' => $do_update && isset($val['status']) ? $val['status'] : 'Pending',
                        'appointment_date' => $date,
                        'appointment_time' => $time
                    );
                }else{
                    $json_values[] = array(
                        'name' => $name,
                        'status' => $do_update && isset($val['status']) ? $val['status'] : 'Pending',
                        'appointment_date' => $date,
                        'appointment_time' => $time,
                        'list' => isset($val['t']) ? $val['t'] : null,
                        'notes' => isset($val['notes']) ? $val['notes'] : null
                    );
                }
            }
            if ($do_update) {
                $post_data = array(
                    'details' => json_encode($json_values)
                );
            }else{
                $post_data = array(
                    'details' => json_encode($json_values),
                    'employee_enrollment_id' => $employee_enrollment_id,
                    'program_code' => strtoupper($program_code),
                    'is_basic' => false
                );
            }
            if($this->subscription->setInterventionAppointment($employee_enrollment_id, $post_data, $do_update)){
                //send notification to the enrollee
                $userinfo = $this->subscription->getUserInfoBySubscriptionEnrollmentId($employee_enrollment_id);
                $subscription = $this->subscription->getProgramInfoByCode('subscription_name', strtoupper($program_code));
                $subject = 'Appointment for ' . $subscription['subscription_name'];

                $details = "Your appointment has been scheduled. To approve or decline, go to <span style='color: #59a5d8; font-weight: 500'>My Health Activities > My Health Programs > Upcoming Events</span>.";

                $recipient[] = array(
                    'type' => 'Patient',
                    'id' => $userinfo['user_id'],
                    'name' => trim($userinfo['name'])
                );
                $return['result']['success'] = 1;
                
                if($this->cloudhealth->sendNotification($subject, $details, $recipient)){
                    $return['result']['message'] = 'User has been notified about the appointment date.';
                    
                    $email_data['details'] = $details;
                    $email_data['title'] = $subject;
                    $email_data['emails'] = array($userinfo['email']);
                    $this->_processEmailSending($email_data);
                }
            }
        }else{
            $return['result']['errors'] = 'Please contact your system administrator.';
        }
        echo json_encode($return);
    }
    
    private function _processEmailSending($data = array()){
        $email_data['datetime'] = date('M j, Y g:i:s A', time());
        $email_data['message'] = $data['details'];
        $email_data['header'] = $data['title'];
        $email_data['is_email'] = true;
        $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
        $email_data['page'] = 'notification';
        $email_data['emails'] = $data['emails'];
        $this->cloudhealth->send_notification_email($email_data, $is_update = false, $individual_sending = true);
    }
    
    function event_new(){
        $post = $this->input->post();
        $subscription = $this->subscription->getEnrolledProgramById($post['ceid']);
        $d['callback'] = 'event_add';
        $d['title'] = 'Set Event Details for '.$subscription['subscription_name'];
        $d['x'] = $post['ceid'];
        $d['default_title'] = $post['htalk'];
        $d['htalk_type'] = (isset($post['htalk_type']) ? $post['htalk_type'] : null);
        
        echo $this->load->view('corporatehealthprograms/subscriptions/event_add', $d, true);
    }
    
    function event_edit(){
        $post = $this->input->post();
        $subscription = $this->subscription->getEnrolledProgramById($post['ceid']);

        $d['title'] = 'Event Details for '.$subscription['subscription_name'];
        $d['x'] = $post['ceid'];

        $this->load->model('Events_model', 'events');
        $event = $this->events->getRow(array('id' => $post['event_id']));
        $d['event_info'] = $event;
        $d['callback'] = 'event_update';
        $d['htalk_type'] = (isset($post['htalk_type']) ? $post['htalk_type'] : null);
      
        echo $this->load->view('corporatehealthprograms/subscriptions/event_add', $d, true);
    }
    
    function setBasicStatus(){
        $post = $this->input->post();
        $program_code = $post['program_code'];
        $data = $post['data'];
        $employee_enrollment_id = $post['xx'];
        $params = array();
        $do_update = false;
        parse_str($data, $params);
        
        $return['result']['success'] = 0;

        //check if program code exists
        if($program_code != null){
            $json_values = array();

            foreach($params as $name=>$val){
                if($name != 'add_ons'){
                    $json_values[] = array(
                        'name' => $name,
                        'status' => $val['status']
                    );
                }else{
                    $json_values[] = array(
                        'name' => $name,
                        'status' => $val['status'],
                        'list' => isset($val['t']) ? $val['t'] : null,
                        'notes' => isset($val['notes']) ? $val['notes'] : null
                    );
                }
            }
            if($do_update){
                $post_data = array(
                    'details' => json_encode($json_values)
                );
            }else{
                $post_data = array(
                    'details' => json_encode($json_values),
                    'is_basic' => true,
                    'employee_enrollment_id' => $employee_enrollment_id,
                    'program_code' => $program_code
                );
            }
            if($this->subscription->interventionExists($employee_enrollment_id)){
                $do_update = true;
            }
            if($this->subscription->setInterventionAppointment($employee_enrollment_id, $post_data, $do_update)){
                $return['result']['success'] = 1;
                $return['result']['message'] = 'Status updated';
            }
        }else{
            $return['result']['errors'] = 'Please contact your system administrator.';
        }
        echo json_encode($return);
    }
    
    function setBasicEvent(){
        $post = $this->input->post();
        
        $event_id = $post['event_id'];
        $return['result']['success'] = 0;
        $company_subscription_id = $post['x'];
        $recipients = [];
        $recipients_email = [];
        $send_notif = true;
        $is_update = $post['is_update'];
        $basic_data = [];
        $for_new_enrolled = (isset($post['for_new_enrolled']) && $post['for_new_enrolled'] == '1' ? true : false);

        $enrolled = $this->subscription->getEnrolledPerCompanyPackage($company_subscription_id, false, null, array('enrollment_status' => 'Approved'));
        $subscription = $this->subscription->getCompanySubscriptionById('program_code', $company_subscription_id);

        //check if all enrolled employess have assigned event id already
        $where['enrollment_status'] = 'Approved';
        $enrolled_with_intervention_details = $this->subscription->getEmployeesPerProgram($company_subscription_id, $where, $subscription['program_code']);
        
        foreach($enrolled as $user){
           
            if($is_update == 'false' || !empty($enrolled_with_intervention_details)){
                $json_values = array(
                    'event_id' => (int)$event_id
                );
                if(!empty($enrolled_with_intervention_details)){
                    //check if user has an assigned event id
                    $pos = array_search($user['employee_enrollment_id'], array_column($enrolled_with_intervention_details, 'id'));
                    if($enrolled_with_intervention_details[$pos]['upi_id'] == null){
                        $basic_data[] = array(
                            'details' => json_encode(array($json_values)),
                            'employee_enrollment_id' => $user['employee_enrollment_id'],
                            'program_code' => $subscription['program_code'],
                            'is_basic' => true
                        );
                        if($for_new_enrolled){
                            $recipients[] = array(
                                'type' => 'Patient', 
                                'id' => $user['user_id'], 
                                'name' => $user['name']
                            );
                            $recipients_email[] = $user['email'];
                        }
                    }else{
                        $is_second_event = false;
                        //check if same event as the one already stored
                        $details = json_decode($enrolled_with_intervention_details[$pos]['details'], true);
                        if(isset($details[0])){
                            if(!in_array((int)$event_id, array_column($details, 'event_id'))){
                                $is_second_event = true;
                            }
                        }else{
                            if($details['event_id'] != $event_id){
                                $is_second_event = true;
                            }
                        }
                        if($is_second_event){
                            //this is the second event of the program
                            //append new event_id
                            $this->subscription->setBasicEventID($enrolled_with_intervention_details[$pos]['upi_id'], $json_values);
                        }
                    }
                }
            }
            if(!$for_new_enrolled){
                $recipients[] = array(
                    'type' => 'Patient', 
                    'id' => $user['user_id'], 
                    'name' => $user['name']
                );
                $recipients_email[] = $user['email'];
            }
        }

        if(!empty($basic_data)){
            if($this->subscription->setBasicIntervention($basic_data)){
                $subject = 'Health Subscription Event';
                
                $details = "An event was created for one of your health subscriptions. ";
                $details .= "Please see details and contact your HR should you not be available on the date set by your company.";
            }else{
                $send_notif = false;
                $return['result']['errors'] = 'An error was encountered. Please contact your system administrator.';
            }
        }else{
            $subject = 'Health Subscription Event (Updated)';

            $details = "An event for one of your health subscriptions was updated. ";
            $details .= "Please see details and contact your HR should you not be available on the date set by your company.";
        }

        if($send_notif){
            //send notifications to enrolled employees
            if($this->cloudhealth->sendNotification($subject, $details, $recipients)){
                //send email notification
                $email_data['details'] = $details;
                $email_data['title'] = $subject;
                $email_data['emails'] = $recipients_email;
                $email_status = $this->_processEmailSending($email_data);
                
                $return['result']['errors'] = $email_status;
                $return['result']['message'] = 'Users have been notified about the event details.';
            }else{
                $return['result']['errors'] = 'Error on sending notifications. Please contact your system administrator.';
            }
            $return['result']['success'] = 1;
        }
        echo json_encode($return);
    }
    
    function updateBasicStatus(){
        $post = $this->input->post();
        $program_code = $post['program_code'];
        $event_id = $post['event_id'];
        $data = $post['data'];
        $employee_enrollment_id = $post['xx'];
        $params = array();
        parse_str($data, $params);

        $return['result']['success'] = 0;

        //check if program code exists
        if($program_code != null){
            $json_values = array();

            foreach($params as $name=>$val){
                if($name != 'add_ons'){
                    $json_values[] = array(
                        'event_id' => (int)$event_id,
                        'name' => $name,
                        'status' => $val['status']
                    );
                }else{
                    $json_values[] = array(
                        'event_id' => (int)$event_id,
                        'name' => $name,
                        'status' => $val['status'],
                        'list' => isset($val['t']) ? $val['t'] : null,
                        'notes' => isset($val['notes']) ? $val['notes'] : null
                    );
                }
            }
            $post_data = array(
                'details' => json_encode($json_values)
            );
            if($this->subscription->setInterventionAppointment($employee_enrollment_id, $post_data, true)){
                $return['result']['success'] = 1;
                $return['result']['message'] = 'Status updated';
            }
        }else{
            $return['result']['errors'] = 'Please contact your system administrator.';
        }
        echo json_encode($return);
    }
    
    function getEmployeesIntervention(){
        $post = array();
        parse_str($this->input->server('QUERY_STRING'), $post);
        $company_enrollment_id = $post['ceid'];
        $code = $post['program_code'];
        $status = array('enrollment_status' => 'Approved');
        $d['event_id'] = $post['event_id'];
        $d['enrolled_employees'] = $this->subscription->getEmployeesPerProgram($company_enrollment_id, $status, $code);
        
        $inclusions_config = $this->config->item('intervention');
        unset($inclusions_config[$code]['health_talk']);
        $d['inclusions'] = $inclusions_config[$code];
        $d['with_add_ons'] = (isset($inclusions_config[$code]['add_ons']) ? $inclusions_config[$code]['add_ons'] : false);
        $d['program_code'] = $code;
        $d['company_enrollment_id'] = $post['ceid'];
        $d['from_hr'] = (admin_role() == Users_model::HR);
        $d['event_type'] = $post['event_type'];
        $d['js'] = array($this->cloudhealth->load_script('assets/js/dropdown_multiple_checkbox.js'));
        
        echo $this->load->view('corporatehealthprograms/subscriptions/employee_list_multiple', $d, true);
    }
    
    function updateBasicStatus_multiple(){
        $post = $this->input->post();
        $program_code = $post['program_code'];
        $event_id = $post['event_id'];
        $data = $post['data'];
        $upi_id = $post['xx'];
        $params = array();
        parse_str($data, $params);

        $return['result']['success'] = 0;
        
        //check if program code exists
        if($program_code != null){
//            $json_values['event_id'] = $event_id;
            $inclusions = array();

            foreach($params as $name=>$val){
                if($name != 'add_ons'){
                    $inclusions[] = array(
                        'name' => $name,
                        'status' => $val['status']
                    );
                }else{
                    $inclusions[] = array(
                        'name' => $name,
                        'status' => $val['status'],
                        'list' => isset($val['t']) ? $val['t'] : null,
                        'notes' => isset($val['notes']) ? $val['notes'] : null
                    );
                }
            }
//            $json_values['inclusions'] = $inclusions;
            //check jsonb array index of the given event_id
            $index = $this->subscription->getEventIDindex($event_id, $program_code, $upi_id);
//            $index_pos = 0;
//            if($index){
//                $index_pos = $index['elem_index'];
//            }
            //append inclusions to corresponding event
            if($this->subscription->setBasicEventIntervention($upi_id, $index['elem_index'], $inclusions)){
                $return['result']['success'] = 1;
                $return['result']['message'] = 'Status updated';
            }
        }else{
            $return['result']['errors'] = 'Please contact your system administrator.';
        }
        echo json_encode($return);
    }

}
