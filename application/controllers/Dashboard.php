<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    private $is_admin = false;
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }

        $this->load->model('Organizations_model');
        $this->load->model('Dashboard_model');
        
        if(admin_role() == Users_model::ADMIN){
            $this->is_admin = true;
        }
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        if(!$this->is_admin){
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company = $this->Organizations_model->get_company_details_by_id($company_id);
            $company_name = trim($company['company_name']);
        }

        switch(admin_role()){
            case Users_model::HR:
                $d['v'] = 'dashboard';
                $d['data'] = $this->getSiteAnalyticsData($company_id, $company_name);
                $d['logo'] = $company['logo'];
                
                $d['js'][] = $this->cloudhealth->load_script('assets/js/dashboard.js');
                break;
            
            case Users_model::MD:
                $this->load->model('Appointments_model', 'appointments');
                $d['v'] = 'dashboard_MD';
                $d['companies'] = $this->Organizations_model->organizations_list();
                $d['doctors'] = $this->Users_model->getUsersByType('Doctor', $active=true);
                
                $d['js'][] = $this->cloudhealth->load_script('assets/js/md_dashboard.js');
                break;
            
            default:
                $d['v'] = 'site';
                $companies = $this->Organizations_model->get_organizations(null, 'company_name, is_active');
                $active_companies = 0;
                $inactive_companies = 0;
                foreach($companies as $company){
                    if($company['is_active'] == 't'){
                        $active_companies++;
                    }else{
                        $inactive_companies++;
                    }
                }
                $doctors = $this->Users_model->getUsersByType('Doctor');
                $active_doctors = 0;
                $inactive_doctors = 0;
                foreach($doctors as $dr){
                    if($dr['status'] == 3){
                        $inactive_doctors++;
                    }else{
                        $active_doctors++;
                    }
                }
                $d['companies']['data'] = $companies;
                $d['companies']['active_cnt'] = $active_companies;
                $d['companies']['inactive_cnt'] = $inactive_companies;
                $d['doctors']['active_cnt'] = $active_doctors;
                $d['doctors']['inactive_cnt'] = $inactive_doctors;
                
                $d['js'][] = $this->cloudhealth->load_script('assets/js/dashboard.js');
        }
        $d['departments'] = $this->Users_model->get_users_departments($company_name);
        $d['branches'] = $this->Users_model->get_users_branches($company_name);
        $d['job_grades'] = $this->Organizations_model->get_company_jobgrades($company_name);
        $d['button_vars'] = $this->cloudhealth->getNotifications();
        $d['company_name'] = $company_name;
        $this->load->view('template', $d);
    }
    
    function getAppointmentsAnalytics($viewby=null){
        $post = $this->input->post();
        $company_id = $post['company'];
        $md_id = $post['doctors'];
        $schedule_from = $post['schedule_from'];
        $schedule_to = $post['schedule_to'];
        
        $schedule['from'] = $schedule_from;
        $schedule['to'] = $schedule_to;
        
        $this->load->model('Appointments_model', 'appointments');
        if($viewby == null){
            $data = $this->appointments->analyticsData($company_id, $md_id, $schedule);
        }elseif($viewby == 'MD'){
            $data = $this->appointments->analyticsData($company_id, $md_id, $schedule, 'doctor_id');
            foreach($data as &$rec1){
                $rec1['md_name'] = $this->Users_model->getUserInfoById($rec1['column_select'], 'name');
            }
        }elseif($viewby == 'company'){
            $this->load->model('Organizations_model', 'organizations');
            $data = $this->appointments->analyticsData($company_id, $md_id, $schedule, 'patient_company_id');
            foreach($data as &$rec2){
                $company = $this->organizations->get_company_details_by_id($rec2['column_select']);
                $rec2['company'] = $company['company_name'];
            }
        }
        echo json_encode($data);
    }

    public function get_users_age() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data['data'] = $this->Dashboard_model->get_users_age($company_name, $post);
        $data['total_registered'] = $this->Users_model->getUserCountPerCompany($company_name, $post);
        $data['total_respondents'] = $this->Dashboard_model->getUserCountPerSurveyStatus($company_name, $post);

        echo json_encode($data, true);
    }

    public function get_users_health() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data = $this->Dashboard_model->get_users_health($company_name, $post);

        echo json_encode($data, true);
    }

    public function get_users_readiness() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $filter = $this->input->post('filter');
        $data = $this->Dashboard_model->get_users_readiness($company_name, $filter);

        if($filter == 'age'){
            foreach($data as &$rec){
                $rec['name'] = $this->_formatAgeAgroup($rec['name']);
            }
        }
        
        echo json_encode($data, true);
    }
    
    private function _formatAgeAgroup($age){
        if($age <= 9){
            return '0 - 9';
        }else if(in_array($age, range(10, 19))){
            return '10 - 19';
        }else if(in_array($age, range(20, 29))){
            return '20 - 29';
        }else if(in_array($age, range(30, 39))){
            return '30 - 39';
        }else if(in_array($age, range(40, 49))){
            return '40 - 49';
        }else if(in_array($age, range(50, 59))){
            return '50 - 59';
        }else if(in_array($age, range(60, 69))){
            return '60 - 69';
        }else if(in_array($age, range(70, 79))){
            return '70 - 79';
        }
    }

    public function get_users_health_score() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data = $this->Dashboard_model->get_users_health_score($company_name, $post);

        echo json_encode($data, true);
    }

    public function get_msq_scattered() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data = $this->Dashboard_model->get_msq_scattered($company_name, $post);
        echo json_encode($data, true);
    }

    public function get_msq_score() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data = $this->Dashboard_model->get_users_msq_score($company_name, $post);
        echo json_encode($data, true);
    }

    function getSiteAnalyticsData($company_id = 0, $company_name = null) {
        $return_data = array();
        $report_data = [];
        $pct_total_unique_login = 0;
        $pct_unique_login_today = 0;
        $unique_login_today = 0;
        $total_unique_login = 0;
        $total_users_atleast1_complete = 0;
        $total_users_with_submitted = 0;
        $total_users = 0;
        $total_dashboard_logins = 0;
        $compliance_rate = 0;

        //  get total number of users under company $company_name
        $allUsers = $this->Users_model->getAllUsersPerCompany($company_name);

        foreach($allUsers as $user){
            if($user['status'] != 3){
                $total_users++;
                
                if ($user['status'] != null) {
                    $total_dashboard_logins++;
                }
            }
        }
        
        if ($total_users > 0) {
            //  get unique logins per date
            $total_unique_login = $this->activity->getTotalUniqueLogins($company_name);
            $total_unique_login = (int) $total_unique_login['count'];
            $unique_login_today = $this->activity->getTotalUniqueLogins($company_name, date('Y-m-d'));
            $unique_login_today = (int) $unique_login_today['count'];

            if ($total_unique_login > 0) {
                //  get percentage
                $pct_total_unique_login = ($total_unique_login / $total_users) * 100;
                $pct_total_unique_login = number_format((float) $pct_total_unique_login, 2, '.', '');
            }
            if ($unique_login_today > 0) {
                //  get percentage
                $pct_unique_login_today = ($unique_login_today / $total_users) * 100;
                $pct_unique_login_today = number_format((float) $pct_unique_login_today, 2, '.', '');
            }
            if($total_dashboard_logins > 0){
                //  get percentage
                $pct_dashboard_login = ($total_dashboard_logins / $total_users) * 100;
                $pct_dashboard_login = $total_dashboard_logins / $total_users == 1 ? $total_dashboard_logins / $total_users * 100 : number_format((float) $pct_dashboard_login, 2, '.', '');
            }

            $health_survey = $this->_getHealthSurveyData($company_name, $total_users);
            $report_data = $this->Dashboard_model->getSurveyStatusReportData($company_name);
            
            $overallStatus = "Incomplete";
            $health_arr = array(
                'health_issues', 'health_goals', 'readiness', 'weight', 'detox', 'movement', 'nutrition',
                'sleep', 'stress', 'activity', 'eyes_ears_nose', 'head_and_mind', 'mouth_throat_lungs',
                'other', 'weight_and_digestion'
            );
            $incomplete = 0;
            $complete = 0;
            foreach($report_data as &$data){
                $complete_cnt = 0;
                $overallStatus = "Incomplete";
                foreach($health_arr as $health){
                    if($data[$health] == 'Complete'){
                        $complete_cnt ++;
                    }
                }
                if($data['healthsurvey_status'] != null){
                    if($complete_cnt == 15){
                        $overallStatus = 'Complete';
                        if($data['healthsurvey_status'] == 'Active'){
                            $complete++;
                        }
                    }else{
                        $incomplete++;
                    }
                }
                $survey_status = '-';
                if($data['healthsurvey_status'] !== null){
                    $survey_status = $data['healthsurvey_status'];
                }
                
                $data['overallStatus'] = $overallStatus;
                $data['survey_status'] = $survey_status;
            }
            $total_users_atleast1_complete = $health_survey['survey_employee_cnt'];
            $total_users_with_submitted = $health_survey['total_users_with_complete'];
            
            if ($total_users_with_submitted > 0 && $company_name != null) {
                $compliance_rate = ($total_users_with_submitted / $total_dashboard_logins) == 1 ? ($total_users_with_submitted / $total_dashboard_logins) * 100 : number_format(($total_users_with_submitted / $total_dashboard_logins) * 100, 2, '.', '');
            }
        }
        $return_data['report_data'] = $report_data;
        $return_data['pct_unique_login_today'] = $pct_unique_login_today;
        $return_data['pct_total_unique_login'] = $pct_total_unique_login;
        $return_data['unique_login_today'] = $unique_login_today;
        $return_data['total_unique_login'] = $total_unique_login;
        $return_data['total_users'] = $total_users;
        $return_data['total_dashboard_logins'] = $total_dashboard_logins;
        $return_data['pct_dashboard_login'] = $pct_dashboard_login;
        $return_data['anonymous'] = $this->Users_model->getUserCountPerCompanyPerStatus($company_name, Users_model::$ANONYMOUS);
        $return_data['no_answer'] = $this->Users_model->getUserCountPerCompanyPerStatus($company_name, Users_model::$NEW_USER, true);
        $return_data['total_users_atleast1_complete'] = $total_users_atleast1_complete;
        $return_data['total_users_with_submitted'] = $total_users_with_submitted;
        $return_data['complete'] = $complete;
        $return_data['incomplete'] = $incomplete;
        $return_data['compliance_rate'] = $compliance_rate;
        return $return_data;
    }
    
    private function _getHealthSurveyData($company_name, $total_users){
        $total_users_with_complete = $this->Dashboard_model->getUserCountPerSurveyStatus($company_name);
        $total_users_with_active_survey = $this->Dashboard_model->getUserCountPerSurveyStatus($company_name, array(), 'Active');
        $compliance_rate = 0;

        if ($total_users_with_complete > 0 && $company_name != null) {
            $compliance_rate = $total_users_with_complete / $total_users_with_active_survey == 1 ? $total_users_with_complete / $total_users_with_active_survey * 100 : number_format(($total_users_with_complete / $total_users_with_active_survey) * 100, 2, '.', '');
        }
        
        return array(
            'compliance_rate'           => $compliance_rate,
            'total_users_with_complete' => $total_users_with_complete,
            'survey_employee_cnt'       => $total_users_with_active_survey
        );
    }

    function upstream() {
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company_name = trim($this->Organizations_model->get_companyname_by_id($company_id));
        $filter = $this->input->post('filter');

        $upstream_data = $this->Dashboard_model->getUpstreamData($company_name, $filter);
        $data['upstream_table_data'] = $this->_processUpstreamTableData($upstream_data);

        echo $this->load->view('dashboard/upstream_manifestations', $data);
    }

    function upstream_tables() {
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company_name = trim($this->Organizations_model->get_companyname_by_id($company_id));
        $table = $this->input->post('table');
        $filter = $this->input->post('filter');

        switch ($table) {
            case 'foodchoices':
                $data['data'] = $this->Dashboard_model->getFoodChoices($company_name, $filter);
                break;

            case 'stresssource':
                $data['data'] = $this->Dashboard_model->getStressSourceRank($company_name, $filter);
                break;

            case 'systemrank':
                $data['data'] = $this->Dashboard_model->getMSQSystemRank($company_name, $filter);
                break;

            case 'symptoms':
                $data['data'] = $this->Dashboard_model->getPrevalentSymptoms($company_name, $filter);
                break;
        }
        $data['table'] = $table;
        $data['total_respondents'] = $this->Dashboard_model->getTotalRespondents($company_name, $filter);
        echo $this->load->view('dashboard/upstream_tables', $data);
    }

    private function _processUpstreamTableData($data = array()) {
        $total_weight = 0;
        $weight_data = [];
        $total_height = 0;
        $height_data = [];
        $total_bmi = 0;
        $bmi_data = [];
        $total_waist = 0;
        $waist_data = [];
        $total_hip = 0;
        $hip_data = [];
        $total_whr = 0;
        $whr_data = [];
        $sleep_cnt_6_to_8 = 0;
        $sleep_cnt_more_than_10 = 0;
        $sleep_cnt_8_to_10 = 0;
        $sleep_cnt_less_than_6 = 0;
        $total_stress = 0;
        $stress_data = [];
        $bmi_cnt_below = 0;
        $bmi_cnt_avg = 0;
        $bmi_cnt_above = 0;
        $bmi_cnt_further = 0;
        $total_tobaccoExposure = 0;
        $total_tobaccoExposure_respondents = 0;
        $total_caffeineexposure = 0;
        $total_caffeineexposure_respondents = 0;
        $total_chemical = 0;
        $total_chemical_respondents = 0;
        $total_exercise = 0;
        $total_exercise_respondents = 0;
        $total_sleeping = 0;
        $total_sleeping_respondents = 0;
        $total_stress_counseling = 0;
        $total_stress_counseling_respondents = 0;
        $total_stress_therapy = 0;
        $total_stress_therapy_respondents = 0;

        foreach ($data as $value) {
            if ($value['weight'] != null) {
                $total_weight += (float) $value['weight'];
                $weight_data[] = (float) $value['weight'];
            }
            if ($value['height'] != null) {
                $total_height += (float) $value['height'];
                $height_data[] = (float) $value['height'];
            }
            if ($value['bmi'] != null) {
                $total_bmi += (float) $value['bmi'];
                $bmi_data[] = (float) $value['bmi'];

                if ((float) $value['bmi'] < 18.5) {
                    $bmi_cnt_below++;
                } else if ((float) $value['bmi'] < 23) {
                    $bmi_cnt_avg++;
                } else if ((float) $value['bmi'] < 27) {
                    $bmi_cnt_above++;
                } else {
                    $bmi_cnt_further++;
                }
            }
            if ($value['waist'] != null) {
                $total_waist += (float) $value['waist'];
                $waist_data[] = (float) $value['waist'];
            }
            if ($value['hip'] != null) {
                $total_hip += (float) $value['hip'];
                $hip_data[] = (float) $value['hip'];
            }
            if ($value['whr'] != null) {
                $total_whr += (float) $value['whr'];
                $whr_data[] = (float) $value['whr'];
            }
            if ($value['averagesleep'] != null) {
                if ($value['averagesleep'] == '>10') {
                    $sleep_cnt_more_than_10++;
                } else if ($value['averagesleep'] == '8-10') {
                    $sleep_cnt_8_to_10++;
                } else if ($value['averagesleep'] == '6-8') {
                    $sleep_cnt_6_to_8++;
                } else if ($value['averagesleep'] == '<6') {
                    $sleep_cnt_less_than_6++;
                }
            }
            if ($value['overallstressscore'] != null) {
                $total_stress += (float) $value['overallstressscore'];
                $stress_data[] = (float) $value['overallstressscore'];
            }
            if ($value['stress_counseling'] != null) {
                $total_stress_counseling += ($value['stress_counseling'] == 'Yes' ? 1 : 0);
                $total_stress_counseling_respondents++;
            }
            if ($value['stress_therapy'] != null) {
                $total_stress_therapy += ($value['stress_therapy'] == 'Yes' ? 1 : 0);
                $total_stress_therapy_respondents++;
            }
            if ($value['tobaccoexposure'] != null) {
                $total_tobaccoExposure += ($value['tobaccoexposure'] == 'Yes' ? 1 : 0);
                $total_tobaccoExposure_respondents++;
            }
            if ($value['caffeineexposure'] != null) {
                $total_caffeineexposure += ($value['caffeineexposure'] == "Don't Know" ? 1 : 0);
                $total_caffeineexposure_respondents++;
            }
            if ($value['chemicallydependent'] != null) {
                $total_chemical += ($value['chemicallydependent'] == "Yes" ? 1 : 0);
                $total_chemical_respondents++;
            }
            if ($value['exercise150min'] != null) {
                $total_exercise += ($value['exercise150min'] == "Yes" ? 1 : 0);
                $total_exercise_respondents++;
            }
            if ($value['usesleepingaids'] != null) {
                $total_sleeping += ($value['usesleepingaids'] == "Yes" ? 1 : 0);
                $total_sleeping_respondents++;
            }
        }

        $result = array(
            "mean_data" => array(
                "weight" => $total_weight > 0 ? number_format(($total_weight / count($weight_data)), 2, '.', '') : 0,
                "height" => $total_height > 0 ? number_format(($total_height / count($height_data)), 2, '.', '') : 0,
                "bmi" => $total_bmi > 0 ? number_format(($total_bmi / count($bmi_data)), 2, '.', '') : 0,
                "waist" => $total_waist > 0 ? number_format(($total_waist / count($waist_data)), 2, '.', '') : 0,
                "hip" => $total_hip > 0 ? number_format(($total_hip / count($hip_data)), 2, '.', '') : 0,
                "whr" => $total_whr > 0 ? number_format(($total_whr / count($whr_data)), 2, '.', '') : 0,
                "stress" => $total_stress > 0 ? number_format(($total_stress / count($stress_data)), 2, '.', '') : 0
            ),
            "median_data" => array(
                "weight" => count($weight_data) > 0 ? $this->_getMedian($weight_data) : 0,
                "height" => count($height_data) > 0 ? $this->_getMedian($height_data) : 0,
                "bmi" => count($bmi_data) > 0 ? $this->_getMedian($bmi_data) : 0,
                "waist" => count($waist_data) > 0 ? $this->_getMedian($waist_data) : 0,
                "hip" => count($hip_data) > 0 ? $this->_getMedian($hip_data) : 0,
                "whr" => count($whr_data) > 0 ? $this->_getMedian($whr_data) : 0,
                "stress" => count($stress_data) > 0 ? $this->_getMedian($stress_data) : 0
            ),
            "weight_cnt" => count($weight_data),
            "height_cnt" => count($height_data),
            "bmi_cnt" => count($bmi_data),
            "waist_cnt" => count($waist_data),
            "hip_cnt" => count($hip_data),
            "whr_cnt" => count($whr_data),
            "sleep_cnt_6_to_8" => $sleep_cnt_6_to_8,
            "sleep_cnt_more_than_10" => $sleep_cnt_more_than_10,
            "sleep_cnt_8_to_10" => $sleep_cnt_8_to_10,
            "sleep_cnt_less_than_6" => $sleep_cnt_less_than_6,
            "stress_cnt" => count($stress_data),
            "bmi_cnt_below" => $bmi_cnt_below,
            "bmi_cnt_avg" => $bmi_cnt_avg,
            "bmi_cnt_above" => $bmi_cnt_above,
            "bmi_cnt_further" => $bmi_cnt_further,
            "total_stress_counseling" => $total_stress_counseling,
            "total_stress_counseling_respondents" => $total_stress_counseling_respondents,
            "total_stress_therapy" => $total_stress_therapy,
            "total_stress_therapy_respondents" => $total_stress_therapy_respondents,
            "total_tobaccoExposure" => $total_tobaccoExposure,
            "total_tobaccoExposure_respondents" => $total_tobaccoExposure_respondents,
            "total_caffeineexposure" => $total_caffeineexposure,
            "total_caffeineexposure_respondents" => $total_caffeineexposure_respondents,
            "total_chemical" => $total_chemical,
            "total_chemical_respondents" => $total_chemical_respondents,
            "total_exercise" => $total_exercise,
            "total_exercise_respondents" => $total_exercise_respondents,
            "total_sleeping" => $total_sleeping,
            "total_sleeping_respondents" => $total_sleeping_respondents
        );

        return $result;
    }

    private function _getMedian($data) {
        $median = 0;
        $numsLen = count($data);

        sort($data);

        if ($numsLen % 2 === 0) { // is even
            // average of two middle numbers
            $median = (($data[($numsLen / 2) - 1]) + ($data[$numsLen / 2])) / 2;
        } else { // is odd
            // middle number only
            $median = $data[($numsLen - 1) / 2];
        }
        return number_format((float) $median, 2, '.', '');
    }

    public function get_users_doh() {
        $role = $this->session->userdata['logged_in']['role'];
        $company_name = '';
        if ($role != '1') {
            $company_id = $this->session->userdata['logged_in']['company_id'];
            $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        }
        $post = $this->input->post();
        $data = $this->Dashboard_model->get_users_doh($company_name, $post);

        $return = array();
        if(empty($data)){
            $return['sleep'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
            $return['weightScore'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
            $return['nutrition'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
            $return['exercise'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
            $return['stress'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
            $return['detox'] = array(
                'mean' => 0,
                'median' => 0,
                'color' => null,
                'min_median' => 0,
                'max_median' => 0
            );
        }else{
            foreach ($data as $row) {
                $median_values = explode(", ", $row['median_values']);
                $return[$row['name']] = array(
                    'mean' => $row['mean_value'],
                    'median' => count($median_values) > 0 ? $this->_getMedian($median_values) : 0,
                    'color' => $this->_getColor($row['mean_value']),
                    'min_median' => number_format($median_values[0], 2, '.', ''),
                    'max_median' => number_format($median_values[count($median_values) - 1], 2, '.', '')
                );
            }
        }
        echo json_encode($return, true);
    }

    private function _getColor($score = 0) {
        if ($score >= 0 && $score < 3) {
            return '#72a153'; //green
        } else if ($score >= 3.0 && $score < 5) {
            return '#71b2ca'; //blue
        } else if ($score >= 5.0 && $score < 7) {
            return '#dfaf29'; //yellow
        } else if ($score >= 7.0 && $score < 9) {
            return '#d77f1a'; //orange
        } else if ($score >= 9.0 && $score <= 10.0) {
            return 'red';
        }
    }

    function ajax_getDepartmentsPerBranch() {
        $company_id = $this->session->userdata['logged_in']['company_id'];
        $company_name = $this->Organizations_model->get_companyname_by_id($company_id);
        $departments = $this->Organizations_model->get_departments_per_branch($company_name, $this->input->post('branch'));

        $options = array();
        foreach ($departments as $row) {
            $options[] = $row['department'];
        }
        echo json_encode($options);
    }
    
    function getLoginAnalyticsGraph($company_name = null){
        $graphData = $this->activity->getLoginGraphData(urldecode($company_name));
        $last10Days = $this->getLastNDays(10, 'M d');
        $last10Days = array_combine($last10Days, $last10Days);
        //set all values to 0
        $last10Days = array_map(create_function('$n', 'return 0;'), $last10Days);
        foreach ($graphData as $rec) {
            $last10Days[$rec['created_date']] = (int) $rec['count'];
        }
        echo json_encode($last10Days);
    }
    
    function getAdminAnalyticsData(){
        $this->load->model('Organizations_model', 'organizations');
        $companies = $this->organizations->company_analytics();
        
        foreach($companies as &$cmpy){            
            $total_unique_login = $this->activity->getTotalUniqueLogins($cmpy['company_name']);
            $cmpy['total_unique_system_login'] = (int) $total_unique_login['count'];
            
            $health_survey = $this->_getHealthSurveyData($cmpy['company_name'], $cmpy['total_enrolled']);
            $cmpy['compliance_rate'] = $health_survey['compliance_rate'];
        }

        $results['data'] = $companies;
        echo json_encode($results);
    }
    
    function getMDAdminAnalyticsData(){
        $post = $this->input->post();
        $company_id = $post['company'];
        $md_id = $post['doctors'];
        $schedule_from = $post['schedule_from'];
        $schedule_to = $post['schedule_to'];
        
        $this->load->model('Appointments_model', 'appointments');

        $schedule['from'] = $schedule_from;
        $schedule['to'] = $schedule_to;
        $r['result'] = $this->appointments->getAppointmentCountByStatus($company_id, $md_id, $schedule);

        echo json_encode($r);
    }
    
    function getLastNDays($days, $format = 'd/m') {
        $m = date("m");
        $de = date("d");
        $y = date("Y");
        $dateArray = array();
        for ($i = 0; $i <= $days - 1; $i++) {
            $dateArray[] = date($format, mktime(0, 0, 0, $m, ($de - $i), $y));
        }
        return array_reverse($dateArray);
    }
    
    function getTickets(){
        $this->load->model('Tickets_model', 'tickets');
        $d['tickets'] = $this->tickets->getAll();
        
        echo $this->load->view('tickets/index', $d, true);
    }
    
    function showTicketInfo($id=0){
        $this->load->model('Tickets_model', 'tickets');
        $info = $this->tickets->getRow(array('id' => $id));
         
        $d['ticket_info'] = $info;
        $d['ticket_replies'] = $this->tickets->getReplies($info['ticket_number']);
        
        echo $this->load->view('tickets/details', $d, true);
    }
    
    function replyToTicket(){
        $this->load->model('Tickets_model', 'tickets');
        $post = $this->input->post();
        $ticket_id = $post['x'];
        $old_status = $post['old_status'];
        $update_status = ($old_status !== $post['status']);
        $r = array();
        $r['result']['errors'] = null;
        $r['result']['success'] = 0; // failed default status
        
        $ticket = $this->tickets->getRow(array('id' => $ticket_id));
        
        $data = array(
            'ticket_number' => $ticket['ticket_number'],
            'reply_message' => $post['reply'],
            'repliedBy_userid' => $this->session->userdata['logged_in']['userid'],
            'datetime_reply' => date(DateTime::ISO8601)
        );
        
        if($reply = $this->tickets->reply($data)){
            //update ticket status
            $this->tickets->updateTicket($ticket_id, array('status' => $post['status']));
            $r['result']['success'] = 1;
            $r['result']['message'] = $post['reply'];
            $r['result']['replied']['date'] = date('M j, Y h:i:s A', strtotime($reply['datetime_reply']));
            $r['result']['replied']['user'] = $this->session->userdata['logged_in']['name'];
        }else{
            $r['result']['errors'] = "An error was encountered while updating the ticket. Please try again later.";
        }
        echo json_encode($r);
    }
    
    function getPatientOutcomesLineChart(){
        $post = $this->input->post();
        
        $date_from = $post['date_from'] ? $post['date_from'] : date('j M, Y', strtotime('-1 year'));
        $date_to = $post['date_to'] ? $post['date_to'] : date('j M, Y');

        $date_from_ymd = date('Y-m-d', strtotime($date_from));
        $date_to_ymd = date('Y-m-d', strtotime($date_to));
        
        $filter['consult_date_from'] = $date_from_ymd;
        $filter['consult_date_to'] = $date_to_ymd;

        $this->load->model('Consults_Model', 'consults');
        $data = $this->consults->getConsults($filter);

        if(isset($post['show_only_uniques'])){
            $data = $this->_cleanPatientOutcomesData($data);
            $chart_data = $this->_processLineGraphData_unique($data, $date_from_ymd, $date_to_ymd);
        }else{
            $data = $this->consults->getConsults($filter);
            $chart_data = $this->_processLineGraphData($data, $date_from_ymd, $date_to_ymd);
        }

        echo json_encode(array('line_data' => $chart_data, 'line_daterange' => array('from' => strtoupper(date("F Y", strtotime($date_from_ymd))), 'to' => strtoupper(date("F Y", strtotime($date_to_ymd))))));
    }
    
    function getPatientOutcomesAnalyticsData(){
        $post = $this->input->post();
        
        $this->load->model('Organizations_model', 'organizations');
        $company = $this->organizations->get_company_details_by_id($post['company_filter']);
        
        $filter['company'] = $company['company_name'];
        $filter['doctor_id'] = $post['doctors_filter'];

        $this->load->model('Consults_Model', 'consults');
        $data = $this->consults->getConsults($filter);
        
        $employee_data = $this->_cleanPatientOutcomesData($data);
        $chart_data = $this->_processPieGraphData($employee_data);
        
        echo json_encode(array('data' => $chart_data));
    }
    
    function _getMonths($start_date, $end_date){
        $date_arr = [];

        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);
        $interval = DateInterval::createFromDateString('1 month');

        if(date("m", strtotime($start_date)) === date("m", strtotime($end_date)) && 
                date("d", strtotime($end_date)) <= date("d", strtotime($start_date)) ){
            //add one month
            $end = new DateTime(date("Y-m-d", strtotime($end_date. " +1 month")));
        }
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
            $date_arr[$dt->format("M Y")] = 0;
        }

        return $date_arr;
    }
    
    function _processLineGraphData_unique($data, $date_from=null, $date_to=null){
        $new_array2 = $this->_getMonths($date_from, $date_to);
        
        foreach($data as $rec){
            $ctr = 0;
            $improving_ctr = 0;
            $notImproving_ctr = 0;
            $forImmediateFollowUp_ctr = 0;
            $followUpNextMonth_ctr = 0;
            $stalledOrNeedsRecovery_ctr = 0;
            if(isset($new_array2[$rec['consult_date_month_year']])){
                $ctr++;
                if($rec['outcome_status'] == 'improving')   $improving_ctr++;
                if($rec['outcome_status'] == 'notImproving')   $notImproving_ctr++;
                if($rec['outcome_status'] == 'forImmediateFollowUp')   $forImmediateFollowUp_ctr++;
                if($rec['outcome_status'] == 'followUpNextMonth')   $followUpNextMonth_ctr++;
                if($rec['outcome_status'] == 'stalledOrNeedsRecovery')   $stalledOrNeedsRecovery_ctr++;

                $new_array2[$rec['consult_date_month_year']] = array(
                    'total' => $ctr,
                    'improving' => $improving_ctr,
                    'notImproving' => $notImproving_ctr,
                    'forImmediateFollowUp' => $forImmediateFollowUp_ctr,
                    'followUpNextMonth' => $followUpNextMonth_ctr,
                    'stalledOrNeedsRecovery' => $stalledOrNeedsRecovery_ctr);
            }
        }
        return $new_array2;
    }
    
    function _processLineGraphData($data, $date_from=null, $date_to=null){
        $new_array2 = $this->_getMonths($date_from, $date_to);

        foreach($data as $rec){
            $details = json_decode($rec['details'], true);
            if(!isset($new_array2[$rec['consult_date_month_year']]['total'])){
                if($details['healthoutcome_details'] == 'improving')   $improving_ctr++;
                elseif($details['healthoutcome_details'] == 'notImproving')   $notImproving_ctr++;
                elseif($details['healthoutcome_details'] == 'forImmediateFollowUp')   $forImmediateFollowUp_ctr++;
                elseif($details['healthoutcome_details'] == 'followUpNextMonth')   $followUpNextMonth_ctr++;
                elseif($details['healthoutcome_details'] == 'stalledOrNeedsRecovery')   $stalledOrNeedsRecovery_ctr++;
                $new_array2[$rec['consult_date_month_year']] = array(
                    'total' => 0,
                    'improving' => 0,
                    'notImproving' => 0,
                    'forImmediateFollowUp' => 0,
                    'followUpNextMonth' => 0,
                    'stalledOrNeedsRecovery' => 0
                );
            }
            $new_array2[$rec['consult_date_month_year']]['total']++;
            $new_array2[$rec['consult_date_month_year']]['improving'] += ($details['healthoutcome_details'] == 'improving') ? 1 : 0;
            $new_array2[$rec['consult_date_month_year']]['notImproving'] += ($details['healthoutcome_details'] == 'notImproving') ? 1 : 0;
            $new_array2[$rec['consult_date_month_year']]['forImmediateFollowUp'] += ($details['healthoutcome_details'] == 'forImmediateFollowUp') ? 1 : 0;
            $new_array2[$rec['consult_date_month_year']]['followUpNextMonth'] += ($details['healthoutcome_details'] == 'followUpNextMonth') ? 1 : 0;
            $new_array2[$rec['consult_date_month_year']]['stalledOrNeedsRecovery'] += ($details['healthoutcome_details'] == 'stalledOrNeedsRecovery') ? 1 : 0;
        }
        return $new_array2;
    }
    
    function _processPieGraphData($data){
        $new_array = [];

        foreach($data as $rec){
            $new_array[$rec['outcome_status']][] = $rec;
        }
        return $new_array;
    }
    
    function _cleanPatientOutcomesData($data = array()) {
        $result = [];
        foreach($data as $row) {
            $details = json_decode($row['details'], true);
            if (!isset($result[$row['patient_id']])) {
                $result[$row['patient_id']] = array
                    (
                    'name' => $row['name'],
                    'age_sex' => $row['age'].' / '.$row['gender'],
                    'employee_id' => $row['employee_id'],
                    'outcome_status' => $details['healthoutcome_details'],
                    'outcome_status_label' => $this->_getConsultStatus($details['healthoutcome_details']),
                    'consult_date_month_year' => $row['consult_date_month_year'],
                    'previous_outcome_status' => 'No Previous Consult'
                );
            }else{
                $result[$row['patient_id']]['previous_outcome_status'] = $result[$row['patient_id']]['outcome_status'];
                $result[$row['patient_id']]['previous_outcome_status_label'] = $result[$row['patient_id']]['outcome_status_label'];
                $result[$row['patient_id']]['outcome_status_label'] = $this->_getConsultStatus($details['healthoutcome_details']);
                $result[$row['patient_id']]['outcome_status'] = $details['healthoutcome_details'];
                $result[$row['patient_id']]['consult_date_month_year'] = $row['consult_date_month_year'];
            }
        }
        return $result;
    }
    
    function _getConsultStatus($status){
            switch($status){
                case 'improving':
                    return 'Improving';
                case 'notImproving':
                    return 'Not improving';
                case 'forImmediateFollowUp':
                    return 'For immediate follow-up';
                case 'followUpNextMonth':
                    return 'Follow-up next month';
                case 'stalledOrNeedsRecovery':
                    return 'Stalled or needs recovery';
                
            }
    }

}