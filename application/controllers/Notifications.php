<?php

class Notifications extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            redirect('login');
        }
        $this->load->model('Announcements_model', 'announcements');
    }
    
    function index(){
        $userid = (int)$this->session->userdata['logged_in']['userid'];
        $company_enrolled_date = null;
        
        if(admin_role() == Users_model::HR){
            $company_enrolled_date = $this->session->userdata['logged_in']['company_enrolled_date'];

            $admin_id = $this->Users_model->getSuperAdminID();
            $my_where['created_by'] = $admin_id['id'];
            $my_where['e.date_created >= '] = $company_enrolled_date;
            $my_where["'HR' = ANY (string_to_array(audience, ', '))"] = null;
            $d['announcements'] = $this->announcements->getAll($my_where);
        }
        $my_notifications = $this->notif->getNotificationsByUser($userid, $type='admin', null, $company_enrolled_date);
        
        $d['v'] = 'notifications/main';
        $d['my_notifications'] = $my_notifications;
        $this->load->view('template', $d);
    }
    
    function showNotificationMessage(){
        $data = $this->input->post();
        $id = $data['id'];
        $is_read = $data['is_read'];
        $notification = $this->notif->getNotificationById($id, $is_read);
        $return['datetime'] = date('M j, Y g:i:s A', strtotime($notification['date_created']));
        $return['message'] = $notification['message'];
        $return['header'] = $notification['subject'];

        echo json_encode($return);
    }
    
    function showAnnouncementDetails(){
        $data = $this->input->post();
        $id = $data['id'];
        $is_read = $data['is_read'];
        $announcement = $this->announcements->getAnnouncementById($id, $is_read);
        $d['datetime'] = date('M j, Y g:i:s A', strtotime($announcement['date_created']));
        $d['message'] = $announcement['details'];
        $d['header'] = $announcement['title'];

        echo $this->load->view('notifications/announcement-details', $d, true);
    }
    
    function getNotification() {
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            echo json_encode(array("data" => false));
            die;
        }
        $notifications = $this->cloudhealth->getNotifications();
        echo json_encode(array('data' => $notifications));
    }
}
