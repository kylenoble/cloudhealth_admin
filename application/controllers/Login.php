<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {

        parent::__construct();


        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->load->model('AdminTokens_Model', 'tokens');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $d['success_msg'] = '';
        if (isset($this->session->userdata['logged_in']['userid'])) {
            if ($this->session->userdata['logged_in']['status'] != 1) {
                redirect(base_url() . 'login/change_password');
            }
            redirect(base_url() . 'dashboard');
        }

        if ($this->session->flashdata('success_msg')) {
            $d['success_msg'] = $this->session->flashdata('success_msg');
        }
        // validation for email and password
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('pass', 'Password', 'required');

        $d['is_hidden'] = true;
        // check if valid data is submitted
        if ($this->form_validation->run() == FALSE) {
            $d['v'] = 'login';
            $this->load->view('template', $d);
        } else {

            $data = array(
                'email' => $this->input->post('username'),
                'pass' => base64_encode($this->input->post('pass'))
            );

            $this->load->model('login_model');
            $auth = $this->login_model->auth_user($data);
            $id = $auth['id'];

            if ($id > 0) {
                // if user valid

                if (isset($id)) {
                    //check if user is hr admin and company is active
                    if ($auth['role'] == Users_model::HR) {
                        $this->load->model('Organizations_model');
                        $company = $this->Organizations_model->get_company_details_by_id($auth['company_id']);
                        if ($company['is_active'] == 'f') {
                            $d['v'] = 'login';
                            $d['error'] = _get_error('Invalid email or Password');
                            $this->load->view('template', $d);
                            return false;
                        }
                    }
                    // check if user needs to change initial password
                    if ($auth['role'] != Users_model::MD && $auth['status'] == null) {
                        //set temporary session for the userid
                        $this->session->set_userdata('userid', $id);
                        $d['v'] = 'change-password';
                        $d['initial'] = true;
                        $d['title_change_password'] = '<p style="font-style: italic;text-align: left;font-size: 18px;">Please update your password</p>';
                        $this->load->view('template', $d);
                    } else {
                        $session_data = array(
                            'userid' => $id,
                            'name' => trim($auth['name']),
                            'email' => $auth['email'],
                            'role' => $auth['role'],
                            'status' => $auth['status']
                        );
                        if ($auth['role'] == Users_model::HR) {
                            //get enrolled date of the company used for filtering announcements and notifications
                            $session_data['company_id'] = $auth['company_id'];
                            $session_data['company_enrolled_date'] = $company['date_created'];
                        } elseif (in_array($auth['role'], array(Users_model::MD, Users_model::OPS, Users_model::SALES, Users_model::FINANCE))) {
                            if($auth['status'] == null){
                                //update status to 1 = ACTIVE
                                $update_data['status'] = Users_model::$ACTIVE;
                                $this->Users_model->update_admin($update_data, $id);
                            }
                            $session_data['is_admin'] = $auth['is_group_head'];
                        }
                        $this->session->set_userdata('logged_in', $session_data);
                        redirect(base_url() . 'dashboard');
                    }
                } else {
                    // if user not valid
                    $d['v'] = 'login';
                    $d['error'] = _get_error('Invalid email or Password');
                    $this->load->view('template', $d);
                }
            } else {
                $d['v'] = 'login';
                $d['error'] = _get_error('Invalid email or Password');
                $this->load->view('template', $d);
            }
        }

        // $this->load->view('login');
    }

    /**
     * forgot password Page for this controller.
     */
    public function forgot_password() {

        if (isset($this->session->userdata['logged_in']['userid'])) {
            redirect(base_url() . 'dashboard');
        }

        // validation for email
        $this->form_validation->set_rules('registered_email', 'Email', 'required|valid_email');

        $d['is_hidden'] = true;
        // check if valid data is submitted
        if ($this->form_validation->run() == FALSE) {
            $d['v'] = 'forgot-password';
            $this->load->view('template', $d);
        } else {
            // check if email exists in db
            $user = $this->Users_model->validate_AdminEmail($this->input->post('registered_email'));

            if ($user) {
                // generate new password
                $password = $this->cloudhealth->random_password();

                //  update admin user's password prior to sending to email
                $data = array('password' => $password['plain'], 'status' => null);
                $this->Users_model->update_admin($data, $user['id']);
                $user['password'] = $password['plain'];
                $user['page'] = 'forgot_password';

                // send email to reset password
                $html = $this->load->view('templates/email', $user, true);

                $this->load->library('email');
                $this->email->from('client.services@cloudhealthasia.com', 'CloudHealthAsia');
                $this->email->to($this->input->post('registered_email'));
                $this->email->subject('Password Reset');
                $this->email->message($html);
                $sent = $this->email->send();
//                $sent = true;
                if (!$sent) {
                    // mail not sent
                    $d['v'] = 'forgot-password';
                    $d['error'] = _get_error('Something went wrong. Please try again later.');
                    $this->load->view('template', $d);
                } else {
                    // mail was successfully sent
                    $d['v'] = 'forgot-password';
                    $d['success_msg'] = 'Your new password was sent to your email address.';
                    $this->load->view('template', $d);
                }
            } else {
                // if email address cant be found
                $d['v'] = 'forgot-password';
                $d['error'] = _get_error('Sorry, that Email Address does not exist.');
                $this->load->view('template', $d);
            }
        }
    }

    /**
     * display the user form and
     * submit the reset password request with md5 password
     *
     * @param string $uid
     */
    public function change_password($initial_login = false) {
        if (!isset($this->session->userdata['logged_in']['userid']) && !$initial_login) {
            redirect(base_url() . 'login');
        }

        // validation for password
        $this->form_validation->set_rules('password_new', 'New Password', 'required');
        $this->form_validation->set_rules('cpassword_new', 'Confirm Password', 'required|matches[password_new]');
//        if(!$initial_login){
//            $this->form_validation->set_rules('password_old', 'Current Password', 'required|callback_validateOldPassword');
//            $this->form_validation->set_message('validateOldPassword', 'Current Password is incorrect.');
//        }
        $this->form_validation->set_message('matches', 'Passwords do not match.');
        // check if valid data is submitted
        if ($this->form_validation->run() == FALSE) {
            $d['initial'] = $initial_login;
            $d['v'] = 'change-password';
            $d['is_hidden'] = true;
            $this->load->view('template', $d);
        } else {
            $password = $this->input->post('password_new');
            $is_md = false;
            if ($this->session->userdata['logged_in']['role'] == Users_model::MD) {
                $password = password_hash($this->input->post('password_new'), PASSWORD_BCRYPT);
                $is_md = true;
            }
            $data = array(
                'password' => $password,
                'status' => 1
            );
            $userid = (isset($this->session->userdata['userid']) ? $this->session->userdata['userid'] : $this->session->userdata['logged_in']['userid']);
            if ($is_md) {
                //get userid from md_admin_tokens table
                $md_token_data = $this->tokens->get($userid, 'user_id');
                
                //update password in users table
                $userUpdate_data['password'] = $password;
                $this->Users_model->update_userInfo($userUpdate_data, $md_token_data['user_id']);
            }
            if ($this->Users_model->update_admin($data, $userid)) {
                $this->logout();
            }
        }
    }

    /**
     * logout the user
     */
    public function logout() {
        // Removing session data
        $sess_array = array(
            'userid' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        session_destroy();
        $d['message_display'] = 'Successfully Logout';
        redirect(base_url('login'));
    }

    function validateOldPassword($old_password) {
        $this->load->model('login_model');
        $data = array(
            'email' => $this->session->userdata['logged_in']['email'],
            'pass' => base64_encode($old_password)
        );
        return $this->login_model->auth_user($data);
    }
    
    private function _authAdminFromMD($user = array()){
        $d['success_msg'] = '';
        $d['is_hidden'] = true;
        $data = array(
            'email' => $user['email'],
            'pass' => base64_encode($user['password']),
            'fromMD_dashboard' => true
        );
        $this->load->model('login_model');
        $auth = $this->login_model->auth_user($data);
        $id = $auth['id'];

        if ($id > 0) {
            $session_data = array(
                'userid' => $id,
                'name' => trim($auth['name']),
                'email' => trim($auth['email']),
                'role' => (int)$auth['role'],
                'status' => $auth['status'],
                'is_admin' => true
            );
            if ($auth['status'] == null) {
                //update status to 1 = ACTIVE
                $update_data['status'] = Users_model::$ACTIVE;
                $this->Users_model->update_admin($update_data, $id);
            }
            $this->session->set_userdata('logged_in', $session_data);
            if($this->session->userdata['logged_in']['userid']){
                redirect(base_url() . 'dashboard');
            }
        } else {
            redirect('/login');
        }
    }
    
    function fromAdminDashboard($qry=null){
        //parse $qry
        $tmp_parsed_qry = base64_decode($qry);
        $parsed_qry = explode("&&1c", $tmp_parsed_qry);
        $token = base64_decode($parsed_qry[0]);
//        echo time(), '<br />';
//        echo (int)$parsed_qry[1], '<br />';
//        var_dump((int)$parsed_qry[1] <= time());
//        die;
        //check if time is valid
        if((int)$parsed_qry[1] <= time()){
            $user = $this->Users_model->getUserAdminByToken($token);
            if($user['is_admin']){
                $this->_authAdminFromMD($user);
            }
        }else{
            redirect('/login');
        }
        $this->load->view('waitme');
    }

}
