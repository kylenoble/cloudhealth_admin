<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Community extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']['userid'])) {
            header(base_url(). index_page().'/login');
        }
        $this->load->model('Events_model', 'events');
        $this->load->model('Announcements_model', 'announcements');
    }
    
    function index($page=null) {
        if(admin_role() == Users_model::ADMIN){
            $default_page = 'analytics';
            if($this->input->post('page') != null){
                $default_page = $this->input->post('page');
            }
            if($page != null){
                $default_page = $page;
            }
            $d['default_page'] = $default_page;
            
            $d['v'] = 'community/admin/index';
        }else{
            $default_page = 'events';
            if($this->input->post('page') != null){
                $default_page = $this->input->post('page');
            }
            if($page != null){
                $default_page = $page;
            }
            $d['default_page'] = $default_page;

            $d['v'] = 'community/hr/index';
        }
        
        $this->load->view('template', $d);
    }
    
    function analytics(){
        $events = $this->events->getAll();
        $joined_cnt = 0;
        $total_hrEvents = 0;
        $avg_attendees = 0;
        foreach($events as $data){
            if($data['created_by'] != 1){
                $total_hrEvents++;
            }
            if($data['read_by'] != null){
                $read_by = json_decode($data['read_by'], true);
                foreach($read_by['data'] as $rec){
                    if($rec['status'] == Events_model::joined){
                        $joined_cnt++;
                    }
                }
            }
        }
        if($joined_cnt > 0){
            $avg_attendees = round($joined_cnt/$total_hrEvents) == 0 ? number_format($joined_cnt/$total_hrEvents, 2, '.', '') : round($joined_cnt/$total_hrEvents);
        }
        $d['no_of_events'] = count($events);
        $d['avg_attendees'] = $avg_attendees;
        echo $this->load->view('community/admin/analytics', $d, true);
    }
    
    function bulletin(){
        $d['health_events_for_hr'] = admin_role() == Users_model::HR || false;
        $default_page = "events";
        if($this->input->post('page') != null){
            $default_page = $this->input->post('page');
        }
        $d['default_page'] = $default_page;
        echo $this->load->view('community/bulletin/main', $d, true);
    }
    
    function event_new($health_talk=false, $company_id=0){
        $d['callback'] = 'event_add';
        $d['title'] = 'CREATE NEW EVENT';
        if($health_talk == 1){
            if(admin_role() == Users_model::ADMIN){
                $companies = $this->subscription->getCompaniesWithBasicPrograms();
                $companies_select = _format_SelectBox($companies, 'id', 'company_name');
                $d['companies'] = $companies_select;
                $d['companies_selected'] = $company_id;
            }
        }
        $d['event_for_basic_programs'] = $health_talk;
        $this->load->view('community/bulletin/events/add_event', $d);
    }
    
    function ajax_getBasicProgramsPerCompany($company_id=0){
        if($company_id == 'undefined'){
            $company_id = (int) $this->session->userdata['logged_in']['company_id'];
        }
        $programs = $this->subscription->getSubscriptionsByCompanyId($company_id);
        $doh_programs = '<option>Please select</option>';
        foreach ($programs as $program) {
            if ($program['is_basic'] == true) {
                //check if with ongoing/active event already
                $where['company_enrollment_id'] = $program['company_enrollment_id'];
                if (!$this->events->getBasicEvents($where)) {
                    //show only basic programs with no event set yet
                    $inclusions = json_decode($program['inclusions'], true);
                    $health_talk = $inclusions['description']['health_talk']['list'];
                    $multiple_htalk_tmp = [];
                    $workshop_tmp = [];
                    foreach($health_talk as $htalk){
                        $workshop_tmp[] = $htalk['description'];
                    }
                    $workshop = implode("~", $workshop_tmp);
                    $doh_programs .= '<option value="'.$program['company_enrollment_id'].'|'.$workshop.'">'.$program['subscription_name'].'</option>';
                }
            }
        }
        echo $doh_programs;
    }
    
    function event_add($is_basic_event=false){
        $post = $this->input->post();

        $healthTalk = null;
        $audience = null;
        $multiple_basic = null;
        $user_type = array();
        $admin_roles = array();
        $ceid = null;
        $send_notif = true;
        
        $is_admin = (admin_role() == Users_model::ADMIN);
        if(!$is_admin){
            //check admin user role
            $role = $this->session->userdata['logged_in']['role'];
            switch($role){
                case Users_model::HR:
                    $audience = (int)$this->session->userdata['logged_in']['company_id'];
                    break;
                
                case Users_model::MD:
                    $recipients = $this->_processAdminEmailRecipients(array('Medical'));

                    $audience = $recipients['audience'];
                    $user_type = $recipients['user_type'];
                    $admin_roles = $recipients['admin_roles'];
                    break;
            }
        }else{
            if(!$is_basic_event){
                $this->form_validation->set_rules('audience[]', 'Audience', 'required');
                
                $recipients = $this->_processAdminEmailRecipients($post['audience']);

                $audience = $recipients['audience'];
                $user_type = $recipients['user_type'];
                $admin_roles = $recipients['admin_roles'];
            }
        }
        if($is_basic_event){
            $send_notif = false;
            $this->form_validation->set_rules('x', 'Health Program', 'required');
            $ceid = (int)$post['x'];
//            if(admin_role() == Users_model::HR){                
                $ceid_tmp = explode("|", $ceid);
                $multiple_basic = ($ceid[1] != "" ? $ceid[1] : null);
                $ceid = $ceid_tmp[0];
//            }else{
//                $multiple_basic = (isset($post['multiple_basic']) ? $post['multiple_basic'] : null);
//            }
            $subscription = $this->subscription->getCompanySubscriptionById('company_id, program_code', $ceid);
            //basic event created by admin
            $audience = (int)$subscription['company_id'];
            $healthTalk = $subscription['program_code'];
        }

        $this->form_validation->set_rules('event_name', 'Event Name', 'required');
        $this->form_validation->set_rules('venue', 'Location', 'required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');

        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $start_date = $post['start_date'];
            $start_time = $post['start_time'];
            
            $end_date = $post['end_date'];
            $end_time = $post['end_time'];

            $data = array(
                'name' => $post['event_name'],
                'description' => $post['description'],
                'venue' => $post['venue'],
                'start_date' => date('Y-m-d', strtotime($post['start_date'])),
                'start_time' => date('H:i:sP', strtotime("$start_date $start_time")),
                'end_date' => date('Y-m-d', strtotime($post['end_date'])),
                'end_time' => date('H:i:sP', strtotime("$end_date $end_time")),
                'status' => $post['status'],
                'health_talk' => $healthTalk,
                'audience' => $audience,
                'created_by' => $this->session->userdata['logged_in']['userid'],
                'company_enrollment_id' => $ceid
            );
            if($event_id = $this->events->save($data)){
                $r['result']['success'] = 1;
                $r['result']['event_id'] = $event_id;
                
                if($send_notif && $audience != null && $data['status'] != 'Completed'){
                    $recipients = [];
                    $subject = 'Event Invitation';
                    if($healthTalk !== null){
                        $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">My Health Activities > My Health Programs > Upcoming Events</span> to view the details.';
                    }else{
                        $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">Health Bulletin > Upcoming Events</span> to join or decline the invite.';
                    }
                    
                    if(!is_int($audience)){
                        $audience = explode(",", $audience);
                        //send notification to event audience
                        foreach($audience as $recipient){
                            $recipients = [];
                            if(trim($recipient) == 'Patients'){
                                $recipient = 'Patient';
                            }elseif(trim($recipient) == 'Doctors'){
                                $recipient = 'Doctor';
                                $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">Dashboard > Doctors Bulletin</span> to join or decline the invite.';
                            }elseif(in_array(trim($recipient), array('HR', 'Sales', 'Ops'))){
                                $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">My Community > Community Bulletin > Events</span> to join or decline the invite.';
                            }else{
                                //Nurse, Kinesiologist, Nutritionist
                                $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">Dashboard > Doctors Bulletin</span> to join or decline the invite.';
                            }
                            $recipients[] = array(
                                'type' => trim($recipient), 
                                'id' => ($is_admin ? strtolower(trim($recipient)) : 0)
                            );
                            $this->cloudhealth->sendNotification($subject, $details, $recipients);
                        }
                        //send email to recipients
                        $email_data['title'] = $data['name'];
                        $email_data['details'] = $details;
                        $email_status = $this->_processEmailSending($email_data, $user_type, $admin_roles, $audience);
                        $r['result']['errors'] = $email_status;
                    }else{
                        $recipients[] = array(
                            'type' => 'Patient', 
                            'id' => 0
                        );
                        if($this->cloudhealth->sendNotification($subject, $details, $recipients)){
                            //send email to recipients
                            $email_data['title'] = $data['name'];
                            $email_data['details'] = $details;
                            $email_status = $this->_processEmailSending($email_data, $user_type, $admin_roles, $audience);
                            $r['result']['errors'] = $email_status;
                        }
                    }
                }
                
                $this->session->set_flashdata('event_add_msg', 'A new event was created successfully.');
            }            
        }
        echo json_encode($r);
    }
    
    function event_edit($event_id=0, $health_talk=0){
        $event = $this->events->getRow(array('id' => $event_id));
        $d['event_info'] = $event;
        $d['callback'] = 'event_update';
        $d['title'] = 'UPDATE EVENT';
        $doh_programs = array();
        if($health_talk == 1){
            $company_subscription = $this->subscription->getEnrolledProgramById($event['company_enrollment_id'], $with_company_name=true);
            if(admin_role() == Users_model::ADMIN){
                $d['company_name'] = $company_subscription['company_name'];
            }
            $d['health_program'] = $company_subscription['subscription_name'];
        }
        $d['event_for_basic_programs'] = $health_talk;
        $d['doh_programs'] = $doh_programs;
        $this->load->view('community/bulletin/events/add_event', $d);
    }
    
    function event_update($company_enrollment_id=0){
        $post = $this->input->post();
        $this->form_validation->set_rules('event_name', 'Event Name', 'required');
        $this->form_validation->set_rules('venue', 'Location', 'required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');

        $r['result']['success'] = 0; // failed default status
        $send_notif = true;
        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $user_type = array();
            $admin_roles = array();
            $recipient_emails = array();
            
            $id = $post['f'];
            unset($post['f']);
            
            $data = array(
                'name' => $post['event_name'],
                'description' => $post['description'],
                'venue' => $post['venue'],
                'start_date' => date('Y-m-d', strtotime($post['start_date'])),
                'start_time' => $post['start_time'],
                'end_date' => date('Y-m-d', strtotime($post['end_date'])),
                'end_time' => $post['end_time'],
                'status' => $post['status']
            );

            $event = $this->events->getRow(array('id' => $id));
            $audience = $event['audience'];
           
            if($event['health_talk'] == null){
                $subject = 'Event Details Update';
                $role = $this->session->userdata['logged_in']['role'];
                switch($role){
                    case Users_model::HR:
                        $audience = (int)$this->session->userdata['logged_in']['company_id'];
                        break;

                    case Users_model::MD:
                        $recipients = $this->_processAdminEmailRecipients(array('Medical'));

                        $audience = $recipients['audience'];
                        $user_type = $recipients['user_type'];
                        $admin_roles = $recipients['admin_roles'];
                        break;
                    
                    default:
                        $recipients = $this->_processAdminEmailRecipients($post['audience']);

                        $audience = $recipients['audience'];
                        $user_type = $recipients['user_type'];
                        $admin_roles = $recipients['admin_roles'];
                }
            }else{
                $send_notif = false;
                $subject = 'Health Subscription Event (Updated)';
            }
            $data['audience'] = $audience;
            
            if($this->events->update($id, $data)){
                $r['result']['success'] = 1;
                
                if($send_notif && $data['status'] != 'Completed'){
                    $recipients = [];
                    $details = 'To view the updated details of an event, go to <span style="color: #59a5d8; font-weight: 500;">My Health Activities > My Health Programs > Upcoming Events</span>.';

                    if(1 === preg_match('~[0-9]~', trim($audience))){
                        //audience is company id
                        if($company_enrollment_id == 0 && $event['health_talk'] != null){
                            $company_enrollment = $this->subscription->getInfoByProgramcode('id', $event['health_talk'], $audience);
                            $enrolled = $this->subscription->getEnrolledPerCompanyPackage($company_enrollment['id']);
                            foreach($enrolled as $user){
                                $recipients[] = array(
                                    'type' => 'Patient', 
                                    'id' => $user['user_id'], 
                                    'name' => $user['name']
                                );
                                $recipient_emails[] = $user['email'];
                            }
                            $this->cloudhealth->sendNotification($subject, $details, $recipients);
                        }else{
                            if($event['health_talk'] == null){
                                //event update by an hr
                                $recipients[] = array(
                                    'type' => 'Patient', 
                                    'id' => 0
                                );
                            }else{
                                $enrolled = $this->subscription->getEnrolledPerCompanyPackage($company_enrollment_id);
                                foreach($enrolled as $user){
                                    $recipients[] = array(
                                        'type' => 'Patient', 
                                        'id' => $user['user_id'], 
                                        'name' => $user['name']
                                    );
                                    $recipient_emails[] = $user['email'];
                                }
                            }
                            if($this->cloudhealth->sendNotification($subject, $details, $recipients)){
                                //send email notifications to recipients
                                $email_data['datetime'] = date('M j, Y g:i:s A', time());
                                $email_data['message'] = $details;
                                $email_data['header'] = $subject;
                                $email_data['is_email'] = true;
                                $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
                                $email_data['page'] = 'notification';
                                $email_data['emails'] = array_unique($recipient_emails);
                                $this->cloudhealth->send_notification_email($email_data, $is_update = false, $individual_sending = true);
                            }
                        }
                    }else{
                        $audience = explode(",", $audience);

                        //send notification to event audience
                        foreach($audience as $recipient){
                            $recipients = [];
                            if(trim($recipient) == 'Patients'){
                                $recipient = 'Patient';
                            }elseif(trim($recipient) == 'Doctors'){
                                $recipient = 'Doctor';
                                $details = 'To view the updated details of an event, go to <span style="color: #59a5d8; font-weight: 500;">Dashboard > Doctors Bulletin</span>.';
                            }else{
                                //HR
                                $details = 'To view the updated details of an event, go to <span style="color: #59a5d8; font-weight: 500;">My Community > Community Bulletin > Events</span>.';
                            }
                            $recipients[] = array(
                                'type' => trim($recipient), 
                                'id' => strtolower(trim($recipient))
                            );
                            $this->cloudhealth->sendNotification($subject, $details, $recipients);
                        }
                        //send email to recipients
                        $email_data['title'] = $data['name'];
                        $email_data['details'] = $details;
                        $this->_processEmailSending($email_data, $user_type, $admin_roles, $audience, $is_update = true);
                    }
                }
            }
            $this->session->set_flashdata('event_add_msg', 'Event details was updated successfully.');
        }
        echo json_encode($r);
    }
    
    function event_delete(){
        $event_id = $this->input->post('id');
        
        echo $this->events->update($event_id, array('status' => 'Deleted'));
    }
    
    function getProgramEvents(){
        $post = array();
        parse_str($this->input->server('QUERY_STRING'), $post);
        $where = array();
        $where['trim(audience)'] = $post['company_id'];
        $where['health_talk'] = $post['program_code'];
        
        $event = $this->events->getBasicEvents($where);
        
        echo json_encode(array('event' => $event));
    }
    
    function events($health_events_for_hr=false){
        $where = array();
        $d['from_hr'] = admin_role() == Users_model::HR;
        if(admin_role() == Users_model::ADMIN){
            $where['health_talk is not null'] = null;
            $d['health_events'] = $this->events->getAll($where, $is_active = true, $admin_events=false, $is_health_talk = true);

            $hr_where['health_talk'] = null;
            $hr_where['role'] = Users_model::HR;
            $d['hr_events'] = $this->events->getAll($hr_where);
            
            $medical_where['health_talk'] = null;
            $medical_where['role'] = Users_model::MD;
            $d['medical_events'] = $this->events->getAll($medical_where, true, true);
        }
        if($health_events_for_hr){
            $where = array();
            $where['audience'] = $this->session->userdata['logged_in']['company_id'];
            $where['health_talk IS NOT NULL'] = null;
            
            $d['health_events'] = $this->events->getAll($where, $is_active = true, $admin_events=false, $is_health_talk = true);
            
            $my_where['created_by'] = $this->session->userdata['logged_in']['userid'];
            $my_where['health_talk'] = null;
            
            $d['my_events'] = $this->events->getAll($my_where, true, true);
        }else{
            $my_where['created_by'] = $this->session->userdata['logged_in']['userid'];
            $my_where['health_talk'] = null;
            
            $d['my_events'] = $this->events->getAll($my_where, true, true);
        }
        $d['health_events_for_hr'] = $health_events_for_hr;
        echo $this->load->view('community/bulletin/events/index', $d, true);
    }
    
    function getEventInfo($event_id=0){
        $d['data'] = $this->events->getRow(array('id' => $event_id));
        
        echo $this->load->view('community/bulletin/events/details', $d, true);
    }
    
    function getAnnouncementInfo($id=0){
        $d['data'] = $this->announcements->getRow(array('id' => $id));
        
        echo $this->load->view('community/bulletin/announcements/details', $d, true);
    }
    
    function eventsList(){
        $d['events'] = $this->events->getAll();
        
        echo $this->load->view('community/admin/events_list', $d, true);
    }
    
    function announcements(){
        if(admin_role() == Users_model::ADMIN){
            $hr_where['created_by != '] = $this->session->userdata['logged_in']['userid'];
            $d['hr_announcements'] = $this->announcements->getAll($hr_where);
        }
        $my_where['created_by'] = $this->session->userdata['logged_in']['userid'];
        $d['my_announcements'] = $this->announcements->getAll($my_where);
        
        echo $this->load->view('community/bulletin/announcements/index', $d, true);
    }
    
    function announcement_new(){
        $d['callback'] = 'announcement_add';
        $d['title'] = 'CREATE NEW ANNOUNCEMENT';
        
        $this->load->view('community/bulletin/announcements/add_announcement', $d);
    }
    
    function announcement_add(){
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('details', 'Details', 'required');
        $is_admin = (admin_role() == Users_model::ADMIN);
        if($is_admin){
            $this->form_validation->set_rules('audience[]', 'Audience', 'required');
        }
        $r['result']['success'] = 0; // failed default status
        $r['result']['errors'] = null;

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $post = $this->input->post();
            $audience = null;
            $user_type = array();
            $admin_roles = array();
            if(!$is_admin){
                //check admin user role
                $role = $this->session->userdata['logged_in']['role'];
                switch($role){
                    case Users_model::HR:
                        $audience = (int)$this->session->userdata['logged_in']['company_id'];
                        break;

                    case Users_model::MD:
                        $recipients = $this->_processAdminEmailRecipients(array('Medical'));

                        $audience = $recipients['audience'];
                        $user_type = $recipients['user_type'];
                        $admin_roles = $recipients['admin_roles'];
                        break;
                }
            }else{
                if(isset($post['audience'])){
                    $recipients = $this->_processAdminEmailRecipients($post['audience']);
                    
                    $audience = $recipients['audience'];
                    $user_type = $recipients['user_type'];
                    $admin_roles = $recipients['admin_roles'];
                }
            }

            $data = array(
                'title' => $post['title'],
                'details' => $post['details'],
                'status' => $post['status'],
                'audience' => $audience,
                'created_by' => $this->session->userdata['logged_in']['userid']
            );
            if($this->announcements->save($data)){
                if($data['status'] == 'Active'){
                    //send email to recipients
                    $email_status = $this->_processEmailSending($data, $user_type, $admin_roles, $audience);
                }
                $r['result']['success'] = 1;
                $r['result']['errors'] = $email_status;
                $this->session->set_flashdata('announcement_add_msg', 'A new announcement was created successfully.');
            }            
        }
        echo json_encode($r);
    }
    
    private function _processAdminEmailRecipients($audience){
        $admin_roles = array();
        $user_type = array();
        if (($key = array_search('Medical', $audience)) !== false) {
            unset($audience[$key]);
            $audience = array_merge($audience, array('Doctors', 'Nurse', 'Kinesiologist', 'Nutritionist'));
            $user_type = array('Doctor', 'Nurse', 'Kinesiologist', 'Nutritionist');
        }
        if (in_array('Patients', $audience)) {
            $user_type[] = 'Patient';
        }
        if (in_array('HR', $audience)) {
            $admin_roles[] = Users_model::HR;
        }
        if (in_array('Sales', $audience)) {
            $admin_roles[] = Users_model::SALES;
        }
        if (in_array('Ops', $audience)) {
            $admin_roles[] = Users_model::OPS;
        }
        $audience = implode(", ", $audience);
        
        return array(
            'audience' => $audience,
            'user_type' => $user_type,
            'admin_roles' => $admin_roles
        );
    }
    
    private function _processEmailSending($data = array(), $user_type = array(), $admin_roles = array(), $audience=null, $is_update=false, $is_cancelled = false){
//        var_dump(func_get_args());die;
        $email_data['datetime'] = date('M j, Y g:i:s A', time());
        $email_data['message'] = $data['details'];
        $email_data['header'] = ($is_cancelled ? 'Cancelled: ' : '').$data['title'];
        $email_data['is_email'] = true;
        $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
        $email_data['page'] = 'notification';
        $user_emails = array();
        $admin_emails = array();
        $company_name = null;
        if(!is_array($audience) && (int)$audience > 0){
            $this->load->model('Organizations_model');
            $company = $this->Organizations_model->get_company_details_by_id($audience);
            $company_name = $company['company_name'];
            $user_emails = $this->Users_model->getActiveUserEmailsByType('Patient', $company_name);
        }
        if (!empty($user_type)) {
            //get emails from users table according to user type of audience
            $user_emails = $this->Users_model->getActiveUserEmailsByType($user_type, $company_name);
        }
        if (!empty($admin_roles)) {
            //get emails from users table according to user type of audience
            $admin_emails = $this->Users_model->getActiveAdminEmailsByRole($admin_roles, $company_name);
        }

        $email_data['emails'] = array_unique(array_column(array_merge($user_emails, $admin_emails), 'email'));
        $this->cloudhealth->send_notification_email($email_data, $is_update);
    }
    
    function announcement_edit($announcement_id=0){
        $announcements = $this->announcements->getRow(array('id' => $announcement_id));
        $d['announcement_info'] = $announcements;
        $d['callback'] = 'announcement_update';
        $d['title'] = 'UPDATE ANNOUNCEMENT';
        $this->load->view('community/bulletin/announcements/add_announcement', $d);
    }
    
    function announcement_update(){
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('details', 'Details', 'required');

        $is_admin = (admin_role() == Users_model::ADMIN);
        if($is_admin){
            $this->form_validation->set_rules('audience[]', 'Audience', 'required');
        }
        
        $r['result']['success'] = 0; // failed default status

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
            $r['result']['errors'] = $errors;
        } else {
            $post = $this->input->post();
            $audience = null;
            $user_type = array();
            $admin_roles = array();
            $id = $post['f'];
            unset($post['f']);
            
            $data = array(
                'title' => $post['title'],
                'details' => $post['details'],
                'status' => $post['status']
            );

            if($is_admin){
                $recipients = $this->_processAdminEmailRecipients($post['audience']);
                    
                $audience = $recipients['audience'];
                $user_type = $recipients['user_type'];
                $admin_roles = $recipients['admin_roles'];
                    
                $data['audience'] = $audience;
            }else{
                $event = $this->announcements->getRow(array('id' => $id));
                $audience = $event['audience'];
                $event_author = $this->Users_model->get_userdetails(array('id' => $event['created_by']));
                if($event_author['role'] == Users_model::MD){
                    $recipients = $this->_processAdminEmailRecipients(array('Medical'));

                    $user_type = $recipients['user_type'];
                    $admin_roles = $recipients['admin_roles'];
                }
            }
            if($this->announcements->update($id, $data)){
                $r['result']['success'] = 1;
                if($data['status'] == 'Active'){
                    //send email to recipients
                    $this->_processEmailSending($data, $user_type, $admin_roles, $audience, $is_update = true, $data['status'] == 'Cancelled');
                }
            }
            $this->session->set_flashdata('event_add_msg', 'Event details was updated successfully.');
        }
        echo json_encode($r);
    }
    
    function announcement_delete(){
        $id = $this->input->post('id');
        
        echo $this->announcements->update($id, array('status' => 'Deleted'));
    }
    
    function getConsultsSummary($is_hr = false){
        $company_id = 0;
        if((bool)$is_hr){
            $company_id = $this->session->userdata['logged_in']['company_id'];
        }
        
        $this->load->model('Appointments_model', 'appointments');
        $analytics_per_day = $this->appointments->getAppointmentCount($company_id);
        $consults_per_day = 0;
        $avg_consults_per_day = 0;
        foreach($analytics_per_day as $rec){
            $consults_per_day = $consults_per_day + (int)$rec['count'];
        }
        if($consults_per_day > 0){
            $avg_consults_per_day = ceil($consults_per_day/count($analytics_per_day));
        }
        
        $analytics_per_month = $this->appointments->getAppointmentCount($company_id, 'month');
        $consults_per_month = 0;
        $avg_consults_per_month = 0;
        foreach($analytics_per_month as $rec){
            $consults_per_month = $consults_per_month + (int)$rec['count'];
        }
        if($consults_per_month > 0){
            $avg_consults_per_month = ceil($consults_per_month/count($analytics_per_month));
        }
        
        $uniqueUsers_per_month = $this->appointments->getAppointmentCount($company_id, 'month', true);
        $users_per_month = 0;
        $avg_unique_users_per_month = 0;
        foreach($uniqueUsers_per_month as $rec){
            $users_per_month = $users_per_month + (int)$rec['count'];
        }
        if($users_per_month > 0){
            $avg_unique_users_per_month = ceil($users_per_month/count($uniqueUsers_per_month));
        }
        
        $data['total_consults_per_day'] = $avg_consults_per_day;
        $data['total_consults_per_month'] = $avg_consults_per_month;
        $data['unique_users_per_month'] = $avg_unique_users_per_month;
        $data['with_data'] = (!empty($analytics_per_day));
        
        echo $this->load->view('community/admin/analytics_consults', $data, true);
    }
    
    function consults_graphs(){
        $this->load->model('Appointments_model', 'appointments');
        $this->load->model('Organizations_model', 'organizations');
        $this->load->model('CompanyQueue_model', 'company_queue');

        
        $d['companies'] = $this->organizations->organizations_list();
        $d['doctors'] = $this->company_queue->getAssignedDoctors(array('company_id' => $this->session->userdata['logged_in']['company_id']));
        
        $d['js'][] = $this->cloudhealth->load_script('assets/js/md_dashboard.js');
        
        echo $this->load->view('dashboard_MD', $d, true);
    }
    
    function showEventEnrolees($event_id=0){
        $event = $this->events->getRow(array('id' => $event_id));
        if($event['company_enrollment_id'] == null){
            $enrolled_employees = array();
            if($event['read_by']){
                $read_by = json_decode($event['read_by'], true);
                foreach($read_by['data'] as $rec){
                    $joined_status = 'Pending';
                    $tmp_enrolled_employees = $this->Users_model->getUserInfoById($rec['id']);
                    if($rec['status'] === Events_model::joined){
                        $joined_status = 'Joined';
                    }elseif($rec['status'] === Events_model::declined){
                        $joined_status = 'Declined';
                    }
                    $tmp_enrolled_employees += array('joined_status' => $joined_status);
                    $enrolled_employees[] = $tmp_enrolled_employees;
                }
            }
            $d['enrolled_employees'] = $enrolled_employees;
        }else{
            $status = array('enrollment_status' => 'Approved');
            $subscription = $this->subscription->getCompanySubscriptionById('company_id, program_code, subscription_end', $event['company_enrollment_id']);
            $d['enrolled_employees'] = $this->subscription->getEnrolledPerCompanyPackage($event['company_enrollment_id'], false, $subscription['program_code'], $status);
        }
        $d['event_id'] = $event_id;
        
        echo $this->load->view('community/bulletin/events/employee_list', $d, true);
    }
    
    function ajax_sendEventDetails(){
        $event_id = $this->input->post('x');
        $recipients = [];
        
        $event = $this->events->getRow(array('id' => $event_id));       
        $subject = 'Event Invitation';
        $details = 'You have been invited to join an event. Go to <span style="color: #59a5d8; font-weight: 500;">My Health Activities > My Health Programs > Upcoming events</span> to see details.';

        $where['event_is_sent IS NULL'] = NULL;
        $where['enrollment_status'] = 'Approved';
        $enrolled = $this->subscription->getEnrolledPerCompanyPackage($event['company_enrollment_id'], false, null, $where);

        foreach ($enrolled as $user) {
            $recipients[] = array(
                'type' => 'Patient',
                'id' => $user['user_id'],
                'name' => $user['name']
            );
            //update column event_is_sent
            $this->subscription->event_is_sent($event['company_enrollment_id'], $user['user_id']);
            $recipient_emails[] = $user['email'];
        }
        $sent['msg'] = 'An error was encountered. Please contact your system administrator.';
        $sent['class'] = 'alert-warning';
        if (!empty($recipients) && $this->cloudhealth->sendNotification($subject, $details, $recipients)) {
            //send email notifications to recipients
            $email_data['datetime'] = date('M j, Y g:i:s A', time());
            $email_data['message'] = $details;
            $email_data['header'] = $subject;
            $email_data['is_email'] = true;
            $email_data['notification_message'] = $this->load->view('notifications/announcement-details', $email_data, true);
            $email_data['page'] = 'notification';
            $email_data['emails'] = array_unique($recipient_emails);
            $notif_sent = $this->cloudhealth->send_notification_email($email_data, $is_update = false, $individual_sending = true);
            if($notif_sent){
                $sent['msg'] = 'Event details were successfully sent.';
                $sent['class'] = 'alert-success';
            }
        }
        echo json_encode($sent);
    }
    
}