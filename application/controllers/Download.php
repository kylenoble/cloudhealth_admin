<?php
/**
 * Description of Download
 *
 * @author AGC_User
 */
class Download extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     *  Downloads CHI MD reports from another server
     */
    function report($filename = null){
        if($filename !== null){
            $path = 'https://cloudhealthasia.com/static/reports/CHI/';
            $this->_download($path . $filename, "pdf");
        }
    }
    
    private function _download($filename, $type){
        $content_type = "";
        switch($type){
            case "pdf":
                $content_type = "application/pdf";
        }
        header("Content-type: ". $content_type);
        header("Content-Disposition: attachment; filename='{$filename}'");
    }
}
